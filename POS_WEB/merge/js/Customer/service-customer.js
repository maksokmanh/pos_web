﻿
$("#register-service-customer").on("click", function () {  
    let $submit = $(this);
    let sc_form = $.form("#service-customer-register");
    let ap_form = $("#create-customer-appointment");
    let item_services = [];
    $.each($(".ServiceID", ap_form), function (i) {
        if ($(this).is(":checked")) {
            item_services.push(this.value);
        }
    });
    
    let appointment = {
        AppointmentCode: $("input[name=AppointmentCode]", ap_form).val(),
        Remark: $("textarea[name=Remark]", ap_form).val(),
        ActivityDate: $("input[name=ActivityDate]", ap_form).attr("data-value"),
        ActivityTime: $("input[name=ActivityTime]", ap_form).val(),
        Reminder: $("input[name=Reminder]", ap_form).val() + "/" + $("select[name=ReminderUom]", ap_form).val(),
        Location: $("textarea[name=Location]", ap_form).val(),
        Priority: $("select[name=Priority]", ap_form).val(),
        Paused: $("input[name=Paused]", ap_form).val(),
        Closed: $("input[name=Closed]", ap_form).val(),
        ItemServices: item_services.join("/"),
        CustomerID: 0  
    };
    
    $.ajax({
        url: "/ServiceCustomer/RegisterServiceCustomer",
        data: $.antiForgeryToken({ customer: sc_form.value, appointment: appointment }),
        type: "POST",
        success: function (res) {   
            new ViewMessage(res);
            if (res.Action == "1") {
                $submit.prop("disabled", true).css("cursor", "default");
                let _confirm = new ViewMessage({
                    summary: {
                        attribute: "ck-message-summary"
                    }
                }, res);
                _confirm.refresh(2000);
            }          
        }
    });
});

//Edit only service customer info
$("#submit-service-customer-update").on("click", function () {
    let $submit = $(this);
    let sc_form = $.form("#service-customer-update");
    $.ajax({
        url: "/ServiceCustomer/UpdateServiceCustomer",
        data: $.antiForgeryToken({ customer: sc_form.value }),
        type: "POST",
        success: function (res) {
            new ViewMessage(res);
            if (res.Action == "1") {
                $submit.prop("disabled", true).css("cursor", "default");
                let _confirm = new ViewMessage({
                    summary: {
                        attribute: "ck-message-summary"
                    }
                }, res);
                _confirm.refresh(2000);
            }         
        }
    });

});


function deleteServiceCustomer(self) {
    let row = $(self).parent().parent();
    let remove_sc = new DialogBox({
        type: "yes-no",
        caption: "Delete",
        content: "Are you sure you want to delete “" + $("td:nth-child(4)", row).text() + "” ?"
    });

    remove_sc.confirm(function () {
        $.ajax({
            url: "/ServiceCustomer/ServiceCustomerUpdate",
            data: { customerID: parseInt($(row).data("customerid")), deleted: true },
            success: function () {           
                remove_sc.shutdown("before", function () {
                    $(self).parent().parent().remove();
                });
            }
        });
    });
}


$("input[data-phone]").on("keydown keyup", function (e) {
    $(this).prop("maxlength", 12);
    if (this.value.startsWith("0")) {
        if (this.value === "00") {
            this.value = "0";
        }
    } else {
        this.value = this.value.substring(0, this.value.length - 1);
    }

    this.value = new libphonenumber.AsYouType('KH').input(this.value);
});

//Regions
$("select[name=Region]").on("change", function (e) {
    dialogUpdateData("/ServiceCustomer/GetRegions", "/ServiceCustomer/UpdateRegions",
        "RegionID", "Name", this, "Region", false);
    $.ajax({
        url: "/ServiceCustomer/GetDistrictsByRegionID",
        data: { related_id: this.value },
        success: function (results) {
            updateSelect("select[name=District]", results, "DistrictID", "Name")
        }
    });

    if (this.value !== "0") {
        $("select[name=District]").prop("disabled", false);
    } else {
        $("select[name=District]").prop("disabled", true);
    }

});

//Districts
$("select[name=District]").on("change", function (e) {
    if (this.value !== "0" || this.value !== "-1") {
        let related = $("select[name=Region] option:selected").val();
        dialogUpdateData("/ServiceCustomer/GetDistrictsByRegionID", "/ServiceCustomer/UpdateDistricts",
            "DistrictID", "Name", this, "District", false, { "RegionID": related });
        $("select[name=District]").prop("disabled", false);
    }
});

$("select[name=Color]").on("change", function (e) {
    dialogUpdateData("/Customer/GetColorCustomers", "/Customer/UpdateColorCustomers",
        "ColorID", "ColorName", this, "Color", true);
});

var selected_value = 0;
function dialogUpdateData(from_url, to_url, key, name, select, dialog_title, has_active, related) {
    if ($(select).val() == "-1") {
        $(select).val(selected_value);
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#dialog-define-new"
            },
            caption: dialog_title,
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let new_index = 1;
            let table = dialog.content.find("table");
            let rel_key = "";
            if (!!related) {
                rel_key = Object.keys(related)[0];
            }
            if (!!has_active) {
                $("tr:first-child th:last-child", table).prop("hidden", false);
            }

            if (!!related) {
                $("tr:first-child th:last-child", table).after("<th style='width: 25px;'>Related</th>");
            }

            let add_new = $("<tr><td class='add-new'></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='Name']").val() !== "") {
                    new_index++;
                    let row = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='form-control create-new'/></td></tr>");
                    if (!!has_active) {
                        row.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        row.append("<td><input name='Related' class='form-control' readonly disabled value='" + related[Object.keys(related)][0] + "'></td>");
                    }
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);
            });

            $.ajax({
                url: from_url,
                dataType: "JSON",
                data: { related_id: !!related ? related[rel_key] : 0 },
                success: function (results) {
                    if (results.length > 0) {
                        let items = [];
                        $.each(results, function (index, data) {
                            index++;
                            let item = {};
                            item[key] = data[key];
                            item["index"] = index;
                            item[name] = "<input name='Name' value='" + data[name] + "' class='form-control' readonly>";

                            if (!!related) {
                                item[rel_key] = "<div name='Related'>" + related[rel_key] + "</div>";
                            }

                            if (!!has_active) {
                                item["active"] = "<input name='Active' type='checkbox'>";

                                if (!!data.Active) {
                                    item.active = "<input name='Active' type=checkbox checked>";
                                }
                            }

                            items.push(item);
                            new_index = index + 1;
                        });

                        $.bindRows(table, items, key, {
                            hide_key: true,
                            text_align: [{ "index": "center" }],
                            dblclick: function (e) {
                                let current_input = $("td input[name=Name]:not(.create-new)", this).prop("readonly", false);
                                let sibling_inputs = current_input.parent().parent().siblings().find("td input[name=Name]:not(.create-new)");
                                sibling_inputs.prop("readonly", true);
                            }
                        });

                        $(document).on("click", function (e) {
                            if (!$(e.target).is(table.find("tr *"))) {
                                table.find("tr td input:not(.create-new)").prop("readonly", true);
                            }

                        });
                    }

                    let create_new = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='create-new form-control' /></tr>");

                    if (!!has_active) {
                        create_new.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        create_new.append("<td><div name='Related'>" + related[Object.keys(related)][0] + "</div></td>");
                    }

                    table.append(create_new).append(add_new);

                }
            });

        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table");
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let items = [];
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    let item = {};
                    item[key] = $(this).data(key.toLowerCase());
                    if (!!related) {
                        let _key = Object.keys(related);
                        item[_key] = $("div[name=Related]", td).text();
                    }
                    item[name] = $.stripHTML($("input[name=Name]", td).val());
                    if (!!has_active) {
                        item["Active"] = $("input[name=Active]", td).is(":checked");
                    }
                    items.push(item);
                }

            });
          
            $.ajax({
                url: to_url,
                type: "post",
                dataType: "json",
                data: $.antiForgeryToken({ data: items }),
                success: function (res) {
                    updateSelect(select, res.Data, key, name);
                }

            });

            this.meta.shutdown();
        });
    }
}

function updateSelect(selector, jsons, key, value) {
    if ($(selector)[0].tagName === "SELECT") {
        $(selector).children().remove();

        let add_new = $("<option value='-1'>Define New</option>");
        $.each(jsons, function (i, json) {
            let option = $("<option value='" + json[key] + "'>" + json[value] + "</option>");
            $(selector).append(option);
        });
        $(selector).append(add_new);
        if ($(selector).children().length <= 1) {
            $(selector).prepend("<option value='0' selected disabled></option>");
        }
    }
}

function customerType(enum_val) { 
    let _groups = ["SERVICE", "WALKIN", "PURCHASE"];
    return _groups[parseInt(enum_val)];
}

//Tab content customer
$("ul#nav-customer-list li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    let content = $("#tab-content-customer");
    if ($(this).is($("#tab-service"))) {
        $("#content-service-customer", content).prop("hidden", false).siblings().prop("hidden", true);
    }

    if ($(this).is($("#tab-motocycle"))) {
        $("#content-motocycle-customer", content).prop("hidden", false).siblings().prop("hidden", true);
        if ($("#motocycle-customer-list tr:not(:first-child)").length === 0) {
            setCustomerList("/Customer/SearchMotocycleCustomers", "#motocycle-customer-list", "CustomerID", "", true);
        }
        
    }
});

//Search service customers
$("#search-service-customers").on("keyup", function () {
    setTimeout(() => {
        setCustomerList("/ServiceCustomer/SearchServiceCustomers", "#service-customer-list", "CustomerID", this.value);
    }, 500);

});

//Search motocycle customers
$("#search-motocycle-customers").on("keyup", function () {
    setTimeout(() => {
        setCustomerList("/Customer/SearchMotocycleCustomers", "#motocycle-customer-list", "CustomerID", this.value);
    }, 500);

});

function setCustomerList(url, dom_table, key, keyword, forServiceFollowUp) {
    let index = 1;
    let option = {
        url: url,
        dataType: "JSON",
        success: function (customers) {
            $(dom_table).bindRows(customers, key, {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                hidden_columns: ["Address", "Color"],
                prebuild: function (data) {
                    data.CustomerType = customerType(data.CustomerType);
                },
                postbuild: function (data) {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    let _action = $("<td></td>");
                    if (data.CustomerType == "SERVICE") {                    
                    _action.append('<a title="Edit" href="/ServiceCustomer/ServiceCustomer?customerID=' + data.CustomerID + '" class= "fas fa-user-edit fn-orange fa-lg csr-pointer"></a>&nbsp;')
                        .append('|&nbsp;<i title="Delete" onclick="deleteServiceCustomer(this)" class="fas fa-user-times fa-lg fn-red csr-pointer"></i>&nbsp;|&nbsp;')
                        .append('<a title="Detail" href="/ServiceCustomer/ServiceCustomerDetail?customerID=' + data.CustomerID + '" class= "fas fa-info-circle fn-sky fa-lg csr-pointer"></a>');
                    } else {
                        _action.append('<a title="Follow up" href="/ServiceCustomer/ServiceAppointmentFollowUp?customerID=' + data.CustomerID + '"class="fas fa-chevron-circle-right fn-green fa-lg csr-pointer"></a>&nbsp;|&nbsp;')
                            .append('<a title="Detail" href="/ServiceCustomer/ServiceCustomerDetail?customerID=' + data.CustomerID + '" class="fas fa-info-circle fn-sky fa-lg csr-pointer"></a>');
                                                  
                    }

                    $("td:last-child", this).after(_action);
                }

            });

        }
    };

    if (!!keyword) {
        option.data = { word: keyword };
    }

    if (!!forServiceFollowUp) {
        option.data = { word: keyword, forServiceFollowUp: true};
    }

    $.ajax(option);
}

//Customer information detail
function showCustomerDetail(id) {
    let _dialog = new DialogBox({
        content: {
            selector: "#block-customer-detail"
        }
    });
    _dialog.startup("after", function (dialog) {
        let content = dialog.content;
        $.ajax({
            url: "/ServiceCustomer/ServiceCustomerDetail",
            type: "GET",
            data: { customerID: id },
            success: function (data) {
                content.find("#block-customer-detail").html(data);
            }
        });
    });
}
﻿
$("#trigger-create-new-service").on("click", function () {
    let _dialog = new DialogBox({
        content: {
            selector: "#dialog-create-item-service",
            class: "item-service"
        },
        caption: "Create Item Service",
        type: "ok-cancel",
        position: "top-right",
        button: {
            ok: {
                text: "Add",
                callback: function (e) {
                    createItemService(this.meta.content);
                }
            },
            cancel: {
                callback: function () {
                    this.meta.shutdown();
                }
            }
        }
    });
    _dialog.startup("after", function (dialog) {
        $.ajax({
            url: "/ServiceCustomer/ItemService",
            data: { onlyItemService: true },
            success: function (service) {
                dialog.content.find("input[name=ServiceCode]").val(service.ServiceCode);
            }
        });
    });

    function createItemService(content) {
        let service = {
            ServiceID: 0,
            ServiceCode: $("input[name=ServiceCode]", content).val(),
            ServiceName: $("input[name=ServiceName]", content).val()
        };
        $.ajax({
            url: "/ServiceCustomer/ItemService",
            type: "POST",
            data: $.antiForgeryToken({ itemService: service, isJSON: true }),
            success: function (result) {
                let error = new ViewMessage(result.ModelMessage);
                if (Object.keys(error.data).length === 0) {
                    let row = $("<tr></tr>");
                    row.append("<td style='width: 10px;'><input readonly type='checkbox' class='ServiceID' value='"+ result.ItemService.ServiceID +"' checked /></td>")
                        .append("<td>" + service.ServiceName + "</td>")
                    $("#table-item-service-list").prepend(row).find("tr#no-item-service").remove();
                    _dialog.refresh();
                }
            }
        });
    }
});

//Item service master data
function changeItemService(self) {
    let row = $(self).parent().parent();
    let input = row.find("td input.ServiceName").addClass("editable").focus();
    input[0].selectionStart = input[0].selectionEnd = input[0].value.length;
    row.siblings().find("td input").removeClass("editable");
    let reserved = input.val();
    input.on("focusout", function () {
        if (reserved.trim() !== input.val().trim()) {
            let data = {
                ServiceID: row.data("serviceid"),
                ServiceCode: row[0].cells[1].textContent,
                ServiceName: row.find("td input.ServiceName").val()
            };

            let dialog = new DialogBox({
                caption: "Edition",
                icon: "warning",
                content: "«" + reserved + "» " + "will change to «" + input.val() + "», Are you sure?",
                type: "ok-cancel",
                close_button: "none"
            });

            dialog.confirm(function () {
                saveItemService("/ServiceCustomer/ItemService", data);
                dialog.shutdown();
            });

            dialog.reject(function () {
                input.val(reserved);
            });
        }
        input.removeClass("editable");

    });
}

function saveItemService(url, data) {
    $.ajax({
        url: url,
        type: "POST",
        data: $.antiForgeryToken({ itemService: data })
    });
}

//Search item services
$("#search-item-services").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/ServiceCustomer/SearchItemServices",
        dataType: "JSON",
        data: { word: this.value },
        success: function (item_services) {
            setTimeout(() => {
                $("#item-service-list").bindRows(item_services, "ServiceID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns: ["Deleted"],
                    prebuild: function (data) {
                        data.ServiceName = "<input class=ServiceName value='" + data.ServiceName + "'/>";
                        data.LastChanged = moment(data.LastChanged).format("DD/MM/YYYY hh:mm A");
                    },
                    postbuild: function (data) {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                        let td = $('<td><i onclick="changeItemService(this);" class="trigger-edit fas fa-edit fa-lg fn-sky csr-pointer"></i></td>');
                        $("td:last-child", this).after(td);
                    }
                });
            }, 250);

        }
    });
});
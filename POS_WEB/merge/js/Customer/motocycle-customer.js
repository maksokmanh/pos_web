﻿
var ajax_loader = new AjaxLoader("/ajaxloader/loading.gif");
let mc_dialog = {};
$("#trigger-sale-consultants").on("click", function () {
    mc_dialog = new DialogBox({
        position: "top-center",
        content: {
            selector: "#sale-consultant-list"
        },
        caption: "Sale Consultant List",
        type: "ok-cancel",
        button: {
            cancel: {
                callback: function (e) {
                    this.meta.shutdown();
                }
            }
        }
    });

    mc_dialog.startup("after", function (dialog) {
        let table = dialog.content.find("table");
        searchSaleConsultants(table);
        $(".search-sale-consultants", dialog.content).focus();
        $(".search-sale-consultants", dialog.content).on("keyup", function () {
            setTimeout(() => {
                searchSaleConsultants(table, this.value);
            }, 500);

        });
    });

    mc_dialog.confirm(function (dialog) {
        chooseSaleConsultant(chosen_row);
    });

});

let chosen_row;
function searchSaleConsultants(table, word) {
    let index = 1;
    let setting = {
        url: "/Customer/SearchSaleConsultants",
        dataType: "JSON",
        success: function (consultants) {
            $(table).bindRows(consultants, "ID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                prebuild: function (data) {
                    data.Gender = parseGender(data.Gender);
                },
                postbuild: function () {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    $("td:last-child", this).after("<td><div onclick='chooseSaleConsultant($(this).parent().parent()[0]);'"
                        + "class='fas fa-caret-square-down fn-sky fa-lg csr-pointer'></div></td>");
                },
                click: function () {
                    chosen_row = this;
                    $(this).addClass("active").siblings().removeClass("active");
                },
                dblclick: function () {
                    chooseSaleConsultant(this);
                }

            });
        }
    };

    if (!!word) {
        setting.data = { word: word };
    }

    $.ajax(setting);
}

function chooseSaleConsultant(row) {
    let data = {
        id: $(row).data("id"),
        name: row.cells[2].textContent,
        position: row.cells[4].textContent
    };

    $("input[Name=SaleConsultant]").val(data.id);
    $("input#consultant-name").val(data.name);
    $("input#consultant-position").val(data.position);
    mc_dialog.shutdown();
}

//Choose motocycle remark
$("#trigger-remarks").on("click", function () {
    mc_dialog = new DialogBox({
        position: "top-center",
        content: {
            selector: ".remark-list"
        },
        caption: "Available Remarks",
        type: "ok-cancel",
        button: {
            cancel: {
                callback: function (e) {
                    this.meta.shutdown();
                }
            }
        }
    });

    mc_dialog.startup("after", function (dialog) {
        let table = dialog.content.find("table");
        searchRemarks(table);
        $(".search-remarks", dialog.content).focus();
        $(".search-remarks", dialog.content).on("keyup", function () {
            setTimeout(() => {
                searchRemarks(table, this.value);
            }, 500);

        });
    });

    mc_dialog.confirm(function (dialog) {
        chooseRemark(chosen_remark);
    });

});

let chosen_remark;
function searchRemarks(table, word) {
    let index = 1;
    let setting = {
        url: "/Customer/SearchDefaultRemarks",
        dataType: "JSON",
        success: function (remarks) {
            $(table).bindRows(remarks, "RemarkID", { 
                hidden_columns: ["LastChanged", "Deleted"],
                postbuild: function () {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    $("td:last-child", this).after("<td><div onclick='chooseRemark($(this).parent().parent()[0]);'"
                        + "class='fas fa-caret-square-down fn-sky fa-lg csr-pointer'></div></td>");
                },
                click: function () {
                    chosen_remark = this;
                    $(this).addClass("active").siblings().removeClass("active");
                },
                dblclick: function () {
                    chooseRemark(this);
                }

            });
        }
    };

    if (!!word) {
        setting.data = { word: word };
    }

    $.ajax(setting);
}

function chooseRemark(row) {
    let data = {
        id: $(row).data("remarkid"),
        name: $(row.cells[2]).text()
    };

    $("textarea[name=Remark]").val(data.name);
    mc_dialog.shutdown();
}

//Search motocycle customers
$("#search-motocycle-customers").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/Customer/SearchMotocycleCustomers",
        dataType: "JSON",
        data: { word: this.value },
        success: function (data) {          
            setTimeout(() => {
                $("#motocycle-customer-list").bindRows(data, "CustomerID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns:["Address", "Color"],
                    prebuild: function (data) {
                        data.CustomerType = customerType(data.CustomerType);                  
                    },
                    postbuild: function (data) {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");     
                        let _action = $("<td></td>");
                        _action.append('<a title="Edit" href="/Customer/MotocycleCustomer?customerID=' + data.CustomerID + '" class= "fas fa-user-edit fn-orange fa-lg csr-pointer"></a>&nbsp;')
                            .append('|&nbsp;<i title="Delete" onclick="deleteMotocycleCustomer(this)" class="fas fa-user-times fa-lg fn-red csr-pointer"></i>&nbsp;|&nbsp;')
                            .append('<a title="Detail" href="/Customer/MotocycleCustomerDetail?customerID=' + data.CustomerID + '" class= "fas fa-info-circle fn-sky fa-lg csr-pointer"></a>');
                        $("td:last-child", this).after(_action);
                    }

                });
            }, 250);

        }
    });

});

function deleteMotocycleCustomer(self) {
    let row = $(self).parent().parent();
    let remove_sc = new DialogBox({
        type: "yes-no",
        caption: "Delete",
        content: "Are you sure you want to delete “" + $("td:nth-child(4)", row).text() + "” ?"
    });

    remove_sc.confirm(function () {
        $.ajax({
            url: "/Customer/DeleteCustomer",
            data: { CusID: parseInt($(row).data("cusid")) },
            type: "POST",
            success: function () {
                remove_sc.shutdown("before", function () {
                    $(self).parent().parent().remove();
                });
            }
        });
    });
}

$("input[data-phone]").on("keydown keyup", function (e) {
    $(this).prop("maxlength", 12);
    if (this.value.startsWith("0")) {
        if (this.value === "00") {
            this.value = "0";
        }
    } else {
        this.value = this.value.substring(0, this.value.length - 1);
    }

    this.value = new libphonenumber.AsYouType('KH').input(this.value);
});

//Source of Info
$("select#source-of-info").on("change", function (e) {
    dialogUpdateData("/Customer/GetSourceOfInfos", "/Customer/UpdateSourceOfInfos",
        "SourceID", "SourceName", this, "Source Of Info", true);
});

//Warranty Type
$("select#warranty-type").on("change", function (e) {
    dialogUpdateData("/Customer/GetWarrantyTypes", "/Customer/UpdateWarrantyTypes",
        "WarrantyID", "WarrantyName", this, "Warranty Types", true);
});

//Customer Groups
$("select#customer-group").on("change", function (e) {
    dialogUpdateData("/Customer/GetCustomerGroups", "/Customer/UpdateCustomerGroups",
        "GroupID", "GroupName", this, "Customer Group", true);
});

//Regions
$("select#region").on("change", function (e) {
   
    dialogUpdateData("/Customer/GetRegions", "/Customer/UpdateRegions",
        "RegionID", "Name", this, "Region", false);
    $.ajax({
        url: "/Customer/GetDistrictsByRegionID",
        data: { related_id: this.value },
        success: function (results) {
            updateSelect("select#district", results, "DistrictID", "Name")
        }
    });

    if (this.value !== "0") {
        $("select#district").prop("disabled", false);
    } else {
        $("select#district").prop("disabled", true);
    }

});

//Districts
$("select#district").on("change", function (e) {
    if (this.value !== "0" || this.value !== "-1") {
        let related = $("select#region option:selected").val();
        dialogUpdateData("/Customer/GetDistrictsByRegionID", "/Customer/UpdateDistricts",
            "DistrictID", "Name", this, "District", false, {"RegionID": related});
        $("select#district").prop("disabled", false);
    }
});


//Color
$("select#color").on("change", function (e) {
    dialogUpdateData("/Customer/GetColorCustomers", "/Customer/UpdateColorCustomers",
        "ColorID", "ColorName", this, "Color", true);
});

//Interesting brands
$("select#i-brand").on("change", function (e) {
    dialogUpdateData("/Customer/GetInterestingBrands", "/Customer/UpdateInterestingBrands",
        "BrandID", "BrandName", this, "Interesting Brand", true);
});

//Interesting models
$("select#i-model").on("change", function (e) {
    dialogUpdateData("/Customer/GetInterestingModels", "/Customer/UpdateInterestingModels",
        "ModelID", "ModelName", this, "Interesting Model", true);
});

//Yamaha branches
$("select#yamaha-branches").on("change", function (e) {
    dialogUpdateData("/Customer/GetYamahaBranches", "/Customer/UpdateYamahaBranches",
        "YmBranchID", "Name", this, "Yamaha Branch", true);
});

var selected_value = 0;
function dialogUpdateData(from_url, to_url, key, name, select, dialog_title, has_active, related) {
    if ($(select).val() == "-1") {
        $(select).val(selected_value);
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#dialog-define-new"
            },
            caption: dialog_title,
            type: "ok-cancel",
            button: {
                ok: {
                    text: "Save",
                },
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let new_index = 1;
            let table = dialog.content.find("table");
            let rel_key = "";
            if (!!related) {
                rel_key = Object.keys(related)[0];
            }
            if (!!has_active) {
                $("tr:first-child th:last-child", table).prop("hidden", false);
            }

            if (!!related) {
                $("tr:first-child th:last-child", table).after("<th style='width: 25px;'>Related</th>");
            }

            let add_new = $("<tr><td><i class='fas fa-plus-circle fa-lg fn-sky csr-pointer'></i></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='Name']").val() !== "") {
                    new_index++;
                    let row = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='form-control create-new'/></td></tr>");
                    if (!!has_active) {
                        row.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        row.append("<td><input name='Related' class='form-control' readonly disabled value='"+ related[Object.keys(related)][0] +"'></td>");
                    }
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);
            });

            $.ajax({
                url: from_url,
                dataType: "JSON",
                data: { related_id: !!related? related[rel_key] : 0},
                success: function (results) {                  
                    if (results.length > 0) {
                        let items = [];
                        $.each(results, function (index, data) {
                            index++;
                            let item = {};
                            item[key] = data[key];
                            item["index"] = "<i model-validation-for='" + $.stripSpace(data[name].toLowerCase()) + "'></i>" + index;
                            item[name] = "<div><input name='Name' value='" + data[name] + "' class='form-control' readonly></div>";
                            
                            if (!!related) {
                                item[rel_key] = "<div name='Related'>" + related[rel_key] +"</div>";
                            }
                            
                            if (!!has_active) {
                                item["active"] = "<input name='Active' type='checkbox'>";

                                if (!!data.Active) {
                                    item.active = "<input name='Active' type=checkbox checked>";
                                }
                            }

                            items.push(item);
                            new_index = index + 1;
                        });

                        $.bindRows(table, items, key, {
                            hide_key: true,
                            text_align: [{ "index": "center" }],
                            dblclick: function (e) {
                                let current_input = $("td input[name=Name]:not(.create-new)", this).prop("readonly", false);
                                let sibling_inputs = current_input.parent().parent().siblings().find("td input[name=Name]:not(.create-new)");
                                sibling_inputs.prop("readonly", true);
                            }
                        });

                        $(document).on("click", function (e) {
                            if (!$(e.target).is(table.find("tr *"))) {
                                table.find("tr td input:not(.create-new)").prop("readonly", true);
                            }
                            
                        });
                    }      

                    let create_new = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='create-new form-control' /></tr>");
                    
                    if (!!has_active) {
                        create_new.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        create_new.append("<td><div name='Related'>" + related[Object.keys(related)][0] + "</div></td>");
                    }
                    
                    table.append(create_new).append(add_new);
                    
                }
            });
           
        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table");
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let items = [];
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    let item = {};
                    item[key] = $(this).data(key.toLowerCase());
                    if (!!related) {
                        let _key = Object.keys(related);
                        item[_key] = $("div[name=Related]", td).text();
                    }
                    item[name] = $.stripHTML($("input[name=Name]", td).val());                 
                    if (!!has_active) {
                        item["Active"] = $("input[name=Active]", td).is(":checked");
                    }
                 
                    items.push(item);
                }
               
            });            
            $.ajax({
                url: to_url,
                type: "post",
                dataType: "json",
                data: $.antiForgeryToken({ data: items }),
                success: function (res) {
                    new ViewMessage(res.Errors);
                    updateSelect(select, res.Data, key, name);
                }

            });
         
        });
    }
}

function updateSelect(selector, jsons, key, value) {
    if ($(selector)[0].tagName === "SELECT") {
        $(selector).children().remove();

        let add_new = $("<option value='-1'>Define New</option>");
        $.each(jsons, function (i, json) {
            let option = $("<option value='" + json[key] + "'>" + json[value] + "</option>");
            $(selector).append(option);
        });
        $(selector).append(add_new);
        if ($(selector).children().length <= 1) {
            $(selector).prepend("<option value='0' selected disabled></option>");
        }
    }
}

function customerType(enum_val) {
    let _groups = ["SERVICE", "WALKIN", "PURCHASE"];
    return _groups[parseInt(enum_val)];
}

function parseGender(enum_val) {
    let genders = ["Female", "Male"];
    return genders[parseInt(enum_val)];
}

//Toggle customer type
$("#motocycle-customer-type").on("change", function () {    
    $("#trigger-remarks").prop("hidden", true);
    $("#purchase-customer-expand").prop("hidden", true); 
    if (this.value == 2) {
        $("#purchase-customer-expand").prop("hidden", false);
        $("#trigger-remarks").prop("hidden", false);
    } 
    window.scrollTo({ top: document.body.scrollHeight, behavior: "smooth" });
});

//Motocycle customer
let _customerType = $("#motocycle-customer-type").val();
$("#register-motocycle-customer").on("click", function (e) {
    let type = $("#motocycle-customer-type option:selected").val();
    submitMotocycle(type);
});

$("#update-motocycle-customer").on("click", function (e) {
    let type = $("#motocycle-customer-type option:selected").val();
    if (_customerType == 2 && type != 2) {
        let _dialog = new DialogBox({
            content: "You have changed type of customer from “PURCHASE” to “WALKIN”, some information will be lost, are you sure?",
            type: "yes-no",
            icon: "warning"
        });

        _dialog.confirm(function () {
            submitMotocycle(type, true);
        });
        _dialog.reject(function () {
            $("#purchase-customer-expand").prop("hidden", false);
            $("#motocycle-customer-type").val(_customerType);
        });
       
    } else {
        submitMotocycle(type, true);
    }
});

function submitMotocycle(customerType, updated) {
    let data = {};
    let c_walkin = $.form("#walkin-customer").value;
    c_walkin.PaymentType = $(".PaymentType:checked", "#walkin-customer").val();
    c_walkin.CustomerType = customerType;
    c_walkin.BirthDate = $($.form("#walkin-customer").dom["BirthDate"]).val();
    data = c_walkin;
    if (customerType == 2) {//0.SERVICE, 1.WALKIN, 2.PURCHASE
        data = $.extend(c_walkin, $.form("#purchase-customer-expand").value);
        data.SaleConsultant = !data.SaleConsultant ? 0 : data.SaleConsultant;
    }

    let option = {
        url: "/Customer/UpdateMotocycleCustomer",
        type: "POST",
        data: $.antiForgeryToken({ customer: data, onlyValidate: true }),
        success: function (res) {    
            new ViewMessage(res.Model);
            $("#tire-customer").addClass("current").removeClass("complete");
            if (res.Model.Action == "1") {
                onValidatedCustomer(res.Data);
                $("#block-motocycle-appointment").show(500);
                $("#block-motocycle-customer").hide(500);
                
                $("#tire-customer").removeClass("current").addClass("complete");
                $("#tire-appointment").addClass("current");
            }
            
        }
    };

    if (!!updated) {
        option.data = $.antiForgeryToken({ customer: data });
        option.success = function (response) {
            new ViewMessage(response.Model);        
        }
    }

    $.ajax(option);
}

 
function onValidatedCustomer(customer) {
    if (customer.CustomerType == "PURCHASE" || customer.CustomerType == 2) {
        $.ajax({
            url: "/Customer/SearchDefaultRemarks",
            success: function (remarks) {
                $("textarea[name=Remark]", "#motocycle-customer-appointment").val(remarks[0].RemarkText);
            }
        });
    }

   //Add new appoitment while customer info is valid.
    $("#add-motocycle-appointment").on("click", function (e) {
        submitMotocycleAppointment(customer, this);
    });
    
}

function submitMotocycleAppointment(customer, self) {
    let $submit = $(self);
    let sca_form = $.form("#motocycle-customer-appointment");

    let appointment = {
        AppointmentID: sca_form.value["AppointmentID"],
        AppointmentCode: sca_form.value["AppointmentCode"],
        Remark: sca_form.value["Remark"],
        ActivityDate: $(sca_form.dom["ActivityDate"]).attr("data-value"),
        ActivityTime: sca_form.value["ActivityTime"],
        Reminder: sca_form.value["Reminder"] + "/" + sca_form.value["ReminderUom"],
        Location: sca_form.value["Location"],
        Priority: sca_form.value["Priority"],
        Paused: sca_form.value["Paused"],
        Closed: sca_form.value["Closed"],
        CustomerID: sca_form.value["CustomerID"]
    };

    $.ajax({
        url: "/Customer/RegisterMotocycleCustomer",
        data: $.antiForgeryToken({ customer: customer, appointment: appointment }),
        type: "POST",
        success: function (model) {
            new ViewMessage(model);
            if (model.Action == "1") {
                $submit.prop("disabled", true).css("cursor", "default");
                let _confirm = new ViewMessage({
                    summary: {
                        attribute: "ck-message-summary"
                    }
                }, model);
                $("#tire-appointment").removeClass("current").addClass("complete");
                _confirm.redirect("/Customer/MotocycleCustomer", 2000);
            }
        }
    });

}



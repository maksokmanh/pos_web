﻿
setTimeout(() => {
    invokeDetectiveTimelyAppointments();
}, 1000);

function detectTimelyAppointments(url, callback, every) {
    $.ajax({
        url: url,
        data: { asJSON: true },
        success: function (response) {
            return callback(response);
        }
    });
    
    if (!!every && typeof every === "number") {
        let interval = setInterval(() => {
            $.ajax({
                url: url,
                data: { asJSON: true },
                success: function (response) {
                    return callback.call(interval, response);
                }
            });
            
        }, every);
    }
    
}

function invokeDetectiveTimelyAppointments() {
    detectTimelyAppointments("/ServiceCustomer/DetectAllTimelyAppointments", function (result) {
        if (result.length > 0) {
            let index = 1;
            let total_count = 0, service_count = 0, motocycle_count = 0;
            for (let ap of result) {
                if (ap.CustomerType != 0 && !ap.ItemServices) {
                    timelyAppointmentList("#motocycle-timely-appointment-list", ap, index);
                    motocycle_count++;
                }

                if ((!!ap.ItemServices)) {
                    timelyAppointmentList("#service-timely-appointment-list", ap, index);
                    service_count++;
                }

                index++;
            }
            $(".timely-service-count").text(service_count);
            $(".timely-motocycle-count").text(motocycle_count);
            total_count = service_count + motocycle_count;
            if (total_count > 0) {
                $("#note-total-appointment").addClass("animate")
                    .children(".n-value").text(total_count);
            }
        }
    }, 30000);
}

function timelyAppointmentList(dom_table, row, index) {
    $(dom_table).updateRow(row, "AppointmentID", {
        text_align: [{ "Name": "left" }, { "Code": "center" }],
        hidden_columns: ["Closed", "ActivityTime", "Paused", "CustomerID", "CustomerType", "ItemServices"],
        prebuild: function (data) {
            data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
        },
        postbuild: function (data) {
            $("td:first-child", this).replaceWith("<td>" + index + "</td>");
            let td = $("<td></td>");
            if (!!data.ItemServices) {
                td.append('<a title="Follow up" href="/ServiceCustomer/ServiceAppointmentFollowUp?appointmentID=' + data.AppointmentID + '&customerID='+ data.CustomerID +'" class="fas fa-chevron-circle-right fn-green fa-lg csr-pointer"></a>')
                  .append('&nbsp;|&nbsp;<a title="Detail" href="/ServiceCustomer/ServiceAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>');
            } 

            if (!data.ItemServices) {
                td.append('<a title="Follow up" href="/Customer/MotocycleAppointmentFollowUp?appointmentID=' + data.AppointmentID + '&customerID=' + data.CustomerID + '"class="fas fa-chevron-circle-right fn-green fa-lg csr-pointer"></a>')
                    .append('&nbsp;|&nbsp;<a title="Detail" href="/Customer/MotocycleAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>');
            }
               
            $("td:last-child", this).after(td);
        },

    });

    if ($("tr:not(:first-child)", dom_table).length > 0) {
        $(".timely").addClass("fa-pulse fn-red");
    }  
}



﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbYamahaBranch", Schema = "dbo")]
    public class YamahaBranch
    {
        [Key]
        public int YmBranchID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}

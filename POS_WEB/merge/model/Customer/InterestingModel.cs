﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbInterestingModel", Schema = "dbo")]
    public class InterestingModel
    {
        [Key]
        public int ModelID { get; set; }
        public string ModelName { get; set; }
        public bool Active { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    public class DefaultRemarkViewModel
    {
        public DefaultRemark DefaultRemark { get; set; }
        public IEnumerable<DefaultRemark> DefaultRemarks { get; set; }
    }
}

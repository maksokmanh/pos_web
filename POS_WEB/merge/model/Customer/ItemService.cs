﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbItemService", Schema = "dbo")]
    public class ItemService
    {
        [Key]
        public int ServiceID { get; set; }
        [Required(ErrorMessage = "Service Code is required.")]
        public string ServiceCode { get; set; }
        [Required(ErrorMessage = "Service Name is required.")]
        public string ServiceName { get; set; }
        public DateTime LastChanged { get; set; }
        public bool Deleted { get; set; }
    }
}

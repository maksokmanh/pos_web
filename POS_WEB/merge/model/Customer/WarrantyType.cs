﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbWarranty",Schema ="dbo")]
    public class WarrantyType
    {
        [Key]
        public int WarrantyID { get; set; }
        public string WarrantyName { get; set; }
        public bool Active { get; set; }
    }
}

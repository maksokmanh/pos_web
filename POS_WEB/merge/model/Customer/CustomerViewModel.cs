﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Appointment;

namespace POS_WEB.Models.Services.Customer
{
    public class CustomerViewModel
    {
        public Customer Customer { get; set; }
        public CustomerAppointment Appointment { get; set;}
        public IEnumerable<CustomerGroup> CustomerGroups { get; set; }
        public Employee SaleConsultant { get; set; }
        public IEnumerable<InterestingModel> InterestingModels { get; set; }
        public IEnumerable<InterestingBrand> InterestingBrands { get; set; }
        public IEnumerable<YamahaBranch> YamahaBranches { get; set; }
        public IEnumerable<ColorCustomer> ColorCustomers { get; set; }
        public IEnumerable<Region> Regions { get; set; }
        public IEnumerable<District> Districts { get; set; }
        public IEnumerable<ItemService> ItemServices { get; set; }
        public IEnumerable<SourceOfInfo> SourceOfInfos { get; set; }
        public IEnumerable<WarrantyType> WarrantyTypes { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbInterestingBrand", Schema = "dbo")]
    public class InterestingBrand
    {
        [Key]
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public bool Active { get; set; }
    }
}

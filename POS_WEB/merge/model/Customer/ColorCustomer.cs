﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbColorCustomer", Schema = "dbo")]
    public class ColorCustomer
    {
        [Key]
        public int ColorID { get; set; }
        public string ColorName { get; set; }
        public bool Active { get; set; }
    }
}

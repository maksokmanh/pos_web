﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbSourceOfInfo",Schema ="dbo")]
    public class SourceOfInfo
    {
        [Key]
        public int SourceID { get; set; }
        public string SourceName { get; set; }
        public bool Active { get; set; }
    }
}

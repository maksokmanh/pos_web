﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory.Category
{
    [Table("ItemGroup1",Schema ="dbo")]
    public class ItemGroup1
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemG1ID { get; set;}

        [Required(ErrorMessage ="Please input name !"),Display(Name="Item Name")]
        public string Name { get;set;}

        public string Images { get; set; }

        public bool Delete { get; set; }
        public bool Visible { get; set; }

        [Display(Name ="Color")]
        public int? ColorID { get; set; }
        [ForeignKey("ColorID")]
        public Colors Colors { get; set; }

        [Display(Name = "Background")]
        public int? BackID { get; set; }
        [ForeignKey("BackID")]
        public Background Background { get; set; }


    }
    
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory
{
    [Table("tbItemGroupCode",Schema ="dbo")]
    public class ItemGroupCode
    {
        [Key]
        public int ID { get; set; }
        public  string Name { get; set; }
    }
}

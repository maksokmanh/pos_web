﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory
{
    [Table("tbManufacturer",Schema ="dbo")]
    public class Manufacturer
    {
        [Key]
        public int ManuID { get; set; }
        public string ManuName { get; set; }
        public bool Delete { get; set; }
    }
}

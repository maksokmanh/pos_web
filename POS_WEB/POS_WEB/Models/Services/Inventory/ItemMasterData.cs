﻿
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Inventory.PriceList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory
{
    [Table("tbItemMasterData",Schema = "dbo")]
    public class ItemMasterData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input code !")]
        public string Code { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string KhmerName { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string EnglishName { get; set; }
        public double StockIn { get; internal set; } = 0;
        public double StockCommit { get; internal set; }= 0;
        public double StockOnHand { get; internal set; } = 0;
        public double Cost { get; set; }
        public double UnitPrice { get; set; }
        public int BaseUomID { get; set; }
        public int PriceListID { get; set; }
        public int GroupUomID { get; set; }        
        public int? PurchaseUomID { get; set; }      
        public int? SaleUomID { get; set; }        
        public int? InventoryUoMID { get; set; }
        public int WarehouseID { get; set; }
        public int ModelID { get; set; }
        public int ColorID { get; set; }
        public string Year { get; set; }
        [Required]
        public string Type { get; set; }//Item,Service,Labor
       
        public int ItemGroup1ID { get; set; }
        public int? ItemGroup2ID { get; set; }
        public int? ItemGroup3ID { get; set; }
        public bool Inventory { get; set; }
        public bool Sale { get; set; }
        public bool Purchase { get; set; }
        public string Barcode { get; set; }
       
        public  int? PrintToID { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Process { get; set; }//FIFO,Average,Standard
        public string ManageExpire { get; set; }
        public bool Delete { get; set; }

        // add new 
       
        public int ManufacturerID { get; set; } 
        public int SourceID { get; set; }
        public string GroupCode { get; set; }
      

        //Foreign Key
        [ForeignKey("ManufacturerID")]
        public Manufacturer Manufacturer { get; set; }
        [ForeignKey("SourceID")]
        public Source Source { get; set; }

        [ForeignKey("PriceListID")]
        public PriceLists PriceList { get; set; }

        [ForeignKey("GroupUomID")]
        public GroupUOM GroupUOM { get; set; }
     
        [ForeignKey("ItemGroup1ID")]
        public ItemGroup1 ItemGroup1 { get; set; }

        [ForeignKey("ItemGroup2ID")]
        public ItemGroup2 ItemGroup2 { get; set; }

        [ForeignKey("ItemGroup3ID")]
        public ItemGroup3 ItemGroup3 { get; set; }
        [ForeignKey("SaleUomID")]
        public UnitofMeasure UnitofMeasureSale { get; set; }
        [ForeignKey("PurchaseUomID")]
        public UnitofMeasure UnitofMeasurePur { get; set; }
        [ForeignKey("InventoryUoMID")]
        public UnitofMeasure UnitofMeasureInv { get; set; }
        [ForeignKey("PrintToID")]
        public PrinterName PrinterName { get; set; }

    }
  
}

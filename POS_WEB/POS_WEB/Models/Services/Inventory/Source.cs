﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory
{
    [Table("tbSource",Schema ="dbo")]
    public class Source
    {
        [Key]
        public int SouID { get; set; }
        public string SouName { get; set; }
        public bool Delete { get; set; }
    }
}

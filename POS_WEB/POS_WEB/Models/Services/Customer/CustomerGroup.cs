﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbCustomerGroup", Schema ="dbo")]
    public class CustomerGroup
    {
        [Key]
        public int GroupID { get; set; }
        public string  GroupName { get; set; }
        public bool Active { get; set; }
    }
}

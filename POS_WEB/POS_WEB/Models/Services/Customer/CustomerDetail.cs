﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    public class CustomerDetail
    {
        public int CustomerID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public CustomerType CustomerType { get; set; }
        public string CustomerGroup { get; set; }
        public CustomerGender Gender { get; set; }
        public string BirthDate { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }       
        public string Address { get; set; }
        public string Plate { get; set; }
        public string Frame { get; set; }
        public string Engine { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public PaymentType PaymentType { get; set; }
        public string SaleConsultant { get; set; }
        public string YamahaBranch { get; set; }
        public string SourceOfInfo { get; set; }
        public string Warranty { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace POS_WEB.Models.Services.Customer
{
    public enum CustomerType { SERVICE, WALKIN, PURCHASE }
    public enum PaymentType { CASH = 1, CREDITCARD = 2, LOAN = 3}
    public enum CustomerGender { Female, Male }
    [Table("tbCustomer", Schema ="dbo")]
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        public string Code { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public CustomerType CustomerType { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Phone is required.")]
        public string Phone { get; set; }
        [EmailAddress(ErrorMessage = "Invalid email address.")]       
        public string Email { get; set; }
        public string BirthDate { get; set; }
        public CustomerGender Gender { get; set; } 
        public string Address { get; set; }       
        public string Plate { get; set; }
        public string Frame { get; set; }     
        public string Engine { get; set; }      
        public string E_Year { get; set; }
        public string E_Brand { get; set; }
        public string E_Model { get; set; }                 
        public string B_Brand { get; set; }
        public string B_Model { get; set; }
        public string B_Year { get; set; }
        public int I_Brand { get; set; }
        public int I_Model { get; set; }
        public string I_Year { get; set; }
        public int Color { get; set; }     
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int Region { get; set; }
        public int District { get; set; }
        public int PaymentType { get; set; }
        public int SaleConsultant { get; set; }
        public int YamahaBranch { get; set; }     
        public int CustomerGroup { get; set; }
        public int SourceOfInfo { get; set; }
        public int Warranty { get; set; }
        public bool Deleted { get; set; }
    }
}

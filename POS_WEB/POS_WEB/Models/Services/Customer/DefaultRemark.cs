﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbDefaultRemark", Schema = "dbo")]
    public class DefaultRemark
    {      
        [Key]
        public int RemarkID { get; set; }
        [Required(ErrorMessage = "Remark Code is required.")]
        public string RemarkCode { get; set; }
        [Required(ErrorMessage = "Remark Text is required.")]
        public string RemarkText { get; set; }
        public DateTime LastChanged { get; set; }
        public bool Deleted { get; set; }
    }
}

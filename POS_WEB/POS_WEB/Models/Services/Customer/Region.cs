﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    [Table("tbRegion", Schema = "dbo")]
    public class Region
    {
        [Key]
        public int RegionID { get; set; }
        public string Name { get; set; }
    }
}

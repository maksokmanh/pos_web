﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Customer
{
    public class ItemServiceViewModel
    {
        public ItemService ItemService { get; set; }
        public IEnumerable<ItemService> ItemServices {get; set;}
    }
}

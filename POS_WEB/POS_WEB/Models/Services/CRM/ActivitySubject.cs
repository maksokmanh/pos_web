﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.CRM
{
    [Table("tbActivitySubject", Schema="dbo")]
    public class ActivitySubject
    {
        [Key]
        public int SubjectID { get; set; }
        [ForeignKey("TypeID")]
        public int TypeID { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.CRM
{
    [Table("tbActivityLocation", Schema = "dbo")]
    public class ActivityLocation
    {
        [Key]
        public int LocationID { get; set; }
        [Required]
        public string Name { get; set; }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.CRM
{
    [Table("tbActivityType",Schema = "dbo")]
    public class ActivityType
    {
        [Key]
        public int TypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Account;


namespace POS_WEB.Models.Services.CRM
{
    public class AppointmentModel
    {
        public IEnumerable<ActivityType> ActivityTypes { get; set; }
        public IEnumerable<ActivitySubject> ActivitySubjects { get; set; }
        public IEnumerable<Appointment> Appointments { get; set; }
        public IEnumerable<ActivityLocation> Locations { get; set; }
        public IEnumerable<UserAccount> Users { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public BusinessPartner BusinessPartners { get; set; }
        
        
    }
}

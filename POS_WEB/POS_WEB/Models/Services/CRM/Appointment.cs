﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS_WEB.Models.Services.CRM
{
    [Table("tbAppointment", Schema = "dbo")]
    public class Appointment
    {
        [Key]
        public int AppointID { get; set; }
        [Required(ErrorMessage = "Activity must be selected.")]
        public string Activity { get; set; }
        public string Remark { get; set; }
        public bool Personal { get; set; }
        public string StartTime { get; set; }    
        public string EndTime { get; set; }
        public string Duration { get; set; }
        public string Reminder { get; set; }
        public string Priority { get; set; }
        public bool Inactive { get; set; }
        public bool Closed { get; set; }
        public int Status { get; set; }
        public string Room { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        [Required(ErrorMessage = "Type must be selected.")]
        public int? TypeID { get; set; }
        [Required(ErrorMessage = "Subject must be selected.")]  
        public int? SubjectID { get; set; }
        public int? LocationID { get; set; }
        [Required(ErrorMessage = "Assigned By is required.")]
        public int UserID { get; set; }
        [Required(ErrorMessage = "Assigned To is required.")]
        public int EmpID { get; set; }
        [Required(ErrorMessage = "BP Code is required.")]
        public int? BPID { get; set; }
      

    }
}

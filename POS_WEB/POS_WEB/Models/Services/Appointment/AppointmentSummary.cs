﻿using POS_WEB.Models.Services.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Appointment
{
    public class AppointmentSummary
    {
        public int AppointmentID { get; set; }
        public string AppointmentCode { get; set; }   
        public string CustomerName { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Remark { get; set; }
        public string ActivityDate { get; set; }
        public string ActivityTime { get; set; }
        public string Reminder { get; set; }     
        public string Priority { get; set; }
        public bool Paused { get; set; }
        public bool Closed { get; set; }
        public int CustomerID { get; set; }
        public CustomerType CustomerType { get; set; }
        public string ItemServices { get; set; }
    }
}

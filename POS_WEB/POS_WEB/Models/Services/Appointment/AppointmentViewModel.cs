﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using POS_WEB.Models.Services.Customer;

namespace POS_WEB.Models.Services.Appointment
{
    public class AppointmentViewModel
    {
        public CustomerAppointment Appointment { get; set; }
        public Customer.Customer Customer { get; set; }
        public CustomerDetail CustomerDetail { get; set; }
        public IEnumerable<ItemService> ItemServices { get; set; }
        public IEnumerable<AppointmentHistory> AppointmentHistories { get; set; }
    }
}

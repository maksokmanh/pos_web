﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Appointment
{
    [Table("tbAppointmentHistory", Schema = "dbo")]
    public class AppointmentHistory
    {
        [Key]
        public int HistoryID { get; set; }
        public int AppointmentID { get; set; }
        public string AppointmentCode { get; set; }
        public string Remark { get; set; }
        [Required(ErrorMessage = "Appointment Time is required.")]
        public DateTime FollowUp { get; set; }       
        public string Reminder { get; set; }
        public string Location { get; set; }
        public string Priority { get; set; }      
        //[Required(ErrorMessage = "At least one item service need to be selected.")]
        public string ItemServices { get; set; }
        [Required]
        public int CustomerID { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using POS_WEB.Models.Services.Customer;

namespace POS_WEB.Models.Services.Appointment
{
    [Table("tbCustomerAppointment", Schema = "dbo")]
    public class CustomerAppointment
    {
        [Key]
        public int AppointmentID { get; set; }
        public string AppointmentCode { get; set; }
        public string Remark { get; set; }
        [Required(ErrorMessage = "Appointment Time is required.")]     
        public string ActivityDate { get; set; }
        public string ActivityTime { get; set; }
        [MaxLength(15, ErrorMessage = "Reminder exceeds the maximum limit.")]
        public string Reminder { get; set; }
        public string Location { get; set; }
        public string Priority { get; set; }
        public bool Paused { get; set; }
        public bool Closed { get; set; }
        //[Required(ErrorMessage = "At least one item service need to be selected.")]
        public string ItemServices { get; set; }
        [Required]
        public int CustomerID { get; set; }
    }
}

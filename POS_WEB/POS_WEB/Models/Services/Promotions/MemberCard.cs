﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Promotions
{
    [Table("tbMemberCard",Schema ="dbo")]
    public class MemberCard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int CardTypeID { get; set; }
        [Column(TypeName ="nvarchar(25)")]
        public string  Ref_No { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        public bool Delete { get; set; }
        public string Approve { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}",ApplyFormatInEditMode =true)]
        public DateTime DateCreate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpireDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateApprove { get; set; }
        [ForeignKey("CardTypeID")]
        public CardType CardType { get; set; }
    }
}

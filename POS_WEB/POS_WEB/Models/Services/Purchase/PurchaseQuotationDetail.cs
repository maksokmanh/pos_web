﻿using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseQuotationDetail", Schema = "dbo")]
    public class PurchaseQuotationDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseQuotaionDetailID { get; set; }
        public int PurchaseQuotationID { get; set; }
        public int LineID { get; set; }
        public int ItemID { get; set; }
        public int UomID { get; set; }
        public int LocalCurrencyID { get; set; }
        public double DiscountRate { get; set; }
        public double DiscountValue { get; set; }
        public string TypeDis { get; set; }//Percent ,cash
        public double Qty { get; set; }
        public double PurchasPrice { get; set; }
        public double Total { get; set; }

        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime ExpireDate { get; set; }
        public double AlertStock { get; set; }
        public double Total_Sys { get; set; }
        public bool Delete { get; set; }
        public double OpenQty { get; set; }
        public double OldQty { get; set; }
        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime RequiredDate { get; set; }
        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime QuotedDate { get; set; }
        public double RequiredQty { get; set; }
        public int requestID { get; set; }
        //ForeignKey

        [ForeignKey("ItemID")]
        public virtual ItemMasterData ItemMasterData { get; set; }
        [ForeignKey("UomID")]
        public virtual UnitofMeasure UnitofMeasure { get; set; }
        [ForeignKey("LocalCurrencyID")]
        public Currency Currency { get; set; }
    }
}
﻿using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.ServicesClass;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseOrder", Schema = "dbo")]
    public class PurchaseOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderID { get; set; }
        public int VendorID { get; set; }
        public int BranchID { get; set; }
        public int LocalCurrencyID { get; set; }
        public int SysCurrencyID { get; set; }
        public int WarehouseID { get; set; }
        public int UserID { get; set; }
        public string Reff_No { get; set; }
        public string InvoiceNo { get; set; }
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PostingDate { get; set; }
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DocumentDate { get; set; }
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DeliveryDate { get; set; }
        public double Sub_Total { get; set; }
        public double Sub_Total_Sys { get; set; }
        public double DiscountValues { get; set; }
        public double DiscountRate { get; set; }
        public string TypeDis { get; set; }
        public double TaxRate { get; set; }
        public double TaxValues { get; set; }
        public double Balance_Due { get; set; }
        public double ExchangeRate { get; set; }
        public double Balance_Due_Sys { get; set; }
        public string Remark { get; set; }
        public double Down_Payment { get; set; } = 0;
        public double Applied_Amount { get; set; }
        public double Return_Amount { get; set; }
        public double Additional_Expense { get; set; } // no calualte but save in database => textbox
        public string Additional_Note { get; set; }// no calualte but save in database => textbox
        public List<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public string Status { get; set; }

        //ForeignKey
        [ForeignKey("VendorID")]
        public BusinessPartner BusinessPartner { get; set; }
        [ForeignKey("WarehouseID")]
        public Warehouse Warehouse { get; set; }
        [ForeignKey("UserID")]
        public UserAccount UserAccount { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }
    }
}
﻿using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseOrderDetail", Schema = "dbo")]
    public class PurchaseOrderDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderDetailID { get; set; }
        public int PurchaseOrderID { get; set; }
        public int LineID { get; set; }
        public int ItemID { get; set; }
        public int UomID { get; set; }
        public int LocalCurrencyID { get; set; }
        public double DiscountRate { get; set; }
        public double DiscountValue { get; set; }
        public string TypeDis { get; set; }//Percent ,cash
        public double Qty { get; set; }
        public double PurchasPrice { get; set; }
        public double Total { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpireDate  { get; set; }
        public double AlertStock { get; set; }
        public double Total_Sys { get; set; }
        public bool Delete { get; set; }
        public double OpenQty { get; set; }
        public double OldQty { get; set; }
        public int QuotationID { get; set; }
        public string Check { get; set; }

        //[ForeignKey("OrderID")]
        //public PurchaseOrder PurchaseOrder { get; set; }
        [ForeignKey("ItemID")]
        public virtual ItemMasterData ItemMasterData { get; set; }
        [ForeignKey("UomID")]
        public virtual UnitofMeasure UniofMeasure { get; set; } 
        [ForeignKey("LocalCurrencyID")]
        public Currency Currency { get; set; }
    }
}

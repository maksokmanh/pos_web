﻿using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseCreditMemoDetail")]
    public class PurchaseCreditMemoDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseMemoDetailID { get; set; }
        public int PurchaseCreditMemoID { get; set; }
        public int ItemID { get; set; }
        public int LineID { get; set; }
        public int UomID { get; set; }
        public int LocalCurrencyID { get; set; }
        public double Qty { get; set; }
        public double Total { get; set; }//total=qty*purchase
        public double Total_Sys { get; set; }
        public double DiscountRate { get; set; }
        public double DiscountValue { get; set; }
        public string TypeDis { get; set; }//Percent ,cash
        public double PurchasPrice { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpireDate { get; set; }
        public double AlertStock { get; set; }
        public string check { get; set; }
        public int APID { get; set; }
        //ForeignKey
        //[ForeignKey("PurchaseMemoID")]
        //public PurchaseCreditMemo PurchaseCreditMemo  { get; set; }
        [ForeignKey("ItemID")]
        public virtual ItemMasterData ItemMasterData { get; set; }
        [ForeignKey("UomID")]
        public virtual UnitofMeasure UnitofMeasure { get; set; }
        [ForeignKey("LocalCurrencyID")]
        public Currency Currency { get; set; }
    }
}

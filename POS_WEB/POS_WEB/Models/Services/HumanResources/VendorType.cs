﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.HumanResources
{
    [Table("tbVendorType",Schema ="dbo")]
    public class VendorType
    {
        [Key]
        public int ID { get; set; }
        public string Type { get; set; }
        public bool Delete { get; set; }
    }
}

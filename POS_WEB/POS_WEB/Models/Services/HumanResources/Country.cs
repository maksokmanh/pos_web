﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.HumanResources
{
    [Table("tbCountry",Schema ="dbo")]
    public class Country
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Delete { get; set; }

    }
}

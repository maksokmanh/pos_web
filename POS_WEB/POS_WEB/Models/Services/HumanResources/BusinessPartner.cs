﻿
using POS_WEB.Models.Services.Inventory.PriceList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.HumanResources
{
    [Table("tbBusinessPartner",Schema = "dbo")]
    public class BusinessPartner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Please input code !")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please input name !")]
        public string Name { get; set; }
        public string Type { get; set; } // Vender,Customer
        public int PriceListID { get; set; }
        [Phone]
        [Required(ErrorMessage ="Please input phone !")]
        public string Phone { get; set; }
        [Phone]
        public string Phone2 { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Address { get; set; }
        public int VendorType { get; set; }
        public int Country { get; set; }
        public string VendorTypeName { get; set; }
        public string CountryName { get; set; }
        public bool Delete { get; set; } = false;
        //Foreign Key
        [ForeignKey("PriceListID")]
        public PriceLists PriceList { get; set; }
        
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.HumanResources
{
    public enum Gender { Female, Male }
    [Table("tbEmployee", Schema ="dbo")]
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [ForeignKey("GroupID")]
        public int GroupID { get; set; }
        [MaxLength(50)]
        [Required(ErrorMessage = "Please input code !")]
        public string Code { get; set; }
        [MaxLength(50)]
        [Required(ErrorMessage = "Please input name !")]
        public string Name { get; set; }
        public Gender Gender { get; set; }//Male,Female      
        [Required(ErrorMessage = "Birthdate is required.")]
        public string Birthdate { get; set; }
        [Required(ErrorMessage = "Hiredate is required.")]
        public string Hiredate { get; set; }
        [MaxLength(220)]
        public string Address { get; set; }
        [Phone]
        [Required(ErrorMessage = "Please input phone !")]
        public string Phone { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public byte[] Image;
        public string Photo { get; set; }       
        public bool Stopwork { get; set; }
        public string Position { get; set; }
        public bool IsUser { get; set; }
        public bool Deleted { get; set; }
    }
    
}

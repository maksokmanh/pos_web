﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.HumanResources
{
    public class EmployeeViewModel
    {
        public Employee Employee { get; set; }
        public IEnumerable<GroupEmployee> GroupEmployees { get; set; }
    }
}

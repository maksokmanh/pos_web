﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Banking 
{
    [Table("tbExchangeRate", Schema = "dbo")]
    public class ExchangeRate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int CurrencyID { get; set; }
        [ForeignKey("CurrencyID")]
        public Currency Currency { get; set; }
        //[DisplayFormat(DataFormatString = "{{0:0.00}}", ApplyFormatInEditMode = true)]
        public double Rate { get; set; }
        public bool Delete { get; set; }
        public double  RateOut { get; set; }
        
    }
}

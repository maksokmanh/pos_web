﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Banking
{
    [Table("tbOutgoingpaymnetDetail", Schema = "dbo")]
    public class OutgoingPaymentDetail
    {
        [Key]
        public int OutgoingPaymentDetailID { get; set; }
        public int OutgoingPaymentID { get; set; }
        public string DocumentNo {get ;set;}
        public string DocumentType { get; set; }
        [Column(TypeName ="Date")]
        public DateTime Date { get; set; }
        public double OverdueDays { get; set; }
        public double Total { get; set; }
        public double BalanceDue { get; set; }
        public double CashDiscount { get; set; }
        public double TotalDiscount { get; set; }
        public double Totalpayment { get; set; }
        public double Applied_Amount { get; set; }
        public int CurrencyID { get; set; }
        public double Cash { get; set; }
        public double ExchangeRate { get; set; }
        public bool Delete { get; set; }
        [ForeignKey("CurrencyID")]
        public  Currency Currency { get; set; }
        

    }
}

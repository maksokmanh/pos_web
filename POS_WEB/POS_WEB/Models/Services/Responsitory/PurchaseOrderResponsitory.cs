﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.ClassCopy;
using POS_WEB.Models.ReportClass;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.ServicesClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IPurchaseOrder
    {
        IEnumerable<Company> GetCurrencyDefualt();
        IEnumerable<GroupDUoM> GetAllGroupDefind();
        IEnumerable<ServiceMapItemMasterDataPurchaseOrder> ServiceMapItemMasterDataPurchaseOrders(int ID);
        IEnumerable<ServiceMapItemMasterDataPurchaseOrder> GetItemFindeBarcode(int WarehouseID,string Barcode);
        void GoodReceiptStockOrder(int PurchaseID);
        IEnumerable<ReportPurchaseOrder> ReportPurchaseOrders(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check);
        IEnumerable<ServiceMapItemMasterDataPurchaseOrder> ServiceMapItemMasterDataPurchaseOrders_Detail(int WarehouseID, string Invoice);
        IEnumerable<ReportPurchaseQuotation> GetAllPurchaeQuotation(int BranchID);
        IEnumerable<PurchaesOrder_from_Quotation> GetPurchaseOrder_From_PurchaseQuotation(int ID, string Invoice);
        IEnumerable<PurchaesOrder_from_Quotation> GetPurchaseOrder_From_PurchaseRequest(int ID, string Invoice);
        IEnumerable<ReportPurchaseRequset> GetAllPurchaseRequest();
        void POCancel(int purchaseID);
    }
    public class PurchaseOrderResponsitory : IPurchaseOrder
    {
        private readonly DataContext _context;
        public PurchaseOrderResponsitory(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<GroupDUoM> GetAllGroupDefind()
        {
            var uom = _context.UnitofMeasures.Where(u => u.Delete == false);
            var guom = _context.GroupUOMs.Where(g => g.Delete == false);
            var duom = _context.GroupDUoMs.Where(o => o.Delete == false);
            var list = (from du in duom
                        join g_uom in guom on du.GroupUoMID equals g_uom.ID
                        join buo in uom on du.BaseUOM equals buo.ID
                        join auo in uom on du.AltUOM equals auo.ID
                        where g_uom.Delete == false
                        select new GroupDUoM
                        {
                            ID = du.ID,
                            GroupUoMID = du.GroupUoMID,
                            UoMID = du.UoMID,
                            AltQty = du.AltQty,
                            BaseUOM = du.BaseUOM,
                            AltUOM = du.AltUOM,
                            BaseQty = du.BaseQty,
                            Factor = du.Factor,

                            UnitofMeasure = new UnitofMeasure
                            {
                                ID = auo.ID,
                                Name = buo.Name,
                                AltUomName = auo.Name
                            },

                        }
                );
            return list;
        }

        public IEnumerable<ReportPurchaseQuotation> GetAllPurchaeQuotation(int BranchID) => _context.ReportPurchaseQuotations.FromSql("sp_GetAllPurchaeQuotation @BranchID={0}",
            parameters:new[] {
                BranchID.ToString()
            });
     
        public IEnumerable<Company> GetCurrencyDefualt()
        {
            IEnumerable<Company> list = (
                from pd in _context.PriceLists.Where(x => x.Delete == false)
                join com in _context.Company.Where(x => x.Delete == false) on
                pd.ID equals com.PriceListID
                join cur in _context.Currency.Where(x => x.Delete == false)
                on pd.CurrencyID equals cur.ID
                select new Company
                {
                    PriceList = new PriceLists
                    {
                        Currency = new Currency
                        {
                            Description = cur.Description
                        }
                    }
                }

                );
            return list;
        }

        public IEnumerable<ServiceMapItemMasterDataPurchaseOrder> GetItemFindeBarcode(int WarehouseID, string Barcode) => _context.ServiceMapItemMasterDataPurchaseOrders.FromSql("sp_FindeBarcodePurchaseOrder @WarehouseID={0},@Barcode={1}",
            parameters:new[] {
                WarehouseID.ToString(),
                Barcode.ToString()
            });
       
        public void GoodReceiptStockOrder(int PurchaseID)
        {
            _context.Database.ExecuteSqlCommand("sp_GoodReceiptStockPurchaseOrder @purchaseID={0}",
                parameters: new[] {
                    PurchaseID.ToString()
                });
        }

        public IEnumerable<ReportPurchaseOrder> ReportPurchaseOrders(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check) => _context.ReportPurchaseOrders.FromSql("sp_ReportPurchaseCreditOrder @BranchID={0},@warehouseID={1},@PostingDate={2},@DeliveryDate={3},@DocumentDate={4},@Check={5}",
            parameters:new[] {
                BranchID.ToString(),
                WarehouseID.ToString(),
                PostingDate.ToString(),
                DocumentDate.ToString(),
                DeliveryDate.ToString(),
                Check.ToString()
            });
       

        public IEnumerable<ServiceMapItemMasterDataPurchaseOrder> ServiceMapItemMasterDataPurchaseOrders(int ID) => _context.ServiceMapItemMasterDataPurchaseOrders.FromSql("sp_GetListItemMasterDataPurchaseOrder @WarehouseID={0}",
            parameters: new[] {
                ID.ToString()
            });

        public IEnumerable<ServiceMapItemMasterDataPurchaseOrder> ServiceMapItemMasterDataPurchaseOrders_Detail(int WarehouseID, string Invoice) => _context.ServiceMapItemMasterDataPurchaseOrders.FromSql("sp_GetListItemMasterDataPurchaseOrder_Detail @WarehouseID={0},@InvoiceNo={1}",
            parameters: new[] {

                WarehouseID.ToString(),
                Invoice.ToString()
            });
        public IEnumerable<PurchaesOrder_from_Quotation> GetPurchaseOrder_From_PurchaseQuotation(int ID, string Invoice) => _context.purchaesOrder_From_Quotations.FromSql("sp_GetPurchaseOrder_form_PurchaseQuotation @PurchaseID={0},@Invoice={1}",
         parameters: new[] {
                ID.ToString(),
                Invoice.ToString()
         });

        public IEnumerable<ReportPurchaseRequset> GetAllPurchaseRequest() => _context.ReportPurchaseRequsets.FromSql("sp_GetAllPurchaeRequest");

        public IEnumerable<PurchaesOrder_from_Quotation> GetPurchaseOrder_From_PurchaseRequest(int ID, string Invoice) => _context.purchaesOrder_From_Quotations.FromSql("sp_GetPurchaseOrder_from_PurchaseRequest @PurchaseID={0},@Invoice={1}",
            parameters:new[] {
                ID.ToString(),
                Invoice.ToString()
            });

        public void POCancel(int purchaseID)
        {
            if (purchaseID != 0)
            {
                var purchase = _context.PurchaseOrders.Include(w => w.PurchaseOrderDetails).FirstOrDefault(w => w.PurchaseOrderID == purchaseID);
                if (purchase != null || purchase.PurchaseOrderDetails != null)
                {
                    purchase.Status = "close";
                    _context.PurchaseOrders.Update(purchase);
                    _context.SaveChanges();
                    foreach (var item in purchase.PurchaseOrderDetails.ToList())
                    {
                        var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                        var gduom = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_master.GroupUomID && w.AltUOM == item.UomID);

                        var warehouse_sum = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == purchase.WarehouseID
                                                                                        && w.ItemID == item.ItemID
                                                                                        );
                        if (warehouse_sum != null)
                        {
                            warehouse_sum.Ordered = warehouse_sum.Ordered - (item.OpenQty * gduom.Factor);
                            _context.WarehouseSummary.Update(warehouse_sum);
                            _context.SaveChanges();
                        }
                        //Update in item maser
                        item_master.StockOnHand = item_master.StockOnHand - (item.OpenQty * gduom.Factor);
                        _context.ItemMasterDatas.Update(item_master);
                        _context.SaveChanges();
                    }
                }
            }
        }
    }
}

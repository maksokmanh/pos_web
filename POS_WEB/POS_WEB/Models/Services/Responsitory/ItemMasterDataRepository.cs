﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Inventory.PriceList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IItemMasterData
    {
        IQueryable<ItemMasterData> GetItemMasterData();
        IEnumerable<ItemMasterData> GetMasterDatas();
        IEnumerable<ItemMasterData> GetMasterDatasByCategory(int ID);
        ItemMasterData GetbyId(int id);
        Task<int> AddOrEdit(ItemMasterData itemMaster);
        Task<int> AddWarehouseDeatail(WarehouseDetail warehouseDetail);
        Task<int> AddWarehouseSummary(WarehouseSummary warehouseSummary);
        Task<int> AddPricelistDetail(PriceListDetail priceListDetail);
        Task<int> DeleteItemMaster(int id);
        IEnumerable<PrinterName> GetPrinter { get; }
        void RemoveItmeInWarehous(int ItemID);
      
    }
    public class ItemMasterDataRepository : IItemMasterData
    {
        private readonly DataContext _context;
        public ItemMasterDataRepository(DataContext dataContext)
        {
            _context = dataContext;
        }
        public IEnumerable<PrinterName> GetPrinter => _context.PrinterNames.Where(p => p.Delete == false).ToList();

        public IQueryable<ItemMasterData> GetItemMasterData()
        {

            IQueryable<ItemMasterData> list = (
                                              from item in _context.ItemMasterDatas.Where(d => d.Delete == false)
                                              join pl in _context.PriceLists.Where(d => d.Delete == false) on item.PriceListID equals pl.ID
                                              join item1 in _context.ItemGroup1.Where(d => d.Delete == false) on item.ItemGroup1ID equals item1.ItemG1ID
                                              join item2 in _context.ItemGroup2.Where(d => d.Delete == false) on item.ItemGroup2ID equals item2.ItemG2ID into item_item2 from item_2 in item_item2.DefaultIfEmpty()
                                              join item3 in _context.ItemGroup3.Where(d => d.Delete == false) on item.ItemGroup3ID equals item3.ID into item_item3 from item_3 in item_item3.DefaultIfEmpty()
                                              join print in _context.PrinterNames.Where(d => d.Delete == false) on item.PrintToID equals print.ID
                                              join uom in _context.UnitofMeasures.Where(d => d.Delete == false) on item.InventoryUoMID equals uom.ID
                                              join sou in _context.Sources.Where(x => x.Delete == false) on item.SourceID equals sou.SouID into source
                                              from sou_id in source.DefaultIfEmpty()
                                              join manu in _context.Manufacturers.Where(x => x.Delete == false) on item.ManufacturerID equals manu.ManuID into manufacturce
                                              from manu_id in manufacturce.DefaultIfEmpty()
                                              where item.Delete==false
                                              select new ItemMasterData
                                              {
                                                  ID = item.ID,
                                                  Code = item.Code,
                                                  KhmerName = item.KhmerName,
                                                  EnglishName = item.EnglishName,
                                                  StockIn = item.StockIn,
                                                  Cost = item.Cost,
                                                  UnitPrice = item.UnitPrice,
                                                  Barcode = item.Barcode,
                                                  Type = item.Type,
                                                  Description = item.Description,
                                                  Image = item.Image,
                                                  ItemGroup1ID = item.ItemGroup1ID,
                                                  ItemGroup2ID = item.ItemGroup2ID,
                                                  ItemGroup3ID = item.ItemGroup3ID,
                                                  PriceListID = item.PriceListID,
                                                  PrintToID = item.PrintToID,
                                                  Process = item.Process,
                                                  SaleUomID=item.SaleUomID,
                                                  SourceID=item.SourceID,
                                                  ManufacturerID=item.ManufacturerID,
                                                  UnitofMeasureInv = new UnitofMeasure
                                                  {
                                                      ID = uom.ID,
                                                      Name = uom.Name

                                                  },
                                                   
                                                  PriceList = new PriceLists
                                                  {
                                                      ID = print.ID,
                                                      Name = print.Name
                                                  },
                                                  ItemGroup1 = new ItemGroup1
                                                  {
                                                      ItemG1ID = item1.ItemG1ID,
                                                      Name = item1.Name
                                                  },
                                                  ItemGroup2 = new ItemGroup2
                                                  {
                                                     
                                                      Name =item_2.Name==null ? "None" :item_2.Name
                                                  },
                                                  ItemGroup3 = new ItemGroup3
                                                  {
                                                      
                                                      Name =item_3.Name==null? "None" : item_3.Name 
                                                  },
                                                  PrinterName = new PrinterName
                                                  {
                                                      ID = print.ID,
                                                      Name = print.Name
                                                  },
                                                  Manufacturer=new Manufacturer
                                                  {
                                                      ManuName=manu_id.ManuName==null ? "No manufacturce":manu_id.ManuName 
                                                  },
                                                  Source = new Source
                                                  {
                                                      SouName = sou_id.SouName == null ? "No source" : sou_id.SouName
                                                  }


                                              }
                );
            return list;
        }

        public async Task<int> AddOrEdit(ItemMasterData itemMaster)
        {

            if (itemMaster.ID == 0)
            {
                var count_2 = _context.ItemGroup2.Count();
                var count_3 = _context.ItemGroup3.Count();
                var ItemGroup2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                var ItemGroup3 = _context.ItemGroup3.FirstOrDefault(x => x.Name == "None");
                var manu = _context.Manufacturers.FirstOrDefault(x => x.ManuName == "None");
                var sourer = _context.Sources.FirstOrDefault(x => x.SouName == "None");
                if (sourer == null)
                {
                    Source s = new Source
                    {
                        Delete=false,
                        SouName="None"
                    };
                    _context.Sources.Add(s);
                    _context.SaveChanges();
                }
                if (manu == null)
                {
                    Manufacturer ma = new Manufacturer
                    {
                        Delete = false,
                        ManuName = "None"
                    };
                    _context.Manufacturers.Add(ma);
                    _context.SaveChanges();
                }
                if (ItemGroup2 == null)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup2 item_2 = new ItemGroup2
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            Name = "None",
                            Delete = false,
                        };
                        _context.ItemGroup2.Add(item_2);
                        _context.SaveChanges();
                    }
                }
                if (ItemGroup3 == null)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var group2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup3 item_3 = new ItemGroup3
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            ItemG2ID = group2.ItemG2ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup3.Add(item_3);
                        _context.SaveChanges();
                    }
                }

                if (count_2 == 0)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup2 item_2 = new ItemGroup2
                        {

                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup2.Add(item_2);
                        _context.SaveChanges();
                    }

                }
                if (count_3 == 0)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var group2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup3 item_3 = new ItemGroup3
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            ItemG2ID = group2.ItemG2ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup3.Add(item_3);
                        _context.SaveChanges();
                    }
                }

                var item2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                var item3 = _context.ItemGroup3.FirstOrDefault(x => x.Name == "None");
                var manu_find = _context.Manufacturers.FirstOrDefault(x => x.ManuName == "None");
                var sourer_find = _context.Sources.FirstOrDefault(x => x.SouName == "None");

                if (itemMaster.ItemGroup2ID == 0 || itemMaster.ItemGroup2ID == null)
                {
                    itemMaster.ItemGroup2ID = item2.ItemG2ID;
                }
                if (itemMaster.ItemGroup3ID == null || itemMaster.ItemGroup3ID==0)
                {
                    itemMaster.ItemGroup3ID = item3.ID;
                }
                if (itemMaster.SourceID == 0)
                {
                    itemMaster.SourceID = sourer_find.SouID;
                }
                if (itemMaster.ManufacturerID == 0)
                {
                    itemMaster.ManufacturerID = manu_find.ManuID;
                }
                itemMaster.Code = itemMaster.GroupCode + "-" + itemMaster.Code;
                await _context.ItemMasterDatas.AddAsync(itemMaster);
            }
            else
            {
                var count_2 = _context.ItemGroup2.Count();
                var count_3 = _context.ItemGroup3.Count();
                var ItemGroup2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                var ItemGroup3 = _context.ItemGroup3.FirstOrDefault(x => x.Name == "None");
                var manu = _context.Manufacturers.FirstOrDefault(x => x.ManuName == "None");
                var sourer = _context.Sources.FirstOrDefault(x => x.SouName == "None");
                if (sourer == null)
                {
                    Source s = new Source
                    {
                        Delete = false,
                        SouName = "None"
                    };
                    _context.Sources.Add(s);
                    _context.SaveChanges();
                }
                if (manu == null)
                {
                    Manufacturer ma = new Manufacturer
                    {
                        Delete = false,
                        ManuName = "None"
                    };
                    _context.Manufacturers.Add(ma);
                    _context.SaveChanges();
                }
                if (ItemGroup2 == null)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup2 item_2 = new ItemGroup2
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            Name = "None",
                            Delete = false,
                        };
                        _context.ItemGroup2.Add(item_2);
                        _context.SaveChanges();
                    }
                }
                if (ItemGroup3 == null)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var group2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup3 item_3 = new ItemGroup3
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            ItemG2ID = group2.ItemG2ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup3.Add(item_3);
                        _context.SaveChanges();
                    }
                }

                if (count_2 == 0)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup2 item_2 = new ItemGroup2
                        {

                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup2.Add(item_2);
                        _context.SaveChanges();
                    }

                }
                if (count_3 == 0)
                {
                    var countBack = _context.Backgrounds.Count();
                    var color = _context.Colors.Count();
                    if (countBack == 0)
                    {
                        Background back = new Background
                        {
                            Delete = false,
                            Name = "None"

                        };
                        _context.Backgrounds.Add(back);
                        _context.SaveChanges();
                    }
                    else if (color == 0)
                    {
                        Colors col = new Colors
                        {
                            Delete = false,
                            Name = "None"
                        };
                        _context.Colors.Add(col);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var group2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                        int BackID = _context.Colors.Min(x => x.ColorID);
                        int ColorID = _context.Backgrounds.Min(x => x.BackID);
                        ItemGroup3 item_3 = new ItemGroup3
                        {
                            BackID = BackID,
                            ColorID = ColorID,
                            Images = "null",
                            ItemG1ID = itemMaster.ItemGroup1ID,
                            ItemG2ID = group2.ItemG2ID,
                            Name = "None",
                            Delete = false,

                        };
                        _context.ItemGroup3.Add(item_3);
                        _context.SaveChanges();
                    }
                }

                var item2 = _context.ItemGroup2.FirstOrDefault(x => x.Name == "None");
                var item3 = _context.ItemGroup3.FirstOrDefault(x => x.Name == "None");
                var manu_find = _context.Manufacturers.FirstOrDefault(x => x.ManuName == "None");
                var sourer_find = _context.Sources.FirstOrDefault(x => x.SouName == "None");


                if (itemMaster.ItemGroup2ID == null)
                {
                    itemMaster.ItemGroup2ID = item2.ItemG2ID;
                }
                if (itemMaster.ItemGroup3ID == null)
                {
                    itemMaster.ItemGroup3ID = item3.ID;
                }
                if (itemMaster.SourceID == 0)
                {
                    itemMaster.SourceID = sourer_find.SouID;
                }
                if (itemMaster.ManufacturerID == 0)
                {
                    itemMaster.ManufacturerID = manu_find.ManuID;
                }
                if (itemMaster.Image == null)
                {
                    var item = _context.ItemMasterDatas.Find(itemMaster.ID);
                    item.Barcode = itemMaster.Barcode;
                    item.BaseUomID = itemMaster.BaseUomID;
                    item.Code = itemMaster.Code;
                    item.Cost = itemMaster.Cost;
                    item.Description = itemMaster.Description;
                    item.EnglishName = itemMaster.EnglishName;
                    item.GroupUomID = itemMaster.GroupUomID;
                    item.Image = item.Image;
                    item.Inventory = itemMaster.Inventory;
                    item.InventoryUoMID = itemMaster.InventoryUoMID;
                    item.ItemGroup1ID = itemMaster.ItemGroup1ID;
                    item.ItemGroup2ID = itemMaster.ItemGroup2ID;
                    item.ItemGroup3ID = itemMaster.ItemGroup3ID;
                    item.KhmerName = itemMaster.KhmerName;
                    item.ManageExpire = itemMaster.ManageExpire;
                    item.PriceListID = itemMaster.PriceListID;
                    item.PrintToID = itemMaster.PrintToID;
                    item.Process = itemMaster.Process;
                    item.Purchase = itemMaster.Purchase;
                    item.PurchaseUomID = itemMaster.PurchaseUomID;
                    item.Sale = itemMaster.Sale;
                    item.SaleUomID = itemMaster.SaleUomID;
                    item.StockCommit = itemMaster.StockCommit;
                    item.StockIn = itemMaster.StockIn;
                    item.StockOnHand = itemMaster.StockOnHand;
                    item.Type = itemMaster.Type;
                    item.UnitofMeasureInv = itemMaster.UnitofMeasureInv;
                    item.UnitofMeasurePur = itemMaster.UnitofMeasurePur;
                    item.UnitofMeasureSale = itemMaster.UnitofMeasureSale;
                    item.UnitPrice = itemMaster.UnitPrice;
                    item.WarehouseID = itemMaster.WarehouseID;
                    item.SourceID = itemMaster.SourceID;
                    item.ManufacturerID = itemMaster.ManufacturerID;

                    var newcode= itemMaster.Code.Split('-').Last();
                    if (itemMaster.GroupCode =="0")
                    {
                        item.Code = item.GroupCode + "-" + newcode;
                    }
                    else
                    {
                        item.Code = itemMaster.GroupCode + "-" + newcode;
                        item.GroupCode = itemMaster.GroupCode;
                    }
                    _context.ItemMasterDatas.Update(item); 
                }
                else
                {
                    var item = _context.ItemMasterDatas.Find(itemMaster.ID);
                    var newcode = itemMaster.Code.Split('-').Last();
                    if (itemMaster.GroupCode == "0")
                    {
                        item.Code = item.GroupCode + "-" + newcode;
                    }
                    else
                    {
                        item.Code = itemMaster.GroupCode + "-" + newcode;
                        item.GroupCode = itemMaster.GroupCode;
                    }
                    _context.ItemMasterDatas.Update(itemMaster);
                }
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<int> AddPricelistDetail(PriceListDetail priceListDetail)
        {
            await _context.PriceListDetails.AddAsync(priceListDetail);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> AddWarehouseDeatail(WarehouseDetail warehouseDetail)
        {
            await _context.WarehouseDetails.AddAsync(warehouseDetail);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> AddWarehouseSummary(WarehouseSummary warehouseSummarry)
        {
            await _context.WarehouseSummary.AddAsync(warehouseSummarry);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> DeleteItemMaster(int id)
        {
            var item = await _context.ItemMasterDatas.FirstAsync(i => i.ID == id);
            item.Delete = true;
            _context.ItemMasterDatas.Update(item);
            return await _context.SaveChangesAsync();
        }

        public ItemMasterData GetbyId(int id)=> _context.ItemMasterDatas.Find(id);

        public IEnumerable<ItemMasterData> GetMasterDatas()
        {
            IEnumerable<ItemMasterData> list = (
                                               from item in _context.ItemMasterDatas.Where(d => d.Delete == false)
                                               join pl in _context.PriceLists.Where(d => d.Delete == false) on item.PriceListID equals pl.ID
                                               join item1 in _context.ItemGroup1.Where(d => d.Delete == false) on item.ItemGroup1ID equals item1.ItemG1ID
                                               join item2 in _context.ItemGroup2.Where(d => d.Delete == false) on item.ItemGroup2ID equals item2.ItemG2ID into item_item2
                                               from item_2 in item_item2.DefaultIfEmpty()
                                               join item3 in _context.ItemGroup3.Where(d => d.Delete == false) on item.ItemGroup3ID equals item3.ID into item_item3
                                               from item_3 in item_item3.DefaultIfEmpty()
                                               join print in _context.PrinterNames.Where(d => d.Delete == false) on item.PrintToID equals print.ID
                                               join uom in _context.UnitofMeasures.Where(d => d.Delete == false) on item.InventoryUoMID equals uom.ID
                                               join sou in _context.Sources.Where(x=>x.Delete==false) on item.SourceID equals sou.SouID into source
                                               from sou_id in source.DefaultIfEmpty()
                                               join manu in _context.Manufacturers.Where(x => x.Delete == false) on item.ManufacturerID equals manu.ManuID into manufacturce
                                               from manu_id in manufacturce.DefaultIfEmpty()
                                               where item.Delete == false
                                              select new ItemMasterData
                                              {
                                                  ID = item.ID,
                                                  Code = item.Code,
                                                  KhmerName = item.KhmerName,
                                                  EnglishName = item.EnglishName,
                                                  StockIn = item.StockIn,
                                                  Cost = item.Cost,
                                                  UnitPrice = item.UnitPrice,
                                                  Barcode = item.Barcode,
                                                  Type = item.Type,
                                                  Description = item.Description,
                                                  Image = item.Image,
                                                  ItemGroup1ID = item.ItemGroup1ID,
                                                  ItemGroup2ID = item.ItemGroup2ID,
                                                  ItemGroup3ID = item.ItemGroup3ID,
                                                  PriceListID = item.PriceListID,
                                                  PrintToID = item.PrintToID,
                                                  Process = item.Process,
                                                  SourceID=item.SourceID,
                                                  ManufacturerID=item.ManufacturerID,
                                                  UnitofMeasureInv = new UnitofMeasure
                                                  {
                                                      ID = uom.ID,
                                                      Name = uom.Name

                                                  },
                                                  PriceList = new PriceLists
                                                  {
                                                      ID = pl.ID,
                                                      Name = pl.Name
                                                  },
                                                  ItemGroup1 = new ItemGroup1
                                                  {
                                                      ItemG1ID = item1.ItemG1ID,
                                                      Name = item1.Name
                                                  },
                                                  ItemGroup2 = new ItemGroup2
                                                  {
                                                      Name = item_2.Name == null ? "None" : item_2.Name
                                                  },
                                                  ItemGroup3 = new ItemGroup3
                                                  {
                                                      Name = item_3.Name == null ? "None" : item_3.Name
                                                  },
                                                  PrinterName = new PrinterName
                                                  {
                                                      ID = print.ID,
                                                      Name = print.Name
                                                  },
                                                  Manufacturer=new Manufacturer
                                                  {
                                                      ManuName=manu_id.ManuName==null ? "No manufacturce" :manu_id.ManuName
                                                  },
                                                  Source=new Source
                                                  {
                                                      SouName=sou_id.SouName==null ? "No source" :sou_id.SouName 
                                                  }
                                                  

                                              }
                );
            return list;
        }

        public IEnumerable<ItemMasterData> GetMasterDatasByCategory(int ID)
        {
            IEnumerable<ItemMasterData> list = (
                                              from item in _context.ItemMasterDatas.Where(d => d.Delete == false)
                                              join pl in _context.PriceLists.Where(d => d.Delete == false) on item.PriceListID equals pl.ID
                                              join item1 in _context.ItemGroup1.Where(d => d.Delete == false) on item.ItemGroup1ID equals item1.ItemG1ID
                                              join item2 in _context.ItemGroup2.Where(d => d.Delete == false) on item.ItemGroup2ID equals item2.ItemG2ID into item_item2
                                              from item_2 in item_item2.DefaultIfEmpty()
                                              join item3 in _context.ItemGroup3.Where(d => d.Delete == false) on item.ItemGroup3ID equals item3.ID into item_item3
                                              from item_3 in item_item3.DefaultIfEmpty()
                                              join print in _context.PrinterNames.Where(d => d.Delete == false) on item.PrintToID equals print.ID
                                              join uom in _context.UnitofMeasures.Where(d => d.Delete == false) on item.InventoryUoMID equals uom.ID
                                              join sou in _context.Sources.Where(x => x.Delete == false) on item.SourceID equals sou.SouID into source
                                              from sou_id in source.DefaultIfEmpty()
                                              join manu in _context.Manufacturers.Where(x => x.Delete == false) on item.ManufacturerID equals manu.ManuID into manufacturce
                                              from manu_id in manufacturce.DefaultIfEmpty()
                                              where item.Delete == false && item1.ItemG1ID==ID
                                             select new ItemMasterData
                                             {
                                                 ID = item.ID,
                                                 Code = item.Code,
                                                 KhmerName = item.KhmerName,
                                                 EnglishName = item.EnglishName,
                                                 StockIn = item.StockIn,
                                                 Cost = item.Cost,
                                                 UnitPrice = item.UnitPrice,
                                                 Barcode = item.Barcode,
                                                 Type = item.Type,
                                                 Description = item.Description,
                                                 Image = item.Image,
                                                 ItemGroup1ID = item.ItemGroup1ID,
                                                 ItemGroup2ID = item.ItemGroup2ID,
                                                 ItemGroup3ID = item.ItemGroup3ID,
                                                 PriceListID = item.PriceListID,
                                                 PrintToID = item.PrintToID,
                                                 Process = item.Process,
                                                 SourceID = item.SourceID,
                                                 ManufacturerID = item.ManufacturerID,
                                                 UnitofMeasureInv = new UnitofMeasure
                                                 {
                                                     ID = uom.ID,
                                                     Name = uom.Name

                                                 },
                                                 PriceList = new PriceLists
                                                 {
                                                     ID = print.ID,
                                                     Name = print.Name
                                                 },
                                                 ItemGroup1 = new ItemGroup1
                                                 {
                                                     ItemG1ID = item1.ItemG1ID,
                                                     Name = item1.Name
                                                 },
                                                 ItemGroup2 = new ItemGroup2
                                                 {
                                                     Name = item_2.Name == null ? "None" : item_2.Name
                                                 },
                                                 ItemGroup3 = new ItemGroup3
                                                 {
                                                     Name = item_3.Name == null ? "None" : item_3.Name
                                                 },
                                                 PrinterName = new PrinterName
                                                 {
                                                     ID = print.ID,
                                                     Name = print.Name
                                                 },
                                                 Manufacturer = new Manufacturer
                                                 {
                                                     ManuName = manu_id.ManuName == null ? "No manufacturce" : manu_id.ManuName
                                                 },
                                                 Source = new Source
                                                 {
                                                     SouName = sou_id.SouName == null ? "No source" : sou_id.SouName
                                                 }


                                             }
               );
            return list;
        }

        public void RemoveItmeInWarehous(int ItemID)
        {
            _context.Database.ExecuteSqlCommand("sp_RemoveItemInWarehous @ItemID={0}",
                parameters: new[] {
                    ItemID.ToString()
                });
        }
    }
}

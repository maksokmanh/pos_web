﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
   public interface ITable
    {
        IQueryable<Table> GetTables();
        Table GetById(int id);
        Task<int> AddOrEdit(Table table);
        Task<int> DeleteTable(int id);
    }
    public class TableRepository : ITable
    {
        private readonly DataContext _context;
        public TableRepository(DataContext dataContext)
        {
            _context = dataContext;
        }

        public async Task<int> AddOrEdit(Table table)
        {
            if (table.ID == 0)
            {
                table.Time = "00:00:00";
                await _context.Tables.AddAsync(table);
            }
            else
            {
                if (table.Image == null)
                {
                    var tables = _context.Tables.Find(table.ID);
                    tables.GroupTableID = table.GroupTableID;
                    tables.Image = tables.Image;
                    tables.Name = table.Name;
                    tables.Status = table.Status;
                    tables.Delete = table.Delete;
                    tables.Time = table.Time;
                    _context.Tables.Update(tables);
                }
                else
                {
                    _context.Tables.Update(table);
                }
               
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteTable(int id)
        {
            var tab = await _context.Tables.FirstAsync(t => t.ID ==id);
            tab.Delete = true;
            _context.Tables.Update(tab);
            return await _context.SaveChangesAsync();
        }

        public Table GetById(int id) => _context.Tables.Find(id);
       
        public IQueryable<Table> GetTables()
        {
            IQueryable<Table> list = (from tab in _context.Tables.Where(t => t.Delete == false)
                                      join

                                        grou in _context.GroupTables.Where(g => g.Delete == false)
                                        on tab.GroupTableID equals grou.ID
                                      select new Table
                                      {
                                          ID = tab.ID,
                                          Name = tab.Name,
                                          GroupTableID = tab.GroupTableID,
                                          Image = tab.Image,
                                          Status = tab.Status,
                                          Time = tab.Time,
                                          GroupTable = new GroupTable
                                          {
                                              Name = grou.Name
                                          }
                                      }
                                    );
            return list;
        } 

       
    }
}

﻿
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.SeverClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IExchangeRate
    {
        IEnumerable<ExchangeRate> GetListCurrency();
        IEnumerable<Currency> GetBaseCurrencyName();
        void UpdateExchangeRate(ExchangeRateService data);
    }
    public class ExcahageRateRepository : IExchangeRate
    {
        private readonly DataContext _Cotext;
        public ExcahageRateRepository(DataContext dataContext)
        {
            _Cotext = dataContext;
        }
        public IEnumerable<ExchangeRate> GetListCurrency()
        {
            IEnumerable<ExchangeRate> exchanges = from exchange in _Cotext.ExchangeRates.Where(d => d.Delete == false)
                                                  join currency in _Cotext.Currency.Where(d => d.Delete == false)
                                                  on exchange.CurrencyID equals currency.ID
                                                  where currency.Delete==false
                                                  select new ExchangeRate
                                                  {
                                                      ID = exchange.ID,
                                                      Rate = exchange.Rate,
                                                      RateOut=exchange.RateOut,
                                                      CurrencyID=exchange.CurrencyID,
                                                      Currency = new Currency
                                                      {
                                                          Description = currency.Description,
                                                          Symbol = currency.Symbol
                                         
                                                      }
                                                  };
            return exchanges;
        }
        public IEnumerable<Currency> GetBaseCurrencyName()
        {
            IEnumerable<Currency> name = from company in _Cotext.Company.Where(d => d.Delete == false)
                       join pricelist in _Cotext.PriceLists.Where(d => d.Delete == false) on company.PriceListID equals pricelist.ID
                       join currency in _Cotext.Currency.Where(d => d.Delete == false) on pricelist.CurrencyID equals currency.ID
                       select new Currency
                       {
                           ID = company.ID,
                           Description = currency.Description + " ( " + currency.Symbol + " )"

                       };

            return name;
        }
        public void UpdateExchangeRate(ExchangeRateService data)
        {
            foreach (var item in data.ExchangeRates)
            {
                _Cotext.Update(item);
                _Cotext.SaveChanges();
            }
        }
    }
}

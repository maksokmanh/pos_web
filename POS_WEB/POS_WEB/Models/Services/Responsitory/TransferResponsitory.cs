﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{

    public interface ITransfer
    {
        IEnumerable<Warehouse> GetFromWarehouse(int BranchID);
        IEnumerable<Warehouse>GetToWarehouse { get; }
        IEnumerable<Branch> GetBranches { get; }
        IEnumerable<Warehouse> GetWarehouse_filter_Branch(int BranchID);
        IEnumerable<TransferDetail> GetItemMasterBy_Warehouse(int warehouseID);
        IEnumerable<TransferDetail> GetItemFindBarcode(int warehouseID,string Barcode);
        void SaveTrasfers(int TransferID);

    }
    public class TransferResponsitory:ITransfer
    {
        private readonly DataContext _context;
        public TransferResponsitory(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<Warehouse> GetFromWarehouse(int BranchID) => _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID);

        public IEnumerable<Warehouse> GetWarehouse_filter_Branch(int BranchID) => _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID);

        public IEnumerable<TransferDetail> GetItemMasterBy_Warehouse(int warehouseID) => _context.TransferDetails.FromSql("sp_Getitem_by_transfer @WarehouseID={0}",
            parameters: new[] {
                warehouseID.ToString()
            });

        public void SaveTrasfers(int TransferID) => _context.Database.ExecuteSqlCommand("sp_TransferStock @TransferID={0}",
            parameters: new[] {
                TransferID.ToString()
            });

        public IEnumerable<TransferDetail> GetItemFindBarcode(int warehouseID, string Barcode) => _context.TransferDetails.FromSql("sp_FindBarcodeTransfer @WarehouseID={0},@Barcode={1}",
            parameters:new[] {
                warehouseID.ToString(),
                Barcode.ToString()
            });
       

        public IEnumerable<Warehouse> GetToWarehouse => _context.Warehouses.Where(x => x.Delete == false);

        public IEnumerable<Branch> GetBranches => _context.Branches.Where(x => x.Delete == false);
    }
}

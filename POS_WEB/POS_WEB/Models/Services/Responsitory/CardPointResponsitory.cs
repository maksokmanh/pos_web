﻿using POS_WEB.AppContext;
using POS_WEB.Models.Services.Promotions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface ICardPoint
    {
        IEnumerable<Point> GetPoints { get; }
        Task AddorEdit(PointCard pointCard);
        IEnumerable<PointCard> GetPointCards();
        Task DeleteCardPoint(int ID);
    }
    public class CardPointResponsitory:ICardPoint
    {
        private readonly DataContext _context;
        public CardPointResponsitory(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<Point> GetPoints => _context.Points.Where(x => x.Delete == false).ToList();

        public async Task AddorEdit(PointCard pointCard)
        {
            if (pointCard.ID == 0)
            {
                _context.PointCards.Add(pointCard);
            }
            else
            {
                _context.PointCards.Update(pointCard);
            }
           await _context.SaveChangesAsync();
        }

        public async Task DeleteCardPoint(int ID)
        {
            var cp = _context.PointCards.FirstOrDefault(x => x.ID == ID);
            if (cp != null)
            {
                cp.Delete = true;
                _context.PointCards.Update(cp);
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<PointCard> GetPointCards()
        {
            IEnumerable<PointCard> list = (
                 from cp in _context.PointCards.Where(x => x.Delete == false)
                 join p in _context.Points.Where(x => x.Delete == false) on
                 cp.PointID equals p.ID
                 where cp.Delete == false
                 select new PointCard
                 {
                     ID = cp.ID,
                     PointID = cp.PointID,
                     Ref_No = cp.Ref_No,
                     Name = cp.Name,
                     Point = cp.Point,
                     Remain = cp.Remain,
                     Approve = cp.Approve,
                     DataApprove = cp.DataApprove,
                     DateCreate = cp.DateCreate,
                     Description = cp.Description,
                     ExpireDate = cp.ExpireDate,
                     Points = new Point
                     {
                         Amount = p.Amount

                     }
                 }
                 ).ToList();
            return list;
        }
    }
}

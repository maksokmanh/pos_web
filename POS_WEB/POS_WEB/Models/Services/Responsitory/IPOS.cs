﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Controllers.Event;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Tables;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.ServicesClass;
using POS_WEB.Models.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IPOS
    {
        //Get groups
        IEnumerable<ItemGroup3> GetGroups();
        IEnumerable<ItemGroup1> GetGroup1s { get; }
        IEnumerable<ItemGroup2> GetGroup2s { get; }
       
        IEnumerable<ItemGroup3> GetGroup3s { get; }
        //Filter Group

        IEnumerable<ItemGroup2> FilterGroup2(int group1_id);
        IEnumerable<ItemGroup3> FilterGroup3(int group1_id,int group2_id);
        
        IEnumerable<ServiceItemSales> GetItemMasterDatas(int priceList_id);
        IEnumerable<ServiceItemSales> GetItemMasterByBarcode(int pricelist, string barcode);
        IEnumerable<ServiceItemSales> FilterItem(int pricelist_id,int itemid);
        IEnumerable<ServiceItemSales> FilterItemByGroup(int pricelist_id);
        IEnumerable<GeneralSetting> GetSetting(int branchid);
        IEnumerable<Order> GetOrder(int tableid, int orderid,int userid);
        IEnumerable<POS.Receipt> GetReceiptReprint(int branchid,string date_from,string date_to);
        IEnumerable<ItemsReturn> SendOrder(Order order,string print_type);
        IEnumerable<OpenShift> OpenShiftData(int userid, double cash);
        IEnumerable<OpenShift> CloseShiftData(int userid, double cashout);
        void SendDataToSecondScreen(Order order);
        void PrintReceiptBill(int orderid, string print_type);
        void PrintReceiptReprint(int orderid, string print_type);
        void MoveTable(int old_id, int new_id);
        void CombineReceipt(CombineReceipt combineReceipt);
        void SendSplit(Order order,Order addnew);
        void ClearUserOrder(int tableid);
        void IntailStatusTable();
        void UpdateTimeOnTable();
        void VoidOrder(int orderid);
        string GetTimeByTable(int TableID);
    }
    public class POSRepository : IPOS
    {
        private readonly DataContext _context;
        private readonly TimeDelivery _timeDelivery;
        public POSRepository(DataContext context, IHubContext<SignalRClient> hubcontext)
        {
            _context = context;
            _timeDelivery = TimeDelivery.GetInstance(hubcontext);
            _timeDelivery.StartTimer();
        }
        public IEnumerable<ItemGroup3> GetGroups() {
            IEnumerable<ItemGroup3> list = from group1 in _context.ItemGroup1.Where(w => w.Delete == false)
                                           join group2 in _context.ItemGroup2.Where(w => w.Delete == false) on group1.ItemG1ID equals group2.ItemG1ID
                                           join group3 in _context.ItemGroup3.Where(w => w.Delete == false) on group2.ItemG2ID equals group3.ItemG2ID

                                           select new ItemGroup3
                                           {
                                               ID = group3.ID,
                                               Name = group3.Name,
                                               Images = group3.Images,
                                               ItemGroup1 = new ItemGroup1
                                               {
                                                   ItemG1ID = group1.ItemG1ID,
                                                   Name = group1.Name,
                                                   Images = group1.Images
                                               },
                                               ItemGroup2 = new ItemGroup2
                                               {
                                                   ItemG2ID = group2.ItemG2ID,
                                                   Name = group2.Name,
                                                   Images = group2.Images
                                               }

                                           };
            return list;
        }
        public IEnumerable<ItemGroup1> GetGroup1s => _context.ItemGroup1.Where(w => w.Delete == false).ToList();
        public IEnumerable<ItemGroup2> GetGroup2s => _context.ItemGroup2.Where(w => w.Delete == false).ToList();

        public IEnumerable<ItemGroup3> GetGroup3s => _context.ItemGroup3.Where(w => w.Delete == false).ToList();

       

        public IEnumerable<ItemGroup2> FilterGroup2(int group1_id)
        {
           var group2 =_context.ItemGroup2.Where(w => w.Delete == false && w.ItemG1ID == group1_id && w.Name!="None").ToList();
           return group2;

        }

        public IEnumerable<ItemGroup3> FilterGroup3(int group1_id,int group2_id)
        {
            var group3 = _context.ItemGroup3.Where(w => w.Delete == false && w.ItemG1ID==group1_id && w.ItemG2ID == group2_id && w.Name!= "None").ToList();
            return group3;

        }
        public IEnumerable<ServiceItemSales> FilterItem(int pricelist_id,int itemid)
        {
            var item = _context.ServiceItemSales.FromSql("pos_FilterItem @PricelistID={0},@ItemID={1}",
                parameters: new[] {
                    pricelist_id.ToString(),
                    itemid.ToString()
                });
            return item.ToList();
        }
        public IEnumerable<ServiceItemSales> GetItemMasterDatas(int priceList_id)
        {
            var item = _context.ServiceItemSales.FromSql("pos_GetItemForSale @PricelistID={0}",
                parameters: new[] {
                    priceList_id.ToString()
                });
            return item;
        }
        public IEnumerable<ServiceItemSales> GetItemMasterByBarcode(int pricelist, string barcode)
        {
            try
            {
                var item = _context.ServiceItemSales.FromSql("pos_FilterItemByBarcode @PricelistID={0}, @Barcode={1}",
                parameters: new[] {
                    pricelist.ToString(),
                    barcode.ToString(),

                });
                return item;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public IEnumerable<GeneralSetting> GetSetting(int branchid)
        {
            var setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);
            return setting;
        }
        public void SendSplit(Order data,Order addnew)//error not check
        {
            //update or remove old item
            foreach (var item in data.OrderDetail.ToList())
            {
                if(item.Qty<=0)
                {
                    _context.OrderDetail.Remove(item);
                    data.OrderDetail.Remove(item);
                    _context.SaveChanges();
                }
            }
            _context.Order.Update(data);
            _context.SaveChanges();
            var remove = _context.OrderDetail.Where(w => w.OrderID == data.OrderID);
            if(remove.Count()<=0)
            {
                var master = _context.Order.FirstOrDefault(w => w.OrderID==data.OrderID);
                _context.Order.Remove(master);
                _context.SaveChanges();
            }
           
            addnew.OrderDetail.RemoveAll(w => w.PrintQty <= 0);
            AddNewSplitOrder(addnew);
            
        }
        public void AddNewSplitOrder(Order order)
        {
            var setting = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == order.BranchID);
            var queues = _context.Order_Queue.Where(w => w.BranchID == order.BranchID);
            var receipt = _context.Order_Receipt.Where(w => w.BranchID == order.BranchID);

            if (queues.Count() >= setting.QueueCount)
            {
                foreach (var queue in queues.ToList())
                {
                    _context.Remove(queue);
                    _context.SaveChanges();
                }
            }
            Order orderNew = new Order();
            orderNew.OrderNo = "Order-" + (queues.Count() + 1);
            orderNew.TableID = order.TableID;
            orderNew.ReceiptNo = order.ReceiptNo;
            orderNew.QueueNo = (queues.Count() + 1).ToString();
            orderNew.DateIn = order.DateIn;
            orderNew.TimeIn = order.TimeIn;
            orderNew.DateOut = Convert.ToDateTime(DateTime.Today);
            orderNew.TimeOut = DateTime.Now.ToString("hh:mm:ss tt");
            orderNew.WaiterID = order.WaiterID;
            orderNew.UserOrderID = order.UserDiscountID;
            orderNew.CustomerID = order.CustomerID;
            orderNew.PriceListID = order.PriceListID;
            orderNew.LocalCurrencyID = order.LocalCurrencyID;
            orderNew.SysCurrencyID = order.SysCurrencyID;
            orderNew.ExchangeRate = order.ExchangeRate;
            orderNew.WarehouseID = order.WarehouseID;
            orderNew.BranchID = order.BranchID;
            orderNew.CompanyID = order.CompanyID;
            orderNew.Sub_Total = 0;
            orderNew.DiscountRate = order.DiscountRate;
            orderNew.DiscountValue = 0;
            orderNew.TypeDis = order.TypeDis;
            orderNew.TaxRate = 0;
            orderNew.TaxValue = 0;
            orderNew.GrandTotal = 0;
            orderNew.GrandTotal_Sys = 0;
            orderNew.Tip = 0;
            orderNew.Received = 0;
            orderNew.Change = 0;
            orderNew.PaymentMeansID = order.PaymentMeansID;
            orderNew.CheckBill = 'N';
            orderNew.Cancel = false;
            orderNew.Delete = false;
            _context.Order.Add(orderNew);
            _context.SaveChanges();
            int orderid_new = orderNew.OrderID;
            //Add new order queue
            AddQueueOrder(orderNew.BranchID,orderNew.OrderNo);
            //Detail
            double SubTotal = 0;
            foreach (var item in order.OrderDetail.ToList())
            {
                OrderDetail detail = new OrderDetail();
                detail.OrderID = orderid_new;
                detail.Line_ID = item.Line_ID;
                detail.ItemID = item.ItemID;
                detail.Code = item.Code;
                detail.KhmerName = item.KhmerName;
                detail.EnglishName = item.EnglishName;
                detail.Qty = item.PrintQty;
                detail.PrintQty = 0;
                detail.UnitPrice = item.UnitPrice;
                detail.Cost = item.Cost;
                detail.DiscountRate = item.DiscountRate;
                detail.TypeDis = item.TypeDis;
                if (item.TypeDis == "Percent")
                {
                    detail.Total = (detail.Qty * detail.UnitPrice) * (1 - (detail.DiscountRate / 100));
                    detail.DiscountValue = (detail.Qty * item.UnitPrice) * detail.DiscountRate / 100;
                   
                }
                else
                {
                    detail.Total = (detail.Qty * detail.UnitPrice) - detail.DiscountRate;
                    detail.DiscountValue = detail.DiscountRate;
                }
                detail.Total_Sys = detail.Total * orderNew.ExchangeRate;
                detail.UomID = item.UomID;
                detail.ItemStatus = item.ItemStatus;
                detail.ItemPrintTo = item.ItemPrintTo;
                detail.Currency = item.Currency;
                SubTotal = SubTotal + detail.Total;
                _context.Add(detail);
                _context.SaveChanges();
            }
            //Update summary
            var order_master = _context.Order.Find(orderid_new);
            order_master.Sub_Total = SubTotal;
            if (order_master.TypeDis == "Percent")
            {
                order_master.DiscountValue = SubTotal * orderNew.DiscountRate / 100;
            }
            else
            {
                order_master.DiscountValue = orderNew.DiscountRate;
            }

            var vat = (order_master.TaxRate + 100 / 100);
            var rate = order_master.TaxRate / 100;
            order_master.TaxValue = (SubTotal / vat) * rate;
            order_master.GrandTotal = SubTotal - order_master.DiscountValue;
            order_master.GrandTotal_Sys = order_master.GrandTotal * order_master.ExchangeRate;

            _context.Update(order_master);
            _context.SaveChanges();

        }
        public void AddQueueOrder(int branchid,string orderno)
        {
            Order_Queue queue = new Order_Queue
            {
                BranchID = branchid,
                OrderNo = orderno,
                DateTime = Convert.ToDateTime(DateTime.Now.ToString())
            };
            _context.Add(queue);
            _context.SaveChanges();

        }
        public void SendToPrintOrder(Order order)
        {
            var Table = _context.Tables.Find(order.TableID);
            var User = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(user=>user.ID==order.UserOrderID);
            var PrinterNames = _context.PrinterNames.Where(w => w.Delete == false).ToList();
            var Setting = _context.GeneralSetting.Where(w => w.BranchID == order.BranchID).ToList();
            List<PrintOrder> items = new List<PrintOrder>();

            
            foreach (var item in order.OrderDetail.ToList())
            {
                if (item.Qty <= 0)
                {
                    var remove = _context.OrderDetail.FirstOrDefault(w => w.Line_ID == item.Line_ID && w.OrderID == order.OrderID);
                    if (remove != null)
                    {
                        _context.Remove(remove);
                        _context.SaveChanges();
                    }
                    if (item.ItemType == "Service")
                    {
                        var time_continue = _timeDelivery.GetTimeTable(order.TableID);
                        var time = _timeDelivery.StartTimeTable(order.TableID, time_continue,'B');
                        var table_up = _context.Tables.Find(order.TableID);
                        table_up.Status = 'B';
                        table_up.Time = time_continue;
                        _context.Update(table_up);
                        _context.SaveChanges();
                    }
                }
                if (item.ItemStatus=="old")
                {
                    if (item.PrintQty!=0)
                    {
                        if (item.Comment==null || item.Comment=="")
                        {
                            PrintOrder data = new PrintOrder
                            {
                                Table = Table.Name,
                                Cashier = User.Employee.Name,
                                OrderNo = order.OrderNo,
                                Item =item.KhmerName,
                                PrintQty = item.PrintQty.ToString(),
                                ItemPrintTo = item.ItemPrintTo
                               
                            };
                            items.Add(data);
                        }
                        else
                        {
                            PrintOrder data = new PrintOrder
                            {
                                Table = Table.Name,
                                Cashier = User.Employee.Name,
                                OrderNo = order.OrderNo,
                                Item =item.KhmerName + " (" + item.Comment + ")",
                                PrintQty = item.PrintQty.ToString(),
                                ItemPrintTo = item.ItemPrintTo
                            };
                            items.Add(data);
                        }
                    }
                }
                else
                {
                    if (item.Comment == null || item.Comment == "")
                    {
                        PrintOrder data = new PrintOrder
                        {
                            Table = Table.Name,
                            Cashier = User.Employee.Name,
                            OrderNo = order.OrderNo,
                            Item =item.KhmerName,
                            PrintQty = item.PrintQty.ToString(),
                            ItemPrintTo = item.ItemPrintTo
                          
                        };
                        items.Add(data);
                    }
                    else
                    {
                        PrintOrder data = new PrintOrder
                        {
                            Table = Table.Name,
                            Cashier = User.Employee.Name,
                            OrderNo = order.OrderNo,
                            Item = item.KhmerName + " (" + item.Comment + ")",
                            PrintQty = item.PrintQty.ToString(),
                            ItemPrintTo = item.ItemPrintTo
                          
                        };
                        items.Add(data);
                    }
                }
                
            };
            if(items.Count()>0)
            {
                _timeDelivery.PrintOrder(items, PrinterNames, Setting);
                
            }
           
        }
        public void IssuseCommittedOrder(int orderid)
        {
            _context.Database.ExecuteSqlCommand("pos_OrderDetailCommittedStock @OrderID={0}",
               parameters: new[] {
                    orderid.ToString()
               });
        }
        public void GoodReceiptCommittedOrder(int orderid)
        {
            _context.Database.ExecuteSqlCommand("pos_OrderDetailCommittedStockGoodReceipt @OrderID={0}",
               parameters: new[] {
                    orderid.ToString()
               });
        }
        public void IssuseInStockOrder(int orderid)
        {
            _context.Database.ExecuteSqlCommand("pos_OrderDetailIssuseStock @OrderID={0}",
               parameters: new[] {
                    orderid.ToString()
               });
        }
        public void SendDataToSecondScreen(Order order)
        {
            _timeDelivery.PushDataOrder(order);
        }

        //private void PushOrder(int TableId)
        //{
        //    var order = _context.Order.Where(w => w.TableID == TableId).ToList();
        //    //var user = _context.UserAccounts.Include(emp => emp.Employee.Name).FirstOrDefault(w => w.ID == orders.FirstOrDefault().UserOrderID);
        //    _timeDelivery.PushOrderByTable(order);
        //}

        public void StartTime(int TableId,string Time)
        {
            if(TableId!=0)
            {
                Table table = _context.Tables.FirstOrDefault(w => w.ID == TableId);

                if (table.Status == 'A')
                {
                    _timeDelivery.StartTimeTable(table.ID, Time, 'B');
                    table.Status = 'B';
                    _context.Update(table);
                    _context.SaveChanges();
                }
                else
                {
                    Time=_timeDelivery.StopTimeTable(TableId,'A');
                    _timeDelivery.StartTimeTable(TableId,Time,'B');
                    table.Status = 'B';
                    _context.Update(table);
                    _context.SaveChanges();
                }

            }
          
        }
       
        public IEnumerable<Order> GetOrder(int tableid, int orderid,int userid)
        {
            var orders = _context.Order.Where(w => w.TableID == tableid && w.Cancel==false).ToList();
            var user = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == userid);
            if (orderid == 0)
            {
                var Count = _context.Order.Where(w => w.TableID == tableid && w.Cancel==false).Count();
                if (Count > 0)
                {
                    var MinOrderId = _context.Order.Where(w => w.TableID == tableid && w.Cancel==false).Max(min => min.OrderID);
                    var order = _context.Order.Include(x => x.OrderDetail).Include(x=>x.Currency).Where(w => w.TableID == tableid && w.OrderID == MinOrderId && w.Cancel==false).ToList();

                    _timeDelivery.PushOrderByTable(orders,tableid,user.Employee.Name);
                    return order;
                }
                else
                {
                    var order = _context.Order.Include(x => x.OrderDetail).Include(x => x.Currency).Where(w => w.TableID == tableid && w.Cancel==false ).ToList();
                   
                    _timeDelivery.PushOrderByTable(orders,tableid,user.Employee.Name);
                    return order;
                }

            }
            else
            {
                var order = _context.Order.Include(x => x.OrderDetail).Include(x => x.Currency).Where(w => w.TableID == tableid && w.OrderID == orderid && w.Cancel==false).ToList();
               
                _timeDelivery.PushOrderByTable(orders,tableid,user.Employee.Name);
                return order;
            }

        }
     
        public void PrintReceiptReprint(int orderid,string print_type)
        {
            if(orderid>0)
            {
                var receipt_reprint = _context.ReceiptDetail.Include(rec => rec.Receipt).Where(w => w.OrderID == orderid);
                var order = receipt_reprint.First().Receipt;
                //Pirint bill or tender
                var table = _context.Tables.Find(order.TableID);
                var user = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == order.UserOrderID);

                var customer = _context.BusinessPartners.FirstOrDefault(w => w.Delete == false && w.Type == "Customer" && w.ID == order.CustomerID);
                var setting = _context.GeneralSetting.Where(w => w.BranchID == order.BranchID).ToList();
                var machine = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                var printer = _context.PrinterNames.FirstOrDefault(w => w.MachineName == machine && w.Delete==false);
                var Printer = "";
                if (printer != null)
                {
                    Printer = printer.Name;
                }
                var syscurrency = _context.Currency.FirstOrDefault(w => w.ID == order.SysCurrencyID);
                var banch = _context.Branches.FirstOrDefault(w => w.Delete == false && w.ID == order.BranchID);
                var receipt = _context.ReceiptInformation.FirstOrDefault(w => w.BranchID == order.BranchID);
                //var exchange = _context.ExchangeRates.Include(curr => curr.Currency).Where(w => w.Currency.Description == "KHR").ToList();
                List<PrintBill> PrintBill = new List<PrintBill>();
                var ReceiptNo = "";
                var Received = "";
                var Change = "";
                var ChangeSys = "";
                foreach (var item in receipt_reprint)
                {
                    //Discount
                    if (item.TypeDis == "Cash")
                    {
                        order.DiscountRate = order.DiscountValue;
                    }
                    //Vat
                    if (setting[0].VatAble == true)
                    {
                        ReceiptNo = "INVOICE #" + order.ReceiptNo + " " + setting[0].VatNum;
                    }
                    else
                    {
                        ReceiptNo = "INVOICE #" + order.ReceiptNo;
                    }
                    if (item.Currency == "KHR" || item.Currency == "៛")
                    {
                        Received = item.Currency + " " + string.Format("{0:n0}", order.Received);
                        Change = item.Currency + " " + string.Format("{0:n0}", order.Change);
                        ChangeSys = item.Currency + " " + string.Format("{0:n0}", order.Change);
                    }
                    else
                    {
                        Received = item.Currency + " " + string.Format("{0:n2}", order.Received);
                        Change = item.Currency + " " + string.Format("{0:n2}", order.Change);
                        ChangeSys = item.Currency + " " + string.Format("{0:n2}", order.Change);
                    }
                    if (item.Currency == "KHR" || item.Currency == "៛")
                    {

                        PrintBill bill = new PrintBill
                        {
                            OrderID = item.OrderID,
                            ReceiptNo = ReceiptNo,
                            Logo = receipt.Logo,
                            BranchName = receipt.Title,
                            Address = receipt.Address,
                            Tel1 = receipt.Tel1,
                            Tel2 = receipt.Tel2,
                            Table = table.Name,
                            OrderNo = order.OrderNo,
                            Cashier = user.Employee.Name,
                            DateTimeIn = order.DateIn.ToString("dd-MM-yyyy") + " " + order.TimeIn,
                            DateTimeOut = order.DateOut.ToString("dd-MM-yyyy") + " " + order.TimeOut,
                            Item =item.KhmerName,
                            Qty = item.Qty + "",
                            Price = string.Format("{0:n0}", item.UnitPrice),
                            DisItem = item.DiscountRate + "",
                            Amount = string.Format("{0:n0}", item.Total),
                            SubTotal = string.Format("{0:n0}", order.Sub_Total),
                            DisRate = order.DiscountRate + "%",
                            DisValue = string.Format("{0:n0}", order.DiscountValue),
                            TypeDis = order.TypeDis,
                            GrandTotal = item.Currency + " " + string.Format("{0:n0}", order.GrandTotal),
                            GrandTotalSys = syscurrency.Description + " " + string.Format("{0:n0}", order.GrandTotal_Sys),
                            VatRate = order.TaxRate + "%",
                            VatValue = item.Currency + " " + string.Format("{0:n0}", order.TaxValue),
                            Received = Received,
                            Change = Change,
                            ChangeSys = ChangeSys,
                            DescKh = receipt.KhmerDescription,
                            DescEn = receipt.EnglishDescription,
                            ExchangeRate = string.Format("{0:n0}", order.ExchangeRate),
                            Printer = Printer,
                            Print = print_type,
                            ItemDesc = item.Description,
                            CustomerInfo = customer.Code + '/' + customer.Name + '/' + customer.Phone + '/' + customer.Address + '/' + customer.Email,
                            Team=receipt.TeamCondition
                        };
                        //Qty
                        if (item.ItemType == "Service")
                        {
                            var arr_time = item.Qty.ToString().Split('.');
                            bill.Qty = arr_time[0] + "h:" + arr_time[1].PadRight(2, '0') + "m";
                        }
                        PrintBill.Add(bill);
                    }
                    else
                    {
                        PrintBill bill = new PrintBill
                        {
                            OrderID = item.OrderID,
                            ReceiptNo = ReceiptNo,
                            Logo = receipt.Logo,
                            BranchName = receipt.Title,
                            Address = receipt.Address,
                            Tel1 = receipt.Tel1,
                            Tel2 = receipt.Tel2,
                            Table = table.Name,
                            OrderNo = order.OrderNo,
                            Cashier = user.Employee.Name,
                            DateTimeIn = order.DateIn.ToString("dd-MM-yyyy") + " " + order.TimeIn,
                            DateTimeOut = order.DateOut.ToString("dd-MM-yyyy") + " " + order.TimeOut,
                            Item =item.KhmerName,
                            Qty = item.Qty + "",
                            Price = string.Format("{0:n2}", item.UnitPrice),
                            DisItem = item.DiscountRate + "",
                            Amount = string.Format("{0:n2}", item.Total),
                            SubTotal = string.Format("{0:n2}", order.Sub_Total),
                            DisRate = order.DiscountRate + "%",
                            DisValue = string.Format("{0:n2}", order.DiscountValue),
                            TypeDis = order.TypeDis,
                            GrandTotal = item.Currency + " " + string.Format("{0:n2}", order.GrandTotal),
                            GrandTotalSys = syscurrency.Description + " " + string.Format("{0:n0}", order.GrandTotal_Sys),
                            VatRate = order.TaxRate + "%",
                            VatValue = item.Currency + " " + string.Format("{0:n2}", order.TaxValue),
                            Received = Received,
                            Change = Change,
                            ChangeSys = ChangeSys,
                            DescKh = receipt.KhmerDescription,
                            DescEn = receipt.EnglishDescription,
                            ExchangeRate = string.Format("{0:n2}", order.ExchangeRate),
                            Printer = Printer,
                            Print = print_type,
                            ItemDesc = item.Description,
                            CustomerInfo = customer.Code + '/' + customer.Name + '/' + customer.Phone + '/' + customer.Address + '/' + customer.Email,
                            Team = receipt.TeamCondition

                        };
                        //Qty
                        if (item.ItemType == "Service")
                        {
                            var arr_time = item.Qty.ToString().Split('.');
                            bill.Qty = arr_time[0] + "h:" + arr_time[1].PadRight(2, '0') + "m";
                        }
                        PrintBill.Add(bill);
                    }
                }
                _timeDelivery.PrintBill(PrintBill,setting);
            }
            
        }
        public void PrintReceiptBill(int orderid,string print_type)
        {
            //Generate receipt id
            var receipt_generated = "";
            var ordered = _context.Order.Include(x => x.OrderDetail).Include(x => x.Currency).FirstOrDefault(w => w.OrderID == orderid);
            if (ordered == null) { return; }
            if (ordered.CheckBill == 'N')
            {
                var receipts = _context.Order_Receipt.Where(w => w.BranchID == ordered.BranchID);
                receipt_generated = (receipts.Count() + 1).ToString().PadLeft(6, '0');//DateTime.Now.Year + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "" + (receipts.Count() + 1).ToString().PadLeft(6, '0');

                Order_Receipt NewReceipt = new Order_Receipt
                {
                    BranchID = ordered.BranchID,
                    ReceiptID = receipt_generated,
                    DateTime = DateTime.Now

                };
                _context.Order_Receipt.Add(NewReceipt);
                _context.SaveChanges();
            }
            else
            {
                var data = _context.Order.FirstOrDefault(w => w.OrderID == ordered.OrderID);
                receipt_generated = data.ReceiptNo;
            }
            //Update check bill order
            var order = _context.Order.Include(x => x.OrderDetail).Include(x => x.Currency).FirstOrDefault(w=>w.OrderID == orderid);
            order.CheckBill = 'Y';
            order.ReceiptNo = receipt_generated;
            order.DateOut = DateTime.Today;
            order.TimeOut = DateTime.Now.ToString("hh:mm:ss tt");
            _context.Order.Update(order);
            _context.SaveChanges();

            if(order.OrderDetail.Count()>0)
            {
                //Pirint bill or tender
             
                var table = _context.Tables.Find(order.TableID);
                var user = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == order.UserOrderID);

                var setting = _context.GeneralSetting.Where(w => w.BranchID == order.BranchID).ToList();
                var machine = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                var printer = _context.PrinterNames.FirstOrDefault(w => w.MachineName== machine && w.Delete==false);
                var Printer = "";
                if(printer!=null)
                {
                    Printer = printer.Name;
                }
                var customer = _context.BusinessPartners.FirstOrDefault(w => w.Delete == false && w.Type == "Customer" && w.ID==order.CustomerID);
                var syscurrency = _context.Currency.FirstOrDefault(w => w.ID == order.SysCurrencyID);
                var banch = _context.Branches.FirstOrDefault(w => w.Delete == false && w.ID == order.BranchID);
                var receipt = _context.ReceiptInformation.FirstOrDefault(w => w.BranchID == order.BranchID);

                //var exchange = _context.ExchangeRates.Include(curr => curr.Currency).Where(w => w.Currency.Description == "KHR").ToList();
                List<PrintBill> PrintBill = new List<PrintBill>();
                string ReceiptNo = "";
                var Received = "";
                var Change = "";
                var ChangeSys = "";
                foreach (var item in order.OrderDetail)
                {
                    if (item.TypeDis == "Cash")
                    {
                        order.DiscountRate = order.DiscountValue;
                    }
                    if (setting[0].VatAble == true)
                    {
                        ReceiptNo = "INVOICE #" + order.ReceiptNo + " " + setting[0].VatNum;
                    }
                    else
                    {
                        ReceiptNo = "INVOICE #" + order.ReceiptNo;
                    }
                   
                    if (item.Currency == "KHR" || item.Currency == "៛")
                    {
                        Received = item.Currency + " " + string.Format("{0:n0}", order.Received);
                        Change = item.Currency + " " + string.Format("{0:n0}", order.Change);
                        ChangeSys = item.Currency + " " + string.Format("{0:n0}", order.Change);
                    }
                    else
                    {
                        Received = item.Currency + " " + string.Format("{0:n2}", order.Received);
                        Change = item.Currency + " " + string.Format("{0:n2}", order.Change);
                        ChangeSys = item.Currency + " " + string.Format("{0:n2}", order.Change);
                    }
                    if (item.Currency=="KHR" || item.Currency=="៛")
                    {

                        PrintBill bill = new PrintBill
                        {
                            OrderID = item.OrderID,
                            ReceiptNo = ReceiptNo,
                            Logo = receipt.Logo,
                            BranchName = receipt.Title,
                            Address = receipt.Address,
                            Tel1 = receipt.Tel1,
                            Tel2 = receipt.Tel2,
                            Table = table.Name,
                            OrderNo = order.OrderNo,
                            Cashier = user.Employee.Name,
                            DateTimeIn = order.DateIn.ToString("dd-MM-yyyy") + " " + order.TimeIn,
                            DateTimeOut = order.DateOut.ToString("dd-MM-yyyy") + " " + order.TimeOut,
                            Item =item.KhmerName,
                            Qty = item.Qty + "",
                            Price = string.Format("{0:n0}", item.UnitPrice),
                            DisItem = item.DiscountRate + "",
                            Amount = string.Format("{0:n0}", item.Total),
                            SubTotal = string.Format("{0:n0}", order.Sub_Total),
                            DisRate = order.DiscountRate + "%",
                            DisValue = string.Format("{0:n0}", order.DiscountValue),
                            TypeDis = order.TypeDis,
                            GrandTotal = item.Currency + " " + string.Format("{0:n0}", order.GrandTotal),
                            GrandTotalSys = syscurrency.Description + " " + string.Format("{0:n0}", order.GrandTotal_Sys),
                            VatRate = order.TaxRate + "%",
                            VatValue = item.Currency + " " + string.Format("{0:n0}", order.TaxValue),
                            Received=Received,
                            Change = Change,
                            ChangeSys=ChangeSys,
                            DescKh = receipt.KhmerDescription,
                            DescEn = receipt.EnglishDescription,
                            ExchangeRate = string.Format("{0:n0}", order.ExchangeRate),
                            ItemDesc = item.Description,
                            CustomerInfo = customer.Code + '/' + customer.Name + '/' + customer.Phone + '/' + customer.Address + '/' + customer.Email,
                            Team = receipt.TeamCondition
                        };
                        //Qty
                        if (item.ItemType == "Service")
                        {
                            var arr_time = item.Qty.ToString().Split('.');
                            bill.Qty = arr_time[0] + "h:" + arr_time[1].PadRight(2, '0') + "m";
                        }
                        PrintBill.Add(bill);
                    }
                    else
                    {
                        PrintBill bill = new PrintBill
                        {
                            OrderID = item.OrderID,
                            ReceiptNo = ReceiptNo,
                            Logo = receipt.Logo,
                            BranchName = receipt.Title,
                            Address = receipt.Address,
                            Tel1 = receipt.Tel1,
                            Tel2 = receipt.Tel2,
                            Table = table.Name,
                            OrderNo = order.OrderNo,
                            Cashier = user.Employee.Name,
                            DateTimeIn = order.DateIn.ToString("dd-MM-yyyy") + " " + order.TimeIn,
                            DateTimeOut = order.DateOut.ToString("dd-MM-yyyy") + " " + order.TimeOut,
                            Item =item.KhmerName,
                            Qty = item.Qty + "",
                            Price = string.Format("{0:n2}", item.UnitPrice),
                            DisItem = item.DiscountRate + "",
                            Amount = string.Format("{0:n2}", item.Total),
                            SubTotal = string.Format("{0:n2}", order.Sub_Total),
                            DisRate = order.DiscountRate + "%",
                            DisValue = string.Format("{0:n2}", order.DiscountValue),
                            TypeDis = order.TypeDis,
                            GrandTotal = item.Currency + " " + string.Format("{0:n2}", order.GrandTotal),
                            GrandTotalSys = syscurrency.Description + " " + string.Format("{0:n0}", order.GrandTotal_Sys),
                            VatRate = order.TaxRate + "%",
                            VatValue = item.Currency + " " + string.Format("{0:n2}", order.TaxValue),
                            Received = Received,
                            Change = Change,
                            ChangeSys = ChangeSys,
                            DescKh = receipt.KhmerDescription,
                            DescEn = receipt.EnglishDescription,
                            ExchangeRate = string.Format("{0:n2}", order.ExchangeRate),
                            Printer = Printer,
                            Print = print_type,
                            ItemDesc = item.Description,
                            CustomerInfo = customer.Code + '/' + customer.Name + '/' + customer.Phone + '/' + customer.Address + '/' + customer.Email,
                            Team = receipt.TeamCondition

                        };
                        //Qty
                        if (item.ItemType == "Service")
                        {
                            var arr_time = item.Qty.ToString().Split('.');
                            bill.Qty = arr_time[0] + "h:" + arr_time[1].PadRight(2, '0') + "m";
                        }
                        PrintBill.Add(bill);
                    }
                    
                }
                _timeDelivery.PrintBill(PrintBill,setting);

                //Update status table
                if (print_type == "Bill")
                {
                    var table_update = _context.Tables.Find(order.TableID);
                    table_update.Status = 'P';
                    table_update.Time = _timeDelivery.StopTimeTable(order.TableID, 'P');
                    _timeDelivery.ResetTimeTable(order.TableID, 'P',table_update.Time);
                    _context.Tables.Update(table_update);
                    _context.SaveChanges();
                  
                }
                else if (print_type == "Pay")
                {
                    var count_order = _context.Order.Where(w => w.TableID == ordered.TableID && w.Cancel==false);
                    if (count_order.Count() == 1)
                    {
                        var table_update = _context.Tables.Find(order.TableID);
                        table_update.Status = 'A';
                        table_update.Time = "00:00:00";
                        _timeDelivery.StopTimeTable(order.TableID, 'A');
                        _timeDelivery.ResetTimeTable(order.TableID,'A',"00:00:00");
                        _context.Tables.Update(table_update);
                        _context.SaveChanges();
                    }
                    //Issuse In Stock 
                    IssuseInStockOrder(orderid);
                    //Remove order
                    _context.Database.ExecuteSqlCommand("pos_InsertReceipt @OrderID={0}",
                        parameters: new[] {
                    orderid.ToString()
                   });
                    GetOrder(order.TableID, order.OrderID, order.UserOrderID);
                }
               _timeDelivery.ClearUserOrder(order.TableID);
            }
            
        }

        public void MoveTable(int old_Id, int new_Id)
        {
            var time = _timeDelivery.StopTimeTable(old_Id,'A');
            _timeDelivery.ResetTimeTable(old_Id, 'A', "00:00:00");
            _timeDelivery.StartTimeTable(new_Id, time, 'B');
            _timeDelivery.GetTableAvailable("Move");
            var oldtable = _context.Tables.Find(old_Id);
            var newtable = _context.Tables.Find(new_Id);
            var orders = _context.Order.Where(w => w.TableID == old_Id);
            foreach (var order in orders)
            {
                order.TableID = new_Id;
                _context.Update(order);
            }
            oldtable.Status = 'A';
            oldtable.Time = "00:00:00";
            newtable.Status = 'B';
            newtable.Time = time;
            _context.Update(oldtable);
            _context.Update(newtable);
            _context.SaveChanges();
        }
        public void CombineReceipt(CombineReceipt combineReceipt)
        {
            List<OrderDetail> CombineItem =new List<OrderDetail>();
            foreach (var receipt in combineReceipt.Receipts.ToList())
            {
                var order_detail = _context.OrderDetail.Where(w => w.OrderID != combineReceipt.OrderID && w.OrderID==receipt.OrderID).ToList();
                foreach (var item in order_detail)
                {
                    var item_update = _context.OrderDetail.FirstOrDefault(w => w.Line_ID == item.Line_ID && w.OrderID==combineReceipt.OrderID);
                    var item_count = _context.OrderDetail.Where(w => w.Line_ID == item.Line_ID && w.OrderID == combineReceipt.OrderID);
                    try
                    {
                        if(item_count.Count()>0)
                        {
                            item_update.KhmerName = item_update.KhmerName + "*";
                            item_update.EnglishName = item_update.EnglishName + "*";
                            item_update.Qty = item_update.Qty + item.Qty;
                            item_update.PrintQty = item_update.Qty + item.Qty;
                            if (item_update.TypeDis == "Percent")
                            {
                                item_update.Total = (item_update.Qty * item_update.UnitPrice) * (1 - item_update.DiscountRate / 100);
                                item_update.DiscountValue = (item_update.Qty * item_update.UnitPrice) / 100;
                            }
                            else
                            {
                                item_update.Total = (item_update.Qty * item_update.UnitPrice) - item_update.DiscountRate;
                                item_update.DiscountValue = item_update.DiscountRate;
                            }
                            _context.OrderDetail.Remove(item);
                            _context.Update(item_update);
                            _context.SaveChanges();

                        }
                        else
                        {
                            _context.Remove(item);
                            _context.SaveChanges();

                            item.OrderDetailID = 0;
                            item.KhmerName = item.KhmerName+"*";
                            item.OrderID = combineReceipt.OrderID;
                            _context.OrderDetail.Add(item);
                            _context.SaveChanges();
                            
                        }
                       
                    }
                    catch (Exception)
                    {
                        //throw new Exception(ex.Message);
                        _context.OrderDetail.Remove(item);
                        _context.SaveChanges();

                        item.OrderDetailID = 0;
                        item.OrderID = combineReceipt.OrderID;
                        _context.OrderDetail.Add(item);
                        _context.SaveChanges();
                        
                    }
                    
                }
                //Update status delete order
                var order_update = _context.Order.Find(receipt.OrderID);
                order_update.Delete = true;
                _context.Order.Update(order_update);
                _context.SaveChanges();
                //Remove order
                var orders_delete = _context.Order.Where(w => w.Delete == true);
                foreach (var order in orders_delete.ToList())
                {
                    _context.Remove(order);
                    _context.SaveChanges();
                }

                //Update status table
                var table_ordered = _context.Order.Where(w => w.TableID == order_update.TableID && w.Cancel==false);
                if (table_ordered.Count() == 0)
                {
                    //_timeDelivery.PushOrderByTableToAvailable(order_update.TableID);
                    var time = _timeDelivery.StopTimeTable(order_update.TableID, 'A');
                    var table_up = _context.Tables.Find(order_update.TableID);
                    table_up.Status = 'A';
                    table_up.Time = "00:00:00";
                    _context.Update(table_up);
                    _context.SaveChanges();

                }
            }

        }

        IEnumerable<ItemsReturn> IPOS.SendOrder(Order data,string print_type)
        {
            List<ItemsReturn> list = new List<ItemsReturn>();
            List<ItemsReturn> list_group =new List<ItemsReturn>();
            foreach (var item in data.OrderDetail.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom=>uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    if(item_warehouse!=null)
                    {
                        ItemsReturn item_group = new ItemsReturn
                        {
                            Line_ID = item.Line_ID,
                            Code = item.Code,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName+' '+item_group_uom.UnitofMeasureInv.Name,
                            InStock = item_warehouse.InStock - item_warehouse.Committed,
                            OrderQty = item.PrintQty * uom_defined.Factor,
                            Committed = item_warehouse.Committed
                        };
                        list_group.Add(item_group);
                    }
                   
                }
                else
                {
                    check.OrderQty = check.OrderQty+item.PrintQty*uom_defined.Factor;
                }
            }
            foreach (var item in list_group)
            {
                if(item.OrderQty>item.InStock)
                {
                    ItemsReturn item_return = new ItemsReturn
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID=item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock =item.InStock,
                        OrderQty = item.OrderQty
                    };
                    list.Add(item_return);
                }
               
            }
            if (list.Count > 0)
            {
                return list;
            }
            if (data.OrderID == 0)
            {
                var setting = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == data.BranchID);
                var queues = _context.Order_Queue.Where(w => w.BranchID == data.BranchID);
                var receipt = _context.Order_Receipt.Where(w => w.BranchID == data.BranchID);
                //Remove queue order
                if (queues.Count() >= setting.QueueCount)
                {
                    foreach (var queue in queues.ToList())
                    {
                        _context.Remove(queue);
                        _context.SaveChanges();
                    }
                }
                data.OrderNo = ("Order-" + (queues.Count() + 1));
                data.QueueNo = (queues.Count() + 1).ToString();
                data.TimeIn = DateTime.Now.ToString("hh:mm:ss tt");
                data.DateIn = Convert.ToDateTime(DateTime.Today);
                data.DateOut = Convert.ToDateTime(DateTime.Today);
                data.CheckBill = 'N';
                _context.Add(data);
                _context.SaveChanges();
                StartTime(data.TableID, "00:00:00");

                //Add queue order
                AddQueueOrder(data.BranchID, data.OrderNo);
                //Send data to print bill
                //Check stock
                IssuseCommittedOrder(data.OrderID);
                
                if (print_type=="Bill" || print_type=="Pay")
                {
                    PrintReceiptBill(data.OrderID, print_type);
                }
                //Send data to print order
                SendToPrintOrder(data);

                //Real time push data
                GetOrder(data.TableID, data.OrderID,data.UserOrderID);
            }
            else
            {
                _context.Update(data);
                _context.SaveChanges();
                //Check stock
                IssuseCommittedOrder(data.OrderID);
                
                if (print_type == "Bill" || print_type == "Pay")
                {
                    PrintReceiptBill(data.OrderID, print_type);
                }
                SendToPrintOrder(data);

                var order_detail = _context.OrderDetail.Where(w => w.OrderID == data.OrderID);
                var order_rm = _context.Order.FirstOrDefault(w => w.OrderID == data.OrderID);
                if(order_rm!=null)
                {
                    if (order_detail.Count() == 0)
                    {
                        var time = _timeDelivery.StopTimeTable(data.TableID, 'A');
                        var table_up = _context.Tables.Find(data.TableID);
                        table_up.Status = 'A';
                        table_up.Time = "00:00:00";
                        _context.Order.Remove(data);
                        _context.Update(table_up);
                        _context.SaveChanges();
                    }
                }
               
            }
            _timeDelivery.ClearUserOrder(data.TableID);
            return list;
        }

        public void ClearUserOrder(int tableid)
        {
            _timeDelivery.ClearUserOrder(tableid);
        }

        public void IntailStatusTable()
        {
            var tables = _context.Tables.Where(w => w.Status != 'A').ToList();
            foreach (var table in tables)
            {
                _timeDelivery.StartTimeTable(table.ID, table.Time,table.Status);
            }
           
        }
      
        IEnumerable<OpenShift> IPOS.OpenShiftData(int userid, double cash)
        {
            var user = _context.UserAccounts.FirstOrDefault(w => w.ID == userid);
            var setting = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == user.BranchID);
            var currency = _context.Currency.FirstOrDefault(w => w.ID == setting.SysCurrencyID);
            var receipts = _context.Receipt.Where(w => w.BranchID == user.BranchID);
            var openshift = _context.OpenShift.Where(w=>w.UserID == userid && w.Open == true);
           
            try
            {
                if (openshift.Count() > 0)
                {
                    var open = _context.OpenShift.Where(w => w.UserID == userid && w.Open==true);
                    var open_update = _context.OpenShift.FirstOrDefault(w => w.UserID == userid && w.Open==true);
                    open_update.CashAmount_Sys = open.Sum(w => w.CashAmount_Sys)+cash;
                    _context.OpenShift.Update(open_update);
                    _context.SaveChanges();
                    return null;
                }
                else
                {
                    OpenShift openShift = new OpenShift
                    {
                        DateIn = DateTime.Today,
                        TimeIn = DateTime.Now.ToString("hh:mm:ss tt"),
                        BranchID = user.BranchID,
                        UserID = userid,
                        CashAmount_Sys = cash,
                        Currency = currency.Description,
                        Trans_From = receipts.Max(m=>m.ReceiptID),
                        Open = true
                    };
                    _context.OpenShift.Add(openShift);
                    _context.SaveChanges();
                    return openshift;
                }
            }
            catch (Exception)
            {
                OpenShift openShift = new OpenShift
                {
                    DateIn = DateTime.Today,
                    TimeIn = DateTime.Now.ToString("hh:mm:ss tt"),
                    BranchID = user.BranchID,
                    UserID = userid,
                    CashAmount_Sys = cash,
                    Currency = currency.Description,
                    Trans_From = 0,
                    Open = true
                };
                _context.OpenShift.Add(openShift);
                _context.SaveChanges();
                return openshift;
            }
            
        }

        public IEnumerable<OpenShift> CloseShiftData(int userid,double cashout)
        {
            var user = _context.UserAccounts.FirstOrDefault(w => w.ID == userid);
            var setting = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == user.BranchID);
            var currency = _context.Currency.FirstOrDefault(w => w.ID == setting.LocalCurrencyID);
            var trans_to = _context.Receipt.Where(w => w.BranchID == user.BranchID).Max(w=>w.ReceiptID);
            var openshift = _context.OpenShift.FirstOrDefault(w =>w.UserID == userid && w.Open==true);
            if(openshift==null)
            {
                return null;
            }
            else
            {
                openshift.Open = false;
                _context.OpenShift.Update(openshift);
                _context.SaveChanges();

                var amount = _context.Receipt.Where(w => w.BranchID == user.BranchID && w.UserOrderID == userid && w.ReceiptID > openshift.Trans_From && w.ReceiptID <= trans_to).Sum(w => w.GrandTotal);
                CloseShift closeShift = new CloseShift
                {
                    DateIn = openshift.DateIn,
                    TimeIn = openshift.TimeIn,
                    DateOut = DateTime.Today,
                    TimeOut = DateTime.Now.ToString("hh:mm:ss tt"),
                    BranchID = user.BranchID,
                    UserID = userid,
                    CashInAmount_Sys = openshift.CashAmount_Sys,
                    SaleAmount = amount,
                    SaleAmount_Sys = amount * setting.RateIn,
                    CashOutAmount_Sys = cashout,
                    LocalCurrency = currency.Description,
                    ExchangeRate = setting.RateIn,
                    Trans_From = openshift.Trans_From,
                    Trans_To = trans_to,
                    Close = true

                };
                _context.CloseShift.Add(closeShift);
                _context.SaveChanges();
                return null;
            }
        }
            
        public IEnumerable<POS.Receipt> GetReceiptReprint(int branchid,string date_from,string date_to)
        {
            if(date_from==null && date_to==null)
            {
                var receipts = _context.Receipt.Include(cur => cur.Currency).Include(user => user.UserAccount.Employee).Include(table => table.Table).Where(w => w.BranchID == branchid && w.DateOut == Convert.ToDateTime(DateTime.Today));
                return receipts;
            }
           else
            {
                var receipts = _context.Receipt.Include(cur => cur.Currency).Include(user => user.UserAccount.Employee).Include(table => table.Table).Where(w => w.BranchID == branchid && w.DateOut >= Convert.ToDateTime(date_from) && w.DateOut <= Convert.ToDateTime(date_to));
                return receipts;
            }
        }

        public IEnumerable<ServiceItemSales> FilterItemByGroup(int pricelist_id)
        {
            var item = _context.ServiceItemSales.FromSql("pos_FilterItemByGroup @PricelistID={0}",
                parameters: new[] {
                    pricelist_id.ToString()
                   
                });
            return item.ToList();
        }

        public void VoidOrder(int orderid)
        {
            var order = _context.Order.FirstOrDefault(w => w.OrderID == orderid);
            order.Cancel = true;
            var orders = _context.Order.Where(w => w.TableID == order.TableID && order.Cancel==false).ToList();
            var user = _context.UserAccounts.Include(emp=>emp.Employee).FirstOrDefault(w => w.ID == order.UserOrderID);
            _context.Update(order);
            _context.SaveChanges();
            //Update status table
            var table_ordered = _context.Order.Where(w => w.TableID == order.TableID && w.Cancel==false);
            if (table_ordered.Count() == 0)
            {
                //_timeDelivery.PushOrderByTableToAvailable(order_update.TableID);
                var time = _timeDelivery.StopTimeTable(order.TableID, 'A');
                var table_up = _context.Tables.Find(order.TableID);
                table_up.Status = 'A';
                table_up.Time = "00:00:00";
                _context.Update(table_up);
                _context.SaveChanges();
            }
            GoodReceiptCommittedOrder(orderid);
            GetOrder(order.TableID, orderid,order.UserOrderID);
        }

        public string GetTimeByTable(int TableID)
        {
            var time_continue = _timeDelivery.GetTimeTable(TableID);
            return time_continue;
        }
        public void UpdateTimeOnTable()
        {
            var tables = _context.Tables.Where(w => w.Status == 'B').ToList();
            foreach (var table in tables)
            {
                var time=_timeDelivery.GetTimeTable(table.ID);
                table.Time = time;
                _context.Update(table);
            }
            _context.SaveChanges();
        }
    }
}

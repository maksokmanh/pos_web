﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IGoodIssuse
    {
        IEnumerable<Warehouse> GetWarehouse_From(int ID);
        IEnumerable<Warehouse> GetWarehouse_To(int ID);
        IEnumerable<GoodIssuesDetail> GetItemByWarehouse_From(int warehouseID);
        IEnumerable<GoodIssuesDetail> GetItemFindBarcode(int warehouseID,string Barcode);
        void SaveGoodIssues(GoodIssues issues);
    }
    public class GoodIssuseResponsitory:IGoodIssuse
    {
        private readonly DataContext _context;
        public GoodIssuseResponsitory(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Warehouse> GetWarehouse_From(int ID) => _context.Warehouses.Where(x => x.Delete == false && x.BranchID==ID).ToList(); 
       public IEnumerable<Warehouse> GetWarehouse_To(int ID) => _context.Warehouses.Where(x => x.Delete == false && x.BranchID==ID).ToList();
        public IEnumerable<GoodIssuesDetail> GetItemByWarehouse_From(int warehouseID) => _context.GoodIssuesDetails.FromSql("sp_GetItembyWarehouseFrom_GoodIssuse @WarehouseID={0}",
            parameters:new[] {
                warehouseID.ToString()
            });

        public void SaveGoodIssues(GoodIssues issues)
        {
            _context.GoodIssues.Add(issues);
            _context.SaveChanges();
            var IssuesID = issues.GoodIssuesID;
            IssuesStock(IssuesID);
        }
        public void IssuesStock(int IssuesID)
        {
            _context.Database.ExecuteSqlCommand("sp_IssusesStock @IssuesID={0}",
                parameters:new[] {
                    IssuesID.ToString()
                });
        }

        public IEnumerable<GoodIssuesDetail> GetItemFindBarcode(int warehouseID, string Barcode) => _context.GoodIssuesDetails.FromSql("sp_FindBarcodeGoodIssues @WarehouseID={0},@Barcode={1}",
            parameters:new[] {
                warehouseID.ToString(),
                Barcode.ToString()
            });
       
    }
}

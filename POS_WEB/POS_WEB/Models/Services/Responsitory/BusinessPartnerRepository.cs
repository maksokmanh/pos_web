﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IBusinessPartner
    {
        IQueryable<BusinessPartner> BusinessPartners { get; }
        IEnumerable<BusinessPartner> BusinessPartner { get; }      
        Task Add(BusinessPartner business);
        Task Update(int? id, BusinessPartner business);
        Task Delete(int? id);
        //Task SetDefualt(int id);

    }
    public class BusinessPartnerRepository : IBusinessPartner
    {
        private readonly DataContext _context;
        public BusinessPartnerRepository(DataContext ctx)
        {
            _context = ctx;
        }
        public IQueryable<BusinessPartner> BusinessPartners => _context.BusinessPartners.Where(x => x.Delete == false).Include(x=>x.PriceList);

        public IEnumerable<BusinessPartner> BusinessPartner => _context.BusinessPartners.Where(x => x.Delete == false).Include(x => x.PriceList);

        public async Task Add(BusinessPartner business)
        {
           if (business != null && business.ID == 0)
            {
                //int count = _context.BusinessPartners.Where(x=>x.Type== "Vendor").Count();
                //if (count <= 0)
                //{
                //    business.Default = true;
                //}
                //int count1 = _context.BusinessPartners.Where(x => x.Type == "Customer").Count();
                //if (count1 <= 0)
                //{
                //    business.Default = true;
                //}
                await _context.AddAsync(business);
               
            }
            else
            {
                _context.Update(business);
            }
           await _context.SaveChangesAsync();
        }

        public async Task Delete(int? id)
        {
            if (id != null)
            {
                var bp = await _context.BusinessPartners.FindAsync(id);
                if(bp != null)
                {
                    bp.Delete = true;
                    _context.Update(bp);
                    await _context.SaveChangesAsync();
                }

            }
        }

        //public async Task SetDefualt(int id )
        //{
        //    var business = _context.BusinessPartners.FirstOrDefault(x => x.ID == id);
        //    var type = business.Type;
        //    List<BusinessPartner> Type = _context.BusinessPartners.Where(w=> w.Type == type).ToList();
        //    if (Type != null){
        //        foreach (var item in Type)
        //        {
        //            if (item.ID == id)
        //            {
        //                item.Default = true;
        //            }
        //            else
        //            {
        //                item.Default = false;
        //            }
        //            _context.Update(item);
        //            await _context.SaveChangesAsync();
        //        }
        //    }
           
        //}

        public async Task Update(int? id, BusinessPartner business)
        {
            if (id != null)
            {
                 _context.BusinessPartners.Update(business);
                await _context.SaveChangesAsync();
            }

        }
    }
}

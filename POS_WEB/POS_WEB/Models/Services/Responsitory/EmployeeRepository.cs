﻿using POS_WEB.AppContext;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IEmployee
    {
        //IQueryable<Employee> Employees { get; }
        IQueryable<Employee> Employee { get; }
        IQueryable<Employee> Emp();
        Task Add(Employee employee);
        Task Update(int? id, Employee employee);
        Task Delete(int? id);

    }
    public class EmployeeRepository : IEmployee
    {
        private readonly DataContext _context;
        public EmployeeRepository(DataContext context)
        {
            _context = context;
        }
        //public IQueryable<Employee> Employees => _context.Employees.Where(x => x.Delete == false);

        public IQueryable<Employee> Employee => _context.Employees.Where(x => x.Deleted == false);

        public async Task Add(Employee employee)
        {
            
            try
            {
                if( employee != null)
                {
                    
                    await _context.AddAsync(employee);
                    await _context.SaveChangesAsync();
                }

            }
            catch (SqlException e)
            {
               Console.WriteLine(e.StackTrace);
                throw;
            }
        }

        public async Task Delete(int? id)
        {            
            if (id != null)
            {
                var emp = await _context.Employees.FindAsync(id);
                if(emp != null)
                {
                     emp.Deleted = true;
                    _context.Update(emp);
                    await _context.SaveChangesAsync();
                }
            }
        }

        public IQueryable<Employee> Emp()
        {
            IQueryable<Employee> data = _context.Employees.Where(x=>x.Deleted==false);
            return data;
        }

        public async Task Update(int? id, Employee employee)
        {
            if (id != null)
            {
                var x = await _context.Employees.FindAsync(id);
                if (x != null)
                {
                    x.Code = employee.Code;
                    x.Name = employee.Name;
                    x.Gender = employee.Gender;
                    x.Birthdate = employee.Birthdate;
                    x.Hiredate = employee.Hiredate;
                    x.Address = employee.Address;
                    x.Phone = employee.Phone;
                    x.Email = employee.Email;
                    x.Photo = employee.Photo;
                    x.Position = employee.Position;
                    x.Stopwork = employee.Stopwork;
                    x.IsUser = employee.IsUser;
                    _context.Update(x);
                    await _context.SaveChangesAsync();
                }
            }
        }
    }
}

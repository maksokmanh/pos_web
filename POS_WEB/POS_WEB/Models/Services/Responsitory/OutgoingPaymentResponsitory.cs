﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{

    public interface IOutgoingPayment
    {
        IEnumerable<OutgoingPaymentVendor> GetOutgoingPaymentVendor(int VendorID);
    }
    public class OutgoingPaymentResponsitory:IOutgoingPayment
    {
        private readonly DataContext _context;
        public OutgoingPaymentResponsitory(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<OutgoingPaymentVendor> GetOutgoingPaymentVendor(int VendorID) => _context.OutgoingPaymentVendors.FromSql("sp_GetOutgoingpayment @VendorID={0}",
            parameters:new[] {
                VendorID.ToString()
            });
       
    }
}

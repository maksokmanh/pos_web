﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.ClassCopy;
using POS_WEB.Models.ReportClass;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.ServicesClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IPurchaseCreditMemo
    {
        IEnumerable<Company> GetCurrencies();
        IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> ServiceMapItemMasterDataPurchaseCreditMemos(int ID);
        IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> GetItemFromBarcode(int WarehouseID,string Barcode);
        IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> ServiceMapItemMasterDataPurchaseCreditMemosDetail(int warehouseID,string invoice);
        void GoodIssuesStock(int PurchaseMemoID, string InvoiceAP);
        IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseCreditMemo_From_PurchaseAP(int ID,string Invoice);
        IEnumerable<ReportPurchasCreditMemo> ReportPurchaseCreditMemo(int BranchID, int WarehoseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check);
        IEnumerable<ReportPurchaseAP> GetAllPurchaseAP(int BranchID);
        IEnumerable<ReportPurchaseAP> GetAllGoodsReceiptPO(int BranchID);
        IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseMemo_From_PurchaseGoodReceipt(int ID, string Invoice);
        void UpdatePurchaseAP(string APInvoice, PurchaseCreditMemo purchase);
    }
    public class PurchaseCreditMemoResponsitory:IPurchaseCreditMemo
    {
        private readonly DataContext _context;
        public PurchaseCreditMemoResponsitory(DataContext context)
        {
            _context = context;
        }
         public IEnumerable<Company> GetCurrencies()
        {
            IEnumerable<Company> list = (
                from pd in _context.PriceLists.Where(x => x.Delete == false)
                join com in _context.Company.Where(x => x.Delete == false) on
                pd.ID equals com.PriceListID join cur in _context.Currency.Where(x => x.Delete == false)
                on pd.CurrencyID equals cur.ID
                select new Company {
                    PriceList = new PriceLists
                    {
                        Currency = new Currency
                        {
                            Description = cur.Description
                        }
                    }
                }
                  
                );
            return list;
        }

        public void GoodIssuesStock(int PurchaseMemoID,string InvoiceAP)
        {
            _context.Database.ExecuteSqlCommand("sp_GoodIssuesStockMemo @PurchaseMemoID={0},@InvoiceAp={1}",
                parameters: new[] {
                    PurchaseMemoID.ToString(),
                    InvoiceAP.ToString()
                });
        }

        public IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseCreditMemo_From_PurchaseAP(int ID,string Invoice) => _context.PurchaseAP_To_PurchaseMemos.FromSql("sp_GetPurchaseCreditMemo_From_PurchaseAP @PurhcaseAPID={0},@Invoice={1}",
            parameters:new[] {
                  ID.ToString(),
                Invoice.ToString()
              
            });
      
        public IEnumerable<ReportPurchasCreditMemo> ReportPurchaseCreditMemo(int BranchID, int WarehoseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check) => _context.ReportPurchasCreditMemos.FromSql("sp_ReportPurchaseCreditMemo @BranchID={0},@WarehouseID={1},@PostingDate={2},@DocumentDate={3},@DeliveryDate={4},@Check={5}",
            parameters: new[] {
                BranchID.ToString(),
                WarehoseID.ToString(),
                PostingDate.ToString(),
                DocumentDate.ToString(),
                DeliveryDate.ToString(),
                Check.ToString()
            });
      
        public IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> ServiceMapItemMasterDataPurchaseCreditMemos(int ID) => _context.ServiceMapItemMasterDataPurchaseCreditMemos.FromSql("sp_GetListItemMasterDataPurchaseCreditMemo @WarehouseID={0}",
            parameters: new[] {
                ID.ToString()
            });

        public IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> ServiceMapItemMasterDataPurchaseCreditMemosDetail(int warehouseID, string invoice) => _context.ServiceMapItemMasterDataPurchaseCreditMemos.FromSql("sp_GetListItemMasterDataPurchaseMemo_Detail @WarehouseID={0},@InvoiceNo={1}",
            parameters:new[] {
                warehouseID.ToString(),
                invoice
            });

        public IEnumerable<ReportPurchaseAP> GetAllPurchaseAP(int BranchID) => _context.ReportPurchaseAPs.FromSql("sp_GetAllPurchaseAP @BranchID={0}",
            parameters:new[] {
                BranchID.ToString()
            });
        public IEnumerable<ReportPurchaseAP> GetAllGoodsReceiptPO(int BranchID) => _context.ReportPurchaseAPs.FromSql("sp_GetAllGoodReceiptPO @BranchID={0}",
          parameters: new[] {
                BranchID.ToString()
          });

        public IEnumerable<ServiceMapItemMasterDataPurchaseCreditMemo> GetItemFromBarcode(int WarehouseID, string Barcode) => _context.ServiceMapItemMasterDataPurchaseCreditMemos.FromSql("sp_FindBarcodePurchaseCreditMemo @WarehouseID={0},@Barcode={1}",
            parameters:new[] {
                WarehouseID.ToString(),
                Barcode.ToString()
            });
        public IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseMemo_From_PurchaseGoodReceipt(int ID, string Invoice) => _context.PurchaseAP_To_PurchaseMemos.FromSql("sp_GetPurchaseAP_From_PurchaseGoodRecepitPO @PurhcasePOID={0},@Invoice={1}",
          parameters: new[] {
                  ID.ToString(),
                Invoice.ToString()

          });

        public void UpdatePurchaseAP(string APInvoice, PurchaseCreditMemo purchase)
        {
            var purchaseAp = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == APInvoice);
            var outgoing = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == APInvoice);
            var subApplied_Amount = _context.Purchase_APs.Where(x => x.InvoiceNo == APInvoice);

            purchaseAp.Applied_Amount = subApplied_Amount.Sum(x => x.Applied_Amount) + purchase.Balance_Due;
            outgoing.Applied_Amount = outgoing.Applied_Amount + purchase.Balance_Due_Sys;
            _context.OutgoingPaymentVendors.Update(outgoing);
            _context.Purchase_APs.Update(purchaseAp);
            _context.SaveChanges();
            int ApID = purchaseAp.PurchaseAPID;
            var detail = _context.PurchaseDetails.Where(x => x.Purchase_APID == ApID && x.Delete == false);
            string isClose = "close";
            foreach (var item in detail)
            {
                if (item.Delete == false)
                {
                    isClose = "open";
                }
            }
            purchaseAp.Status = isClose;
            _context.Update(purchaseAp);
            _context.SaveChanges();
        }
    }
}

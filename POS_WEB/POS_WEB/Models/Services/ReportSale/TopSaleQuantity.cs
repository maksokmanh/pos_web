﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.ReportSale
{
    [Table("rp_TopSaleQuantity",Schema ="dbo")]
    public class TopSaleQuantity
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string KhmerName { get; set; }
        public string EnglishName { get; set; }
        public double Qty { get; set; }
        public string Uom { get; set; }
        public string DateOut { get; set; }
        public string TimeOut { get; set; }
        public string Branch { get; set; }
        public int ItemID { get; set; }
        public int UomID { get; set; }

    }
}

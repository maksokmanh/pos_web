﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.ReportSale
{
    [Table("rp_Cashout", Schema = "dbo")]
    public class CashoutReport
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string KhmerName { get; set; }
        public string EnglishName { get; set; }
        public double Qty { get; set; }
        public double Price { get; set; }
        public double DisItemValue { get; set; }
        public double Total { get; set; }
        public string Currency { get; set; }
        public string Currency_Sys { get; set; }
        public double TotalSoldAmount { get; set; }
        public double TotalDiscountItem { get; set; }
        public double TotalDiscountTotal { get; set; }
        public double TotalVat { get; set; }
        public double GrandTotal { get; set; }
        public double GrandTotal_Sys { get; set; }
        public double TotalCashIn_Sys { get; set; }
        public double TotalCashOut_Sys { get; set; }
        public double ExchangeRate { get; set; }
        public string ItemGroup1 { get; set; }
        public string DateIn { get; set; }
        public string DateOut { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string EmpName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.Inventory
{
    [Table("tbGroupUoM",Schema ="dbo")]
    public class GroupUOM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }        
        [Required(ErrorMessage ="Please input code !")]
        public string Code { get; set; }
        [Required(ErrorMessage ="Please input name !"),Column(TypeName ="nvarchar(max)")]
        public string Name { get; set; }
        public bool Delete { get; set; }
    }
}

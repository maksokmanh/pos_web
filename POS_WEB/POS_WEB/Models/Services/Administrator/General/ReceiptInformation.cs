﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.General
{
    [Table("tbReceiptInformation", Schema ="dbo")]
    public class ReceiptInformation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int? BranchID { get; set; }
        [Required(ErrorMessage ="Please input title !")]
        public string Title { get; set; }
        [Required(ErrorMessage ="Please input address !")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please input tel !")]
        public string Tel1 { get; set; }
        [Required(ErrorMessage = "Please input tel !")]
        public string Tel2 { get; set; }
        [Required(ErrorMessage = "Please input description !")]
        public string KhmerDescription { get; set; }
        [Required(ErrorMessage = "Please input description !")]
        public string EnglishDescription { get; set; }
        public string PowerBy { get; set; }
        public string TeamCondition { get; set; }
        //[Required]
       // public string Language { get; set; }//Khmer,English, Chinese
        public string Logo { get; set; }
        [ForeignKey("BranchID")]
        public virtual Branch Branch { get; set; }
       
    }
}

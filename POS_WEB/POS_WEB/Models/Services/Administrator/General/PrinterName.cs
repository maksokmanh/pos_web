﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.General
{
    [Table("tbPrinterName",Schema ="dbo")]
    public class PrinterName
    {
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        public string MachineName { get; set; }
        public bool Delete { get; set; }
    }
}

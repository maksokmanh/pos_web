﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.Tables
{
    [Table("tbGroupTable",Schema ="dbo")]
    public class GroupTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        [Required]
        public string Types { get; set; }//Normal,Time
        public string Image { get; set; }
        public bool Delete { get; set; }
    }
 
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.Tables
{
    [Table("tbTable", Schema ="dbo")]
    public class Table
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        public int GroupTableID { get; set; }
        public string Image { get; set; }
        [Required]
        public char Status { get; set; } = 'A';// A,B,P,R 
        public string  Time { get; set; }
        public bool Delete { get; set; }
        //Foreign Key
        [ForeignKey("GroupTableID")]
        public GroupTable GroupTable { get; set; }
        
    }
}

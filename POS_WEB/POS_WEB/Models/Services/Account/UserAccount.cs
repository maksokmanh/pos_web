﻿using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Account
{
    [Table("tbUserAccount", Schema = "dbo")]
    public class UserAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int? EmployeeID { get; set; }
        public int? CompanyID { get; set; }
        public int? BranchID { get; set; }
        [StringLength(50)]
        [MinLength(4, ErrorMessage = "Username must have at least 4 Characters!")]
        [Required(ErrorMessage = "Please input username !")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please input password !")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        public string ComfirmPassword { get; set; }
        public bool UserPos { get; set; }
        public string Language { get; set; }
        public bool Status { get; set; }
        public bool Delete { get; set; }
        [ForeignKey("EmployeeID")]
        public Employee Employee { get; set; }
        [ForeignKey("CompanyID")]
        public Company Company { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }       
      
    }
}

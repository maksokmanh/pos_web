﻿using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.General
{
    [Table("tbUserPrivillege", Schema ="dbo")]
    public class UserPrivillege
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int UserID { get; set; }
        public int FunctionID { get; set; }
        public bool Used { get; set; }
        public bool Delete { get; set; }
        public string Code { get; set; }
        [ForeignKey("UserID")]
        public List<UserAccount> UserAccount { get; set; }
        [ForeignKey("FunctionID")]
        public Function Function { get; set; }
      
    }
}

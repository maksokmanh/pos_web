﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS.service
{
    public class ItemsReturn
    {
        public int Line_ID { get; set; }
        public int ItemID { get; set; }
        public string Code { get; set; }
        public string KhmerName { get; set; }
        public double InStock { get; set; }
        public double OrderQty { get; set; }
        public double Committed { get; set; }
    }
}

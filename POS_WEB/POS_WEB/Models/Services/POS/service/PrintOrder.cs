﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS.service
{
    public class PrintOrder
    {
        public string Table { get; set; }
        public string OrderNo { get; set; }
        public string Cashier { get; set; }
        public string Item { get; set; }
        public string PrintQty { get; set; }
        public string ItemPrintTo { get; set; }
        public string Comment { get; set; }
    }
}

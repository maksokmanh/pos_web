﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS
{
    [Table("tbOpenShift",Schema ="dbo")]
    public class OpenShift
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateIn { get; set; }
        public string TimeIn { get; set; }
        public int? BranchID { get; set; }
        public int? UserID { get; set; }
  
        public double CashAmount_Sys { get; set; }
      
        public string Currency { get; set; }
       
        public double Trans_From { get; set; }
        public bool Open { get; set; }

    }
}

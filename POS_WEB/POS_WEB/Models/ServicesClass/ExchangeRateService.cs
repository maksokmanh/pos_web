﻿using POS_WEB.Models.Services.Banking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.SeverClass
{
    public class ExchangeRateService
    {
        public List<ExchangeRate> ExchangeRates { get; set; }
    }
}

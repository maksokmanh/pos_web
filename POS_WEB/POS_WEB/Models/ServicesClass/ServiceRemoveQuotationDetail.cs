﻿using POS_WEB.Models.Services.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.ServicesClass
{
    public class ServiceRemoveQuotationDetail
    {
        public List<PurchaseQuotationDetail> RemovePurchase { get; set; }
    }
}

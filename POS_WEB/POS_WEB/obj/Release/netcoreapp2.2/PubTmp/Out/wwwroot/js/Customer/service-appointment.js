﻿
let sc_dialog = {};
$("#trigger-service-customers").on("click", function () {
    sc_dialog = new DialogBox({
        content: {
            selector: "#service-customer-list"
        },
        type: "ok-cancel",
        caption: "Service customers for appointment",
        button: {
            "ok": {
                "text": "Choose"
            },
            "cancel": { callback: function() { this.meta.shutdown(); } }
        }
    });

    
    sc_dialog.startup("after", function (dialog) {
        let table = dialog.content.find("table");
        searchServiceCustomers(table);
        $(".search-service-customers", dialog.content).focus();
        $(".search-service-customers", dialog.content).on("keyup", function () {
            setTimeout(() => {
                searchServiceCustomers(table, this.value);
            }, 500);
           
        });
    });
  
    sc_dialog.confirm(function (dialog) {
        chooseServiceCustomer(chosen_row);
    });
});

let chosen_row;
function searchServiceCustomers(table, word) {
    let index = 1;
    let setting = {
        url: "/ServiceCustomer/SearchServiceCustomers",
        dataType: "JSON",
        success: function (customers) {          
            $(table).bindRows(customers, "ServiceCustomerID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                postbuild: function () {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    $("td:last-child", this).after("<td><div onclick='chooseServiceCustomer($(this).parent().parent()[0]);'"
                       + "class='fas fa-caret-square-down fn-sky fa-lg csr-pointer'></div></td>");
                },
                click: function () {
                    chosen_row = this;
                    $(this).addClass("active").siblings().removeClass("active");
                },
                dblclick: function () {
                    chooseServiceCustomer(this);
                }

            });
        }
    };
 
    if (!!word) {
        setting.data = { word: word};
    } 

    $.ajax(setting);
}

function chooseServiceCustomer(row) {
    let data = {
        id: $(row).data("servicecustomerid"),
        code: row.cells[1].textContent,
        name: row.cells[2].textContent,
        gender: row.cells[3].textContent,
        phone: row.cells[4].textContent,
    };

    console.log(data)

    $("input[name=CustomerID]").val(data.id);
    $("input[name=CustomerCode]").val(data.code);
    $("input[name=CustomerName]").val(data.name);
    $("input[name=Gender]").val(data.gender);
    $("input[name=Phone]").val(data.phone);
    sc_dialog.shutdown();
}

//Submit Service Customer Appointment.
let init_form = $.form("#service-customer-appointment");
$("#submit-sc-appointment").on("click", function () {
    let sca_form = $.form("#service-customer-appointment");
    let appointment = {
        AppointmentID: sca_form.value["AppointmentID"],
        AppointmentCode: sca_form.value["AppointmentCode"],
        Subject: sca_form.value["Subject"],
        Remark: sca_form.value["Remark"],
        ActivityDate: $(sca_form.dom["ActivityDate"]).data("value"),
        ActivityTime: sca_form.value["ActivityTime"],      
        Reminder: sca_form.value["Reminder"] + "/" + sca_form.value["ReminderUom"],
        Location: sca_form.value["Location"],
        Priority: sca_form.value["Priority"],
        Paused: sca_form.value["Paused"],
        Closed: sca_form.value["Closed"],
        UserID: sca_form.value["UserID"],
        CustomerID: sca_form.value["CustomerID"],
        CustomerType: 0,

    };
    
    $.ajax({
        url: "/ServiceCustomer/SaveServiceAppointment",
        type: "POST",
        data: $.antiForgeryToken({ appointment: appointment }),
        success: function (res) {
            countAlertableAppointments();
            new Message(res);
            if (res.Type === "confirm") {
                setTimeout(() => {
                    location.href = "/ServiceCustomer/ServiceAppointment";
                }, 2500);
            }
        }
    });
});

//Search service customer appointments
$("#search-service-appointments").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/ServiceCustomer/SearchServiceAppointments",
        dataType: "JSON",
        data: { word: this.value },
        success: function (appointments) {  
            console.log(appointments)
            setTimeout(() => {
                $("#service-appointment-list").bindRows(appointments, "AppointmentID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns: ["CustomerID", "CustomerCode", "Closed", "ActivityTime"],     
                    prebuild: function (data) {
                        data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime; 
                        data.Paused = data.Paused ? "<div class='pseudo-check cross'></div>" : "<div class='pseudo-check none'></div>"
                    },
                    postbuild: function (data) {                  
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                        let td = $("<td></td>")
                            .append('<a title="Detail" href="/ServiceCustomer/ServiceAppointmentDetail?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '" class="fas fa-info-circle fn-sky fa-lg csr-pointer"></a>&nbsp;|&nbsp;')
                            .append('<a title="Edit" href="/ServiceCustomer/ServiceAppointment?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '" class="fas fa-edit fn-orange fa-lg csr-pointer"></a>');
                        $("td:last-child", this).after(td);
                    },
                   
                });
            }, 250);

        }
    });
});


﻿'use strict'
var ajax_loader = new AjaxLoader("/ajaxloader/loading.gif")

    //setDate($("input[name='DateOfBirth']"), $("input[name='DateOfBirth']").val());
    //$("input[type='date']").on("change", function () {
    //    setDate(this, this.value);
    //});

function setDate(selector, date_value) {
    if (!!date_value) {
        $(selector)[0].valueAsDate = new Date(date_value);
        $(selector)[0].setAttribute(
            "data-date",
            moment($(selector)[0].valueAsDate)
                .format($(selector)[0].getAttribute("data-date-format"))
        );
    }
}

//Group Employee
$("select#group-employee").on("change", function (e) {
    dialogUpdateData("/Employee/GetGroupEmployees", "/Employee/UpdateGroupEmployees",
        "GroupID", "GroupName", this, "Group of Employees", true);
});

$.ajax({
    url: "/Employee/GetGroupEmployees",
    dataType: "JSON", 
    success: function (groups) {
        updateSelect("select#group-employee", groups, "GroupID", "GroupName");
    }
});

var selected_value = 0;
function dialogUpdateData(from_url, to_url, key, name, select, dialog_title, has_active, related) {
    if ($(select).val() == "-1") {
        $(select).val(selected_value);
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#dialog-define-new"
            },
            caption: dialog_title,
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let new_index = 1;
            let table = dialog.content.find("table");
            if (!!has_active) {
                $("tr:first-child th:last-child", table).prop("hidden", false);
            }

            let add_new = $("<tr><td class='add-new'></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='Name']").val() !== "") {
                    new_index++;
                    let row = $("<tr><td>" + new_index + "</td><td><input name='Name' class='form-control'/></td><td><input name='Active' type='checkbox' checked></td></tr>");
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);
            });

            $.ajax({
                url: from_url,
                dataType: "JSON",
                success: function (results) {                  
                    if (results.length > 0) {
                        let items = [];
                        $.each(results, function (index, data) {
                            index++;
                            let item = {};
                            item[key] = data[key];
                            item["index"] = index;
                            item[name] = "<input name='Name' value='" + data[name] + "' class='form-control' readonly>";

                            if (!!related && related != 0) {
                                item["Related"] = related; 
                            }
                            
                            if (!!has_active) {
                                item["active"] = "<input name='Active' type='checkbox'>";

                                if (!!data.Active) {
                                    item.active = "<input name='Active' type=checkbox checked>";
                                }
                            }

                            items.push(item);
                            new_index = index + 1;
                        });

                        $.bindRows(table, items, key, {
                            hide_key: true,
                            text_align: [{ "index": "center" }],
                            dblclick: function (e) {
                                let current_input = $("td input", this).prop("readonly", false);
                                let sibling_inputs = current_input.parent().parent().siblings().find("td  input:not(.create-new)");
                                sibling_inputs.prop("readonly", true);
                            }
                        });

                        $(document).on("click", function (e) {
                            if (!$(e.target).is(table.find("tr *"))) {
                                table.find("tr td input:not(.create-new)").prop("readonly", true);
                            }
                            
                        });
                    }                 
                    if (!!has_active) {
                        table.append("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='create-new form-control' /></td><td><input name='Active' type='checkbox' checked></td></tr>");
                    }
                   
                    table.append(add_new);
                    
                }
            });
           
        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table");
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let db = new Warehouse();
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    let item = {};
                    item[key] = $(this).data(key.toLowerCase());
                    item[name] = $.stripHTML($("input[name=Name]", td).val());                 
                    item["Active"] = $("input[name=Active]", td).is(":checked");
                    db.insert("tb_reserved", item, key);
                }
               
            });
          
            $.ajax({
                url: to_url,
                type: "post",
                dataType: "json",
                cache: false,
                data: $.antiForgeryToken({ data: db.from("tb_reserved") }),
                success: function (res) {
                    console.log(res)
                    updateSelect(select, res.Data, key, name);
                }

            });

            this.meta.shutdown();
        });
    }
}

function updateSelect(selector, jsons, key, value) {
    if ($(selector)[0].tagName === "SELECT") {
        $(selector).children().remove();

        let add_new = $("<option value='-1'>Define New</option>");
        $.each(jsons, function (i, json) {
            let option = $("<option value='" + json[key] + "'>" + json[value] + "</option>");
            $(selector).append(option);
        });
        $(selector).append(add_new);
        if ($(selector).children().length <= 1) {
            $(selector).prepend("<option value='0' selected disabled></option>");
        }
    }
}


$("input[data-phone]").on("keydown keyup", function (e) {
    $(this).prop("maxlength", 12);
    if (this.value.startsWith("0")) {
        if (this.value === "00") {
            this.value = "0";
        } 
    } else {
        this.value = this.value.substring(0, this.value.length - 1);
    }
   
    this.value = new libphonenumber.AsYouType('KH').input(this.value);
});

﻿"use strict";

countAlertableAppointments();
setInterval(async () => {
    countAlertableAppointments();
}, 10000);


async function countAlertableAppointments() {
    var wh = new Warehouse();
    $.ajax({
        url: "/Customer/MotocycleAppointmentList",
        data: { alertable: true },
        success: function (appointments) {
            for (let app of appointments) {
                let datetime = app.ActivityDate + " " + app.ActivityTime;
                if (onStartTime(datetime, app.Reminder)) {
                    wh.insert("tb_alerted_appointment", app, "AppointmentID");
                }
            }
            let count_note = wh.table("tb_alerted_appointment").size;
            if (count_note > 0) {
                if (parseFloat(count_note) > 99) {
                    count_note = 99;
                }
                $("#active-motocycle-appointments").addClass("danger").addClass("animate");
            } else {
                $("#active-motocycle-appointments").addClass("info").removeClass("animate");
            }
            $("#active-motocycle-appointments").children(".n-value").text(count_note);

            $("#active-motocycle-appointments").on("click", async function () {
                await popupAlertableAppointments(wh.from("tb_alerted_appointment"));
            });

        }

    });

}

async function popupAlertableAppointments(data) {
    let sa_dialog = new DialogBox({
        content: {
            selector: ".motocycle-appointment-list"
        },
        button: {
            ok: {
                text: "Cancel",
                callback: function () {
                    this.meta.shutdown();
                }
            }
        }

    });

    sa_dialog.startup("after", function (dialog) {
        let table = $("table", dialog.content);
        let index = 1;
        $(table).bindRows(data, "AppointmentID", {
            text_align: [{ "Name": "left" }, { "Code": "center" }],
            hidden_columns: ["CustomerID", "CustomerCode", "ActivityTime", "Paused", "Closed"],
            prebuild: function (data) {
                data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
                //data.Paused = data.Paused ? "<div class='pseudo-check cross'></div>" : "<div class='pseudo-check none'></div>"
            },
            postbuild: function (data) {
                $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                let td = $("<td></td>")
                    .append('&nbsp;<a href="/Customer/MotocycleAppointment?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '"'
                        + 'class= "fas fa-chevron-circle-right fn-green fa-lg csr-pointer" title="Follow up"></a>&nbsp;')
                    .append('|&nbsp;<a href="/Customer/MotocycleAppointmentDetail?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '"'
                        + 'class= "fas fa-info-circle fa-lg fn-sky csr-pointer" title="Close"></a>');
                $("td:last-child", this).after(td);
            }

        });
    });

}

function handleFollowup() {

}

function getAlertIcon(app) {
    let icon_alert = "";

    if (onBeforeStartTime(app.StartTime, app.Reminder)) {
        icon_alert = '<i class="fas fa-bell fa-lg fn-green"></i>';
    }

    if (onStartTime(app.StartTime, app.Reminder) && app.Activity === "NOTE") {
        icon_alert = '<i class="fas fa-bell fa-pulse fn-red fa-lg"></i>';
    }

    if (onDuringStartEndTime(app.StartTime, app.EndTime, app.Reminder)) {
        icon_alert = '<i class="fas fa-bell fa-pulse fn-red fa-lg"></i>';
    }

    if (onTimeout(app.EndTime)) {
        icon_alert = '<i class="fas fa-bell-slash fa-lg fn-orange"></i>';
    }

    if (app.Inactive || app.Closed) {
        icon_alert = '<i class="fas fa-bell-slash fa-lg fn-white"></i>';
    }

    return icon_alert;
}


function onDuringStartEndTime(start_time, end_time, reminder) {
    let now = moment(new Date());
    let end = moment(end_time);
    let start = moment(start_time);
    if (!!reminder) {
        let _rem = reminder.split("/");
        start = start.add(-parseFloat(_rem[0]), _rem[1]);
    }

    return moment.duration(now.diff(start)).asMinutes() >= 0 && moment.duration(now.diff(end)).asMinutes() < 0;
}

function onBeforeStartTime(start_time, reminder) {
    let now = moment(new Date());
    let start = moment(start_time);
    if (!!reminder) {
        let _rem = reminder.split("/");
        start = start.add(-parseFloat(_rem[0]), _rem[1]);
    }

    return moment.duration(now.diff(start)).asMinutes() < 0;
}

function onStartTime(start_time, reminder) {
    let now = moment(new Date());
    let start = moment(start_time);
    if (!!reminder) {
        let _rem = reminder.split("/");
        start.add(-parseFloat(_rem[0]), _rem[1]);
    }
    return moment.duration(now.diff(start)).asMinutes() >= 0;
}

function onTimeout(end_time) {
    let now = moment(new Date());
    let end = moment(end_time);
    return moment.duration(now.diff(end)).asMinutes() >= 0;
}




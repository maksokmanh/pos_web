﻿
class Message
{
    constructor(config, values) {
        this._setting = {
            summary: {
                title: {
                    text: "SUMMARY",
                    text_align: "left",
                    font_size: "1rem",
                    font_weight: "bold",
                    color: "",
                    background_color: "#C0D0E0"
                },
                content: {
                    symbol: "",
                    color: "",
                    background_color: "#C9D9E9"
                },
                selector: "",
                attribute: "ck-message-summary", 
            },
            for: {
                attribute: "ck-message-for",
                color: ""
            },       
            model: {
                type: "reject", //["REJECT", "ALERT", "CONFIRM"]
                data: {},
                redirect: ""
            },
            autoclose: {
                delay: 1000,
                duration: 5000,
                type: "confirm", //["REJECT", "ALERT", "CONFIRM"]
                callback: function (self_object, self_html) {}
            }
        }

        
        if (!values) {  
            if (!!config && typeof config === "string") {
                this.setting.summary.selector = config;
            } 

            this.map(config);
        } else {
            this.map(values);
        }

        this.setting = config;
        
        setTimeout(() => {
            this.build();
        }, 0);
        
    }

    set setting(value) {
        if (!!value && value.constructor === Object && Object.keys(value).length > 0) {
            this._setting = $.extend(true, {}, this._setting, value);
        } 
    }

    get setting() { return this._setting; }

    set data(value) {
        
        if (!!value) {
            if (Array.isArray(value) && value.length > 0) {
                for (let item of value) {
                    this.data = item;
                }
            }
            
            if (value.constructor === Object && Object.getOwnPropertyNames(value).length > 0) {
                for (let p of Object.getOwnPropertyNames(value)) {
                    if (value.hasOwnProperty(p)) {   
                        this.data[p] = value[p];
                    }
                }
            }
        }
        
        this.build();
    }

    get data() { return this.setting.model.data; }

    add(key, value) {
        let item = {};
        item[key] = value;
        this.data = item;
        this.build();
    }

    map(model) {      
        if (!!model && model.constructor === Object && Object.getOwnPropertyNames(model).length > 0) {
            let __model = {};
            for (let p of Object.getOwnPropertyNames(model)) {
                if (model.hasOwnProperty(p) && !!model[p]) {
                    __model[p.toLowerCase()] = model[p];
                }
            }
            this.setting.model = $.extend(this.setting.model, __model);
        }
        this.build();
        return this;    
    }

    remove(key) {

        if (!!key) {
            if (typeof key === "string") {
                delete this.setting.model.data[key];
            }

            if (Array.isArray(key) && key.length > 0) {
                for (let k of key) {
                    this.remove(k);
                }
            }
            this.build();
        } 
    }

    clear() {
        this.setting.model.data = {};
        this.build();
    }
   
    build(values) {
        if (!!values) {
            this.data = values;
        }
        
        let _message_summary = !this.setting.summary.selector? $("["+ this.setting.summary.attribute +"]") : $(this.setting.summary.selector);
        let _message_for = $("[" + this.setting.for.attribute + "]");
        let _box = $("<div></div>");
        _box.css({
            "border-radius": "5px",
            "-webkit-box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)",
            "-moz-box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)",
            "box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)"
        });

        let _title = $("<div>" + this.setting.summary.title.text + "</div>").css({
            "border-top-left-radius": "5px",
            "border-top-right-radius": "5px",
            "height": "35px",
            "line-height": "35px",
            "border-bottom": ".5px solid rgba(0, 0, 0, .1)",
            "padding": "0 10px",
            "background-color": this.setting.summary.title.background_color,
            "font-size": this.setting.summary.title.font_size,
            "font-weight": this.setting.summary.title.font_weight,
            "text-align": this.setting.summary.title.text_align
        });
        
        let ul = $("<ul></ul>").css({
            "list-style-type": "'" + this.setting.summary.content.symbol + "'",
            "background-color": this.setting.summary.content.background_color,
            "margin": "0",
            "padding": "15px 30px",
            "border-bottom-left-radius" : "5px",
            "border-bottom-right-radius": "5px"
        }).appendTo(_box);

        if (this.setting.summary.content.symbol === "") {
            switch (this.setting.model.type) {
                case "confirm": case "CONFIRM":
                    ul.css({ "list-style-type": "'✔'" });
                    break;
                case "alert": case "ALERT":
                    ul.css({ "list-style-type": "'⚠'" });
                    break;
                case "reject": case "REJECT":
                    ul.css({ "list-style-type": "'✘'" });
                    break;
            }
        }
       
        for (let c of Object.getOwnPropertyNames(this.data)) {
            let li = $("<li>" + this.data[c] + "</li>");
            li.css({
                "padding": "5px",
                "color": "#EC5B5B",
                "width": "100%",
                "white-space": "normal",
                "word-break": "break-all"
            });

            li.css({ "color": this.setting.summary.content.color });
            if (this.setting.summary.content.color === "") {
                switch (this.setting.model.type) {
                    case "reject": case "REJECT": case -1:
                        li.css({ "color": "#EC5B5B" });
                        break;
                    case "alert": case "ALERT": case 0:
                        li.css({ "color": "#F09F09" });
                        break;
                    case "confirm": case "CONFIRM": case 1:
                        li.css({ "color": "#50C05E" });
                        break;              
                }
            }

            ul.append(li);
        }

        
        if (Object.getOwnPropertyNames(this.data).length > 0) {
            if (!!this.setting.summary.title.text) {
                ul.before(_title);
            }
            _message_summary.html(_box);
        } else {
            this.close();
        }

        //Validation Message For Each Controls
        let _self = this;
        _message_for.each(function (i, dom) { 
            let _value = _self.data[this.getAttribute(_self.setting.for.attribute)];      
            $(this).children().remove();
            if (!!_value) {
                $(this).html("<span>" + _value + "</span>");
                $(this).css("color", _self.setting.for.color);
                if (_self.setting.for.color === "") {
                    switch (_self.setting.model.type) {
                        case "reject": case "REJECT": case "-1":
                            $(this).css("color", "#EC5B5B");
                            break;
                        case "alert": case "ALERT": case "0":
                            $(this).css("color", "#F09F09");
                            break;
                        case "confirm": case "CONFIRM": case "1":
                            $(this).css("color", "#50C05E");
                            break;
                    }
                }
            }   
        });

        let _autoclose = this.setting.autoclose;
        let _model = this.setting.model;
        if (_autoclose.type.toLowerCase() === _model.type.toLowerCase()) {
            this.autoclose(this.setting.autoclose.callback);
        } 

        if (!!this.setting.model.redirect) {
            this.redirect(this.setting.model.redirect);  
        }
    }

    close(callback) {
        if (this instanceof Message) {
            let _summary = !this.setting.summary.selector ? $("[" + this.setting.summary.attribute + "]") : $(this.setting.summary.selector);
            let _for = $("[" + this.setting.for.attribute + "]");
            setTimeout(() => {
                _summary.children().fadeOut(0, function () {
                    if (!!callback && typeof callback === "function") {
                        Promise.resolve(callback(_self, this)).then($(this).remove());
                    }
                });

                _for.children().fadeOut(0, function () {
                    if (!!callback && typeof callback === "function") {
                        Promise.resolve(callback(_self, this)).then($(this).remove());
                    }
                });
            }, 0);
            this.setting.model.data = {};
            delete this; 
        }  
    }

    autoclose(callback) {
        if (this instanceof Message) {
            let _self = this;
            let _autoclose = this.setting.autoclose;
            let _summary = !this.setting.summary.selector ? $("[" + this.setting.summary.attribute + "]") : $(this.setting.summary.selector);
            let _for = $("[" + this.setting.for.attribute + "]");
            setTimeout(() => {
                _summary.children().fadeOut(_autoclose.duration, function () {
                    if (!!callback && typeof callback === "function") {
                        Promise.resolve(callback(_self, this)).then($(this).remove());
                    }
                });

                _for.children().fadeOut(_autoclose.duration, function () {            
                    if (!!callback && typeof callback === "function") {
                        Promise.resolve(callback(_self, this)).then($(this).remove());
                    }
                });
            }, _autoclose.delay);
            delete this;            
        }
       
    }

    redirect(url, force) {
        if (url === "." || url === "..." || url === "current" || url === "CURRENT") {
            if (force) {
                location.reload(force);
            }
            location.reload();
        } else {
            location.href = url;
        }
        
    }
}

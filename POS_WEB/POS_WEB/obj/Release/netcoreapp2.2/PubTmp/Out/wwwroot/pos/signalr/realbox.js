﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/realbox").build();
var tableTimerGloble = [];
//Table timer
connection.on("TimeWalker", function(response) {
    tableTimerGloble = response;
    //console.log(response);
    $.each(tableTimerGloble, function (key, val) {
        
        $('#' + val.tableTime.id).html(val.tableTime.time);
        if (val.tableTime.status === 'A') {
            $('#' + val.tableTime.id).html("");
            $("#user" + val.tableTime.id).text("");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#CC9");
                    $(grid).find(".grid-image").find(".time").remove();
                }
            });
            $('#' + val.tableTime.id).hide();
        }
        else if (val.tableTime.status === 'B') {
            $('#' + val.tableTime.id).css("color", "white");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#e03454");
                    $(grid).find(".grid-image").find(".time").remove();
                    $(grid).find(".grid-image").append("<div class='time'>"+ val.tableTime.time +"</div>");
                }
            });
        }
        else if (val.tableTime.status === 'P') {
            $("#user" + val.tableTime.id).text("");
            $('#' + val.tableTime.id).css("color", "white");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#50A775");
                }
            });
           
        }
    });

});
//Initail connection
connection.start().then(function () {
    console.log("time connected!");
}).catch(function (err) {
    return console.error(err.toString());
});

﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/appointment").build();

invokeAppointmentSignal();
connection.start().then(function () {
    setInterval(() => {
        invokeAppointmentSignal();
    }, 10000);
}).catch(function (err) {
    return console.error(err.toString());
});

function invokeAppointmentSignal() {
    var wh = new Warehouse();
    $.getJSON("/CRM/SearchAppointmentsByName", function (appointments) {
        for (let app of appointments) {
            if (onDuringStartEndTime(app.StartTime, app.EndTime, app.Reminder)) {
                wh.insert("tb_alerted_appointments", app, "AppointID");
            }
        }
        $("#active-appointments").children().text(wh.table("tb_alerted_appointments").size);
    });
}

function getAlertIcon(app) {
    let icon_alert = "";
    if (onBeforeStartTime(app.StartTime, app.Reminder)) {
        icon_alert = '<i class="fas fa-bell fa-lg fn-green"></i>';
    }

    if (onStartTime(app.StartTime, app.Reminder) || onDuringStartEndTime(app.StartTime, app.EndTime, app.Reminder)) {
        icon_alert = '<i class="fas fa-bell fa-pulse fn-red fa-lg"></i>';
    }

    if (onTimeout(app.EndTime)) {
        icon_alert = '<i class="fas fa-bell-slash fa-lg fn-orange"></i>';
    }

    if (app.Inactive || app.Closed) {
        icon_alert = '<i class="fas fa-bell-slash fa-lg fn-white"></i>';
    }

    return icon_alert;
}

function onDuringStartEndTime(start_time, end_time, reminder) {
    let now = moment(new Date());
    let end = moment(end_time);
    let start = moment(start_time).add(-1, "MINUTES");
    if (!!reminder) {
        let _rem = reminder.split("/");
        start = start.add(-parseFloat(_rem[0]), _rem[1]);
    }

    return moment.duration(now.diff(start)).asMinutes() >= 0 && moment.duration(now.diff(end)).asMinutes() < 0;
}

function onBeforeStartTime(start_time, reminder) {
    let now = moment(new Date());
    let start = moment(start_time).add(-1, "MINUTES");
    if (!!reminder) {
        let _rem = reminder.split("/");
        start = start.add(-parseFloat(_rem[0]), _rem[1]);
    }

    return moment.duration(now.diff(start)).asMinutes() < 0;
}

function onStartTime(start_time, reminder) {
    let now = moment(new Date());
    let start = moment(start_time);
    let due_before = start.add(-1, "MINUTES");
    if (!!reminder) {
        let _rem = reminder.split("/");
        due_before = due_before.add(-parseFloat(_rem[0]), _rem[1]);
    }
    return moment.duration(now.diff(due_before)).asMinutes() >= 0 && moment.duration(now.diff(start)).asMinutes() <= 0;
}

function onTimeout(end_time) {
    let now = moment(new Date());
    let end = moment(end_time);
    return moment.duration(now.diff(end)).asMinutes() >= 0;
}




﻿var ajax_loader = new AjaxLoader("/ajaxloader/loading.gif")
$("#submit-service-customer").on("click", function () {
    let sc_form = $.form("#service-customer-register");
    sc_form.value["Gender"] = sc_form.value["Gender"] ? "M" : "F";
    sc_form.value["DateOfBirth"] = sc_form.dom["DateOfBirth"].getAttribute("data-value");
    console.log(sc_form.value)
    $.ajax({
        url: "/ServiceCustomer/SaveServiceCustomer",
        data: $.antiForgeryToken({ customer: sc_form.value }),
        type: "POST",
        success: function (res) {
            new Message(res);
        }
    });
});

//Search service customers
$("#search-service-customers").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/ServiceCustomer/SearchAllServiceCustomers",
        dataType: "JSON",
        data: { word: this.value },
        success: function (customers) {
            setTimeout(() => {
                $("#service-customer-list").bindRows(customers, "ServiceCustomerID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns:["Address"],
                    prebuild: function (data) {
                        data.Action = '<a href="/ServiceCustomer/ServiceCustomer?cus_id=' + data.ServiceCustomerID + '" class= "fas fa-user-edit fn-orange fa-lg csr-pointer"></a>&nbsp;'
                            + '|&nbsp;<i onclick="deleteServiceCustomer(this)" class="fas fa-minus-circle fa-lg fn-red csr-pointer"></i>';
                            //'<a href="/ServiceCustomer/ServiceAppointment?cus_id=' + data.ServiceCustomerID + '" class= "fas fa-calendar-check fa-lg csr-pointer"></a>&nbsp;'                        
                    },
                    postbuild: function () {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");                     
                    }

                });
            }, 250);

        }
    });

});

function deleteServiceCustomer(self) {
    let row = $(self).parent().parent();
    let remove_sc = new DialogBox({
        type: "yes-no",
        caption: "Delete",
        content: "Are you sure you want to delete " + $("td:nth-child(3)", row).text() + "?"
    });

    remove_sc.confirm(function () {
        ajax_loader.show();  
        $.ajax({
            url: "/ServiceCustomer/ServiceCustomer",
            data: { cus_id: parseInt($(row).data("servicecustomerid")), deleted: true },
            success: function () {
                location.href = "/ServiceCustomer/ServiceCustomer";
            }
        });
    });
}


$("input[data-phone]").on("keydown keyup", function (e) {
    $(this).prop("maxlength", 12);
    if (this.value.startsWith("0")) {
        if (this.value === "00") {
            this.value = "0";
        }
    } else {
        this.value = this.value.substring(0, this.value.length - 1);
    }

    this.value = new libphonenumber.AsYouType('KH').input(this.value);
});

//Regions
$("select[name=RegionID]").on("change", function (e) {
    console.log(this)
    dialogUpdateData("/ServiceCustomer/GetRegions", "/ServiceCustomer/UpdateRegions",
        "RegionID", "Name", this, "Region", false);
    $.ajax({
        url: "/ServiceCustomer/GetDistrictsByRegionID",
        data: { related_id: this.value },
        success: function (results) {
            updateSelect("select[name=DistrictID]", results, "DistrictID", "Name")
        }
    });

    if (this.value !== "0") {
        $("select[name=DistrictID]").prop("disabled", false);
    } else {
        $("select[name=DistrictID]").prop("disabled", true);
    }

});

//Districts
$("select[name=DistrictID]").on("change", function (e) {
    if (this.value !== "0" || this.value !== "-1") {
        let related = $("select[name=RegionID] option:selected").val();
        dialogUpdateData("/ServiceCustomer/GetDistrictsByRegionID", "/ServiceCustomer/UpdateDistricts",
            "DistrictID", "Name", this, "District", false, { "RegionID": related });
        $("select[name=DistrictID]").prop("disabled", false);
    }
});

var selected_value = 0;
function dialogUpdateData(from_url, to_url, key, name, select, dialog_title, has_active, related) {
    if ($(select).val() == "-1") {
        $(select).val(selected_value);
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#dialog-define-new"
            },
            caption: dialog_title,
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let new_index = 1;
            let table = dialog.content.find("table");
            let rel_key = "";
            if (!!related) {
                rel_key = Object.keys(related)[0];
            }
            if (!!has_active) {
                $("tr:first-child th:last-child", table).prop("hidden", false);
            }

            if (!!related) {
                $("tr:first-child th:last-child", table).after("<th style='width: 25px;'>Related</th>");
            }

            let add_new = $("<tr><td class='add-new'></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='Name']").val() !== "") {
                    new_index++;
                    let row = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='form-control create-new'/></td></tr>");
                    if (!!has_active) {
                        row.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        row.append("<td><input name='Related' class='form-control' readonly disabled value='" + related[Object.keys(related)][0] + "'></td>");
                    }
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);
            });

            $.ajax({
                url: from_url,
                dataType: "JSON",
                data: { related_id: !!related ? related[rel_key] : 0 },
                success: function (results) {
                    if (results.length > 0) {
                        let items = [];
                        $.each(results, function (index, data) {
                            index++;
                            let item = {};
                            item[key] = data[key];
                            item["index"] = index;
                            item[name] = "<input name='Name' value='" + data[name] + "' class='form-control' readonly>";

                            if (!!related) {
                                item[rel_key] = "<div name='Related'>" + related[rel_key] + "</div>";
                            }

                            if (!!has_active) {
                                item["active"] = "<input name='Active' type='checkbox'>";

                                if (!!data.Active) {
                                    item.active = "<input name='Active' type=checkbox checked>";
                                }
                            }

                            items.push(item);
                            new_index = index + 1;
                        });

                        $.bindRows(table, items, key, {
                            hide_key: true,
                            text_align: [{ "index": "center" }],
                            dblclick: function (e) {
                                let current_input = $("td input[name=Name]:not(.create-new)", this).prop("readonly", false);
                                let sibling_inputs = current_input.parent().parent().siblings().find("td input[name=Name]:not(.create-new)");
                                sibling_inputs.prop("readonly", true);
                            }
                        });

                        $(document).on("click", function (e) {
                            if (!$(e.target).is(table.find("tr *"))) {
                                table.find("tr td input:not(.create-new)").prop("readonly", true);
                            }

                        });
                    }

                    let create_new = $("<tr data-" + key + "=0><td>" + new_index + "</td><td><input name='Name' class='create-new form-control' /></tr>");

                    if (!!has_active) {
                        create_new.append("<td><input name='Active' type='checkbox' checked></td>");
                    }

                    if (!!related) {
                        create_new.append("<td><div name='Related'>" + related[Object.keys(related)][0] + "</div></td>");
                    }

                    table.append(create_new).append(add_new);

                }
            });

        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table");
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let db = new Warehouse();
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    let item = {};
                    item[key] = $(this).data(key.toLowerCase());
                    if (!!related) {
                        let _key = Object.keys(related);
                        item[_key] = $("div[name=Related]", td).text();
                    }
                    item[name] = $.stripHTML($("input[name=Name]", td).val());
                    if (!!has_active) {
                        item["Active"] = $("input[name=Active]", td).is(":checked");
                    }
                    db.insert("tb_reserved", item, key);
                }

            });
          
            $.ajax({
                url: to_url,
                type: "post",
                dataType: "json",
                data: $.antiForgeryToken({ data: db.from("tb_reserved") }),
                success: function (res) {
                    updateSelect(select, res.Data, key, name);
                }

            });

            this.meta.shutdown();
        });
    }
}

function updateSelect(selector, jsons, key, value) {
    if ($(selector)[0].tagName === "SELECT") {
        $(selector).children().remove();

        let add_new = $("<option value='-1'>Define New</option>");
        $.each(jsons, function (i, json) {
            let option = $("<option value='" + json[key] + "'>" + json[value] + "</option>");
            $(selector).append(option);
        });
        $(selector).append(add_new);
        if ($(selector).children().length <= 1) {
            $(selector).prepend("<option value='0' selected disabled></option>");
        }
    }
}





﻿
let mc_dialog = {};
$("#trigger-motocycle-customers").on("click", function () {
    mc_dialog = new DialogBox({
        content: {
            selector: "#motocycle-customer-list"
        },
        type: "ok-cancel",
        caption: "Motocycle Customers for appointment",
        button: {
            "ok": {
                "text": "Choose"
            },
            "cancel": { callback: function () { this.meta.shutdown(); } }
        }
    });


    mc_dialog.startup("after", function (dialog) {
        let table = dialog.content.find("table");
        searchMotocycleCustomers(table);
        $(".search-motocycle-customers", dialog.content).focus();
        $(".search-motocycle-customers", dialog.content).on("keyup", function () {
            setTimeout(() => {
                searchMotocycleCustomers(table, this.value);
            }, 500);

        });
    });

    mc_dialog.confirm(function (dialog) {
        chooseMotocycleCustomer(chosen_row);
    });
});

let chosen_row;
function searchMotocycleCustomers(table, word) {
    let index = 1;
    let setting = {
        url: "/Customer/SearchMotocycleCustomers",
        dataType: "JSON",
        success: function (customers) {
            console.log(customers);
            $(table).bindRows(customers, "CusID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],               
                postbuild: function () {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    $("td:last-child", this).after("<td><div onclick='chooseMotocycleCustomer($(this).parent().parent()[0]);'"
                        + "class='fas fa-caret-square-down fn-sky fa-lg csr-pointer'></div></td>");
                },
                click: function () {
                    chosen_row = this;
                    $(this).addClass("active").siblings().removeClass("active");
                },
                dblclick: function () {
                    chooseMotocycleCustomer(this);
                }

            });
        }
    };

    if (!!word) {
        setting.data = { word: word };
    }

    $.ajax(setting);
}

function chooseMotocycleCustomer(row) {
    let data = {
        id: $(row).data("cusid"),
        code: row.cells[1].textContent,
        name: row.cells[2].textContent,
        gender: row.cells[3].textContent,
        phone: row.cells[4].textContent,
        activity_date: row.cells[7].textContent,

    };

    $("input[name=CustomerID]").val(data.id);
    $("input[name=CustomerCode]").val(data.code);
    $("input[name=CustomerName]").val(data.name);
    $("input[name=Gender]").val(data.gender);
    $("input[name=Phone]").val(data.phone);
    $("input[name=ActivityDate]").val(data.activity_date);
    mc_dialog.shutdown();
}

//Submit Service Customer Appointment.

$("#submit-mc-appointment").on("click", function () {
    let mca_form = $.form("#motocycle-customer-appointment").value;
    let appointment = {
        AppointmentID: mca_form["AppointmentID"],
        AppointmentCode: mca_form["AppointmentCode"],
        Subject: mca_form["Subject"],
        Remark: mca_form["Remark"],
        ActivityDate: mca_form["ActivityDate"],
        ActivityTime: mca_form["ActivityTime"],
        Reminder: mca_form["Reminder"] + "/" + mca_form["ReminderUom"],
        Location: mca_form["Location"],
        Priority: mca_form["Priority"],
        Paused: mca_form["Paused"],
        Closed: mca_form["Closed"],
        UserID: mca_form["UserID"],
        CustomerID: mca_form["CustomerID"],
        CustomerType: 1,

    };

    $.ajax({
        url: "/Customer/SaveMotocycleAppointment",
        type: "POST",
        data: $.antiForgeryToken({ appointment: appointment }),
        success: function (res) {
            countAlertableAppointments();
            new Message(res);
            if (res.Type === "confirm") {
                setTimeout(() => {
                    location.href = "/Customer/MotocycleAppointment";
                }, 2500);
            }
        }
    });
});

//Search motocycle customer appointments
$("#search-motocycle-appointments").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/Customer/SearchMotocycleAppointments",
        dataType: "JSON",
        data: { word: this.value },
        success: function (appointments) {          
            setTimeout(() => {
                $("#motocycle-appointment-list").bindRows(appointments, "AppointmentID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns: ["CustomerID", "CustomerCode", "Closed", "ActivityTime"],
                    prebuild: function (data) {
                        data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
                        data.Paused = data.Paused ? "<div class='pseudo-check cross'></div>" : "<div class='pseudo-check none'></div>"
                    },
                    postbuild: function (data) {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                        let td = $("<td></td>")
                            .append('<a title="Detail" href="/Customer/MotocycleAppointmentDetail?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '" class="fas fa-info-circle fn-sky fa-lg csr-pointer"></a>&nbsp;|&nbsp;')
                            .append('<a title="Edit" href="/Customer/MotocycleAppointment?appoint_id=' + data.AppointmentID + '&cus_id=' + data.CustomerID + '" class="fas fa-edit fn-orange fa-lg csr-pointer"></a>');
                        $("td:last-child", this).after(td);
                    },

                });
            }, 250);

        }
    });
});


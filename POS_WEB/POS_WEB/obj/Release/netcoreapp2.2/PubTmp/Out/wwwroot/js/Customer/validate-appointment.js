﻿let msg = new Message();

//$("input[type='date']").on("change", function () {
//    this.setAttribute(
//        "data-date",
//        moment(this.value)
//            .format(this.getAttribute("data-date-format"))
//    );
//});

//$("input[data-date], input[data-time]").on("change", changeDatetime);

$("input[name=Reminder]").on("keyup change", function (e) {
    if (!this.value) {
        this.value = "0";
    }
    this.value = _$_.validNumber(this.value);
});

$("input[data-time]").on("keyup", function (e) {
    this.value = validTime(this.value);
});

if ($("#unlimited-time").is(":checked")) {
    $("#unlimited-time-block *").prop("disabled", true);
} 
$("#unlimited-time").on("change", function () {
    $("#unlimited-time-block *").prop("disabled", false);
    if ($(this).is(":checked")) {
        $("#unlimited-time-block *").prop("disabled", true);
    } 
    changeDatetime();
});

$("select").each(function (i, e) {
    if (!!$(this).data("value")) {
        $(this).val($(this).data("value"))
    }
});

//setDate("input[name=ActivityDate]", $("input[name=ActivityDate]").val());
setReminder($("input[name=Reminder]").data("value"));

function setReminder(value) {
    
    if (!!value && typeof value === "string" && value.includes("/")) {
        let _value = value.split("/");
        $("input[name=Reminder]").val(_value[0]);
        $("select[name=ReminderUom").val(_value[1]);
    }
}

function setDate(selector, date_value) {
    $(selector)[0].valueAsDate = new Date(date_value);
    $(selector)[0].setAttribute(
        "data-date",
        moment($(selector)[0].valueAsDate)
            .format($(selector)[0].getAttribute("data-date-format"))
    );
}

function changeDatetime() {
    if ($(this).is($("input[data-time]"))) {
        if (!isValidTime(this.value)) {
            msg.add($(this).attr("name"), $(this).attr("name") + " is not valid.");
        } else {
            msg.remove([$(this).attr("name")]);
        }
    }
    
    $("#submit-sc-appointment").prop("disabled", false).removeClass("csr-no-drop");
    if (Object.getOwnPropertyNames(msg.data).length > 0) {
        $("#submit-sc-appointment").prop("disabled", true).addClass("csr-no-drop");
    }
}

function validTime(value) {
    if (!(/^(\d)+[:]?\d*$/).test(value)) {
        value = value.toString().substring(0, value.length - 1);
    }

    return value;
}

function getDuration(diff) {
    let minutes = 0;
    minutes = moment.duration(diff).asMinutes();

    if (minutes < 60) {
        return minutes + "/MINUTES";
    } else if (minutes < 60 * 24) {
        return minutes / 60 + "/HOURS";
    } else {
        return minutes / (60 * 24) + "/DAYS";
    }
}


function defaultStartTime() {
    let now = new Date();
    let start_time = now.getHours() + ":" + now.getMinutes();
    return timeFormat(moment.duration(start_time).asMinutes());
}

function defaultEndTime(min_from_now) {
    let start_time = defaultStartTime();
    let time = moment.duration(start_time).asMinutes() + parseFloat(min_from_now);
    return timeFormat(time);
}

function fillZero(value) {
    let result = "";
    if (parseFloat(value) >= 10) {
        result = parseFloat(value);
    } else {
        result = "0" + parseFloat(value);
    }
    return result;
}

function timeFormat(minutes) {
    let HH = 0, mm = 0;
    let result = "";
    if (minutes > 60) {
        HH = parseInt(minutes / 60);

        if (minutes % 60 !== 0) {
            mm = minutes % 60;
        }
    } else {
        mm = minutes;
    }

    return fillZero(HH) + ":" + fillZero(mm);
}

function isValidTime(value) {
    return (/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/).test(value);
};

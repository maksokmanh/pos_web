#pragma checksum "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4d181f4c2ad02d9619fa6d1086bdffa37cdab5cb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ServiceCustomer_ServiceCustomerDetail), @"mvc.1.0.view", @"/Views/ServiceCustomer/ServiceCustomerDetail.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ServiceCustomer/ServiceCustomerDetail.cshtml", typeof(AspNetCore.Views_ServiceCustomer_ServiceCustomerDetail))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 18 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 19 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ServicesClass;

#line default
#line hidden
#line 20 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
#line 21 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Purchase;

#line default
#line hidden
#line 22 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ReportClass;

#line default
#line hidden
#line 23 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
using POS_WEB.Models.Services.Customer;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4d181f4c2ad02d9619fa6d1086bdffa37cdab5cb", @"/Views/ServiceCustomer/ServiceCustomerDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5dac942b91f95f9cdaba70fb3bf32582e3bf7543", @"/Views/_ViewImports.cshtml")]
    public class Views_ServiceCustomer_ServiceCustomerDetail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<POS_WEB.Models.Services.Customer.CustomerDetail>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(99, 6, true);
            WriteLiteral("\r\n<h1>");
            EndContext();
            BeginContext(106, 36, false);
#line 4 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
Write(Localizer["Service Customer Detail"]);

#line default
#line hidden
            EndContext();
            BeginContext(142, 110, true);
            WriteLiteral("</h1>\r\n<div class=\"flexbox flex-end\">\r\n    <a href=\"/ServiceCustomer/ServiceCustomerList\" class=\"btn btn-info\"");
            EndContext();
            BeginWriteAttribute("title", " title=\"", 252, "\"", 299, 1);
#line 6 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
WriteAttributeValue("", 260, Localizer["List of service customers"], 260, 39, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(300, 101, true);
            WriteLiteral("><i class=\"fas fa-list fa-lg\"></i></a>\r\n</div>\r\n\r\n<div class=\"block border\">\r\n    <div class=\"title\">");
            EndContext();
            BeginContext(402, 33, false);
#line 10 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                  Write(Localizer["Customer Information"]);

#line default
#line hidden
            EndContext();
            BeginContext(435, 162, true);
            WriteLiteral("</div>\r\n    <div class=\"wrap-block col-two\">\r\n        <div class=\"left-block\">\r\n            <table class=\"detail\">\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(598, 26, false);
#line 15 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Customer Code"]);

#line default
#line hidden
            EndContext();
            BeginContext(624, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(656, 10, false);
#line 16 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Code);

#line default
#line hidden
            EndContext();
            BeginContext(666, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(745, 22, false);
#line 20 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Full Name"]);

#line default
#line hidden
            EndContext();
            BeginContext(767, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(799, 10, false);
#line 21 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(809, 76, true);
            WriteLiteral("</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(886, 19, false);
#line 24 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Gender"]);

#line default
#line hidden
            EndContext();
            BeginContext(905, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(937, 12, false);
#line 25 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Gender);

#line default
#line hidden
            EndContext();
            BeginContext(949, 76, true);
            WriteLiteral("</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1026, 26, false);
#line 28 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Date of Birth"]);

#line default
#line hidden
            EndContext();
            BeginContext(1052, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1084, 15, false);
#line 29 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.BirthDate);

#line default
#line hidden
            EndContext();
            BeginContext(1099, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1178, 18, false);
#line 33 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Phone"]);

#line default
#line hidden
            EndContext();
            BeginContext(1196, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1228, 11, false);
#line 34 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Phone);

#line default
#line hidden
            EndContext();
            BeginContext(1239, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1318, 18, false);
#line 38 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Email"]);

#line default
#line hidden
            EndContext();
            BeginContext(1336, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1368, 11, false);
#line 39 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Email);

#line default
#line hidden
            EndContext();
            BeginContext(1379, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1458, 20, false);
#line 43 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Address"]);

#line default
#line hidden
            EndContext();
            BeginContext(1478, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1510, 13, false);
#line 44 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Address);

#line default
#line hidden
            EndContext();
            BeginContext(1523, 207, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n            </table>\r\n        </div>\r\n      \r\n        <div class=\"right-block border-left\">\r\n            <table class=\"detail\">\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1731, 26, false);
#line 53 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Customer Type"]);

#line default
#line hidden
            EndContext();
            BeginContext(1757, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1789, 18, false);
#line 54 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.CustomerType);

#line default
#line hidden
            EndContext();
            BeginContext(1807, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1886, 27, false);
#line 58 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Customer Group"]);

#line default
#line hidden
            EndContext();
            BeginContext(1913, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1945, 19, false);
#line 59 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.CustomerGroup);

#line default
#line hidden
            EndContext();
            BeginContext(1964, 32, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n");
            EndContext();
#line 62 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                 if (Model.CustomerType != CustomerType.WALKIN)
                {

#line default
#line hidden
            BeginContext(2080, 54, true);
            WriteLiteral("                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(2135, 18, false);
#line 65 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Localizer["Plate"]);

#line default
#line hidden
            EndContext();
            BeginContext(2153, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(2189, 11, false);
#line 66 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Model.Plate);

#line default
#line hidden
            EndContext();
            BeginContext(2200, 34, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n");
            EndContext();
            BeginContext(2236, 54, true);
            WriteLiteral("                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(2291, 18, false);
#line 70 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Localizer["Frame"]);

#line default
#line hidden
            EndContext();
            BeginContext(2309, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(2345, 11, false);
#line 71 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Model.Frame);

#line default
#line hidden
            EndContext();
            BeginContext(2356, 88, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(2445, 19, false);
#line 74 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Localizer["Engine"]);

#line default
#line hidden
            EndContext();
            BeginContext(2464, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(2500, 12, false);
#line 75 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                       Write(Model.Engine);

#line default
#line hidden
            EndContext();
            BeginContext(2512, 34, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n");
            EndContext();
#line 77 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                }

#line default
#line hidden
            BeginContext(2565, 48, true);
            WriteLiteral("\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(2614, 19, false);
#line 80 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["Region"]);

#line default
#line hidden
            EndContext();
            BeginContext(2633, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2665, 12, false);
#line 81 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.Region);

#line default
#line hidden
            EndContext();
            BeginContext(2677, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(2756, 21, false);
#line 85 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Localizer["District"]);

#line default
#line hidden
            EndContext();
            BeginContext(2777, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2809, 14, false);
#line 86 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\ServiceCustomer\ServiceCustomerDetail.cshtml"
                   Write(Model.District);

#line default
#line hidden
            EndContext();
            BeginContext(2823, 92, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n\r\n            </table>\r\n        </div>\r\n    </div>\r\n\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<POS_WEB.Models.Services.Customer.CustomerDetail> Html { get; private set; }
    }
}
#pragma warning restore 1591

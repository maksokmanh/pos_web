#pragma checksum "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fa814e7c30a24ad00fb853c4b1602f1f1c8d68bd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Customer_MotocycleAppointmentDetail), @"mvc.1.0.view", @"/Views/Customer/MotocycleAppointmentDetail.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Customer/MotocycleAppointmentDetail.cshtml", typeof(AspNetCore.Views_Customer_MotocycleAppointmentDetail))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 18 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 19 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ServicesClass;

#line default
#line hidden
#line 20 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
#line 21 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Purchase;

#line default
#line hidden
#line 22 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ReportClass;

#line default
#line hidden
#line 23 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
using POS_WEB.Models.Services.Customer;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fa814e7c30a24ad00fb853c4b1602f1f1c8d68bd", @"/Views/Customer/MotocycleAppointmentDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5dac942b91f95f9cdaba70fb3bf32582e3bf7543", @"/Views/_ViewImports.cshtml")]
    public class Views_Customer_MotocycleAppointmentDetail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<POS_WEB.Models.Services.Appointment.AppointmentViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(108, 78, true);
            WriteLiteral("<div class=\"flexbox space-between align-items-center\">\r\n    <h1 class=\"title\">");
            EndContext();
            BeginContext(187, 41, false);
#line 4 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                 Write(Localizer["Motocycle Appointment Detail"]);

#line default
#line hidden
            EndContext();
            BeginContext(228, 82, true);
            WriteLiteral("</h1>\r\n    <a href=\"/Customer/MotocycleTimelyAppointmentList\" class=\"btn btn-info\"");
            EndContext();
            BeginWriteAttribute("title", " title=\"", 310, "\"", 362, 1);
#line 5 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
WriteAttributeValue("", 318, Localizer["List of Motocycle Appointments"], 318, 44, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(363, 99, true);
            WriteLiteral("><i class=\"fas fa-list fa-lg\"></i></a>\r\n</div>\r\n<div class=\"block border\">\r\n    <div class=\"title\">");
            EndContext();
            BeginContext(463, 33, false);
#line 8 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                  Write(Localizer["Customer Information"]);

#line default
#line hidden
            EndContext();
            BeginContext(496, 162, true);
            WriteLiteral("</div>\r\n    <div class=\"wrap-block col-two\">\r\n        <div class=\"left-block\">\r\n            <table class=\"detail\">\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(659, 26, false);
#line 13 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Localizer["Customer Code"]);

#line default
#line hidden
            EndContext();
            BeginContext(685, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(717, 19, false);
#line 14 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Model.Customer.Code);

#line default
#line hidden
            EndContext();
            BeginContext(736, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(815, 26, false);
#line 18 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Localizer["Customer Type"]);

#line default
#line hidden
            EndContext();
            BeginContext(841, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(873, 27, false);
#line 19 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Model.Customer.CustomerType);

#line default
#line hidden
            EndContext();
            BeginContext(900, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(979, 22, false);
#line 23 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Localizer["Full Name"]);

#line default
#line hidden
            EndContext();
            BeginContext(1001, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1033, 19, false);
#line 24 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Model.Customer.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1052, 76, true);
            WriteLiteral("</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1129, 19, false);
#line 27 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Localizer["Gender"]);

#line default
#line hidden
            EndContext();
            BeginContext(1148, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1180, 21, false);
#line 28 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Model.Customer.Gender);

#line default
#line hidden
            EndContext();
            BeginContext(1201, 78, true);
            WriteLiteral("</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(1280, 18, false);
#line 32 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                   Write(Localizer["Phone"]);

#line default
#line hidden
            EndContext();
            BeginContext(1298, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
#line 33 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                     if (Model.Customer.Phone != "")
                    {

#line default
#line hidden
            BeginContext(1382, 28, true);
            WriteLiteral("                        <td>");
            EndContext();
            BeginContext(1411, 20, false);
#line 35 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Model.Customer.Phone);

#line default
#line hidden
            EndContext();
            BeginContext(1431, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
#line 36 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                    }
                    else
                    {

#line default
#line hidden
            BeginContext(1510, 28, true);
            WriteLiteral("                        <td>");
            EndContext();
            BeginContext(1539, 26, false);
#line 39 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Localizer["No phone yet."]);

#line default
#line hidden
            EndContext();
            BeginContext(1565, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
#line 40 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                    }

#line default
#line hidden
            BeginContext(1595, 146, true);
            WriteLiteral("                </tr>\r\n\r\n            </table>\r\n        </div>\r\n        <div class=\"right-block border-left\">\r\n            <table class=\"detail\">\r\n");
            EndContext();
#line 47 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                 if (Model.Customer.CustomerType != CustomerType.WALKIN)
                {

#line default
#line hidden
            BeginContext(1834, 54, true);
            WriteLiteral("                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(1889, 18, false);
#line 50 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Localizer["Plate"]);

#line default
#line hidden
            EndContext();
            BeginContext(1907, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(1943, 20, false);
#line 51 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Model.Customer.Plate);

#line default
#line hidden
            EndContext();
            BeginContext(1963, 34, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n");
            EndContext();
            BeginContext(1999, 54, true);
            WriteLiteral("                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(2054, 18, false);
#line 55 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Localizer["Frame"]);

#line default
#line hidden
            EndContext();
            BeginContext(2072, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(2108, 20, false);
#line 56 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Model.Customer.Frame);

#line default
#line hidden
            EndContext();
            BeginContext(2128, 88, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(2217, 19, false);
#line 59 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Localizer["Engine"]);

#line default
#line hidden
            EndContext();
            BeginContext(2236, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(2272, 21, false);
#line 60 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(Model.Customer.Engine);

#line default
#line hidden
            EndContext();
            BeginContext(2293, 34, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n");
            EndContext();
#line 62 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                }

#line default
#line hidden
            BeginContext(2346, 188, true);
            WriteLiteral("            </table>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n<div class=\"wrap-block myt1\">\r\n    <div class=\"full-block\">\r\n        <div class=\"block border\">\r\n            <div class=\"title\">");
            EndContext();
            BeginContext(2535, 33, false);
#line 71 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                          Write(Localizer["History of Follow up"]);

#line default
#line hidden
            EndContext();
            BeginContext(2568, 76, true);
            WriteLiteral("</div>\r\n            <table id=\"list-appointment-histories\" class=\"detail\">\r\n");
            EndContext();
#line 73 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                 foreach (var app in Model.AppointmentHistories)
                {

#line default
#line hidden
            BeginContext(2729, 89, true);
            WriteLiteral("                    <tr>\r\n                        <td>\r\n                            <div>");
            EndContext();
            BeginContext(2819, 44, false);
#line 77 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                            Write(app.FollowUp.ToString("dd/MM/yyyy HH:mm tt"));

#line default
#line hidden
            EndContext();
            BeginContext(2863, 123, true);
            WriteLiteral("</div>                          \r\n                        </td>\r\n                        <td>\r\n                            ");
            EndContext();
            BeginContext(2987, 10, false);
#line 80 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                       Write(app.Remark);

#line default
#line hidden
            EndContext();
            BeginContext(2997, 60, true);
            WriteLiteral("\r\n                        </td>\r\n                    </tr>\r\n");
            EndContext();
#line 83 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\pos_web\POS_WEB\POS_WEB\Views\Customer\MotocycleAppointmentDetail.cshtml"
                }

#line default
#line hidden
            BeginContext(3076, 60, true);
            WriteLiteral("            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<POS_WEB.Models.Services.Appointment.AppointmentViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591

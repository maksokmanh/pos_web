﻿
let sc_dialog = {};
$("#trigger-service-customers").on("click", function () {
    sc_dialog = new DialogBox({
        content: {
            selector: "#service-customer-list"
        },
        type: "ok-cancel",
        caption: "Service customers for appointment",
        button: {
            "ok": {
                "text": "Choose"
            },
            "cancel": { callback: function() { this.meta.shutdown(); } }
        }
    });

    
    sc_dialog.startup("after", function (dialog) {
        let table = dialog.content.find("table");
        searchServiceCustomers(table);
        $(".search-service-customers", dialog.content).focus();
        $(".search-service-customers", dialog.content).on("keyup", function () {
            setTimeout(() => {
                searchServiceCustomers(table, this.value);
            }, 500);
           
        });
    });
  
    sc_dialog.confirm(function (dialog) {
        chooseServiceCustomer(chosen_row);
    });
});

let chosen_row;
function searchServiceCustomers(table, word) {
    let index = 1;
    let setting = {
        url: "/ServiceCustomer/SearchServiceCustomers",
        dataType: "JSON",
        success: function (customers) {          
            $(table).bindRows(customers, "ServiceCustomerID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                postbuild: function () {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    $("td:last-child", this).after("<td><div onclick='chooseServiceCustomer($(this).parent().parent()[0]);'"
                       + "class='fas fa-caret-square-down fn-sky fa-lg csr-pointer'></div></td>");
                },
                click: function () {
                    chosen_row = this;
                    $(this).addClass("active").siblings().removeClass("active");
                },
                dblclick: function () {
                    chooseServiceCustomer(this);
                }

            });
        }
    };
 
    if (!!word) {
        setting.data = { word: word};
    } 

    $.ajax(setting);
}

function chooseServiceCustomer(row) {
    let data = {
        id: $(row).data("servicecustomerid"),
        code: row.cells[1].textContent,
        name: row.cells[2].textContent,
        gender: row.cells[3].textContent,
        phone: row.cells[4].textContent,
    };

    $("input[name=CustomerID]").val(data.id);
    $("input[name=CustomerCode]").val(data.code);
    $("input[name=CustomerName]").val(data.name);
    $("input[name=Gender]").val(data.gender);
    $("input[name=Phone]").val(data.phone);
    sc_dialog.shutdown();
}

//Save follow-up service appoitment.
$("#save-service-appointment").on("click", function () {
    let $submit = $(this);
    let sca_form = $.form("#service-customer-appointment");
    let item_services = [];
    $.each($(".ServiceID", "#table-item-service-list"), function (i) {     
        if ($(this).is(":checked")) {
            item_services.push(this.value);
        }
    });
    let appointment = {
        AppointmentID: sca_form.value["AppointmentID"],
        AppointmentCode: sca_form.value["AppointmentCode"],     
        Remark: sca_form.value["Remark"],
        ActivityDate: $(sca_form.dom["ActivityDate"]).attr("data-value"),
        ActivityTime: sca_form.value["ActivityTime"],      
        Reminder: sca_form.value["Reminder"] + "/" + sca_form.value["ReminderUom"],
        Location: sca_form.value["Location"],
        Priority: sca_form.value["Priority"],
        Paused: sca_form.value["Paused"],
        Closed: sca_form.value["Closed"],
        CustomerID: sca_form.value["CustomerID"],
        ItemServices: item_services.join("/")
    };
    console.log(appointment)
    $.ajax({
        url: "/ServiceCustomer/SaveServiceAppointment",
        data: $.antiForgeryToken({ appointment: appointment }),
        type: "POST",
        success: function (res) {        
            new ViewMessage(res);
            if (res.Action == "1") {
                $submit.prop("disabled", true).css("cursor", "default");
                let _confirm = new ViewMessage({
                    summary: {
                        attribute: "ck-message-summary"
                    }
                }, res);
                _confirm.redirect("/ServiceCustomer/ServiceTimelyAppointmentList", 2000);
            }
        }
    });
    
});

//Search service customer appointments
$("#search-service-timely-appointments").on("keyup", function () {
    setTimeout(() => {
        setTimelyAppointmentList("/ServiceCustomer/ServiceTimelyAppointmentList", this.value);
    }, 500);
});

//Filter general service appointments appointment by priority
$("#filter-general-by-priority").on("change", function () {
    setGeneralAppointmentList("/ServiceCustomer/ServiceGeneralAppointmentList", "", this.value);
});

//Search service customer appointments
$("#search-service-general-appointments").on("keyup", function () {
    $("#filter-by-priority").val("ALL");
    setTimeout(() => {
        setGeneralAppointmentList("/ServiceCustomer/ServiceGeneralAppointmentList", this.value);
    }, 250);
});

$("ul#nav-service-appointment-list li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    let content = $("#content-appointment-list");
    if ($(this).is($("#timely"))) {
        $("#timely-appointment", content).prop("hidden", false).siblings().prop("hidden", true);
    }

    if ($(this).is($("#general"))) {
        $("#filter-by-priority").val("ALL");
        $("#general-appointment", content).prop("hidden", false).siblings().prop("hidden", true);     
        setGeneralAppointmentList("/ServiceCustomer/ServiceGeneralAppointmentList");
    }
});

function setTimelyAppointmentList(url, keyword, priority) {
    let index = 1;
    let option = {
        url: url,
        dataType: "JSON",
        data: { asJSON: true, word: keyword },
        success: function (appointments) {
            $("#service-timely-appointment-list").bindRows(appointments, "AppointmentID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                hidden_columns: ["Closed", "ActivityTime", "Paused", "CustomerID", "CustomerType", "ItemServices"],
                prebuild: function (data) {
                    data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
                    data.Paused = data.Paused ? "<div class='pseudo-check cross'></div>" : "<div class='pseudo-check none'></div>"
                },
                postbuild: function (data) {
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    let td = $("<td></td>")
                        .append('<a title="Follow up" href="/ServiceCustomer/ServiceAppointmentFollowUp?appointmentID=' + data.AppointmentID + '"class="fas fa-chevron-circle-right fn-green fa-lg csr-pointer"></a>&nbsp;|&nbsp;')
                        .append('<a title="Detail" href="/ServiceCustomer/ServiceAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>');
                    $("td:last-child", this).after(td);
                },

            });

        }
    };

    if (!!priority) {
        option.data = { asJSON: true, word: keyword, priority: priority };
    }
    $.ajax(option);
}

function setGeneralAppointmentList(url, keyword, priority) {
    let index = 1;
    let setting = {
        text_align: [{ "Name": "left" }, { "Code": "center" }],
        hidden_columns: ["Closed", "ActivityTime", "CustomerID", "CustomerType", "ItemServices", "Paused"],
        prebuild: function (data) {
            data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
            //data.Paused = data.Paused ? "<i class='fas fa-times fa-lg fn-red'></i>" : "<i class='fas fa-check fn-green'></i>";
        },
        postbuild: function (data) {
            let row = this;
            $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
            let _pause = (data.Paused) ? $('<i title="Activate" class="fas fa-pause-circle fn-orange fa-lg csr-pointer"></i>')
                : $('<i title="Pause" class="fas fa-play-circle fn-green fa-lg csr-pointer"></i>');
            let td = $("<td></td>")
                .append(_pause.on("click", function () { toggleAppointment($("td[cell-paused]", row), this); }))
                .append('&nbsp;|&nbsp;<a title="Detail" href="/ServiceCustomer/ServiceAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>');
            $("td:last-child", this).after(td);

        }
    };
    let option = {
        url: url,
        success: function (items) {
            $("#service-general-appointment-list").bindRows(items, "AppointmentID", setting);
        }
    };

    if (!!keyword) {
        option.data = { word: keyword };
    }

    if (!!priority) {
        option.data = {word: keyword, priority: priority };
    }

    $.ajax(option);
}

//Pause service appointment
let toggle_dialog = null;
function toggleAppointment(paused, self) {
    let id = $(self).parent().parent().data("appointmentid");

    if (paused.text() === "false") {
        _data = { appointmentID: id, paused: true };
        toggle_dialog = new DialogBox({
            content: "Are you sure you want to pause this appointment, this will not alert you on timely unless you set it active? ",
            type: "yes-no",
            icon: "warning"
        });
            
    } else {
        _data = { appointmentID: id, paused: false };
        toggle_dialog = new DialogBox({
            content: "Will you set this appointment to active?",
            type: "ok-cancel"
        });
    }

    toggle_dialog.confirm(function () {
        $.ajax({
            url: "/ServiceCustomer/ToggleAppointment", 
            data: _data,
            type: "POST", 
            success: function (data) {
                location.reload();
            }
        });
    });
}


﻿var $ajax_loader = new AjaxLoader("/ajaxloader/loading.gif");

let sc_dialog = {};
//Search motocycle customer appointments
$("#search-motocycle-timely-appointments").on("keyup", function () {
    setTimeout(() => {
        let index = 1;
        $.ajax({
            url: "/Customer/MotocycleTimelyAppointmentList",
            dataType: "JSON",
            data: { asJSON: true, word: this.value },
            success: function (appointments) {
                $("#motocycle-timely-appointment-list").bindRows(appointments, "AppointmentID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns: ["Closed", "ActivityTime", "Paused", "CustomerID", "CustomerType", "ItemServices"],
                    prebuild: function (data) {
                        data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
                        data.Paused = data.Paused ? "<i class='fas fa-times fa-lg fn-red'></i>" : "<i class='fas fa-check fn-green'></i>";
                    },
                    postbuild: function (data) {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                        let td = $("<td></td>")
                            .append('<a title="Follow up" href="/ServiceCustomer/ServiceAppointmentFollowUp?appointmentID=' + data.AppointmentID + '&customerID='+ data.CustomerID +'"class="fas fa-chevron-circle-right fn-green fa-lg csr-pointer"></a>&nbsp;|&nbsp;')
                            .append('<a title="Detail" href="/ServiceCustomer/ServiceAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>')
                        $("td:last-child", this).after(td);
                    },

                });

            }
        });
    }, 500);
});

//Search motocycle customer appointments
$("#search-motocycle-general-appointments").on("keyup", function () {
    setTimeout(() => {
        setAppointmentList("/Customer/MotocycleGeneralAppointmentList", this.value);
    }, 250); 
});

//Search motocycle customer appointments
$("#filter-general-by-priority").on("change", function () {
    setAppointmentList("/Customer/MotocycleGeneralAppointmentList","", this.value);
});


$("ul#nav-motocycle-appointment-list li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    let content = $("#content-appointment-list");
    if ($(this).is($("#timely"))) {
        $("#timely-appointment", content).prop("hidden", false).siblings().prop("hidden", true);
    }

    if ($(this).is($("#general"))) {
        $("#general-appointment", content).prop("hidden", false).siblings().prop("hidden", true);
        setAppointmentList("/Customer/MotocycleGeneralAppointmentList");
    }
});

function setAppointmentList(url, keyword, priority) {
    let index = 1;
    let option = {
        url: url,
        success: function (items) {
            $("#motocycle-general-appointment-list").bindRows(items, "AppointmentID", {
                text_align: [{ "Name": "left" }, { "Code": "center" }],
                hidden_columns: ["Closed", "ActivityTime", "CustomerID", "CustomerType", "ItemServices", "Paused"],
                prebuild: function (data) {
                    data.ActivityDate = data.ActivityDate + " / " + data.ActivityTime;
                    //data.Paused = data.Paused ? "<i class='fas fa-times fa-lg fn-red'></i>" : "<i class='fas fa-check fn-green'></i>"
                },
                postbuild: function (data) {
                    let row = this;
                    $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                    let _pause = (data.Paused) ? $('<i title="Activate" class="fas fa-pause-circle fn-orange fa-lg csr-pointer"></i>')
                        : $('<i title="Pause" class="fas fa-play-circle fn-green fa-lg csr-pointer"></i>');
                    let td = $("<td></td>")
                        .append(_pause.on("click", function () { toggleAppointment($("td[cell-paused]", row), this); }))
                        .append('&nbsp;|&nbsp;<a title="Detail" href="/Customer/MotocycleAppointmentDetail?customerID=' + data.CustomerID + '" class="fas fa-history fn-sky fa-lg csr-pointer"></a>');
                    $("td:last-child", this).after(td);
                },
            });

        }
    }

    if (!!keyword) {
        option.data = { word: keyword };
    }

    if (!!priority) {
        option.data = { word: keyword, priority: priority };
    }

    $.ajax(option);
}

//Toggle Motocycle Appointment (Pause/Activate)
let toggle_dialog = null;
function toggleAppointment(paused, self) {
    let id = $(self).parent().parent().data("appointmentid");

    if (paused.text() === "false") {
        _data = { appointmentID: id, paused: true };
        toggle_dialog = new DialogBox({
            content: "Are you sure you want to pause this appointment, this will not alert you on timely unless you set it active? ",
            type: "yes-no",
            icon: "warning"
        });

    } else {
        _data = { appointmentID: id, paused: false };
        toggle_dialog = new DialogBox({
            content: "Will you set this appointment to active?",
            type: "ok-cancel"
        });
    }

    toggle_dialog.confirm(function () {
        $.ajax({
            url: "/Customer/ToggleAppointment",
            data: _data,
            type: "POST",
            success: function (data) {
                location.reload();
            }
        });
    });
}

//Save follow-up motocycle appoitment.
$("#save-motocycle-appointment").on("click", function () {
    let $submit = $(this);
    let sca_form = $.form("#motocycle-customer-appointment");
   
    let appointment = {
        AppointmentID: sca_form.value["AppointmentID"],
        AppointmentCode: sca_form.value["AppointmentCode"],
        Remark: sca_form.value["Remark"],
        ActivityDate: $(sca_form.dom["ActivityDate"]).attr("data-value"),
        ActivityTime: sca_form.value["ActivityTime"],
        Reminder: sca_form.value["Reminder"] + "/" + sca_form.value["ReminderUom"],
        Location: sca_form.value["Location"],
        Priority: sca_form.value["Priority"],
        Paused: sca_form.value["Paused"],
        Closed: sca_form.value["Closed"],
        CustomerID: sca_form.value["CustomerID"]       
    };

    $.ajax({
        url: "/Customer/SaveMotocycleAppointment",
        data: $.antiForgeryToken({ appointment: appointment }),
        type: "POST",
        success: function (res) {
            new ViewMessage(res);
            if (res.Action == "1") {
                $submit.prop("disabled", true).css("cursor", "default");
                let _confirm = new ViewMessage({
                    summary: {
                        attribute: "ck-message-summary"
                    }
                }, res);
                _confirm.redirect("/Customer/MotocycleTimelyAppointmentList", 2000);
            }
        }
    });

});
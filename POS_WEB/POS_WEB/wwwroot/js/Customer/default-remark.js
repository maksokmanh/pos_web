﻿

//Default remark master data
function changeDefaultRemark(self) {
    let row = $(self).parent().parent();
    $("#RemarkID").val(row.data("remarkid"));
    $("#RemarkCode").val($(".RemarkCode", row).text());
    $("#RemarkText").val($(".RemarkText", row).text());    
    $("#remark-title").text("Edit Default Remark");
    window.scrollTo({
        top: 100,
        behavior: 'smooth'
    });
}

//Search default remarks
$("#search-default-remarks").on("keyup", function () {
    let index = 1;
    $.ajax({
        url: "/Customer/SearchDefaultRemarks",
        dataType: "JSON",
        data: { word: this.value },
        success: function (remarks) {
            setTimeout(() => {
                $("#default-remark-list").bindRows(remarks, "RemarkID", {
                    text_align: [{ "Name": "left" }, { "Code": "center" }],
                    hidden_columns: ["Deleted"],
                    prebuild: function (data) {
                        data.RemarkCode = "<div class=RemarkCode>" + data.RemarkCode + "</div>";
                        data.RemarkText = "<div class=RemarkText>" + data.RemarkText + "</div>";
                        data.LastChanged = moment(data.LastChanged).format("DD/MM/YYYY hh:mm A");
                    },
                    postbuild: function (data) {
                        $("td:first-child", this).replaceWith("<td>" + index++ + "</td>");
                        let td = $('<td><i onclick="changeDefaultRemark(this);" class="trigger-edit fas fa-edit fa-lg fn-sky csr-pointer"></i></td>');
                        $("td:last-child", this).after(td);
                    }
                });
            }, 250);

        }
    });
});
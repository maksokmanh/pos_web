﻿;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) : global.ViewMessage = factory;
}(this,
    (class ViewMessage {
        constructor(config, values) {      
            this._setting = {
                summary: {
                    title: {
                        text: "Validation Summary",
                        text_align: "left",
                        font_size: "1rem",
                        font_weight: "bold",
                        color: "",
                        background_color: "#C0D0E0"
                    },
                    content: {
                        symbol: "",
                        color: "",
                        background_color: "#C9D9E9"
                    },
                    selector: "",
                    attribute: "",
                },
                for: {
                    attribute: "model-validation-for",
                    color: ""
                },
                model: {
                    action: "-1" | "reject", //{REJECT = -1, ALERT = 0, CONFIRM = 1 }
                    data: {},
                    redirect: "" //{ CURRENT = 0 }
                },
                autoclose: {
                    delay: 1000,
                    duration: 5000,
                    action: "1" | "confirm", //{REJECT = -1, ALERT = 0, CONFIRM = 1}
                    callback: function (self_object, self_dom) { }
                }
            }


            if (!values) {
                if (!!config && typeof config === "string") {
                    this.setting.summary.selector = config;
                }

                this.map(config);
            } else {
                this.map(values);
            }

            this.setting = config;

            setTimeout(() => {
                this.build();
            }, 0);
        }

        set setting(value) {
            if (!!value && value.constructor === Object && Object.keys(value).length > 0) {
                this._setting = $.extend(true, {}, this._setting, value);
            }
        }

        get setting() { return this._setting; }

        set data(value) {

            if (!!value) {
                if (Array.isArray(value) && value.length > 0) {
                    for (let item of value) {
                        this.data = item;
                    }
                }

                if (value.constructor === Object && Object.getOwnPropertyNames(value).length > 0) {
                    for (let p of Object.getOwnPropertyNames(value)) {
                        if (value.hasOwnProperty(p)) {
                            this.data[p] = value[p];
                        }
                    }
                }
            }

            this.build();
        }

        get data() { return this.setting.model.data; }

        add(key, value) {
            let item = {};
            item[key] = value;
            this.data = item;
            this.build();
        }

        map(model) {
            if (!!model && model.constructor === Object && Object.getOwnPropertyNames(model).length > 0) {
                let __model = {};
                for (let p of Object.getOwnPropertyNames(model)) {
                    if (model.hasOwnProperty(p) && !!model[p]) {
                        __model[p.toLowerCase()] = model[p];
                    }
                }
                this.setting.model = $.extend(this.setting.model, __model);
            }
            this.build();
            return this;
        }

        remove(key) {
            if (!!key) {
                if (typeof key === "string") {
                    delete this.setting.model.data[key];
                }

                if (Array.isArray(key) && key.length > 0) {
                    for (let k of key) {
                        this.remove(k);
                    }
                }
                this.build();
            }
        }

        clear() {
            this.setting.model.data = {};
            this.build();
        }

        build(values) {
            let model_action = this.setting.model.action.toString().toLowerCase();
            let autoclose_action = this.setting.autoclose.action.toString().toLowerCase();
            if (!!values) {
                this.data = values;
            }

            let summary_message = $(this.setting.summary.selector);
            if (!!this.setting.summary.attribute) {
                summary_message = $("[" + this.setting.summary.attribute + "]");
            }

            let for_messages = $("[" + this.setting.for.attribute + "]");
            let _box = $("<div></div>");
            _box.css({
                "border-radius": "5px",
                "-webkit-box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)",
                "-moz-box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)",
                "box-shadow": "0px 0px 0.5px 0px rgba(0, 0, 0, 0.15)"
            });

            let _title = $("<div>" + this.setting.summary.title.text + "</div>").css({
                "border-top-left-radius": "5px",
                "border-top-right-radius": "5px",
                "height": "35px",
                "line-height": "35px",
                "border-bottom": ".5px solid rgba(0, 0, 0, .1)",
                "padding": "0 10px",
                "background-color": this.setting.summary.title.background_color,
                "font-size": this.setting.summary.title.font_size,
                "font-weight": this.setting.summary.title.font_weight,
                "text-align": this.setting.summary.title.text_align
            });

            let ul = $("<ul></ul>").css({
                "list-style-type": "'" + this.setting.summary.content.symbol + "'",
                "background-color": this.setting.summary.content.background_color,
                "margin": "0",
                "padding": "15px 30px",
                "border-bottom-left-radius": "5px",
                "border-bottom-right-radius": "5px"
            }).appendTo(_box);

            if (this.setting.summary.content.symbol === "") {
                switch (model_action) {
                    case "reject": case "-1":
                        ul.css({ "list-style-type": "'✘'" });
                        break;
                    case "alert": case "0":
                        ul.css({ "list-style-type": "'⚠'" });
                        break;
                    case "confirm": case "1":
                        ul.css({ "list-style-type": "'✔'" });
                        break;

                }
            }

            for (let c of Object.getOwnPropertyNames(this.data)) {
                let li = $("<li>" + this.data[c] + "</li>");
                li.css({
                    "padding": "5px",
                    "color": "#EC5B5B",
                    "width": "100%",
                    "white-space": "normal",
                    "word-break": "break-all"
                });

                li.css({ "color": this.setting.summary.content.color });
                if (this.setting.summary.content.color === "") {
                    switch (model_action) {
                        case "reject": case "-1":
                            li.css({ "color": "#EC5B5B" });
                            break;
                        case "alert": case "0":
                            li.css({ "color": "#F09F09" });
                            break;
                        case "confirm": case "1":
                            li.css({ "color": "#50C05E" });
                            break;

                    }
                }

                ul.append(li);
            }


            if (Object.getOwnPropertyNames(this.data).length > 0) {
                if (!!summary_message) {
                    if (!!this.setting.summary.title.text) {
                        ul.before(_title);
                    }
                    summary_message.html(_box);
                }

            } else {
                this.close();
            }


            //Validation Message For Each Controls
            let _self = this;
            for_messages.each(function (i, dom) {
                let _value = _self.data[this.getAttribute(_self.setting.for.attribute)];
                $(this).children().remove();
                if (!!_value) {
                    $(this).html("<span>" + _value + "</span>");
                    $(this).css("color", _self.setting.for.color).css("font-size", ".8em");
                    if (_self.setting.for.color === "") {
                        switch (model_action) {
                            case "reject": case "-1":
                                $(this).css("color", "#EC5B5B");
                                break;
                            case "alert": case "0":
                                $(this).css("color", "#F09F09");
                                break;
                            case "confirm": case "1":
                                $(this).css("color", "#50C05E");
                                break;
                        }
                    }
                }
            });

            if (autoclose_action === model_action) {
                this.autoclose(this.setting.autoclose.callback);
            }

            if (!!this.setting.model.redirect.toLowerCase()) {
                this.redirect(this.setting.model.redirect);
            }
        }

        close(callback) {
            if (this instanceof ViewMessage) {
                let _summary = $(this.setting.summary.selector);
                if (!!this.setting.summary.attribute) {
                    _summary = $("[" + this.setting.summary.attribute + "]");
                }
                let _for = $("[" + this.setting.for.attribute + "]");
                setTimeout(() => {
                    _summary.children().fadeOut(0, function () {
                        if (!!callback && typeof callback === "function") {
                            Promise.resolve(callback(_self, this)).then($(this).remove());
                        }
                    });

                    _for.children().fadeOut(0, function () {
                        if (!!callback && typeof callback === "function") {
                            Promise.resolve(callback(_self, this)).then($(this).remove());
                        }
                    });
                }, 0);
                this.setting.model.data = {};
                delete this;
            }
        }

        autoclose(callback) {
            if (this instanceof ViewMessage) {
                let _self = this;
                let _autoclose = this.setting.autoclose;
                let _summary = $(this.setting.summary.selector);
                if (!!this.setting.summary.attribute) {
                    _summary = $("[" + this.setting.summary.attribute + "]");
                }
                let _for = $("[" + this.setting.for.attribute + "]");
                setTimeout(() => {
                    _summary.children().fadeOut(_autoclose.duration, function () {
                        if (!!callback && typeof callback === "function") {
                            Promise.resolve(callback(_self, this)).then($(this).remove());
                        }
                    });

                    _for.children().fadeOut(_autoclose.duration, function () {
                        if (!!callback && typeof callback === "function") {
                            Promise.resolve(callback(_self, this)).then($(this).remove());
                        }
                    });
                }, _autoclose.delay);
                delete this;
            }

        }

        refresh(delay, force) {
            setTimeout(() => {
                if (force) {
                    location.reload(force);
                }
                location.reload();
            }, delay);
        }

        redirect(url, delay, force) {
            url = url.toLowerCase();
            setTimeout(() => {
                if (url === "." || url === "current" || url == "0") {
                    if (force) {
                        location.reload(force);
                    }
                    location.reload();
                } else {
                    location.href = url;
                }
            }, delay);
        }
    }

)));






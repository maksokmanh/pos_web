﻿let msg = new Message(".message-summary");

$("input[name=start-time]").val(defaultStartTime());
$("input[name=end-time]").val(defaultEndTime($("input[name=duration]").val()));
$.each($("input[type=date]"), function () {
    this.valueAsDate = new Date();
    setDate(this, this.valueAsDate);
});

$("input[type='date']").on("change", function () {
    this.setAttribute(
        "data-date",
        moment(this.value)
            .format(this.getAttribute("data-date-format"))
    );
});

$("input[data-date], input[data-time]").on("change", changeDatetime);

$("input[name=duration], input[name=reminder]").on("keyup change", function (e) {
    if (!this.value) {
        this.value = "0";
    }
    this.value = _$_.validNumber(this.value);
});

$("input[data-time]").on("keyup", function (e) {
    this.value = validTime(this.value);
});

$("input[name=duration], select[name=duration-uom]").on("change", changeDuration);

$("input[name=activity-reminder]").click(function () {
    $("input[name=reminder]").prop("disabled", true);
    $("select[name=reminder-uom]").prop("disabled", true);
    if ($(this).is(":checked")) {
        $("input[name=reminder]").prop("disabled", false);
        $("select[name=reminder-uom]").prop("disabled", false);
    }
});


function setDate(selector, date_value) {
    $(selector)[0].valueAsDate = new Date(date_value);
    $(selector)[0].setAttribute(
        "data-date",
        moment($(selector)[0].valueAsDate)
            .format($(selector)[0].getAttribute("data-date-format"))
    );
}

function changeDatetime() {
    let start_datetime = $("input[name=start-date]").val() + " " + $("input[name=start-time]").val();
    let end_datetime = $("input[name=end-date]").val() + " " + $("input[name=end-time]").val();
    let start = moment(start_datetime);
    let end = moment(end_datetime);
    let durations = getDuration(end.diff(start)).split("/");
    if (parseFloat(durations) >= 0) {
        $("input[name=duration]").val(parseFloat(durations[0]).toFixed(2));
        $("select[name=duration-uom]").val(durations[1]);
        $(this).val(this.value);
        msg.remove("StartDatetime");
    } else {
        msg.add("StartDatetime", "Start Time must be less than End Time.");
        if ($(this).is($("input[data-time]"))) {
            if (!isValidTime(this.value)) {
                msg.add($(this).attr("name"), $(this).attr("name") + " is not valid.");
            } else {
                msg.remove([$(this).attr("name")]);
            }
        }
    } 
}

function changeDuration() {
    let start_datetime = $("input[name=start-date]").val() + " " + $("input[name=start-time]").val();
    let duration = $("input[name=duration]").val();
    let time_uom = $("select[name=duration-uom] option:selected").val();
    let result = moment(start_datetime).add(duration, time_uom); 
    let datetime = moment(result).format("YYYY-MM-DD/HH:mm").split("/");
    setDate("input[name=end-date]", datetime[0]);
    $("input[name=end-time").val(datetime[1]);
}

function validTime(value) {
    if (!(/^(\d)+[:]?\d*$/).test(value)) {
        value = value.toString().substring(0, value.length - 1);
    }

    return value;
}

function getDuration(diff) {
    let minutes = 0;
    minutes = moment.duration(diff).asMinutes();

    if (minutes < 60) {
        return minutes + "/MINUTES";
    } else if (minutes < 60 * 24) {
        return minutes / 60 + "/HOURS";
    } else {
        return minutes / (60 * 24) + "/DAYS";
    }
}

function diffTime() {
    let start_time = $("input[name=start-date]").val() + " " + $("input[name=start-time]").val();
    let end_time = $("input[name=end-date]").val() + " " + $("input[name=end-time]").val();
    let start = new moment(start_time);
    let end = new moment(end_time);
    return end.diff(start);
}


function defaultStartTime() {
    let now = new Date();
    let start_time = now.getHours() + ":" + now.getMinutes();
    return timeFormat(moment.duration(start_time).asMinutes());
}

function defaultEndTime(min_from_now) {
    let start_time = defaultStartTime();
    let time = moment.duration(start_time).asMinutes() + parseFloat(min_from_now);
    return timeFormat(time);
}

function fillZero(value) {
    let result = "";
    if (parseFloat(value) >= 10) {
        result = parseFloat(value);
    } else {
        result = "0" + parseFloat(value);
    }
    return result;
}

function timeFormat(minutes) {
    let HH = 0, mm = 0;
    let result = "";
    if (minutes > 60) {
        HH = parseInt(minutes / 60);

        if (minutes % 60 !== 0) {
            mm = minutes % 60;
        }
    } else {
        mm = minutes;
    }
   
    return fillZero(HH) + ":" + fillZero(mm);  
}

function isValidTime(value) {
    return (/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/).test(value);
};

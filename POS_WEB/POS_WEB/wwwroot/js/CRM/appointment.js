﻿
$("#active-appointments").on("click", function () {
    popupAppointmentDialog();
});

function popupAppointmentDialog(data) {
    let appoint = new DialogBox({
        content: {
            selector: ".appointment-list"
        },
        position: "top-center",
        type: "ok-cancel",
        caption: "Appointment",
        button: {
            ok: {
                text: "Edit"
            },
            cancel: {
                callback: function () {
                    this.meta.shutdown();
                }
            }
        }

    });
    
    
    appoint.startup("after", function (dialog) {
        let table = dialog.content.find(".appointment-list table");
        let searchbox = dialog.content.find(".search-appointments").focus();
        table.find("tr:not(:first-child)").remove();
        ajaxAppointment();
        searchbox.on("keyup", function () {     
            setTimeout(() => {
                ajaxAppointment(this.value);
            }, 250);
        });

        function ajaxAppointment(name) {
            if (!name) {
                $.ajax({
                    url: "/CRM/SearchAppointmentsByName",
                    dataType: "JSON",
                    success: function (results) {
                        handleAppointments(appoint, table, results);
                    }
                });
            } else {
                $.ajax({
                    url: "/CRM/SearchAppointmentsByName",
                    dataType: "JSON",
                    data: { name: name },
                    success: function (results) {
                        handleAppointments(appoint, table, results);
                    }
                });
            }

        }

    });
    appoint.confirm(function () {
        chooseAppointment(this);
    });
}


let db = new Warehouse();
function handleAppointments(app_dialog, table, result) {
    db.insert("tb_appointment", result, "AppointID");
    table.find("tr:not(:first-child)").remove();
    let index = 1;

    for (let item of result) {
        let inactive = (item.Inactive) ? '<div class="pseudo-check cross"></div>' : '<div class="pseudo-check none"></div>';
        let closed = (item.Closed) ? '<div class="pseudo-check cross"></div>' : '<div class="pseudo-check none"></div>';
        item.EndTime = (!item.EndTime) ? "" : item.EndTime;
        item.Remark = (!item.Remark) ? "" : item.Remark;
        $.updateRow(table, item, "AppointID", {
            html: [
                {
                    insertion: "replace",
                    column: "AppointID",
                    element: "<span style='margin-right: 5px;'>" + index + "</span>"

                },
                {
                    insertion: "append",
                    column: "AppointID",
                    element: getAlertIcon(item)
                },
               
                {
                    insertion: "replace",
                    column: "Inactive",
                    element: inactive
                },
                {
                    insertion: "replace",
                    column: "Closed",
                    element: closed
                },
            ],
            build: function (data) {
                if (data.Closed) {
                    $(this).addClass("closed");
                }
            },
            click: function () {
                $(this).addClass("active").siblings().removeClass("active");
                appoint_id = $(this).data("appointid");
            },
            dblclick: function () {
                chooseAppointment(app_dialog);
            }
        });
        index++;
    }

};

let appoint_id = 0;
function chooseAppointment(app_dialog) {
    $.ajax({
        url: "/CRM/AppointmentEdit",
        data: { appoint_id: appoint_id },
        success: function (app) {
            if (!!app) {
                if (!app.Closed) {
                    bindAppointmentToView(app);
                    app_dialog.shutdown("after", function () {
                        $("#appointment-submit").html('<i class="fas fa-save fa-lg"></i> Save');
                    });
                }
            }

        }
    });
}

async function bindAppointmentToView(app) {
    let form = $.form("#appointment").data;
    let reminder = app.Reminder.split("/");
    switch (app.Activity) {
        case "NOTE":
            let note_time = app.StartTime.split(" ");
            form["note-date"].value = note_time[0];
            form["note-time"].value = note_time[1];
            break;
        default:
            let start_datetime = app.StartTime.split(" ");
            let end_datetime = app.EndTime.split(" ");
            let duration = app.Duration.split("/");
            setDate(form["start-date"], start_datetime[0]);
            form["start-time"].value = start_datetime[1];
            setDate(form["end-date"], end_datetime[0]);              
            form["end-time"].value = end_datetime[1];
            form["duration"].value = duration[0];
            form["duration-uom"].value = duration[1];  
            form["priority"].value = app.Priority;
            form["task-status"].value = app.Status;             
            form["room"].value = app.Room;
            form["street"].value = app.Street;
            form["city"].value = app.City;
            form["country"].value = app.Country;
            form["meeting-location"].value = app.LocationID;
            break;
    }

    form["inactive"].checked = app.Inactive;
    form["closed"].checked = app.Closed;
    form["appoint-id"].value = app.AppointID;
    form["activity"].value = app.Activity;
    form["remark"].value = app.Remark;
    form["personal"].value = app.personal;          
    form["activity-type"].value = app.TypeID;
    form["activity-subject"].value = app.SubjectID;
    form["assigned-by"].value = app.UserID;
    form["assigned-to"].value = app.EmpID;
    form["bp-id"].value = app.BPID;
    if (app.Reminder !== "0/MINUTES") {
        form["activity-reminder"].checked = false;
        $(form["reminder"]).prop("disabled", false).val(reminder[0]);
        $(form["reminder-uom"]).prop("disabled", false).val(reminder[1]);

    } else {
        form["activity-reminder"].checked = false;
        form["reminder"].value = 0;
        form["reminder-uom"].value = "MINUTES";
    }

    $.getJSON("/CRM/GetBusinessPartnerByID", {bp_id: app.BPID}, function (bp) {
        chooseBP(bp);
    });

    updateViewOnActivityChange(app.Activity);      
}

async function updateViewOnActivityChange(activity) {
    let mutable_default = $("#activity-default-mutable");
    mutable_default.prop("hidden", false);
    $("#activity-note").find("*").prop("disabled", true);
    $("#activity-default").prop("hidden", false);
    $("#activity-meeting-location").prop("hidden", false);

    mutable_default.children().prop("hidden", true);
    switch (activity) {
        case "MEETING":
            mutable_default.find("#activity-meeting").prop("hidden", false);
            break;
        case "TASK":
            mutable_default.find("#activity-task").prop("hidden", false);
            break;
    }

    if (activity === "NOTE") {
        $("#activity-default").prop("hidden", true);
        $("#activity-note").prop("hidden", false).find("*").prop("disabled", false);
        $("#activity-meeting-location").prop("hidden", true);
    }
}
$("#activity").on("change", function () {
    updateViewOnActivityChange(this.value);
});


let _selected_type = 1;
$("#activity-type").on("change", function (e) { 
    if (this.value != -1) {
        $.ajax({
            url: "/CRM/GetSubjectsByType",
            type: "GET",
            data: { type_id: $(this).find("option:selected").val(), only_active: true },
            dataType: "JSON",
            success: function (response) {
                updateSelect("#activity-subject", response, "SubjectID", "Name");
            }
        });         
    }
    
    if (this.value == -1) {
        this.value = _selected_type;
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#activity-define-new"
            },
            caption: "Activity Type",
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });
        
        define_new.startup("after", function (dialog) {
            let table = dialog.content.find("table");
            let new_index = 0;
            let add_new = $("<tr><td class='add-new'></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='TypeName']").val() !== "") {
                    new_index++;
                    let row = $("<tr><td>" + new_index + "</td><td><input name='Name' /></td><td><input name='Active' type='checkbox' checked></td></tr>");
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);             
            });

            $.ajax({
                url: "/CRM/GetActivityTypes",               
                dataType: "JSON",
                success: function (results) {
                    let activity_types = [];
                    $.each(results, function (index, type) {
                        index++;
                        let item = {};
                        item.TypeID = type.TypeID;
                        item.Index = index;
                        item.Name = "<input name='Name' value='" + type.Name + "'>";
                        item.Active = "<input name='Active' type=checkbox>";
                        if (type.Active) {
                            item.Active = "<input name='Active' type=checkbox checked>";
                            if (index == 1) {
                                item.Active = "<input name='Active' type=checkbox checked disabled>";
                            }
                        }
                       
                        activity_types.push(item);
                        new_index = index;
                    });
                    
                    $.bindRows(table, activity_types, "TypeID", {
                        hide_key: true,
                        text_align: [{ "Index": "center" }]
                    });
                    table.append(add_new);
                     
                }
            });        
            
        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table");
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let items = [];
            rows.each(function () {
                let td = $("td", this);   
                if ($("input[name=Name]", td).val() !== "") {
                    items.push({
                        TypeID: $(this).data("typeid"),
                        Name: $.stripHTML($("input[name=Name]", td).val()),
                        Active: $("input[name=Active]", td).is(":checked")
                    });               
                }
            });
         
            $.ajax({
                url: "/CRM/UpdateActivityTypes",
                dataType: "JSON",
                type: "POST",
                data: $.antiForgeryToken({
                    data: items 
                }),
                success: function (res) {    
                    updateSelect("#activity-type", res.Data, "TypeID", "Name");    
                }

            });
            
            define_new.shutdown();            
        });
    } 
});

let _selected_subject = 1;
$("#activity-subject").on("change", function (e) {
    console.log(this.value)
    if (this.value != -1) {
        _selected_subject = $(this).find("option:selected").val();
    }

    if (this.value == -1) {
        this.value = _selected_subject;
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#activity-define-new"
            },
            caption: "Activity Subject",
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let table = dialog.content.find("table");
            table.find("tr:first-child th:last-child").before("<th>Related Type</th>");
            table.find("tr:first-child").find("th:last-child");
            let related_type = $("#activity-type option:selected");
            let new_index = 0;
            let add_new = $("<tr><td class='add-new'></td></tr>").on("click", function (e) {
                if ($(this).prev().find("input[name='Name']").val() !== "") {
                    new_index++;
                    let row = $("<tr><td>" + new_index + "</td><td><input name='Name' /></td><td>"+ related_type.text() +"</td><td><input name='Active' type='checkbox' checked></td></tr>");
                    $(this).before(row);
                }

                table.parent().scrollTop(table[0].scrollHeight);
            });

            
            $.ajax({
                url: "/CRM/GetSubjectsByType",
                dataType: "JSON",
                data: { type_id: related_type.val(), only_active: false},
                success: function (results) {
                    let activity_subjects = [];
                    $.each(results, function (index, data) {
                        index++;
                        let item = {
                            SubjectID: data.SubjectID,
                            Index: index,
                            Name: "<input name='Name' value='" + data.Name + "'",
                            Related: related_type.text(),
                            Active: "<input name='Active' type=checkbox>"
                        };
                       
                        if (data.Active) {
                            item.Active = "<input name='Active' type=checkbox checked>";
                        }

                        activity_subjects.push(item);
                        new_index = index;
                    });
                
                    $.bindRows(table, activity_subjects, "SubjectID", {
                        hide_key: true,
                        text_align: [{ "Index": "center" }]
                    });
                    table.append(add_new);
                }
            });        
            
            
        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table").clone(true);
            let related_type = $("#activity-type option:selected");          
            
            let rows = table.find("tr:not(:first-child):not(:last-child)");
            let db = new Warehouse();
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    db.insert("tb_activity", {
                        SubjectID: $(this).data("subjectid"),
                        TypeID: related_type.val(),
                        Name: $.stripHTML($("input[name=Name]", td).val()),
                        Active: $("input[name=Active]", td).is(":checked")                     
                    }, "SubjectID");
                }
            });

            $.ajax({
                url: "/CRM/UpdateActivitySubjects",
                type: "post",
                dataType: "json",
                data: $.antiForgeryToken({type_id: related_type.val(), data: db.from("tb_activity")}),
                success: function (res) {
                    updateSelect("#activity-subject", res, "SubjectID", "Name");
                }

            });

            this.meta.shutdown();
        });
    }
});

$("select[name=meeting-location]").on("change", function (e) {
    if (this.value === "-1") {
        this.value = _selected_subject;
        let define_new = new DialogBox({
            position: "top-center",
            content: {
                selector: "#activity-define-new"
            },
            caption: "Meeting Location",
            type: "ok-cancel",
            button: {
                cancel: {
                    callback: function (e) {
                        this.meta.shutdown();
                    }
                }
            }
        });

        define_new.startup("after", function (dialog) {
            let table = dialog.content.find("table");
            table.find("tr:first-child th:last-child").remove();
            table.find("tr:first-child").find("th:last-child").css({ "width": "90%" });
            let new_index = 1;
            $.ajax({
                url: "/CRM/GetLocations",
                dataType: "JSON",
                success: function (results) {
                    let locations = [];
                    $.each(results, function (index, data) {
                        index++;
                        let item = {
                            LocationID: data.LocationID,
                            Index: index,
                            Name: "<input name='Name' value='" + data.Name + "'>"                       
                        };                     
                        locations.push(item);
                        new_index = index + 1;
                    });

                    $.bindRows(table, locations, "LocationID", {
                        hide_key: true,
                        text_align: [{ "Index": "center" }]
                    });
                    table.append("<tr><td>" + new_index + "</td><td><input name='Name'></td></tr>");
                }
            });
        });

        define_new.confirm(function (e) {
            let table = this.meta.content.find("table").clone(true);
            let rows = table.find("tr:not(:first-child)");
            let db = new Warehouse();
            rows.each(function () {
                let td = $("td", this);
                if ($("input[name=Name]", td).val() !== "") {
                    db.insert("tb_activity", {
                        LocationID: $(this).data("locationid"),  
                        Name: $.stripHTML($("input[name=Name]", td).val()),          
                    }, "LocationID");
                }
            });

            $.ajax({
                url: "/CRM/UpdateLocations",
                type: "post",
                dataType: "json",
                data: $.antiForgeryToken({ data: db.from("tb_activity") }),
                success: function (res) {
                    updateSelect("select[name=meeting-location]", res, "LocationID", "Name");
                }

            });

            this.meta.shutdown();
        });
    }
});

function updateSelect(selector, jsons, key, value) {
    if ($(selector)[0].tagName === "SELECT") {
        $(selector).children().remove();
       
        let add_new = $("<option value='-1'>Define New</option>");   
        $.each(jsons, function (i, json) {        
            let option = $("<option value='" + json[key] + "'>" + json[value] + "</option>");
            $(selector).append(option);        
        });
        $(selector).append(add_new);
        if ($(selector).children().length <= 1) {
            $(selector).prepend("<option value='0' selected disabled></option>");
        }      
    }
}

function chooseBP(item) {
    $("input[name=bp-id]").val(item.ID);
    $("input[name='bp-code']").val(item.Code);
    $("input[name='bp-type']").val(item.Type);
    $("input[name='bp-name']").val(item.Name);
    $("input[name='bp-contact-person']").val(item.Name);
    $("input[name='bp-phone']").val(item.Phone);
}


$("#bp-name-trig").click(function (e) {
    let choose_bp = new DialogBox({
        position: "top-center",
        content: {
            selector: "#choose-business-partner"
        },
        caption: "Business Partner",
        type: "ok-cancel",
        button: {
            cancel: {
                callback: function (e) {
                    this.meta.shutdown();
                }
            }, 
            ok: {text: "Choose"}
           
        }
    });
    
    let bp_id = 0;
    choose_bp.startup("after", function (dialog) {
        let table = dialog.content.find("table");   
        $.ajax({
            url: "/CRM/GetBusinessPartners",
            dataType: "JSON",
            success: function (results) {           
                db.insert("tb_business_partner", results, "ID");
                $.bindRows(table, results, "ID", {
                    hidden_columns: ["ID", "Name", "Delete", "PriceList", "PriceListID"],
                    click: function () {
                        $(this).addClass("active");
                        bp_id = $(this).data("id");
                    },
                    dblclick: function () {
                        let item = db.table("tb_business_partner").find($(this).data("id"));
                        chooseBP(item);
                        choose_bp.shutdown();
                    }
                });
                         
            }
        });

    });

    choose_bp.confirm(function () {
        let person = db.table("tb_business_partner").find(bp_id);
        if (!!person) {
            chooseBP(person);
            choose_bp.shutdown();
        }   
    });
});



$("#appointment-submit").click(function (e) {
    let _form = $.form("#appointment");
    let start_time = moment(_form.value["start-date"]).format("YYYY-MM-DD") + " " + _form.value["start-time"];
    let end_time = moment(_form.value["end-date"]).format("YYYY-MM-DD") + " " + _form.value["end-time"];
    if (_form.value["activity"] === "NOTE") {
        start_time = moment(_form.value["note-date"]).format("YYYY-MM-DD") + " " + _form.value["note-time"];
        end_time = "";
    }
   
    let appointment = {
        "AppointID": _form.value["appoint-id"],
        "Activity": _form.value["activity"],
        "Remark": _form.value["remark"],
        "Personal": _form.value["personal"],
        "StartTime": start_time,
        "EndTime": end_time,
        "Duration": _form.value["duration"] + "/" + _form.value["duration-uom"],
        "Reminder": (!_form.value["activity-reminder"]) ? "0/MINUTES" : _form.value["reminder"] + "/" + _form.value["reminder-uom"],
        "Priority": _form.value["priority"],
        "Status": _form.value["task-status"],
        "Inactive": _form.value["inactive"],
        "Closed": _form.value["closed"],
        "Room": _form.value["room"],
        "Street": _form.value["street"],
        "City": _form.value["city"],
        "Country": _form.value["country"],
        "TypeID": _form.value["activity-type"],
        "SubjectID": _form.value["activity-subject"],
        "LocationID": _form.value["meeting-location"],
        "UserID": _form.value["assigned-by"],
        "EmpID": _form.value["assigned-to"],
        "BPID": _form.value["bp-id"]
        
    };
  
    $.ajax({
        url: "/CRM/UpdateAppointment",
        data: $.antiForgeryToken({
            appointment: appointment
        }),
        dataType: "JSON",
        type: "POST",
        success: function (message) {      
            msg.map(message);
            console.log(message)
        }
    });

});




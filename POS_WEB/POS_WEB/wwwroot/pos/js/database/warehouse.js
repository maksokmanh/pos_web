 (function( $ ){
    let data_context = [    
        {
            "id": "001",
            "name_kh": "កូកា កូឡា",
            "name_en": "Coca Cola",
            "price": 0.9,
            "image": "pictures/goods.jpg",
            "bonus": 0
        }, 
        {
            "id": "002",
            "name_kh": "ទឹកដោះគោស្រស់",
            "name_en": "Fresh Milk",
            "price": 2.3,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "003",
            "name_kh": "ឡុកឡាក់សាច់គោ",
            "name_en": "Beef Dice",
            "price": 4.5,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "004",
            "name_kh": "កាហ្វេទឹកដោះគោ",
            "name_en": "White Coffee",
            "price": 1.5,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "005",
            "name_kh": "មីឆាសាច់មាន់",
            "name_en": "Chicken noodle",
            "price": 3.2,
            "image": "pictures/goods.jpg",
            "bonus": 0.25
        },
        {
            "id": "006",
            "name_kh": "មីឆាបង្កង",
            "name_en": "Shrimp noodles",
            "price": 4.8,
            "image": "pictures/goods.jpg",
            "bonus": 0.30
        },
        {
            "id": "007",
            "name_kh": "គុយទាវខគោ",
            "name_en": "Beef khor noodle",
            "price": 3.4,
            "image": "pictures/goods.jpg",
            "bonus": 0.10
        },
        {
            "id": "008",
            "name_kh": "ពោតលីង",
            "name_en": "Popcorn",
            "price": 1.6,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "009",
            "name_kh": "ឆារក្តៅសាច់មាន់",
            "name_en": "Hot chicken fried",
            "price": 3.2,
            "image": "pictures/goods.jpg",
            "bonus": 0.15
        },
        {
            "id": "010",
            "name_kh": "ម្ជូរគ្រឿងសាច់គោ",
            "name_en": "Beef Pins",
            "price": 2.5,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "011",
            "name_kh": "ស្រាបៀរអង្គរ",
            "name_en": "Angkor Beer",
            "price": 0.95,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
            {
            "id": "012",
            "name_kh": "ស្រាបៀរអង្គរ",
            "name_en": "Angkor Beer",
            "price": 0.95,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
            {
            "id": "013",
            "name_kh": "ស្រាបៀរអង្គរ",
            "name_en": "Angkor Beer",
            "price": 0.95,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "014",
            "name_kh": "ស្រាបៀរអង្គរ",
            "name_en": "Angkor Beer",
            "price": 0.95,
            "image": "pictures/goods.jpg",
            "bonus": 0
        },
        {
            "id": "015",
            "name_kh": "ស្រាបៀរអង្គរ",
            "name_en": "Angkor Beer",
            "price": 0.95,
            "image": "pictures/goods.jpg",
            "bonus": 0
        }
    ];
    $.extend({
        getItems: function(){
            return data_context;
        },

    });
 })(jQuery);

class _$ 
{
    //Check if json object is valid.
    static validJson(json){
        return json !== undefined && json.constructor === Object && Object.keys(json).length > 0;
    }

    //Check if valid array.
    static validArray(values){
        return values !== undefined && Array.isArray(values);
    }

    //Check if two value has the same type.
    static uniform(a, b, equal){
        let identical = false;
        if(_$.validJson(a) && _$.validJson(b)){
            $.each(a, function(ak, av){
                $.each(b, function(bk, bv){
                    if(equal === undefined || !equal){
                        identical = ak === bk && typeof av === typeof bv;
                    } else {
                        identical = ak === bk && av === bv;
                    }    
                });
            }); 
        }

        if(a !== undefined && !_$.validJson(a)){
            if(b !== undefined && !_$.validJson(b)){
                return a === b;
            }
        }

        return identical; 
    }

    //Checkif two value has the same type and the same value.
    static equal(a, b){
        return _$.uniform(a, b, true); 
    }

    //Check if string is html
    static isHtml(value){
        return value !== undefined && (/<[a-z/][\s\S]*>/i).test(value);
    }

    //Check if any event bound to target element
    static targetBound(target, event_type){
        let bound = false
        if(target !== undefined){
            $.each($._data(target, "events"), function(k, v){
                if(k === event_type){
                    bound = true;
                }
            });
        }
        return bound;
    }

    //Get highest z-index value of all specified elements.
    static highestZindex(highest, tag_name){
        let tag = "*";
        if(tag_name !== undefined){
            tag = tag_name;
        }

        $(tag).each(function(){
            let zindex = $(this).css("z-index");
            if((zindex > highest) && (zindex != 'auto')){
                highest = zindex;
            }  
        });
        return highest;
    }

}

class Warehouse extends Map
{
    constructor(option = 0){
        super();  
        if(_$.validJson(option)){
            this.addTable(option.table, option.jsons, option.key);
        } 
        
        Map.prototype.find = super.get;
        Warehouse.prototype.table = this.map;
        Warehouse.prototype.from = this.array;
        Warehouse.prototype.select = this.array;

        //Filter for objects by specified condition.
        Array.prototype.where = function(condition){
            return $.grep(this, function(json, i){
                return condition(json, i);
            });
        };

        //Find single object from Array by specified condition.
        Array.prototype.first = function(condition){
            return $.grep(this, function(json, i){
                return condition(json, i);
            })[0];
        };

        //Iterate through each of values in array.
        Array.prototype.each = function(callback){
            return $.each(this, function(i, v){
                return callback(i, v);
            });
        }
      
        //Let any object could be inserted into map as extension method.
        Map.prototype.insert = function(value, key){
            let _keys = [];
            if(key !== undefined){
                if(_$.validJson(value)){
                    if(this.size > 0){
                        _keys = [...this.keys()];
                        if(_$.uniform(value, this.get(_keys[0]))){
                            this.set(value[key], value); 
                        } else {
                            console.error("Object["+ key +" : "+ value[key] +"] has wrong data format.");
                        }

                    } else {
                        this.set(value[key], value);   
                    }
                }

                if(_$.validArray(value)){
                    for(let item of value){
                        this.insert(item, key);
                    }
                }
            } else {
                if(_$.validArray(value)){
                    this.insert(value[0], value[1]);
                }
            }   
        };

        //Extension method for modifying object.
        Map.prototype.update = function(value, key){
            if(key !== undefined){
                if(_$.validJson(value)){
                    if(this.has(value[key]) 
                    && _$.uniform(value, this.get(value[key]))){
                        this.set(value[key], value);  
                    } else {
                        console.error("Object["+ key +" : "+ value[key] +"] has not matched or wrong data format.");
                    }            
                }
                
                if(_$.validArray(value)){      
                    for(let item of value){
                        this.update(item, key);               
                    }
                } 
            } else {
                if(_$.validArray(value)){
                    this.update(value[0], value[1]);
                }
            }     
        }

        
    }

    //Using Map to add new table to warehouse
    addTable(table, jsons, key){
        if(Array.isArray(jsons) && key !== undefined){
            if(this.get(table) === undefined){
                this.set(table, new Map());
            }

            for(let json of jsons){
                if(_$.validJson(json)){
                    this.get(table).set(json[key], json);
                }        
            }
        }
    }

    //Use async promise as iteration param.
    async arrayAsync(table, promise){
        if(this.array(table).length > 0){
            return await Promise.all(this.array(table)).then(function(values){
                return promise(values);      
            });
        } else {
            console.error("Cannot iterate through empty array.");
        }
        
    }

    //Select all object in array value, if async param is defined then use the method 
    //as asynchronous iteration otherwise, as normal iteration.
    array(table, promise){
        if(promise!== undefined){
            return this.arrayAsync(table, promise);
        } else {
            let data = [];
            if(table !== undefined && this.has(table)){ 
                data = [...this.get(table).values()];
                if(data.length === 0){ return 0; }
                return data;
            }
            return 0;
        } 
    }

    map(table){
        if(this.get(table) === undefined){
            this.set(table, new Map());  
            setTimeout(() => {
                if(this.get(table).size === 0){
                    this.delete(table);
                } 
            }, 60000);     
        } 
       
        return this.get(table);
    }

    //Copy objects(s) from one table to another by specified key(s).
    async copy(from_table, to_table, data){

        if(this.get(from_table) === undefined){
            console.error("Source is invalid.");
            return;
        }

        if(this.get(to_table) === undefined){
            this.set(to_table, new Map());
        }

        let _keys = [];
        if(data === undefined){
            if(this.get(to_table).size > 0){
                _keys = [...this.get(to_table).keys()];
                if(_$.uniform(this.array(from_table)[0], this.get(to_table).get(_keys[0]))){
                    this.set(to_table, this.get(from_table)); 
                }
            } else {
                this.set(to_table, this.get(from_table));   
            }
            
        } 

        let item = {};
        if(!_$.validArray(data)){
            if(!_$.validJson(data)){
                let key = data;
                if((item = this.get(from_table).get(key)) !== undefined){    
                    if(this.get(to_table).size > 0){
                        _keys = [...this.get(to_table).keys()];
                        if(_$.uniform(item, this.get(to_table).get(_keys[0]))){
                            await this.get(table).set(key, item); 
                        } else {
                            console.error("Object["+ key +"] has wrong data format.");
                        }

                    } else {
                        await this.get(to_table).set(key, item);   
                    }
                     
                } 
            }        
        } else {
            let value = data[0];
            let key_name = data[1];
            if(!_$.validJson(value)){
                for(let k of data){
                    this.copy(from_table, to_table, k);
                }
            } 
            
            if(data.length === 2){    
                if(_$.validJson(value)){
                    await this.copy(from_table, to_table, value[key_name]);
                }

                if(_$.validArray(value)){
                    if(_$.validJson(value[0])){
                        for(let json of value){
                            this.copy(from_table, to_table, json[key_name]);
                        }
                    }
                    
                }

            }
    
        }

    }

    //Move objects(s) from one table to another by specified key(s).
    async cut(from_table, to_table, key){
        if(key === undefined){
            this.copy(from_table, to_table);
            let copied = false;
            for(let e of this.get(from_table).keys()){
                copied = this.get(to_table).has(e);      
            }

            if(copied){
               this.set(from_table, new Map());
            }
        } 

        if(!_$.validArray(key)){
            this.copy(from_table, to_table, key);
            await this.get(from_table).delete(key);
        }

        if(_$.validArray(key)){
            for(let k of key){
                this.cut(from_table, to_table, k);
            }
        } 
   
    }

    //Delete object from table by specified key(s).
    async remove(table, key){
        if(!_$.validArray(key)){
            if(this.get(table).has(key)){
                await this.get(table).delete(key);
            }
        }

        if(_$.validArray(key)){
            for(let k of key){
                this.remove(table, k);
            }
        }
    }

    //Insert an object into table.
    async insert(table, value, key){
        let _keys = [];
        if(key !== undefined){
            if(this.get(table) === undefined){
                this.set(table, new Map());                
            } 

            if(_$.validJson(value)){
                if(this.get(table).size > 0){
                    _keys = [...this.get(table).keys()];
                    if(_$.uniform(value, this.get(table).get(_keys[0]))){
                        await this.get(table).set(value[key], value); 
                    } else {
                        console.error("Object["+ key +" : "+ value[key] +"] has wrong data format.");
                    }

                } else {
                    await this.get(table).set(value[key], value);   
                }
            }

            if(_$.validArray(value)){
                for(let item of value){
                    this.insert(table, item, key);
                }
            }
        } else {
            if(_$.validArray(value)){
                await this.insert(table, value[0], value[1]);
            }
        }
    }

    //Modify existing object in table.
    async update(table, value, key){    
        if(key !== undefined){
            if(_$.validJson(value)){
                if(this.get(table).has(value[key]) 
                && _$.uniform(value, this.get(table).get(value[key]))){
                    await this.get(table).set(value[key], value);  
                } else {
                    console.error("Object["+ key +" : "+ value[key] +"] has not matched or wrong data format.");
                }            
            }
            
            if(_$.validArray(value)){      
                for(let item of value){
                    this.update(table, item, key);               
                }
            } 
        } else {
            if(_$.validArray(value)){
                await this.update(table, value[0], value[1]);
            }
        }     
    }
    
    //Search valid json objects by specified condition.
    async filter(table, condition){
        if(table !== undefined){
            if(typeof condition === "function"){
                return await $.grep(this.from(table), function(json, i){
                    return condition(json, i);
                });
            }
        }
    }

    searchText(table, column, value, equal = 0){
        return $.grep(this.from(table), function(json, i){
            if(typeof json[column] === "string" && typeof value === "string"){
                if(typeof equal !== undefined && option.equal){
                    return json[column].toLowerCase() === value.toLowerCase();
                } else {
                    return (json[column].toLowerCase()).includes(value.toLowerCase());
                }        
            }
        });
    }
    
    distinct(list, key){
       return list.filter((json, index) => {
           return list.map(json => json[key]).indexOf(json[key]) === index;
       });
    }
}








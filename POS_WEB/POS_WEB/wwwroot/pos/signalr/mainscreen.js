﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/mainscreen").build();
//Initail connection
connection.start().then(function () {
    console.log("main screen connected!");
}).catch(function (err) {
    return console.error(err.toString());
});
//real time push notification count order on receipt and user stay in table
connection.on("PushOrder", function (orders, tableid, user) {
    $("#user" + tableid).text(user+'...');
    let dropbox_order = $('#dropbox-order');
    if (orders.length !== 0) {
        
        $("#badge-order").text(orders.length);
      
        dropbox_order.children().remove();
        $.each(orders, function (i, order) {
           
            if (order.checkBill === 'Y') {
                dropbox_order.append('<a href="#" class="option " style="color:white;background:#50A775;" data-id=' + order.orderID + ' onclick=clickOnOrderNo(' + order.tableID + ',' + order.orderID + ')><i class="fas fa-receipt"></i> ' + order.orderNo + '/' + order.timeIn + ' </a>');
            }
            else {
                dropbox_order.append('<a href="#" class="option" data-id=' + order.orderID + ' onclick=clickOnOrderNo(' + order.tableID + ',' + order.orderID + ')><i class="fas fa-receipt"></i> ' + order.orderNo + '/' + order.timeIn + ' </a>');
             
            }
        });
    }
    else {

        $('#badge-order').text(orders.length);
        dropbox_order.children().remove();
    }

});
connection.on("ClearUserOrder", function (tableid) {
    $("#user" + tableid).text("");
});
//real time change status bill in client
connection.on("PushStatusBill", function (order_id) {

    let dropbox_order = $('#dropbox-order .option');
    $.each(dropbox_order, function (i, order) {
        let id = $(order).data("id");
        if (id === order_id) {
          
            $(order).css("background-color", "#50A775");
            $(order).css("color", "white");
        }
    });
    
});

//real time move table not work error
connection.on("GetTableAvailable", function (response) {
    
    let out = "";
    let tables = $.ajax({
        url: '/POS/GetTableAvailable',
        type: 'GET',
        async: false,
        data: { group_id: 0, tableid: table_info.id }
    }).responseJSON;
    $.each(tables, function (i, table) {

        out += "<label class='grid rc-container'>"
            + "<input data-id=" + table.ID + " name='table' type='radio'>"
            + "<span class='radiomark'></span>"
            + table.Name
            + "</label>";
    });
    $('#list-table-free').html(out);
   
});
//Update status table
//connection.on("PushOrderToAvailable", function (table_id) {
   
//    $('#badge-order').text(0);
//    let dropbox_order = $('#dropbox-order');
//    dropbox_order.children().remove();
    
//    $('#' + table_id).text("");
//    let grids = $("#table-item-gridview .wrap-grid .grid");
//    $.each(grids, function (i, grid) {
//        let grid_id = $(grid).children().data('id');
//        if (grid_id === table_id) {
//            $(grid).css("background-color", "#CC9");
//        }
//    });

//});


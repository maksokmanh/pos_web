﻿$("#dbl-comment").click(function (e) {
    confirmComment();
});
function confirmComment() {
    let user_privillege = db.table("tb_user_privillege").get('P017');
    if (user_privillege.Used === false) {
        let dlg = new DialogBox({
            // close_button: false,
            position: "top-center",
            content: {
                selector: "#admin-authorization",
                class: "login"
            },
            icon: "fas fa-lock",
            button: {
                ok: {
                    text: "Login",
                    callback: function (e) {
                        let access = accessSecurity(this.meta.content, 'P017');
                        if (access === false) {
                            this.meta.content.find('.error-security-login').text('You can not access ...!');
                            return;
                        }
                        else {
                            this.meta.content.find('.security-username').focus();
                            this.meta.setting.icon = "fas fa-lock fa-spin";
                            this.text = "Logging...";
                            this.meta.content.find('.error-security-login').text('');
                            this.meta.build(this.setting);
                            setTimeout(() => {
                                this.meta.build(this.setting);
                                this.meta.setting.icon = "fas fa-unlock-alt";
                                setTimeout(() => {
                                    this.meta.shutdown();
                                    initFormComment();
                                }, 100);
                            }, 500);
                        }
                    }
                }
            }

        });
    }
    else {

        initFormComment();
    }
}
function initFormComment() {
    if ($("#item-listview").find('tr:not(:first-child)').length !== 0) {
        db.map("tb_comment").clear();
        let msg = new DialogBox(
            {
                caption: "Item Comment",
                content: {
                    selector: "#item-comment"
                },
                position: "top-center",
                type: "ok-cancel"
            }
        );
        msg.setting.animation.shutdown.animation_type = "slide-up";
        msg.setting.button.ok.text = "Done";
        msg.confirm(itemComment);
        msg.reject(function (e) {
            this.meta.shutdown();
        });

        $.each(db.from("tb_order_detail").where(w => { return w.PrintQty > 0; }), function (i, item) {
            let item_add = {};
            item_add.line_id = item.Line_ID;
            item_add.code = item.Code;
            item_add.name = item.KhmerName;
            item_add.qty = item.PrintQty;
            if (item.Comment == null || item.Comment=='') {
                item_add.comment = "<input name=comment value='" + '' + "'>";
            }
            else {
                item_add.comment = "<input name=comment value='" + item.Comment + "'>";
            }
            
            db.insert("tb_comment", item_add, "line_id");
        });
        bindRowItemComment();
    }
    else {
        let msg = new DialogBox(
            {
                caption: "Information",
                content: "Data was empty...!",
                position: "top-center",
                type: "ok",
                icon: "info"
            }
        );
        msg.setting.button.ok.callback = function (e) {
            this.meta.shutdown();
        };
    }
}
function itemComment() {
    let items_comment = $("input[name='comment']");
    $.each(items_comment, function (i, item) {
        let comment = $(item).val();
        if (comment != '' || comment!= null) {
            let line_id = parseFloat($(item).parent().parent().data("line_id"));
            item_com = db.get("tb_order_detail").get(line_id);
            item_com.Comment = comment;
            db.insert("tb_order_detail", item_com, "Line_ID");
        }
       
    });
    //summaryTotal(0, 'Y');
    this.meta.shutdown();
}
function bindRowItemComment() {
    let item_comment = db.from("tb_comment");
    $.bindRows("#item-comment-listview", item_comment, "line_id", {
        text_align: [{ "name": "left" }],
        hidden_columns: ["line_id"]
    });
}
function rowAddComment() {
  
}
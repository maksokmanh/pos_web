﻿function clearOrder() {
   
    const $wrap_grid = $("#group-item-gridview .wrap-grid");
    singleItem($wrap_grid, db.from("tb_item_master"));
    order_master.OrderID = 0;

    db.map("tb_order_detail").clear();
    db.from("tb_item_master").where(function (json) {
        json.PrintQty = 0;
    })
    $.bindRows("#item-listview", db.from("tb_order_detail"), 'Line_ID', {});
    summaryTotal(0, 'Y');

}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using POS_WEB.Models.ClassCopy;
using POS_WEB.Models.InventoryAuditReport;
using POS_WEB.Models.ReportClass;
using POS_WEB.Models.Services;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Administrator.Tables;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.POS.tmptable;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.ReportSale;
using POS_WEB.Models.Services.ReportPurchase;
using POS_WEB.Models.ServicesClass;
using POS_WEB.Models.Temporary_Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Receipt = POS_WEB.Models.Services.POS.Receipt;
using POS_WEB.Models.Services.ReportDashboard;
using POS_WEB.Models.Services.ReportInventory;
using POS_WEB.Models.Services.Purchase.Print;
using POS_WEB.Models.Services.Customer;
using POS_WEB.Models.Services.CRM;
using POS_WEB.Models.Services.Appointment;

namespace POS_WEB.AppContext
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<ItemGroup1> ItemGroup1 { get; set; }
        public DbSet<ItemGroup2> ItemGroup2 { get; set; }
        public DbSet<ItemGroup3> ItemGroup3 { get; set; }
        public DbSet<Colors> Colors { get; set; }
        public DbSet<Background> Backgrounds { get; set; }
        public DbSet<UnitofMeasure> UnitofMeasures { get; set; }
        public DbSet<GroupUOM> GroupUOMs { get; set; }
        public DbSet<GroupDUoM> GroupDUoMs { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<UserPrivillege> UserPrivilleges { get; set; }
        public DbSet<Function> Functions { get; set; }
        public DbSet<GroupEmployee> GroupEmployees { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<BusinessPartner> BusinessPartners { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<PriceLists> PriceLists { get; set; }
        public DbSet<PriceListDetail> PriceListDetails { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<ReceiptInformation> ReceiptInformation { get; set; }
        public DbSet<Tax> Tax { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseDetail> WarehouseDetails { get; set; }
        public DbSet<GroupTable> GroupTables { get; set; }
        public DbSet<ItemMasterData> ItemMasterDatas { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<LoginModal> LoginModals { get; set; }
        public DbSet<PrinterName> PrinterNames { get; set; }
        public DbSet<PaymentMeans> PaymentMeans { get; set; }     
        public DbSet<MemberCard> MemberCards { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<PointCard> PointCards { get; set; }
        public DbSet<PointDetail> PointDetails { get; set; }
        public DbSet<CardType> CardTypes { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderDetail>PurchaseOrderDetails { get; set; }
        public DbSet<Purchase_AP> Purchase_APs { get; set; }
        public DbSet<Purchase_APDetail> PurchaseDetails { get; set; }
        public DbSet<PurchaseCreditMemo> PurchaseCreditMemos { get; set; }
        public DbSet<PurchaseCreditMemoDetail> PurchaseCreditMemoDetails { get; set; }
        public DbSet<PurchaseQuotation> PurchaseQuotations { get; set; }
        public DbSet<PurchaseQuotationDetail> PurchaseQuotationDetails { get; set; }
        public DbSet<PromotionPrice> PromotionPrice { get; set; }
        public DbSet<GoodReciptPODetail> GoodReciptPODetails { get; set; }
        public DbSet<GoodsReciptPO> GoodsReciptPOs { get; set; }
        public DbSet<GoodIssues> GoodIssues { get; set; }
        public DbSet<GoodIssuesDetail> GoodIssuesDetails { get; set; }
        public DbSet<GoodsReceipt> GoodsReceipts { get; set; }
        public DbSet<GoodReceiptDetail> GoodReceiptDetails { get; set; }
        public DbSet<Transfer> Transfers { get; set; }
        public DbSet<TransferDetail> TransferDetails { get; set; }
        public DbSet<OutgoingPayment> OutgoingPayments { get; set; }
        public DbSet<OutgoingPaymentDetail> OutgoingPaymentDetails { get; set; }
        public DbSet<OutgoingPaymentVendor> OutgoingPaymentVendors { get; set; }
        public DbSet<ItemCopyToPriceListDetail> ItemCopyToPriceListDetail { get; set; }
        public DbSet<ItemCopyToPriceList> ItemCopyToPriceList { get; set; }
        public DbSet<ServicePriceListCopyItem> ServicePriceListCopyItem { get; set; }
        public DbSet<ItemCopyToWHDetail> ItemCopyToWHDetail { get; set; }
        public DbSet<ItemCopyToWH> ItemCopyToWH { get; set; }
        public DbSet<DiscountItemDetail> DiscountItemDetail { get; set; }
        public DbSet<PricelistSetPrice> PricelistSetPrice { get; set; }
        //public DbSet<GeneralSetting> GeneralSetings { get; set; }
        public DbSet<InventoryAudit> InventoryAudits { get; set; }
        public DbSet<GoodsReceiptPoReturn> GoodsReceiptPoReturns { get; set; }
        public DbSet<GoodsReceiptPoReturnDetail> GoodsReceiptPoReturnDetails { get; set; }
        public DbSet<WarehouseSummary> WarehouseSummary { get; set; }
        //Service map data

        public DbSet<ItemMasterDataService> ItemMasterDataServices { get; set; }
        public DbSet<ServicePointDetail> ServicePointDetails { get; set; }
        public DbSet<ServiceMapItemMasterDataQuotation> ServiceMapItemMasterDatas { get; set; }
        public DbSet<ServiceQuotationDetail> ServiceQuotationDetails { get; set; }
        public DbSet<ServiceMapItemMasterDataPurchasAP>ServiceMapItemMasterDataPurchasAPs { get; set; }
        public DbSet<ServiceMapItemMasterDataPurchaseCreditMemo> ServiceMapItemMasterDataPurchaseCreditMemos { get; set; }
        public DbSet<ServiceMapItemMasterDataPurchaseOrder> ServiceMapItemMasterDataPurchaseOrders { get; set; }
        //public DbSet<PricelistSetUpdatePrice> PricelistSetUpdatePrice { get; set; }
        //02.02.2019
        public DbSet<PurchaseRequest> PurchaseRequests { get; set; }
        public DbSet<PurchaseRequestDetail> PurchaseRequestDetails { get; set; }
        public DbSet<ServiceMapItemPurchaseRequest> ServiceMapItemPurchaseRequests { get; set; }
        public DbSet<ReportPurchaseRequset> ReportPurchaseRequsets { get; set; }
        public DbSet<PurchaesOrder_from_Quotation> purchaesOrder_From_Quotations { get; set; }

        //Report serive
        public DbSet<ReportPurchaseQuotation> ReportPurchaseQuotations { get; set; }
        public DbSet<ReportPurchaseAP> ReportPurchaseAPs { get; set; }
        public DbSet<ReportPurchasCreditMemo> ReportPurchasCreditMemos { get; set; }
        public DbSet<ReportPurchaseOrder> ReportPurchaseOrders { get; set; }

        // stock
        public DbSet<GoodReceiptStock> GoodReceiptStocks { get; set; }
        // invetory Audit
        public DbSet<ServiceInventoryAudit> ServiceInventoryAudits { get; set; }
        //temporary table
        public DbSet<TpPriceList> TpPriceLists { get; set; }
        //POS
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<OrderDetail_Addon> OrderDetail_Addon { get; set; }
        public DbSet<Order_Receipt> Order_Receipt { get; set; }
        public DbSet<Order_Queue> Order_Queue { get; set; }
        public DbSet<Receipt> Receipt { get; set; }
        public DbSet<ReceiptDetail> ReceiptDetail { get; set; }
        public DbSet<Models.Services.POS.GeneralSetting> GeneralSetting { get; set; }
        public DbSet<OpenShift> OpenShift { get; set; }
        public DbSet<CloseShift> CloseShift { get; set; }
        public DbSet<TmpOrderDetail> TmpOrderDetail { get; set; }
        public DbSet<SystemType> SystemType { get; set; }
        //Service map data
        public DbSet<ServiceItemSales> ServiceItemSales { get; set; }
        public DbSet<CheckPayment> CheckPayments { get; set; }

        public DbSet<StockMoving> StockMoving { get; set; }
        // service copy
        public DbSet<PurchaseAP_To_PurchaseMemo> PurchaseAP_To_PurchaseMemos { get; set; }
        //Report
        public DbSet<SummarySale> SummarySales { get; set; }
        public DbSet<DetailSale> DetailSales { get; set; }
        public DbSet<SummaryPurchaseAP> SummaryPurchaseAPs { get; set; }
        public DbSet<DetailPurchaseAp> DetailPurchaseAps { get; set; }
        public DbSet<ReportCloseShft> ReportCloseShfts { get; set; }
        public DbSet<TopSaleQuantity> TopSaleQuantities { get; set; }
        public DbSet<DetailTopSaleQty> DetailTopSaleQties { get; set; }
        public DbSet<DashboardSaleSummary> DashboardSaleSummary { get; set; } 
        public DbSet<StockExpired> StockExpireds { get; set; }
        public DbSet<StockInWarehouse> StockInWarehouses { get; set; }
        public DbSet<StockInWarehouse_Detail> StockInWarehouse_Details { get; set; }
        public DbSet<SummaryOutgoingPayment> SummaryOutgoingPayments { get; set; }
        public DbSet<SummaryDetailOutgoingPayment> SummaryDetailOutgoingPayments { get; set; }
        public DbSet<SummaryTransferStock> SummaryTransferStocks { get; set; }
        public DbSet<SummaryDetaitTransferStock> SummaryDetaitTransferStocks { get; set; }
        public DbSet<SummaryRevenuesItem> SummaryRevenuesItems { get; set; }
        public DbSet<DashboardTopSale> DashboardTopsale { get; set; }
        public DbSet<RevenueItem> RevenueItems { get; set; }
        public DbSet<CashoutReport> CashoutReport { get; set; }
   
        // Customer
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerGroup> CustomerGroups { get; set; }
        public DbSet<SourceOfInfo> SourceOfInfos { get; set; }
        public DbSet<WarrantyType> WarrantyTypes { get; set; }
        // KGN itemmater data
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<ItemGroupCode> ItemGroupCodes { get; set; }
        // KGN Vendor
        public DbSet<Country> Countries { get; set; }
        public DbSet<VendorType> VendorTypes { get; set; }
        // Print 
        public DbSet<PrintPurchaseAP> PrintPurchaseAPs { get; set; }
        // CRM
        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<ActivitySubject> ActivitySubjects { get; set; }
        public DbSet<ActivityLocation> ActivityLocations { get; set; }
        public DbSet<Appointment> Appointments { get; set; }

        //Customer Appointment
        public DbSet<DefaultRemark> DefaultRemarks { get; set; }
        public DbSet<ItemService> ItemServices { get; set; }
        public DbSet<CustomerAppointment> CustomerAppointments { get; set; }
        public DbSet<AppointmentHistory> AppointmentHistories { get; set; }

        //KNG Yamaha branches, Model, Brand, Color
        public DbSet<YamahaBranch> YamahaBranches { get; set; }
        public DbSet<InterestingBrand> InterestingBrands { get; set; }
        public DbSet<InterestingModel> InterestingModels { get; set; }
        public DbSet<ColorCustomer> ColorCustomers { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<District> Districts { get; set; }

        protected override void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder builder)
        {
            builder.Entity<UnitofMeasure>().HasIndex(x => x.Code).IsUnique();
            builder.Entity<Employee>().HasIndex(x => x.Code).IsUnique();
            builder.Entity<BusinessPartner>().HasIndex(x => x.Code).IsUnique();
            builder.Entity<GroupUOM>().HasIndex(x => x.Code).IsUnique();
            builder.Entity<Warehouse>().HasIndex(x => x.Code).IsUnique();
        }

    }
}

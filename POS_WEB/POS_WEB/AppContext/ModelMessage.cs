﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading.Tasks;
using System;

namespace POS_WEB.AppContext
{
    public enum ModelAction { REJECT = -1, ALERT = 0, CONFIRM = 1 };
    public enum ModelRedirect { CURRENT };
    public class ModelMessage
    {
        public ModelMessage(){}

        public ModelMessage(ModelStateDictionary ModelState)
        {
            BindModelState(ModelState);
        }

        public ModelAction Action { get; set; }
        public int Count { get { return Data.Count; } }
        public string Redirect { get; set; }

        public Dictionary<string, string> Data = new Dictionary<string, string>();
        public ModelStateDictionary ModelState
        {
            set { BindModelState(value); }
        }

        private void BindModelState(ModelStateDictionary ModelState)
        {
            foreach (var key in ModelState.Keys)
            {
                foreach (var error in ModelState[key].Errors)
                {
                    if (!Data.ContainsKey(key))
                    {
                        Data.Add(key, error.ErrorMessage);
                    }
                }
            }
        }

        public ModelMessage Bind(ModelStateDictionary ModelState)
        {
            BindModelState(ModelState);
            return this;
        }

        public void Add(string key, string value)
        {
            if (!Data.ContainsKey(key))
            {
                Data.Add(key, value);
            }
        }

        public void Remove(string key)
        {
            Data.Remove(key);
        }

        public async void SetTimeout(Action<ModelMessage> action, int timeout)
        {
            if (timeout > 0)
            {
                await Task.Delay(timeout);
            }
            else
            {
                await Task.Yield();
            }

            action(this);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbCustomer_tbCustomerType_CusTypeID",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_tbCustomer_tbSourceOfInfo_SourceID",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.DropIndex(
                name: "IX_tbCustomer_CusTypeID",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.DropIndex(
                name: "IX_tbCustomer_SourceID",
                schema: "dbo",
                table: "tbCustomer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_tbCustomer_CusTypeID",
                schema: "dbo",
                table: "tbCustomer",
                column: "CusTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_tbCustomer_SourceID",
                schema: "dbo",
                table: "tbCustomer",
                column: "SourceID");

            migrationBuilder.AddForeignKey(
                name: "FK_tbCustomer_tbCustomerType_CusTypeID",
                schema: "dbo",
                table: "tbCustomer",
                column: "CusTypeID",
                principalSchema: "dbo",
                principalTable: "tbCustomerType",
                principalColumn: "CusTypeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_tbCustomer_tbSourceOfInfo_SourceID",
                schema: "dbo",
                table: "tbCustomer",
                column: "SourceID",
                principalSchema: "dbo",
                principalTable: "tbSourceOfInfo",
                principalColumn: "SourceID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

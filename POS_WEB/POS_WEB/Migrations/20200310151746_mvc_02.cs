﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Hiredate",
                schema: "dbo",
                table: "tbEmployee",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<string>(
                name: "Birthdate",
                schema: "dbo",
                table: "tbEmployee",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "date");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Hiredate",
                schema: "dbo",
                table: "tbEmployee",
                type: "date",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Birthdate",
                schema: "dbo",
                table: "tbEmployee",
                type: "date",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerClass",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.AddColumn<int>(
                name: "Group",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.AddColumn<int>(
                name: "CustomerClass",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                defaultValue: 0);
        }
    }
}

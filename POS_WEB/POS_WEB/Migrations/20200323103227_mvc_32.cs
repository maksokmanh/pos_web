﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_32 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbCustomerType",
                schema: "dbo");

            migrationBuilder.DropColumn(
                name: "CusTypeID",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.RenameColumn(
                name: "Delete",
                schema: "dbo",
                table: "tbWarranty",
                newName: "Deleted");

            migrationBuilder.RenameColumn(
                name: "Delete",
                schema: "dbo",
                table: "tbSourceOfInfo",
                newName: "Deleted");

            migrationBuilder.RenameColumn(
                name: "WarrantyName",
                schema: "dbo",
                table: "tbCustomer",
                newName: "BirthDate");

            migrationBuilder.RenameColumn(
                name: "WarrantyID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "Warranty");

            migrationBuilder.RenameColumn(
                name: "SourceID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "SourceOfInfo");

            migrationBuilder.RenameColumn(
                name: "RegionID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "Region");

            migrationBuilder.RenameColumn(
                name: "DistrictID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "District");

            migrationBuilder.RenameColumn(
                name: "CusID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "CustomerID");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentType",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "I_Model",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "I_Brand",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerType",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "tbCustomerGroup",
                schema: "dbo",
                columns: table => new
                {
                    GroupID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupName = table.Column<string>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbCustomerGroup", x => x.GroupID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbCustomerGroup",
                schema: "dbo");

            migrationBuilder.DropColumn(
                name: "CustomerType",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.RenameColumn(
                name: "Deleted",
                schema: "dbo",
                table: "tbWarranty",
                newName: "Delete");

            migrationBuilder.RenameColumn(
                name: "Deleted",
                schema: "dbo",
                table: "tbSourceOfInfo",
                newName: "Delete");

            migrationBuilder.RenameColumn(
                name: "Warranty",
                schema: "dbo",
                table: "tbCustomer",
                newName: "WarrantyID");

            migrationBuilder.RenameColumn(
                name: "SourceOfInfo",
                schema: "dbo",
                table: "tbCustomer",
                newName: "SourceID");

            migrationBuilder.RenameColumn(
                name: "Region",
                schema: "dbo",
                table: "tbCustomer",
                newName: "RegionID");

            migrationBuilder.RenameColumn(
                name: "District",
                schema: "dbo",
                table: "tbCustomer",
                newName: "DistrictID");

            migrationBuilder.RenameColumn(
                name: "BirthDate",
                schema: "dbo",
                table: "tbCustomer",
                newName: "WarrantyName");

            migrationBuilder.RenameColumn(
                name: "CustomerID",
                schema: "dbo",
                table: "tbCustomer",
                newName: "CusID");

            migrationBuilder.AlterColumn<string>(
                name: "PaymentType",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "I_Model",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "I_Brand",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CusTypeID",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DateOfBirth",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "tbCustomerType",
                schema: "dbo",
                columns: table => new
                {
                    CusTypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CusName = table.Column<string>(nullable: true),
                    Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbCustomerType", x => x.CusTypeID);
                });
        }
    }
}

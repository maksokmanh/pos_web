﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppontmentDate",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.DropColumn(
                name: "Followup",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.RenameColumn(
                name: "Delete",
                schema: "dbo",
                table: "tbCustomer",
                newName: "Deleted");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Deleted",
                schema: "dbo",
                table: "tbCustomer",
                newName: "Delete");

            migrationBuilder.AddColumn<string>(
                name: "AppontmentDate",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Followup",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AppointHistoryID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                newName: "HistoryID");

            migrationBuilder.AddColumn<string>(
                name: "ActivityDate",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ActivityTime",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AppointmentCode",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Closed",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CustomerID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ItemServices",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Paused",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Priority",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Remark",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reminder",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityDate",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "ActivityTime",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "AppointmentCode",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Closed",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "CustomerID",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "ItemServices",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Location",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Paused",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Priority",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Remark",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Reminder",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.RenameColumn(
                name: "HistoryID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                newName: "AppointHistoryID");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerType",
                schema: "dbo",
                table: "tbCustomerAppointment");

            migrationBuilder.DropColumn(
                name: "UserID",
                schema: "dbo",
                table: "tbCustomerAppointment");

            migrationBuilder.RenameColumn(
                name: "Subject",
                schema: "dbo",
                table: "tbCustomerAppointment",
                newName: "ItemServices");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment",
                newName: "Subject");

            migrationBuilder.AddColumn<int>(
                name: "CustomerType",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: false,
                defaultValue: 0);
        }
    }
}

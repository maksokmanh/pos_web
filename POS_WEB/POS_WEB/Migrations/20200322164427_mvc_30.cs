﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_30 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RemarkName",
                schema: "dbo",
                table: "tbDefaultRemark",
                newName: "RemarkText");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RemarkText",
                schema: "dbo",
                table: "tbDefaultRemark",
                newName: "RemarkName");
        }
    }
}

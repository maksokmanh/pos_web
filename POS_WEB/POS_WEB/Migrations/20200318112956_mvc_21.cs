﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerID",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.AlterColumn<string>(
                name: "AppointmentID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AppointmentID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: 0);
        }
    }
}

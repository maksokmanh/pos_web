﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_35 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

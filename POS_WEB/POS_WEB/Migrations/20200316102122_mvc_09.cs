﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_09 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.AddColumn<int>(
                name: "CustomerClass",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerClass",
                schema: "dbo",
                table: "tbCustomer");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true);
        }
    }
}

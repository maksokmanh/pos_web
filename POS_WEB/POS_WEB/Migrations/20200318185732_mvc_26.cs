﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_26 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityDate",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "ActivityTime",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Closed",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.DropColumn(
                name: "Paused",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.AddColumn<DateTime>(
                name: "FollowUp",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FollowUp",
                schema: "dbo",
                table: "tbAppointmentHistory");

            migrationBuilder.AddColumn<string>(
                name: "ActivityDate",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ActivityTime",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Closed",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Paused",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                defaultValue: false);
        }
    }
}

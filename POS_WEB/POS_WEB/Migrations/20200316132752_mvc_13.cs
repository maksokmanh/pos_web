﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbItemService_tbCustomerAppointment_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.DropIndex(
                name: "IX_tbItemService_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.DropColumn(
                name: "CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.AddColumn<string>(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment");

            migrationBuilder.AddColumn<int>(
                name: "CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_tbItemService_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                column: "CustomerAppointmentAppointmentID");

            migrationBuilder.AddForeignKey(
                name: "FK_tbItemService_tbCustomerAppointment_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                column: "CustomerAppointmentAppointmentID",
                principalSchema: "dbo",
                principalTable: "tbCustomerAppointment",
                principalColumn: "AppointmentID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AppointmentID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AppointmentID",
                schema: "dbo",
                table: "tbAppointmentHistory",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment");

            migrationBuilder.AddColumn<int>(
                name: "CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_tbItemService_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                column: "CustomerAppointmentAppointmentID");

            migrationBuilder.AddForeignKey(
                name: "FK_tbItemService_tbCustomerAppointment_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService",
                column: "CustomerAppointmentAppointmentID",
                principalSchema: "dbo",
                principalTable: "tbCustomerAppointment",
                principalColumn: "AppointmentID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbItemService_tbCustomerAppointment_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.DropIndex(
                name: "IX_tbItemService_CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.DropColumn(
                name: "CustomerAppointmentAppointmentID",
                schema: "dbo",
                table: "tbItemService");

            migrationBuilder.AddColumn<string>(
                name: "ItemServices",
                schema: "dbo",
                table: "tbCustomerAppointment",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(string));

           
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_31 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Plate",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Frame",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Engine",
                schema: "dbo",
                table: "tbCustomer",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Plate",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Frame",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Engine",
                schema: "dbo",
                table: "tbCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

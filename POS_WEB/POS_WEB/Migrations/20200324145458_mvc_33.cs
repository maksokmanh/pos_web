﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_33 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Deleted",
                schema: "dbo",
                table: "tbWarranty",
                newName: "Active");

            migrationBuilder.RenameColumn(
                name: "Deleted",
                schema: "dbo",
                table: "tbCustomerGroup",
                newName: "Active");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Active",
                schema: "dbo",
                table: "tbWarranty",
                newName: "Deleted");

            migrationBuilder.RenameColumn(
                name: "Active",
                schema: "dbo",
                table: "tbCustomerGroup",
                newName: "Deleted");
        }
    }
}

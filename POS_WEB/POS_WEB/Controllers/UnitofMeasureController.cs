﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers.Developer2
{
    [Authorize]
    public class UnitofMeasureController : Controller
    {
        private readonly DataContext _context;
        private readonly IUOM _uOM;

        public UnitofMeasureController(DataContext context,IUOM uOM)
        {
            _context = context;
            _uOM = uOM;
        }
        [HttpGet]
        public async Task<IActionResult> Index(string category="5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
           
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure";
            ViewBag.Administrator = "show";
            ViewBag.Inventory = "show";
            ViewBag.UnitofMeasure = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A006");
            var data = _uOM.GetUnitofMeasures();
            var Cate = from s in data select s;
            int pageSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["CodeSortParm"] = sortOrder == "Code" ? "Code_desc" : "Code";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Code.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Code":
                        Cate = Cate.OrderBy(s => s.Code);
                        break;
                    case "Code_desc":
                        Cate = Cate.OrderByDescending(s => s.Code);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                //int pageSize=0;
                int.TryParse(category, out int MaxSize);
                //int t = int.Parse(category);
                //int t1 = Convert.ToInt16(category);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = category;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = category;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = category;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = category;
                    }
                    else
                    {
                        ViewBag.sizepage5 = category;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<UnitofMeasure>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["CodeSortParm"] = sortOrder == "Code" ? "Code_desc" : "Code";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Code.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Code":
                            Cate = Cate.OrderBy(s => s.Code);
                            break;
                        case "Code_desc":
                            Cate = Cate.OrderByDescending(s => s.Code);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    //int pageSize=0;
                    int.TryParse(category, out int MaxSize);
                    //int t = int.Parse(category);
                    //int t1 = Convert.ToInt16(category);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = category;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = category;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = category;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = category;
                        }
                        else
                        {
                            ViewBag.sizepage5 = category;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<UnitofMeasure>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }       
        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A006");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Name,Delete")] UnitofMeasure unitofMeasure)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";
            if (ModelState.IsValid)
            {

               
                var m = _context.UnitofMeasures.FirstOrDefault(x => x.Code == unitofMeasure.Code);
                if (m == null)
                {                    
                    await _uOM.AddOrEidt(unitofMeasure);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    
                    ViewBag.Error = "This code already exist!";
                }
            }
            return View(unitofMeasure);
        }
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var unitofMeasure = _uOM.getid(id);
                if (unitofMeasure.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (unitofMeasure == null)
                {
                    return NotFound();
                }
                return View(unitofMeasure);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A006");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var unitofMeasure = _uOM.getid(id);
                    if (unitofMeasure.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (unitofMeasure == null)
                    {
                        return NotFound();
                    }
                    return View(unitofMeasure);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,Name,Delete")] UnitofMeasure unitofMeasure)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            if (unitofMeasure.Code == null)
            {
                ViewBag.Requied = "Pleaes input code !";
                return View(unitofMeasure);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await _uOM.AddOrEidt(unitofMeasure);
                }
                catch (Exception)
                {
                    ViewBag.code = "This is code already exist ";
                    return View(unitofMeasure);
                }
               
                return RedirectToAction(nameof(Index));
            }
            return View(unitofMeasure);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteUOm(int id)
        {
           await _uOM.DeleteUOM(id);
            return Ok();
        }
        [HttpGet]
        public IActionResult GetUOM()
        {
            var list = _uOM.GetUnitofMeasures().Where(u=>u.Delete==false).ToList();
            return Ok(list);
        }
   }
}

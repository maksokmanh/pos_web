﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.Responsitory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class GoodsReceiptController : Controller
    {
        private readonly DataContext _context;
        private readonly IGoodsReceipt _receipt;
        public GoodsReceiptController(DataContext context, IGoodsReceipt receipt)
        { 
            _context = context; 
            _receipt =receipt;
        }
        [HttpGet]
        public IActionResult GoodsReceipt()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Transaction";
            ViewBag.Subpage = "Goods Receipt";
            ViewBag.InventoryMenu = "show";
            ViewBag.Transaction = "show";
            ViewBag.GoodsReceipt = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A039");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.GoodsReceipts.Count() + 1;
            var list = "SI-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousesFrom(int ID)
        {
            var list = _receipt.GetWarehouse_From(ID);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseFrom(int ID)
        {
            var list = _receipt.GetItemByWarehouse_From(ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveGoodsReceipt(GoodsReceipt goodsReceipt)
        {
            List<GoodReceiptDetail> list = new List<GoodReceiptDetail>();
            foreach (var item in goodsReceipt.GoodReceiptDetails.ToList())
            {
                var checkwarehouse = _context.WarehouseDetails.FirstOrDefault(x => x.ExpireDate == item.ExpireDate &&
                                                                              x.Cost == item.Cost &&
                                                                              x.UomID == item.UomID &&
                                                                              x.ItemID == item.ItemID &&
                                                                              x.WarehouseID == goodsReceipt.WarehouseID);
                if (checkwarehouse == null)
                {
                    item.Check = "Item not found in stock";
                    list.Add(item);
                }
            }
            if (list.Count() > 0)
            {
                return Ok(list);
            }
            else
            {
                _receipt.SaveGoodIssues(goodsReceipt);
                return Ok();
            }
          
        }
        [HttpGet] 
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            try
            {
                var list = _receipt.GetItemFindeBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
    }
}

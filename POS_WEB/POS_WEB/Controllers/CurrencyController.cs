﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class CurrencyController : Controller
    {
        private readonly DataContext _context;
        private readonly ICurrency _currency;

        public CurrencyController(DataContext context,ICurrency currency)
        {
            _context = context;
            _currency = currency;
        }
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Currency";
            ViewBag.Banking = "show";
            ViewBag.Currency = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A013");
            var data = _currency.Cuurencies().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["SymbolSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Symbol_desc" : "";
                ViewData["DescriptionSortParm"] = sortOrder == "Description" ? "Description_desc" : "Description";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Symbol.Contains(searchString) || s.Description.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "Symbol_desc":
                        Cate = Cate.OrderByDescending(s => s.Symbol);
                        break;
                    case "Description_desc":
                        Cate = Cate.OrderByDescending(s => s.Description);
                        break;
                    case "Description":
                        Cate = Cate.OrderBy(s => s.Description);
                        break;

                    default:
                        Cate = Cate.OrderBy(s => s.Symbol);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<Currency>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["SymbolSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Symbol_desc" : "";
                    ViewData["DescriptionSortParm"] = sortOrder == "Description" ? "Description_desc" : "Description";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Symbol.Contains(searchString) || s.Description.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "Symbol_desc":
                            Cate = Cate.OrderByDescending(s => s.Symbol);
                            break;
                        case "Description_desc":
                            Cate = Cate.OrderByDescending(s => s.Description);
                            break;
                        case "Description":
                            Cate = Cate.OrderBy(s => s.Description);
                            break;

                        default:
                            Cate = Cate.OrderBy(s => s.Symbol);
                            break;
                    }
                    // int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<Currency>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }     
        public IActionResult Create()
        {
            ViewBag.style = "fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Currency";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A013");
            try
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("AccessDenied", "Account");
            }       
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Symbol,Description,Delete")] Currency currency)
        {
            ViewBag.style = "fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Currency";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                await _currency.AddorEdit(currency);
                return RedirectToAction(nameof(Index));
            }
            return View(currency);
        }
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Currency";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var currency = _currency.GetId(id);
                if (currency.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (currency == null)
                {
                    return NotFound();
                }
                return View(currency);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A013");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    var currency = _currency.GetId(id);
                    if (currency.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (currency == null)
                    {
                        return NotFound();
                    }
                    return View(currency);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }          
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Symbol,Description,Delete")] Currency currency)
        {
            ViewBag.style = "fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Currency";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                try
                {
                    await _currency.AddorEdit(currency);
                }
                catch (Exception)
                {
                    
                }
                return RedirectToAction(nameof(Index));
            }
            return View(currency);
        }
       [HttpDelete]
       public async Task<IActionResult> DeleteCurrency(int id)
        {
            await _currency.Delete(id);
            return Ok();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetCurrency()
        {
            var list = _currency.Cuurencies().OrderByDescending(x=>x.ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> AddCurrency(Currency currency)
        {
           await _currency.AddorEdit(currency);
           return Ok();
        }

    }
}

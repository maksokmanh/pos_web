﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly DataContext _context;
        private readonly IUserAccount _user;
        private readonly IEmployee _employee;
        
        public IActionResult ShowNotFound()
        {
            return NotFound();
        }
        public AccountController(DataContext context, IUserAccount user,IEmployee employee)
        {
            _context = context;
            _user = user;
            _employee = employee;
          
        }
      
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "User Account";
            ViewBag.Administrator = "show";
            ViewBag.UserAccount = "highlight";
            ViewBag.General = "show";
            var useid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var Login = _context.LoginModals.FirstOrDefault(w => w.Status == true && w.UserID==int.Parse(User.FindFirst("UserID").Value));
                useid = Login.UserID;
            }
            
            var permison = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == useid && x.Code == "A004");
            var data = _user.UserAccounts().OrderBy(x => x.ID);
            var Cate = from s in data select s;
            int pageSize = 5, MaxSize = 0;
            try
            {
                if (permison.Used == true)
                {
                    //var data = _user.UserAccounts().OrderBy(x => x.ID);
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["UsernameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["EmployeeSortParm"] = sortOrder == "Emp" ? "Emp_desc" : "Emp";
                    ViewData["CompanySortParm"] = sortOrder == "com" ? "com_desc" : "com";
                    ViewData["BarnchSortParm"] = sortOrder == "bran" ? "bran_desc" : "bran";


                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Username.Contains(searchString) ||
                        s.Employee.Name.Contains(searchString) || s.Company.Name.Contains(searchString) ||
                         s.Branch.Name.Contains(searchString));


                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Username);
                            break;
                        case "Emp":
                            Cate = Cate.OrderBy(s => s.EmployeeID);
                            break;
                        case "Emp_desc":
                            Cate = Cate.OrderByDescending(s => s.EmployeeID);
                            break;
                        case "com":
                            Cate = Cate.OrderBy(s => s.CompanyID);
                            break;
                        case "com_desc":
                            Cate = Cate.OrderByDescending(s => s.CompanyID);
                            break;
                        case "bran":
                            Cate = Cate.OrderBy(s => s.BranchID);
                            break;
                        case "bran_desc":
                            Cate = Cate.OrderByDescending(s => s.BranchID);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Username);
                            break;

                    }
                   
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<UserAccount>.CreateAsync(Cate, page ?? 1, pageSize));
                    
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
               
            }
            catch (Exception)
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["UsernameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["EmployeeSortParm"] = sortOrder == "Emp" ? "Emp_desc" : "Emp";
                ViewData["CompanySortParm"] = sortOrder == "com" ? "com_desc" : "com";
                ViewData["BarnchSortParm"] = sortOrder == "bran" ? "bran_desc" : "bran";


                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;



                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Username.Contains(searchString) ||
                    s.Employee.Name.Contains(searchString) || s.Company.Name.Contains(searchString) ||
                     s.Branch.Name.Contains(searchString));


                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Username);
                        break;
                    case "Emp":
                        Cate = Cate.OrderBy(s => s.EmployeeID);
                        break;
                    case "Emp_desc":
                        Cate = Cate.OrderByDescending(s => s.EmployeeID);
                        break;
                    case "com":
                        Cate = Cate.OrderBy(s => s.CompanyID);
                        break;
                    case "com_desc":
                        Cate = Cate.OrderByDescending(s => s.CompanyID);
                        break;
                    case "bran":
                        Cate = Cate.OrderBy(s => s.BranchID);
                        break;
                    case "bran_desc":
                        Cate = Cate.OrderByDescending(s => s.BranchID);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Username);
                        break;

                }

                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<UserAccount>.CreateAsync(Cate, page ?? 1, pageSize));
            }
        }
        [HttpGet]
        public IActionResult Register()
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "User Account";
            ViewBag.type = "Cearte";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
     
            var useid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var Login = _context.LoginModals.FirstOrDefault(w => w.Status == true && w.UserID == int.Parse(User.FindFirst("UserID").Value));
                useid = Login.UserID;
            }
            var permison = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == useid && x.Code == "A004");
            try
            {
                if (permison.Used == true)
                {
                    ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                    ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                    ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false && e.IsUser == false), "ID", "Name");
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false && e.IsUser == false), "ID", "Name");
            }

            return View();
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]      
        public async Task<IActionResult> Register([Bind("ID,EmployeeID,CompanyID,BranchID,Username,Password,ComfirmPassword,Language,Delete")] UserAccount userAccount)
        {

            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "User Account";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var count = 0;
            if (userAccount.BranchID == null)
            {
                count++;
                ViewBag.requriedBrand = "Please select branch !";
            }
            if (userAccount.CompanyID == null)
            {
                count++;
                ViewBag.requiredcompany = "Please select company !";
            }
            if (userAccount.EmployeeID == null)
            {
                count++;
                ViewBag.requiredEmp = "Please select employee !";
            }
            if (count <= 0)
            {
                if (ModelState.IsValid)
                {
                    var pass = userAccount.Password;
                    userAccount.Password = Encrypt(pass);
                    await _user.Register(userAccount);
                    var emp = _context.Employees.FirstOrDefault(e => e.ID == userAccount.EmployeeID);
                    if (emp.IsUser == true)
                    {
                        emp.IsUser = true;
                    }
                    else
                    {
                        emp.IsUser = true;
                    }

                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }        
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name", userAccount.BranchID);
            ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name", userAccount.CompanyID);
            ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false && e.IsUser == false), "ID", "Name", userAccount.EmployeeID);
            return View(userAccount);
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "User Account";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
     
            var useid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var Login = _context.LoginModals.FirstOrDefault(w => w.Status == true && w.UserID ==int.Parse(User.FindFirst("UserID").Value));
                useid = Login.UserID;
            }
            var permison = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == useid && x.Code == "A004");
            try
            {
                if (permison.Used == true)
                {
                    var user = _user.Getid(id);
                    user.Password = Decrypt(user.Password);
                    ViewBag.pass = user.Password;
                    if (user.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                    ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                    ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false), "ID", "Name");
                    return View(user);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                var user = _user.Getid(id);
                user.Password = Decrypt(user.Password);
                ViewBag.pass = user.Password;
                if (user.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false), "ID", "Name");
                return View(user);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,[Bind("ID,EmployeeID,CompanyID,BranchID,Username,Password,ComfirmPassword,Language,Delete")] UserAccount userAccount)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "User Account";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var count = 0;
            if (userAccount.BranchID == null)
            {
                count++;
                ViewBag.requriedBrand = "Please select branch !";
            }
            if (userAccount.CompanyID == null)
            {
                count++;
                ViewBag.requiredcompany = "Please select company !";
            }
            if (userAccount.EmployeeID == null)
            {
                count++;
                ViewBag.requiredEmp = "Please select employee !";
            }
            if (count <= 0)
            {
                if (ModelState.IsValid)
                {
                    userAccount.Password = Encrypt(userAccount.Password);
                    await _user.Register(userAccount);
                    var emp = _context.Employees.FirstOrDefault(e => e.ID == userAccount.EmployeeID);
                    if (emp.IsUser == true)
                    {
                        emp.IsUser = true;
                    }
                    else
                    {
                        emp.IsUser = true;
                    }

                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name", userAccount.BranchID);
            ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name", userAccount.CompanyID);
            ViewData["EmployeeID"] = new SelectList(_context.Employees.Where(e => e.Deleted == false && e.IsUser == false), "ID", "Name", userAccount.EmployeeID);
            return View(userAccount);
        }

        public string Encrypt(string clearText)
        {
            if (clearText != null)
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            else
            {
                return clearText;
            }
           
        }

        public  string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false),"ID","Name");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserAccount userAccount, LoginViewsModal login, string returnUrl)
        {
                bool isUservalid = false;
                login.Password = Encrypt(login.Password);
             if (login.Password == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && login.UserName == "Kernel")
                {
                    isUservalid = true;
                    if (isUservalid)
                    {

                        var claims = new List<Claim>();
                        claims.Add(new Claim("UserID", "1"));
                        claims.Add(new Claim("BranchID", "1"));
                        claims.Add(new Claim(ClaimTypes.Name, "Manager"));
                        claims.Add(new Claim("Password", login.Password));
                        claims.Add(new Claim("Username", login.UserName));
                        claims.Add(new Claim("Photo", "~/Images/User/default_user.png"));
                        claims.Add(new Claim("FullName", "KERNEL"));
                        claims.Add(new Claim("Position", "Manager"));
                        claims.Add(new Claim("Gender", "Male"));
                        claims.Add(new Claim("Phone", "0962342667"));
                        claims.Add(new Claim("Email", "Kernel@gmail.com"));
                        claims.Add(new Claim("Address", "Phnom Penh"));
                        claims.Add(new Claim("Code", "K001"));
                        claims.Add(new Claim("Birthdate", "8/8/2018"));
                        claims.Add(new Claim("Hiredate", "9/9/2019"));
                        claims.Add(new Claim("language", "Khmer"));
                        claims.Add(new Claim("Logo", "~/Images/User/Logo.gif"));
                        claims.Add(new Claim("CompanyName", "KERNEL"));
                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var principal = new ClaimsPrincipal(identity);
                        var props = new AuthenticationProperties
                        {
                            IsPersistent = login.Check,
                            ExpiresUtc = DateTime.UtcNow.AddHours(24)
                        };
                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                else
                {
                    var psw = login.Password;
                    var user = await _context.UserAccounts.FirstOrDefaultAsync(x =>
                    x.Username == login.UserName && x.Password == psw);
                    if (user != null)
                    {     
                        if (login.Branch == 0)
                        {
                               ViewBag.rquriedBranch = "Please select branch !";
                               ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false),"ID","Name");
                               return View();
                        }
                        else {
                              var Loginbrach = await _context.UserAccounts.FirstOrDefaultAsync(x => x.BranchID == login.Branch);
                              if (Loginbrach != null)
                              {
                                    //var currentUser = _context.UserAccounts.FirstOrDefault(x => x.ID == user.ID && x.Status == false);

                                    //if (currentUser != null)
                                    //{
                                    //    currentUser.Status = true;

                                        LoginModal savelogin = new LoginModal
                                        {
                                            UserID = user.ID,
                                            Datelogin = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")),
                                            Timelogin = DateTime.Now.ToString("hh:mm:ss tt"),
                                            Status = true
                                        };
                                        _context.LoginModals.Add(savelogin);
                                        _context.SaveChanges();
                                        var emp = _context.Employees.FirstOrDefault(x => x.ID == user.EmployeeID);
                                        var com = _context.Company.FirstOrDefault(x => x.ID == user.CompanyID);
                                        var barn = _context.Branches.FirstOrDefault(x => x.ID == user.BranchID);
                                        var claims = new List<Claim>();
                                        claims.Add(new Claim(ClaimTypes.Name, (user.Username != null) ? user.Username : "KERNEL"));
                                        claims.Add(new Claim("Password", login.Password));
                                        claims.Add(new Claim("Username", login.UserName));
                                        claims.Add(new Claim("UserID", user.ID.ToString()));
                                        claims.Add(new Claim("Photo", (emp.Photo != null) ? "~/Images/employee/" + emp.Photo : "~/Images/User/default_user.png"));
                                        claims.Add(new Claim("FullName", (emp.Name != null) ? emp.Name : ""));
                                        claims.Add(new Claim("Position", (emp.Position != null) ? emp.Position : ""));

                                        claims.Add(new Claim("Gender", (emp.Gender.ToString() != null) ? emp.Gender.ToString() : ""));
                                        claims.Add(new Claim("Phone", (emp.Phone.ToString() != null) ? emp.Phone.ToString() : ""));
                                        claims.Add(new Claim("Email", (emp.Email != null) ? emp.Email : ""));
                                        claims.Add(new Claim("Address", (emp.Address != null) ? emp.Address : ""));
                                        claims.Add(new Claim("Code", (emp.Code != null) ? emp.Code : "K001"));
                                        claims.Add(new Claim("Birthdate", (DateTime.Parse(emp.Birthdate).ToString("dd-MM-yyyy") != "") ? DateTime.Parse(emp.Birthdate).ToString("dd-MM-yyyy") : "01/01/2019"));
                                        claims.Add(new Claim("Hiredate", (DateTime.Parse(emp.Hiredate).ToString("dd-MM-yyyy") != "") ? DateTime.Parse(emp.Hiredate).ToString("dd-MM-yyyy") : "9/9/2019"));
                                        //claims.Add(new Claim("language", (user.Language != "") ? user.Language : "Khmer"));
                                        claims.Add(new Claim("Logo", (com.Logo != null) ? "~/Images/company/" + com.Logo : "~/Images/User/Logo.gif"));
                                        claims.Add(new Claim("CompanyName", (com.Name != null) ? com.Name : ""));
                                        claims.Add(new Claim("CompanyID",com.ID.ToString()));
                                        claims.Add(new Claim("BranchID", barn.ID.ToString()));
                                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                                        var principal = new ClaimsPrincipal(identity);
                                        var props = new AuthenticationProperties
                                        {
                                            IsPersistent = login.Check,
                                            ExpiresUtc = DateTime.UtcNow.AddHours(24)
                                        };
                                        //if (props.ExpiresUtc == DateTime.UtcNow)
                                        //{
                                        //    currentUser.Status = false;

                                        //}
                                        //_context.Update(currentUser);
                                        //await _context.SaveChangesAsync();
                                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                                        return RedirectToAction("Dashboard", "Home");
                                    //}
                                    //  else
                                    //  {
                                    //        ViewBag.VilidUser = "User stay Login";
                                    //  }
                             }
                             else
                             {
                                ModelState.AddModelError("", "User can not login this branch !");
                                ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                                return View();
                            }
                        }
                    }
                    else
                    {

                        ModelState.AddModelError("", "Username or Password is incorrect!");
                        ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
                        return View();
                    }
               
                }
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(x => x.Delete == false), "ID", "Name");
            return View(); 
            
        }
        [AllowAnonymous]
        public IActionResult Logout()
        {
            if(User.FindFirst("Password").Value== "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction(nameof(Login));
            }
            else
            {
                var Login = _context.LoginModals.FirstOrDefault(w => w.UserID ==int.Parse(User.FindFirst("UserID").Value) && w.Status == true);
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                Login.Status = false;
               _context.Update(Login);
                //var CheckLogin = _context.UserAccounts.FirstOrDefault(x => x.ID == int.Parse(User.FindFirst("UserID").Value) && x.Status == true);
                //CheckLogin.Status = false;
                //_context.Update(CheckLogin);
                _context.SaveChanges();
                return RedirectToAction(nameof(Login));
            }
                  
        }
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Userprivillege(int id)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Set User Privillege";
            ViewBag.Menu = "show";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _context.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A043");
            try
            {
                if (permision.Used == true)
                {
                    ViewData["UserPrivillege"] = _user.GetUserPrivilleges(id).ToList();
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            catch (Exception)
            {
                ViewData["UserPrivillege"] = _user.GetUserPrivilleges(id).ToList();
                return View();
            }

           
        }
      
        [HttpPost]
        public IActionResult UpdateUserPrivillege(List<UserPrivillege> userPrivilleges)
        {
            _user.UpdateUserPrivilleges(userPrivilleges);
            return Ok('Y');
        }
        [HttpPost]
        public IActionResult SelectAll(bool All,int UserID)
        {
             _user.updateAllselect(All, UserID);
            return Ok();
        }
        public IActionResult ChangPassword()
        {
            ViewBag.style = "fa fa-cog";
            ViewBag.Page = "Change Username And Password Account";
            ViewBag.Menu = "show";
            return View();
        }
        [HttpPost]
        public IActionResult ChangPassword([Bind("Username,OldPassword,NewPassword,comfirmd")] ChangPasswordAccountService changPassword)
        {
            ViewBag.style = "fa fa-cog";
            ViewBag.Page = "Change Username And Password Account";
            if (ModelState.IsValid)
            {
                var account = _context.UserAccounts.FirstOrDefault(x => x.ID == int.Parse(User.FindFirst("UserID").Value));
                if (account != null)
                {
                    var pass = Encrypt(changPassword.NewPassword);
                     account.Password = pass;
                    _context.Update(account);
                    _context.SaveChanges();

                }
            }
            else
            {
                if (changPassword.Username ==null)
                {
                    ViewBag.requreduserman = "Please input username !";
                }
                if (changPassword.OldPassword == null)
                {
                    ViewBag.requredoldpass = "Please input old password !";
                }
                if (changPassword.NewPassword == null)
                {
                    ViewBag.requrednewpass = "Please input new password !";
                }
                if (changPassword.comfirmd == null)
                {
                    ViewBag.requredcomfirm = "Please input comfirmd password !";
                }
            }   
            return View();
        }
        public IActionResult Profile()
        {
            ViewBag.style = "fa fa-user";
            ViewBag.Page = "My Profile";
            ViewBag.Menu = "show";

            return View(); 

        }
        [HttpGet]
        public IActionResult CheckOldPassword(string passwordold)
        {
            var pass = Encrypt(passwordold);
            var data= _context.UserAccounts.FirstOrDefault(x => x.Password == pass);
            if (data == null)
            {
                return Ok("F");
            }
            else
            {
                return Ok("T");
            }
        }
        
        public async Task<IActionResult> Delete(int id)
        {
            await _user.Delete(id);
            var User = _context.UserAccounts.FirstOrDefault(x => x.ID == id);
            var Emp = _context.Employees.FirstOrDefault(x => x.ID ==User.EmployeeID);
            Emp.IsUser = false;
            _context.Employees.Update(Emp);
           await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.Responsitory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace POS_WEB.Controllers
{
    [Authorize]
    public class PurchaseOrderController : Controller
    {
        private readonly DataContext _context;
        private readonly IPurchaseOrder _order;
        public PurchaseOrderController(DataContext context,IPurchaseOrder order)
        {
            _context = context;
            _order = order;
        }
        public IActionResult Purchaseorder()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Purchase Order";
            ViewBag.Menu = "show";
            ViewBag.PurchaseMenu = "show";
            ViewBag.PurchasePO = "highlight";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _context.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A023");
            try
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }
        public IActionResult PurchaseOrderHistory()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Purchase Order";
            ViewBag.Subpage = "Purchase Order History";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _context.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A023");
            try
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.PurchaseOrders.Count() + 1;
            var list = "PO-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartners()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type== "Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCurrencyDefualt()
        {
            var list = _order.GetCurrencyDefualt().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID && x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetGroupDefind()
        {
            var list = _order.GetAllGroupDefind().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult Getcurrency()
        {
            var list = _context.Currency.Where(x => x.Delete == false);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_Order(int ID)
        {
            var list = _order.ServiceMapItemMasterDataPurchaseOrders(ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SavePurchaseOrder(PurchaseOrder purchase,string Type)
        {
           
            if (Type == "Add" || Type=="Update")
            {
                if (purchase.PurchaseOrderID == 0)
                {
                    _context.PurchaseOrders.Add(purchase);
                    _context.SaveChanges();
                    foreach (var check in purchase.PurchaseOrderDetails.ToList())
                    {
                        if (check.Qty <=0)
                        {
                            _context.Remove(check);
                            _context.SaveChanges();
                        }
                    }
                    _order.GoodReceiptStockOrder(purchase.PurchaseOrderID);
                }
                else
                {
                    _context.PurchaseOrders.Update(purchase);
                    _context.SaveChanges();

                    foreach (var item in purchase.PurchaseOrderDetails.ToList())
                    {
                       
                        var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID ==item.ItemID);
                        var gduom = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_master.GroupUomID && w.AltUOM == item.UomID);
                        //Remove item order
                        if (item.Qty<=0)
                        {            
                            var warehouse_sum = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == purchase.WarehouseID
                                                                                            && w.ItemID == item.ItemID);
                            if(warehouse_sum!=null)
                            {
                                warehouse_sum.Ordered = warehouse_sum.Ordered - item.OldQty*gduom.Factor;
                                _context.WarehouseSummary.Update(warehouse_sum);
                                _context.SaveChanges();

                            }
                            //Update in item maser
                            item_master.StockOnHand = item_master.StockOnHand - (item.OldQty * gduom.Factor);
                            _context.ItemMasterDatas.Update(item_master);

                            _context.PurchaseOrderDetails.Remove(item);
                            _context.SaveChanges();

                           
                        }
                        else
                        {
                            var add_qty = item.Qty - item.OldQty;
                            var warehouse_sum = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == purchase.WarehouseID
                                                                                            && w.ItemID == item.ItemID
                                                                                            );
                            if (warehouse_sum != null)
                            {
                                warehouse_sum.Ordered = warehouse_sum.Ordered + (add_qty * gduom.Factor);
                                _context.WarehouseSummary.Update(warehouse_sum);
                                _context.SaveChanges();
                            }
                            //Update in item maser
                            item_master.StockOnHand=item_master.StockOnHand+(add_qty * gduom.Factor);
                            _context.ItemMasterDatas.Update(item_master);
                            _context.SaveChanges();
                        }
                      
                    }
                }
            }
            else if (Type == "PQ")
            {
                var remark = purchase.Remark;
                string InvoiceOrder = "";
                double openqty = 0;
                InvoiceOrder = (remark.Split("/"))[1];
                List<PurchaseOrderDetail> Comfirn = new List<PurchaseOrderDetail>();
                foreach (var items in purchase.PurchaseOrderDetails.ToList())
                {
                    if (items.Qty > 0)
                    {
                        Comfirn.Add(items);
                    }
                }
                if (purchase.PurchaseOrderID == 0)
                {
                    _context.PurchaseOrders.Add(purchase);
                    _context.SaveChanges();
                    foreach (var check in purchase.PurchaseOrderDetails.ToList())
                    {
                        if (check.Qty <= 0)
                        {
                            _context.Remove(check);
                            _context.SaveChanges();
                        }
                    }
                    _order.GoodReceiptStockOrder(purchase.PurchaseOrderID);

                    var purchaseQuotation = _context.PurchaseQuotations.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);

                    int QuoID = purchaseQuotation.PurchaseQuotationID;
                    var detail = _context.PurchaseQuotationDetails.Where(x => x.PurchaseQuotationID == QuoID && x.Delete == false);
                    if (Comfirn.Count() > 0)
                    {
                        foreach (var item in detail.ToList())
                        {
                            foreach (var items in Comfirn.ToList())
                            {
                                if (item.PurchaseQuotaionDetailID == items.QuotationID)
                                {
                                    if (items.Qty > item.OpenQty)
                                    {
                                        openqty = item.OpenQty - item.OpenQty;
                                        var purchaseDetail_1 = _context.PurchaseQuotationDetails.FirstOrDefault(x => x.PurchaseQuotaionDetailID == item.PurchaseQuotaionDetailID);
                                        purchaseDetail_1.OpenQty = openqty;
                                        _context.Update(purchaseDetail_1);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail_1.Delete = true;
                                            _context.Update(purchaseDetail_1);
                                            _context.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        openqty = item.OpenQty - items.Qty;
                                        var purchaseDetail = _context.PurchaseQuotationDetails.FirstOrDefault(x => x.PurchaseQuotaionDetailID == item.PurchaseQuotaionDetailID);
                                        purchaseDetail.OpenQty = openqty;
                                        _context.Update(purchaseDetail);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail.Delete = true;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();
                                        }
                                    }
                                   
                                }
                            }
                        }
                    }
                    if (checkStatus(detail))
                    {
                        var purchaseAP = _context.PurchaseQuotations.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);
                        purchaseAP.Status = "close";
                        _context.Update(purchaseAP);
                        _context.SaveChanges();
                    }
                }
            }
            else if (Type == "PR")
            {
                var remark = purchase.Remark;
                string InvoiceOrder = "";
                double openqty = 0;
                InvoiceOrder = (remark.Split("/"))[1];
                List<PurchaseOrderDetail> Comfirn = new List<PurchaseOrderDetail>();
                foreach (var items in purchase.PurchaseOrderDetails.ToList())
                {
                    if (items.Qty > 0)
                    {
                        Comfirn.Add(items);
                    }
                }
                if (purchase.PurchaseOrderID == 0)
                {
                    _context.PurchaseOrders.Add(purchase);
                    _context.SaveChanges();
                    foreach (var check in purchase.PurchaseOrderDetails.ToList())
                    {
                        if (check.Qty <= 0)
                        {
                            _context.Remove(check);
                            _context.SaveChanges();
                        }
                    }
                    _order.GoodReceiptStockOrder(purchase.PurchaseOrderID);

                    var purchaseRequest = _context.PurchaseRequests.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);

                    int ReqestID = purchaseRequest.PurchaseRequestID;
                    var detail = _context.PurchaseRequestDetails.Where(x => x.PurchaseRequestID == ReqestID && x.Delete == false);
                    if (Comfirn.Count() > 0)
                    {
                        foreach (var item in detail.ToList())
                        {
                            foreach (var items in Comfirn.ToList())
                            {
                                if (item.RequiredDetailID == items.QuotationID)
                                {
                                    if (items.Qty > item.OpenQty)
                                    {
                                        openqty = item.OpenQty - item.OpenQty;
                                        var purchaseDetail_1 = _context.PurchaseRequestDetails.FirstOrDefault(x => x.RequiredDetailID == item.RequiredDetailID);
                                        purchaseDetail_1.OpenQty = openqty;
                                        _context.Update(purchaseDetail_1);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail_1.Delete = true;
                                            _context.Update(purchaseDetail_1);
                                            _context.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        openqty = item.OpenQty - items.Qty;
                                        var purchaseDetail = _context.PurchaseRequestDetails.FirstOrDefault(x => x.RequiredDetailID == item.RequiredDetailID);
                                        purchaseDetail.OpenQty = openqty;
                                        _context.Update(purchaseDetail);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail.Delete = true;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();
                                        }
                                    }

                                }
                            }
                        }
                    }
                    if (checkStatus(detail))
                    {
                        var purchaseAP = _context.PurchaseRequests.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);
                        purchaseAP.Status = "close";
                        _context.Update(purchaseAP);
                        _context.SaveChanges();
                    }
                }
            }
            return Ok();
        }
        public bool checkStatus(IEnumerable<PurchaseQuotationDetail> invoices)
        {
            bool result = true;
            foreach (var inv in invoices)
            {
                if (inv.Delete == false)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        public bool checkStatus(IEnumerable<PurchaseRequestDetail> invoices)
        {
            bool result = true;
            foreach (var inv in invoices)
            {
                if (inv.Delete == false)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseOrder_Detail(int warehouseid,string invoice)
        {
            var list = _order.ServiceMapItemMasterDataPurchaseOrders_Detail(warehouseid, invoice).ToList();
            return Ok(list);
        }
        [HttpGet]
        public  IActionResult FindPurchaseOrder(string number)
        {
            
            var list = _context.PurchaseOrders.Include(x => x.PurchaseOrderDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetBusinessPartner_Order()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type=="Vendor").ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult GetUserAccout_Order(int UserID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join
                       emp in _context.Employees.Where(x => x.Deleted == false) on user.EmployeeID equals emp.ID
                       where user.ID == UserID
                       select new UserAccount
                       {
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       };
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_Order(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseOrderReport(int BranchID,int WarehouseID,string PostingDate,string DocumentDate,string DeliveryDate,string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses_order(int ID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseOrderByWarehouse(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseOrderByPostingDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
         [HttpGet]
         public IActionResult GetPurchaseOrderDocumentDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseOrderDeliveryDatedDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
         public IActionResult GetPurchaseOrderAllItem(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeliveryDate, string Check)
        {
            var list = _order.ReportPurchaseOrders(BranchID, WarehouseID, PostingDate, DocumentDate, DeliveryDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            try
            {
                var list = _order.GetItemFindeBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotation(int BranchID)
        {
            var list = _order.GetAllPurchaeQuotation(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurchasQuotation(int ID, string Invoice)
        {
            var list = _order.GetPurchaseOrder_From_PurchaseQuotation(ID, Invoice);
             return Json(list);

        } 
        [HttpGet]
        public IActionResult GetPurchaseRequest()
        {
            var list = _order.GetAllPurchaseRequest().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurchaseRequest(int ID,string Invoice)
        {
            var list = _order.GetPurchaseOrder_From_PurchaseRequest(ID, Invoice).ToList();
            return Ok(list);
        }
        //02.02.2020
        public IActionResult POCancel(int PurchaseID)
        {
            _order.POCancel(PurchaseID);
            return Ok();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PrinterNameController : Controller
    {
        private readonly DataContext _context;
        private readonly IPrinterName _printer;

        public PrinterNameController(DataContext context,IPrinterName printerName)
        {
            _context = context;
            _printer = printerName;
        }


        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Printer Name";
            ViewBag.Administrator = "show";
            ViewBag.General = "show";
            ViewBag.PrinterName = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A005");
            var data = _printer.PrintNames().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "Name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;

                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<PrinterName>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";

                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "Name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;

                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    // int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<PrinterName>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }


        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Printer Name";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
        
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A005");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Delete")] PrinterName printerName)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Printer Name";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
     

            if (ModelState.IsValid)
            {
                await _printer.AddorEdit(printerName);
                return RedirectToAction(nameof(Index));
            }
            return View(printerName);
        }


        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Printer Name";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
    
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var printerName = _printer.GetbyId(id);
                if (printerName.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (printerName == null)
                {
                    return NotFound();
                }
                return View(printerName);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A005");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var printerName = _printer.GetbyId(id);
                    if (printerName.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (printerName == null)
                    {
                        return NotFound();
                    }
                    return View(printerName);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }          
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Delete")] PrinterName printerName)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Printer Name";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
       

            if (id != printerName.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                   await _printer.AddorEdit(printerName);
                }
                catch (Exception)
                {
                    
                }
                return RedirectToAction(nameof(Index));
            }
            return View(printerName);
        }
        [HttpDelete]
        public async Task<IActionResult> DeletePrinter(int id)
        {
            await _printer.Delete(id);
            return Ok();
        }
        [HttpPost]
        public  IActionResult InsertPrinter(PrinterName printer)
        {
            var list = _printer.AddorEdit(printer);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPrinterName()
        {
            var list =  _printer.PrintNames().OrderByDescending(x=>x.ID).Where(s => s.Delete == false);
            return Ok(list);
        }
    }
}

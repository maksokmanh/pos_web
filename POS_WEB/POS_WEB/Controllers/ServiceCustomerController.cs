﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Customer;
using POS_WEB.Models.Services.Appointment;
using Microsoft.AspNetCore.Authorization;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class ServiceCustomerController : Controller
    {
        private readonly DataContext _context;

        public ServiceCustomerController(DataContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RegisterServiceCustomer(Customer customer, CustomerAppointment appointment)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Register Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
           
            ModelMessage message = new ModelMessage(ModelState);
            ValidateServiceCustomer(customer);
            ValidateServiceAppointment(appointment);
            if (ModelState.IsValid)
            {
                var _history = new AppointmentHistory
                {
                    AppointmentID = appointment.AppointmentID,
                    CustomerID = appointment.CustomerID,
                    AppointmentCode = appointment.AppointmentCode,
                    FollowUp = DateTime.Parse(string.Format("{0} {1}", appointment.ActivityDate, appointment.ActivityTime)),
                    Remark = appointment.Remark,
                    Reminder = appointment.Reminder,
                    Location = appointment.Location,
                    Priority = appointment.Priority,
                    ItemServices = appointment.ItemServices
                };

                using (var dbTransaction = _context.Database.BeginTransaction())
                {                   
                    customer.CustomerType = CustomerType.SERVICE;
                    _context.Customers.Update(customer);
                    _context.SaveChanges();
                    appointment.CustomerID = customer.CustomerID;
                    _context.CustomerAppointments.Update(appointment);
                    _context.SaveChanges();
                    _history.AppointmentID = appointment.AppointmentID;
                    _history.CustomerID = appointment.CustomerID;
                    _context.AppointmentHistories.Add(_history);
                    _context.SaveChanges();
                    dbTransaction.Commit();

                    ModelState.AddModelError("customer.Success", "A service customer has been registered.");
                    message.Bind(ModelState);
                    message.Action = ModelAction.CONFIRM;
                    return Ok(message);
                }
                
            }

            message.Bind(ModelState);
            return Ok(message);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveServiceAppointment(CustomerAppointment appointment)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            var _history = new AppointmentHistory
            {
                CustomerID = appointment.CustomerID,
                AppointmentCode = appointment.AppointmentCode,
                Remark = appointment.Remark,
                Reminder = appointment.Reminder,
                Location = appointment.Location,
                Priority = appointment.Priority,
                ItemServices = appointment.ItemServices
            };

            ValidateServiceAppointment(appointment, _history);
            if (ModelState.IsValid)
            {

                using (var dbTransaction = _context.Database.BeginTransaction())
                {
                    _context.CustomerAppointments.Update(appointment);
                    _context.SaveChanges();
                    _history.AppointmentID = appointment.AppointmentID;
                    _history.CustomerID = appointment.CustomerID;
                    _context.AppointmentHistories.Add(_history);
                    _context.SaveChanges();

                    dbTransaction.Commit();
                    msg.Add("appointment.Success", "Next follow-up appointment has been saved.");
                    msg.Action = ModelAction.CONFIRM;
                    return Ok(msg.Bind(ModelState));
                }
            }

            return Ok(msg.Bind(ModelState));
        }

        private void ValidateServiceAppointment(CustomerAppointment appointment, AppointmentHistory history = null)
        {

            if (DateTime.TryParse(string.Format("{0} {1}", appointment.ActivityDate, appointment.ActivityTime), out DateTime _followUp))
            {
                if (!(appointment.Paused || appointment.Closed))
                {
                    if (IsTimely(appointment.ActivityDate, appointment.ActivityTime, appointment.Reminder))
                    {
                        ModelState.AddModelError("appointment.FollowUp", "Next appointment datetime must be later from now.");
                    }

                    if (history != null)
                    {
                        history.FollowUp = _followUp;
                    }
                } 
                             
            }
            else
            {
                ModelState.AddModelError("appointment.FollowUp", "Invalid appointment date or time.");
            }

            if (appointment.ItemServices == null)
            {
                ModelState.AddModelError("appointment.ItemServices", "Please choose at least one item service.");
            }
        }

        private void ValidateServiceCustomer(Customer customer)
        {
            if (!string.IsNullOrEmpty(customer.Name))
            {
                if (_context.Customers.Any(c => c.Name.Replace(" ", "").ToLower() == customer.Name.Replace(" ", "")))
                {
                    ModelState.AddModelError("customer.Name", "The customer's name is already existed.");
                }
            }

            if (customer.Plate == null)
            {
                ModelState.AddModelError("customer.Plate", "Plate is required.");
            }
            if (customer.Frame == null)
            {
                ModelState.AddModelError("customer.Frame", "Frame is required.");
            }

            if (customer.Engine == null)
            {
                ModelState.AddModelError("customer.Engine", "Engine is required.");
            }

            if (customer.Phone == null)
            {
                ModelState.AddModelError("customer.Phone", "Phone is required.");
            }

            if (customer.BirthDate != null)
            {
                if (GetValidDate(customer.BirthDate, "-") == "")
                {
                    ModelState.AddModelError("customer.BirthDate", "Date of Birth is invalid.");
                }
                else
                {
                    customer.BirthDate = GetValidDate(customer.BirthDate, "-");
                }

            }

        }

        public IActionResult ServiceCustomer(int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Register Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            var colors = _context.ColorCustomers.Where(c => c.Active).ToList();
            var sc_vm = new CustomerViewModel
            {
                Regions = _context.Regions,
                Districts = new List<District>(),
                ItemServices = _context.ItemServices.Where(i => !i.Deleted),
                Appointment = new CustomerAppointment
                {
                    AppointmentCode = "AP-" + (_context.CustomerAppointments.Count() + 1).ToString().PadLeft(7, '0'),
                    ActivityDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    ActivityTime = DateTime.Now.ToString("hh:mm tt")
                },
                ColorCustomers = colors ?? new List<ColorCustomer>(),
            };

            if (customerID == null)
            {
                int count = _context.Customers.Count() + 1;

                sc_vm.Customer = new Customer {
                    Code = "SC-" + count.ToString().PadLeft(7, '0'),
                    BirthDate = DateTime.Now.ToString("MM-dd-yyyy")
                };

                return View(sc_vm);
            }
          
            var service_customer = _context.Customers.Find(customerID);
            sc_vm.Districts = _context.Districts.Where(d => d.RegionID == service_customer.Region);
            sc_vm.Customer = service_customer;
            return View(sc_vm);
        }     

        public IActionResult ServiceCustomerUpdate(int? customerID, bool deleted = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Edit Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            if (deleted)
            {
                try
                {
                    _context.Customers.Find(customerID).Deleted = true;
                    var app = _context.CustomerAppointments.FirstOrDefault(ap => ap.CustomerID == customerID);
                    if (app != null)
                    {
                        app.Closed = true;
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                catch (Exception ex) { }             
            }
            var colors = _context.ColorCustomers.Where(c => c.Active).ToList();
            Customer customer = new Customer();
            customer = _context.Customers.Find(customerID);
            CustomerViewModel sc_vm = new CustomerViewModel
            {
                Customer = customer,
                ColorCustomers = colors ?? new List<ColorCustomer>(),
                Regions = _context.Regions,          
                Districts = _context.Districts.Where(d => d.DistrictID == customer.District)
            };      
            return View(sc_vm);
        }

        public IActionResult ServiceCustomerDetail(int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Detail Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            Customer c = _context.Customers.Find(customerID);
            if(c != null)
            {
                return View(new CustomerDetail
                {
                    CustomerID = c.CustomerID,
                    Code = c.Code,
                    Name = c.Name,
                    CustomerType = c.CustomerType,
                    CustomerGroup = c.CustomerGroup == 0? "(No group yet.)" : _context.CustomerGroups.Find(c.CustomerGroup).GroupName,
                    Gender = c.Gender,
                    BirthDate = string.IsNullOrEmpty(c.BirthDate) ? "(No birth-date yet.)" : DateTime.Parse(c.BirthDate).ToString("dd/MM/yyyy"),
                    Phone = c.Phone,
                    Email = c.Email ?? "(No email yet.)",
                    Address = c.Address ?? "(No address yet.)",
                    Plate = c.Plate,
                    Frame = c.Frame,
                    Engine = c.Engine,
                    Region = c.Region == 0 ? "(No region yet.)" : _context.Regions.FirstOrDefault(r => r.RegionID == c.Region).Name,
                    District = c.District == 0 ? "(No district yet.)" : _context.Districts.FirstOrDefault(d => d.DistrictID == c.District).Name
                });
            }
                                      
            return View(new CustomerDetail());
        }

        private string GetValidDate(string dmy, string separator)
        {
            var values = dmy.Split(separator);
            var date = values[2] + separator + values[1] + separator + values[0];
            var _valid = "";
            if (DateTime.TryParse(date, out DateTime validDate))
            {
                _valid = validDate.ToString("yyyy-MM-dd");
            }
            return _valid;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateServiceCustomer(Customer customer)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Edit Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            ModelMessage msg = new ModelMessage(ModelState);
            ValidateServiceCustomer(customer);            
            if (ModelState.IsValid)
            {
                customer.CustomerType = CustomerType.SERVICE;
                
                _context.Customers.Update(customer);
                _context.SaveChanges();
                msg.Action = ModelAction.CONFIRM;
                msg.Add("customer.Success", "A service customer has been updated.");
            }

            return Ok(msg.Bind(ModelState));
        }

        public IActionResult ServiceCustomerList()
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "List of all customers";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            if (!_context.Customers.Any(c => !c.Deleted))
            {
                return View(new List<Customer>());
            }
            return View(_context.Customers.Where(sc => sc.CustomerType == CustomerType.SERVICE && !sc.Deleted).Take(10));
        }

        public IActionResult SearchServiceCustomers(string word = "")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "List of all customers";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";

            var service_customers = (from sc in _context.Customers
                                     where sc.CustomerType == CustomerType.SERVICE && !sc.Deleted
                                     select new
                                     {
                                         sc.CustomerID,
                                         sc.Code,
                                         sc.CustomerType,
                                         sc.Name,
                                         sc.Gender,
                                         Phone = sc.Phone?? "No phone yet.",                                                                       
                                         Plate = sc.Plate ?? "",
                                         Frame = sc.Frame?? "",
                                         Engine = sc.Engine?? "",                                                                             
                                     }).AsQueryable();

            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                service_customers = service_customers.Where(sc =>
                    sc.Name.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Code.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Phone.Replace(" ", string.Empty).ToLower().Contains(word)                              
                   | sc.Plate.Replace(" ", string.Empty).ToLower().Contains(word)       
                   | sc.Frame.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Engine.Replace(" ", string.Empty).ToLower().Contains(word)
                );

                return Ok(service_customers);
            }

            return Ok(service_customers.Take(10));
        }

        [HttpPost]
        public IActionResult ToggleAppointment(int appointmentID, bool paused)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            if (ModelState.IsValid)
            {
                _context.CustomerAppointments.Find(appointmentID).Paused = paused;
                _context.SaveChanges();
            }
            return Ok(_context.CustomerAppointments.Find(appointmentID));
        }

        public IActionResult ServiceAppointmentFollowUp(int appointmentID, int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer Appointment";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceAppointment = "highlight";
            AppointmentViewModel avm = new AppointmentViewModel
            {
                Appointment = new CustomerAppointment
                {
                    AppointmentCode = "AP-" + (_context.CustomerAppointments.Count() + 1).ToString().PadLeft(7, '0'),
                    ActivityDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    ActivityTime = DateTime.Now.ToString("hh:mm tt")
                }
            };
            List<ItemService> itemServices = new List<ItemService>();
            itemServices = _context.ItemServices.Where(i => !i.Deleted).ToList();
            var appointment = _context.CustomerAppointments.Find(appointmentID);          
            if (customerID != null)
            {
                appointment = _context.CustomerAppointments.FirstOrDefault(a => a.CustomerID == customerID);
            }
            avm.Appointment = appointment;
            avm.ItemServices = itemServices;
            if (!IsTimely(avm.Appointment.ActivityDate, avm.Appointment.ActivityTime, avm.Appointment.Reminder))
            {
                return NotFound();
            }
            return View(avm);
        }

        private bool CannotFollowUp(string nextDate, string nextTime)
        {
            bool cannot_followup = false;
            string _datetime = string.Format("{0} {1}", nextDate, nextTime);
            if(DateTime.TryParse(_datetime, out DateTime _followup))
            {
                cannot_followup = DateTime.Now.CompareTo(_followup) >= 0;
            }
            return cannot_followup;
        }

        public IActionResult ServiceTimelyAppointmentList(bool asJSON = false, string word = "")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "List of service appointments";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceAppointment = "highlight";

            var appointments = (from ap in _context.CustomerAppointments
                                join cu in _context.Customers on
                                ap.CustomerID equals cu.CustomerID
                                where IsTimely(ap.ActivityDate, ap.ActivityTime, ap.Reminder) 
                                && !string.IsNullOrWhiteSpace(ap.ItemServices) && !cu.Deleted && !ap.Paused && !ap.Closed
                                select new AppointmentSummary
                                {
                                    CustomerID = cu.CustomerID,
                                    AppointmentID = ap.AppointmentID,
                                    AppointmentCode = ap.AppointmentCode,
                                    CustomerName = cu.Name,
                                    Gender = cu.Gender == CustomerGender.Female ? "Female" : "Male",
                                    Phone = cu.Phone ?? "No phone yet.",
                                    Remark = ap.Remark ?? "No remark yet.",
                                    ActivityDate = DateTime.Parse(ap.ActivityDate).ToString("dd-MM-yyyy"),
                                    ActivityTime = ap.ActivityTime ?? "",
                                    Reminder = ap.Reminder ?? "0",
                                    Priority = ap.Priority,
                                    Paused = ap.Paused
                                }).AsQueryable();
            if(appointments.Count() > 0)
            {
                ViewBag.Timely = "fa-pulse fn-red";
            }
            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                appointments = appointments.Where(ap =>
                    ap.CustomerName.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.Phone.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.Remark.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.ActivityDate.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.ActivityTime.Replace(" ", string.Empty).ToLower().Contains(word)

                );
                return Ok(appointments);
            }

            if (asJSON)
            {
                return Ok(appointments);
            }

            return View(appointments);
        }

        //Appointment
        private bool IsTimely(string date, string time, string reminder)
        {
            string datetime = string.Format("{0} {1}", date, time);
            string[] _reminder = reminder.Split("/");
            if (DateTime.TryParse(datetime, out DateTime _dateTime) && _reminder[0].Length <= 7)
            {
                TimeSpan timespan_reminder = new TimeSpan();
                switch (_reminder[1].ToLower())
                {
                    case "minutes":
                        timespan_reminder = TimeSpan.FromMinutes(double.Parse(_reminder[0]));
                        break;
                    case "hours":
                        timespan_reminder = TimeSpan.FromHours(double.Parse(_reminder[0]));
                        break;
                    case "days":
                        timespan_reminder = TimeSpan.FromDays(double.Parse(_reminder[0]));
                        break;
                }
                double reminder_in_minutes = timespan_reminder.TotalMinutes;
                return DateTime.Compare(DateTime.Now, _dateTime.AddMinutes(-reminder_in_minutes)) >= 0;
            }
            return false;
        }

        public IActionResult ServiceGeneralAppointmentList(string word = "", string priority = "ALL")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer Appointment";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceAppointment = "highlight";

            var appointments = (from ap in _context.CustomerAppointments
                                join cu in _context.Customers on
                                ap.CustomerID equals cu.CustomerID
                                where !string.IsNullOrWhiteSpace(ap.ItemServices) && !cu.Deleted && !ap.Closed
                                select new AppointmentSummary
                                {
                                    CustomerID =  cu.CustomerID,
                                    AppointmentID = ap.AppointmentID,
                                    AppointmentCode = ap.AppointmentCode,
                                    CustomerName = cu.Name,
                                    Gender = cu.Gender == CustomerGender.Female? "Female" : "Male",
                                    Phone = cu.Phone ?? "No phone yet.",
                                    Remark = ap.Remark ?? "No remark yet.",
                                    ActivityDate = DateTime.Parse(ap.ActivityDate).ToString("dd-MM-yyyy"),
                                    ActivityTime = ap.ActivityTime ?? "",
                                    Priority = ap.Priority,
                                    Reminder = ap.Reminder ?? "0",
                                    Paused = ap.Paused
                                }).AsQueryable();
           
            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                appointments = appointments.Where(ap =>
                    ap.CustomerName.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Phone.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Remark.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.ActivityDate.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.ActivityTime.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Reminder.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Priority.Replace(" ", string.Empty).ToLower().Contains(word)
                );
                return Ok(appointments);
            }

           if(priority != "ALL")
            {
                appointments = appointments.Where(a => a.Priority == priority);
                return Ok(appointments);
            }
            return Ok(appointments);
        }

        //Region 
        public IActionResult GetRegions()
        {
            return Ok(_context.Regions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateRegions(IEnumerable<Region> data)
        {
            var items = data.GroupBy(s => s.Name.ToLower())
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            List<string> errors = new List<string>();
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    errors.Add("The " + error + " is duplicated!");
                }

            }
            else
            {
                if (ModelState.IsValid)
                {
                    _context.Regions.UpdateRange(data);
                    _context.SaveChanges();
                }
            }

            return Ok(new
            {
                Data = _context.Regions,
                Errors = errors
            });
        }

        //District
        public IActionResult GetDistrictsByRegionID(int related_id)
        {
            return Ok(_context.Districts.Where(d => d.RegionID == related_id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateDistricts(IEnumerable<District> data)
        {

            var items = data.GroupBy(s => s.Name.ToLower())
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            List<string> errors = new List<string>();
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    errors.Add("The " + error + " is duplicated!");
                }

            }
            else
            {
                if (ModelState.IsValid)
                {
                    _context.Districts.UpdateRange(data);
                    _context.SaveChanges();
                }
            }

            return Ok(new
            {
                Data = _context.Districts,
                Errors = errors
            });
        }

        //Item Service
        public IActionResult ItemService(bool onlyItemService = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Item Service";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ItemService = "highlight";
            int count = _context.ItemServices.Count() + 1;
            ItemService itemService = new ItemService
            {
                ServiceCode = "S-" + count.ToString().PadLeft(7, '0'),
            };
            IEnumerable<ItemService> itemServices = new List<ItemService>();

            if (onlyItemService)
            {
                return Ok(itemService);
            }
            return View(new ItemServiceViewModel {
                ItemService = itemService,
                ItemServices = _context.ItemServices.Where(i => !i.Deleted).Take(10)
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ItemService(ItemService itemService, bool isJSON = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Item Service";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ItemService = "highlight";
            ModelMessage message = new ModelMessage(ModelState);
            if (!string.IsNullOrWhiteSpace(itemService.ServiceName) 
                && _context.ItemServices.Any(s => s.ServiceName.Replace(" ", string.Empty).ToLower()
                == itemService.ServiceName.Replace(" ", string.Empty).ToLower()))
            {
                ModelState.AddModelError("itemService.ServiceName", "The service name is already existed.");
                message.Bind(ModelState);
            }

            if (!ModelState.IsValid && isJSON)
            {
                return Ok(new { ItemService = itemService, ModelMessage = message });
            }

            if (ModelState.IsValid)
            {
                itemService.ServiceCode = itemService.ServiceCode.Trim();
                itemService.ServiceName = itemService.ServiceName.Trim();
                itemService.LastChanged = DateTime.Now;
                _context.ItemServices.Update(itemService);
                _context.SaveChanges();
                if (isJSON)
                {
                    return Ok(new { ItemService = itemService, ModelMessage = message});
                }
                return RedirectToAction(nameof(ItemService));
            }
            
            int count = _context.ItemServices.Count() + 1;
            IEnumerable<ItemService> itemServices = new List<ItemService>();
            return View(new ItemServiceViewModel
            {
                ItemService = itemService,
                ItemServices = _context.ItemServices.Where(i => !i.Deleted).Take(10)
            });
        }

        public IActionResult SearchItemServices(string word = "")
        {
            var itemServices = from s in _context.ItemServices select s;
                                
            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                itemServices = itemServices.Where(ap =>
                    ap.ServiceCode.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.ServiceName.Replace(" ", string.Empty).ToLower().Contains(word)
                );
                return Ok(itemServices);
            }
            return Ok(itemServices.Take(10));
        }

        public IActionResult ServiceAppointmentDetail(int customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Service Appointment Detail";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceAppointment = "highlight";
            var appointments = from ah in _context.AppointmentHistories where ah.CustomerID == customerID select ah;
            var customer = _context.Customers.FirstOrDefault(c => c.CustomerID == customerID);

            var appoint_details = new AppointmentViewModel
            {
                Customer = customer,
                AppointmentHistories = appointments.OrderByDescending( h => h.FollowUp ),
                ItemServices = _context.ItemServices
            };
            return View(appoint_details);
        }

        public IActionResult DetectAllTimelyAppointments()
        {
            var appointments = (from ap in _context.CustomerAppointments
                                join cu in _context.Customers on
                                ap.CustomerID equals cu.CustomerID
                                where (IsTimely(ap.ActivityDate, ap.ActivityTime, ap.Reminder))
                                
                                && !cu.Deleted && !ap.Paused && !ap.Closed
                                select new AppointmentSummary
                                {
                                    CustomerID = cu.CustomerID,
                                    AppointmentID = ap.AppointmentID,
                                    AppointmentCode = ap.AppointmentCode,                                
                                    CustomerName = cu.Name,
                                    Gender = cu.Gender == CustomerGender.Female ? "Female" : "Male",
                                    Phone = cu.Phone ?? "No phone yet.",
                                    Remark = ap.Remark ?? "No remark yet.",
                                    ActivityDate = DateTime.Parse(ap.ActivityDate).ToString("dd-MM-yyyy"),
                                    ActivityTime = ap.ActivityTime ?? "",
                                    Reminder = ap.Reminder ?? "0",
                                    Priority = ap.Priority,
                                    Paused = ap.Paused,
                                    CustomerType = cu.CustomerType,
                                    ItemServices = ap.ItemServices
                                }).AsQueryable();
            
            return Ok(appointments);
        }


    }
}
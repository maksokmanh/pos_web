﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.CRM;
using POS_WEB.Models.Services.HumanResources;
using Newtonsoft.Json;
using System;
using MoreLinq;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using POS_WEB.Models.SignalR;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class CRMController : Controller
    {
        // GET: /<controller>/
        private readonly DataContext _context;
        private readonly IHubContext<SignalRClient> _hubcontext;
        public CRMController(DataContext context, IHubContext<SignalRClient> hubcontext)
        {
            _context = context;
            _hubcontext = hubcontext;
        }

        public IActionResult Appointment()
        {
            ViewBag.Page = "CRM";
            ViewBag.Subpage = "Appointment";
            ViewBag.Appointment = "show";
            ViewBag.Highlight = "highlight";
            AppointmentModel apppoint = new AppointmentModel
            {
                ActivityTypes = _context.ActivityTypes.Where(a => a.Active == true),
                ActivitySubjects = _context.ActivitySubjects,
                Employees = _context.Employees,
                Locations = _context.ActivityLocations,
                Appointments = _context.Appointments
            };
            return View(apppoint);
        }

        public async Task<IActionResult> GetValidAppointments()
        {
            var appointments = _context.Appointments.Where(a => !a.Inactive || !a.Closed);
            await _hubcontext.Clients.All.SendAsync("ReceiveMessage", appointments);
            return Ok(appointments);
        }

        public IActionResult SearchAppointmentsByName(string name = "", int appoint_id = 0)
        {
            
            var appointments = from ap in _context.Appointments
                               join bp in _context.BusinessPartners on ap.BPID equals bp.ID
                               join at in _context.ActivityTypes on ap.TypeID equals at.TypeID
                               join sj in _context.ActivitySubjects on ap.SubjectID equals sj.SubjectID
                               join ur in _context.UserAccounts on ap.UserID equals ur.ID
                               join em in _context.Employees on ap.EmpID equals em.ID
                               select new
                               {
                                   ap.AppointID,
                                   ap.Activity,
                                   ActivityType = at.Name,
                                   ActivitySubject = sj.Name,
                                   ap.Remark,
                                   Partner = bp.Name,
                                   AssignedBy = ur.Username,
                                   AssignedTo = em.Name,
                                   ap.StartTime, 
                                   ap.EndTime,
                                   ap.Duration,
                                   ap.Inactive,
                                   ap.Closed
                               };
            
            if (!string.IsNullOrEmpty(name))
            {
                Regex regx = new Regex(string.Format(@"\b{0}\w*\b", Regex.Replace(name, @"\s+", string.Empty)), RegexOptions.IgnoreCase | RegexOptions.Singleline);
                appointments = appointments.Where(a => regx.IsMatch(Regex.Replace(a.Activity, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.ActivityType, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.ActivitySubject, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.AssignedBy, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.AssignedTo, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.Partner, @"\s+", string.Empty))
                               || regx.IsMatch(Regex.Replace(a.Duration, @"\s+", string.Empty))
                );

            }
            
            return Ok(appointments);
        }

        public IActionResult AppointmentEdit(int appoint_id)
        {
            return Ok(_context.Appointments.Find(appoint_id));
        }

        public IActionResult GetActivityTypes()
        {
            return Ok(_context.ActivityTypes.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateActivityTypes(IEnumerable<ActivityType> data)
        {
            var items = data.GroupBy(s => s.Name.ToLower())
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            List<string> errors = new List<string>();
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    errors.Add("The " + error + " is duplicated!");
                }

            }
            else
            {
                if (ModelState.IsValid)
                {
                    _context.ActivityTypes.UpdateRange(data);
                    _context.SaveChanges();
                }
            }

            return Ok(new
            {
                Data = _context.ActivityTypes.Where(a => a.Active == true),
                Errors = errors
            });
        }

        public IActionResult GetSubjectsByType(int type_id, bool only_active)
        {
            var subjects = _context.ActivitySubjects.Where(asj => asj.TypeID == type_id);
            if (only_active)
            {
                subjects = _context.ActivitySubjects.Where(asj => asj.TypeID == type_id && asj.Active == true);
            }

            return Ok(subjects);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateActivitySubjects(int type_id, ActivitySubject[] data)
        {
            if (ModelState.IsValid)
            {
                _context.ActivitySubjects.UpdateRange(data);
                _context.SaveChanges();
            }

            return Ok(_context.ActivitySubjects.Where(s => s.TypeID == type_id && s.Active == true));
        }

        public IActionResult GetBusinessPartners()
        {
            return Ok(_context.BusinessPartners);
        }

        public IActionResult GetBusinessPartnerByID(int bp_id)
        {
            return Ok(_context.BusinessPartners.Find(bp_id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateLocations(IEnumerable<ActivityLocation> data)
        {
            if (ModelState.IsValid)
            {
                _context.ActivityLocations.UpdateRange(data);
                _context.SaveChanges();
            }

            return Ok(_context.ActivityLocations);
        }

        public IActionResult GetLocations()
        {
            return Ok(_context.ActivityLocations);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateRangeAppointments(IEnumerable<Appointment> appointments)
        {
            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateAppointment(Appointment appointment)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            Validate(appointment, msg);
            msg.Bind(ModelState);
            if (msg.Data.Count == 0)
            {
                _context.Appointments.Update(appointment);
                _context.SaveChanges();
                msg.Action = ModelAction.CONFIRM;
                msg.Redirect = ".";

            }

            return Ok(msg);
        }



        private void Validate(Appointment data, ModelMessage msg)
        {
            if (data.Activity == "NOTE")
            {
                if (string.IsNullOrWhiteSpace(data.StartTime))
                {
                    msg.Add("NoteTime", "Note Time is required.");
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(data.StartTime))
                {
                    msg.Add("StartTime", "Start Time is required.");
                }

                if (string.IsNullOrWhiteSpace(data.EndTime))
                {
                    msg.Add("EndTime", "End Time is required.");
                }
            }

            if (data.SubjectID == 0)
            {
                msg.Add("SubjectID", "Subject need to be selected.");
            }

            if (data.Activity == "MEETING")
            {
                if (data.Room == null)
                {
                    msg.Add("Room", "Room is required.");
                }

                if (data.Street == null)
                {
                    msg.Add("Street", "Street is required.");
                }
            }

        }

    }
}

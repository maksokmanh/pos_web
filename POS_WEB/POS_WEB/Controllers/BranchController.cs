﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class BranchController : Controller
    {
        private readonly DataContext _context;
        private readonly IBranch _branch;
        private readonly IHostingEnvironment _appEnvironment;

        public BranchController(DataContext context, IBranch branch, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _branch = branch;
            _appEnvironment = hostingEnvironment;
        }

        // GET: Branch
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Branch";
            ViewBag.Administrator = "show";
            ViewBag.General = "show";
            ViewBag.Branch = "highlight";
           
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A002");
            var data = _branch.GetBranch().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["CompanyIDSortParm"] = sortOrder == "CompanyID" ? "CompanyID_desc" : "CompanyID";
                ViewData["LocationSortParm"] = sortOrder == "Location" ? "Location_desc" : "Location";
                ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";


                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) ||
                    s.Address.Contains(searchString) || s.Location.Contains(searchString) ||
                     s.Company.Name.Contains(searchString));


                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "CompanyID":
                        Cate = Cate.OrderBy(s => s.CompanyID);
                        break;
                    case "CompanyID_desc":
                        Cate = Cate.OrderByDescending(s => s.CompanyID);
                        break;
                    case "Location":
                        Cate = Cate.OrderBy(s => s.Location);
                        break;
                    case "Location_desc":
                        Cate = Cate.OrderByDescending(s => s.Location);
                        break;
                    case "Address":
                        Cate = Cate.OrderBy(s => s.Address);
                        break;
                    case "Address_desc":
                        Cate = Cate.OrderByDescending(s => s.Address);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<Branch>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if (permision!=null)
            {
                if (permision.Used == true)
                {
                    //var data = _branch.GetBranch().OrderBy(g => g.ID);
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["CompanyIDSortParm"] = sortOrder == "CompanyID" ? "CompanyID_desc" : "CompanyID";
                    ViewData["LocationSortParm"] = sortOrder == "Location" ? "Location_desc" : "Location";
                    ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";


                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) ||
                        s.Address.Contains(searchString) || s.Location.Contains(searchString) ||
                         s.Company.Name.Contains(searchString));


                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "CompanyID":
                            Cate = Cate.OrderBy(s => s.CompanyID);
                            break;
                        case "CompanyID_desc":
                            Cate = Cate.OrderByDescending(s => s.CompanyID);
                            break;
                        case "Location":
                            Cate = Cate.OrderBy(s => s.Location);
                            break;
                        case "Location_desc":
                            Cate = Cate.OrderByDescending(s => s.Location);
                            break;
                        case "Address":
                            Cate = Cate.OrderBy(s => s.Address);
                            break;
                        case "Address_desc":
                            Cate = Cate.OrderByDescending(s => s.Address);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<Branch>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
                   
        }

        // GET: Branch/Create
        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Branch";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
        
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A002");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,CompanyID,Location,Address,Delete")] Branch branch)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Branch";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
     

            if (branch.CompanyID == 0)
            {
                ViewData["error.company"] = "Please select company!";
            }
            if (branch.Name == null)
            {
                ViewData["error.Nane"] = "Please input name";
            }
            if(branch.Location == null)
            {
                ViewData["error.Location"] = "Please input location";
            }
            else
            {
                await _branch.AddOrEdit(branch);
              
                return RedirectToAction(nameof(Index));
            }
               
            
            ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
            return View(branch);
        }
        // GET: Branch/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Branch";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
           
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (id == null)
                {
                    return NotFound();
                }

                var branch = await _context.Branches.FindAsync(id);
                if (branch == null)
                {
                    return NotFound();
                }
                ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                return View(branch);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A002");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var branch = await _context.Branches.FindAsync(id);
                    if (branch == null)
                    {
                        return NotFound();
                    }
                    ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
                    return View(branch);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,CompanyID,Location,Address,Delete")] Branch branch)
        {
           

            if (id != branch.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                   await _branch.AddOrEdit(branch);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BranchExists(branch.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyID"] = new SelectList(_context.Company.Where(c => c.Delete == false), "ID", "Name");
            return View(branch);
        }



        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            await _branch.Delete(id);
            return Ok();
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _context.Company.FindAsync(id);
            _context.Company.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BranchExists(int id)
        {
            return _context.Branches.Any(e => e.ID == id);
        }
    }
}

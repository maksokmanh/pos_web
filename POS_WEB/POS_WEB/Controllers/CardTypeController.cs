﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class CardTypeController : Controller
    {
        private readonly DataContext _context;
        private readonly ICardType _cardType;

        public CardTypeController(DataContext context,ICardType cardType)
        {
            _context = context;
            _cardType = cardType;
        }
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Type";
            ViewBag.PromotionMenu = "show";
            ViewBag.Member = "show";
            ViewBag.CreateType = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);

            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A034");
            var data = _cardType.GetCardTypes.OrderByDescending(x => x.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Discount.ToString().Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }

                int.TryParse(minpage, out MaxSize);
                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<CardType>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewData["CurrentFilter"] = searchString;
                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Discount.ToString().Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    
                    int.TryParse(minpage, out MaxSize);
                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<CardType>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        public IActionResult Create()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Type";
            ViewBag.Menu = "show";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);

            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A034");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Discount,TypeDis,Delete")] CardType cardType)
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Type";
            ViewBag.Menu = "show";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
               
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A034");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    if (cardType.TypeDis == null)
                    {
                        ViewBag.type_requried = "Please select type discount !";
                    }
                    if (ModelState.IsValid)
                    {
                        await _cardType.AddorEdit(cardType);
                        return RedirectToAction(nameof(Index));
                    }
                    return View(cardType);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Type";
            ViewBag.Menu = "show";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";

            var cardType = _cardType.GetId(id);
            if (cardType.Delete == true)
            {
                return RedirectToAction(nameof(Index));
            }
            if (cardType == null)
            {
                return NotFound();
            }
            return View(cardType);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Discount,TypeDis,Delete")] CardType cardType)
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Type";
            ViewBag.Menu = "show";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            if (cardType.TypeDis == null)
            {
                ViewBag.type_requried = "Please select type discount !";
            }
            if (id != cardType.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _cardType.AddorEdit(cardType);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CardTypeExists(cardType.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cardType);
        }
      
        private bool CardTypeExists(int id)
        {
            return _context.CardTypes.Any(e => e.ID == id);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteCardType(int ID)
        {
            await _cardType.DeleteCardType(ID);
            return Ok();
        }
    }
}

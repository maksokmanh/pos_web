﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using POS_WEB.AppContext;
using POS_WEB.Controllers.Event;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Tables;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;
using POS_WEB.Models.SignalR;

namespace POS_WEB.Controllers
{
    public class POSController : Controller
    {
        private readonly DataContext _context;
        private readonly IPOS _pos;
        
        public POSController(IPOS pos,DataContext context, IHubContext<SignalRClient> hubcontext)
        {
            _pos = pos;
            _context = context;
        }
        [HttpGet]
        public IActionResult datatable()
        {
            return View();
        }
        public IActionResult Table()
        {
           
            List<GroupTable> groupTables = _context.GroupTables.Where(w=>w.Delete==false).ToList();
            List<Table> tables = _context.Tables.Where(w=>w.Delete==false).ToList();

            ServiceTable service_Table = new ServiceTable
            {
                GroupTables = groupTables,
                Tables = tables
            };
            return Ok(service_Table);
        }
        public IActionResult IntailStatusTable()
        {
            _pos.IntailStatusTable();
            return Ok();
        }
        public IActionResult UpdateTimeOnTable()
        {
            _pos.UpdateTimeOnTable();
            return Ok();
        }
        [HttpGet]
        public IActionResult GetTableAvailable(int group_id,int tableid)
        {
            if(group_id==0)
            {
                var Tables = _context.Tables.Where(w => w.ID != tableid && w.Status == 'A' && w.Delete==false).OrderBy(by=>by.ID);
                return Ok(Tables);
            }
            else
            {
                var Tables = _context.Tables.Where(w => w.ID != tableid && w.Status == 'A' && w.GroupTableID==group_id && w.Delete==false).OrderBy(by => by.ID);
                return Ok(Tables);
            }
        }
        [HttpGet]
        public IActionResult GetReceiptCombine(int orderid)
        {
            var orders = _context.Order.Where(w => w.OrderID != orderid).OrderBy(by=>by.TableID).ToList();
            return Ok(orders);
        }
        public IActionResult MoveTable(int old_id,int new_id)
        {
            _pos.MoveTable(old_id, new_id);
            return Ok();
        }
        public IActionResult CombineReceipt(CombineReceipt combineReceipt)
        {
            _pos.CombineReceipt(combineReceipt);
            return Ok();
        }
        public IActionResult SplitItem(Order order)
        {
           // _pos.SplitItem(order);
            return Ok();
        }
       public IActionResult SecondScreen()
        {
            return View();
        }
       [HttpGet]
        public IActionResult GetTableByGroup(int group)
        {
            if (group == 0)
            {
                var table = _context.Tables.Where(w => w.Delete == false).OrderBy(by=>by.ID);
                return Ok(table);
            }
            else
            {
                var table = _context.Tables.Where(w => w.GroupTableID == group && w.Delete == false).OrderBy(by => by.ID);
                return Ok(table);
            }

        }
        [HttpGet]
        public IActionResult GetGroupUomDefined()
        {
            var GroupUomDefined = _context.GroupDUoMs.Where(w => w.Delete == false).ToList();
            return Ok(GroupUomDefined);
        }
        public IActionResult KRMS()
        {
            
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                if (check_setting.Count() > 0)
                {
                    var group = _pos.GetGroup1s;
                    return View(group);
                }
                else
                {
                    return RedirectToAction("Setting", "POS");
                }

            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A044");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                    var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                    if (check_setting.Count() > 0)
                    {
                        var group = _pos.GetGroup1s;
                        return View(group);
                    }
                    else
                    {
                        return RedirectToAction("Setting", "POS");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        public IActionResult KBMS()
        {
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                if (check_setting.Count() > 0)
                {
                    var group = _pos.GetGroup1s;
                    return View(group);
                }
                else
                {
                    return RedirectToAction("Setting", "POS");
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A044");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                    var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                    if (check_setting.Count() > 0)
                    {
                        var group = _pos.GetGroup1s;
                        return View(group);
                    }
                    else
                    {
                        return RedirectToAction("Setting", "POS");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        public IActionResult KTMS()
        {
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                if (check_setting.Count() > 0)
                {
                    var group = _pos.GetGroup1s;
                    return View(group);
                }
                else
                {
                    return RedirectToAction("Setting", "POS");
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A044");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    int branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                    var check_setting = _context.GeneralSetting.Where(w => w.BranchID == branchid);

                    if (check_setting.Count() > 0)
                    {
                        var group = _pos.GetGroup1s;
                        return View(group);
                    }
                    else
                    {
                        return RedirectToAction("Setting", "POS");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        public IActionResult Setting()
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "POS";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Setting";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            return View();
        }
        public IActionResult KRMS1()
        {
            var group = _pos.GetGroup1s;
            return View(group);
        }
        //public IActionResult KBMS()
        //{
        //    var group = _pos.GetGroup1s;

        //    return View(group);
        //}
        public IActionResult GetSetting(int branchid)
        {
            return Ok(_pos.GetSetting(branchid));
        }
        [HttpPost]
        public IActionResult GetOrder(int tableid, int orderid,int userid)
        {
            var order = _pos.GetOrder(tableid,orderid,userid);
            return Ok(order);
        }
        public IActionResult ClearUserOrder(int tableid)
        {
            _pos.ClearUserOrder(tableid);
            return Ok();
        }
        public IActionResult GetItemMasterData(int PriceListID)
        {
            var item = _pos.GetItemMasterDatas(PriceListID).ToList();
            return Ok(item);
        }
        public IActionResult GetItemMasterByBarcode(int pricelist, string barcode)
        {
            if (string.IsNullOrWhiteSpace(barcode)) { return Ok(); }
            try
            {
                var item = _pos.GetItemMasterByBarcode(pricelist, barcode).ToList();
                return Ok(item);
            }
            catch (Exception)
            {
                return Ok();
            }
        }
        public IActionResult GetItemMasterDataByGroup(int PriceListID)
        {
            var item = _pos.FilterItemByGroup(PriceListID).ToList();
            return Ok(item);
        }
        public IActionResult GetReceiptReprint(int branchid,string date_from,string date_to)
        {
            var receipts = _pos.GetReceiptReprint(branchid,date_from,date_to);
            return Ok(receipts);
        }
        public IActionResult GetGroupItem(int group1_id,int group2_id, int? level,int itemid,int pricelistid)
        {
            switch (level)
            {
                case 0:
                    return Ok(_pos.GetGroup1s);
                case 1:
                    return Ok(_pos.FilterGroup2(group1_id));
                case 2:
                    return Ok(_pos.FilterGroup3(group1_id,group2_id));
                case 3:
                    return Ok(_pos.FilterItem(pricelistid, itemid));
            }
            return Ok();
        }
       
        public IActionResult GetLocalCurrecny(int currencyid)
        {
            var localcurrency = _context.ExchangeRates.Include(c => c.Currency).FirstOrDefault(w => w.CurrencyID == currencyid);
            return Ok(localcurrency);
        }
        public IActionResult GetExchangeRate()
        {
            var exchage_rate = _context.ExchangeRates.Include(cur => cur.Currency).ToList();
            return Ok(exchage_rate);
        }
        public IActionResult GetVat()
        {
            var Date  = Convert.ToDateTime(DateTime.Today);
            var vat = _context.Tax.Where(w => w.Delete == false && w.Effective>=Date);
            return Ok(vat);
        }
        public IActionResult GetMemberCard()
        {
            var Date = Convert.ToDateTime(DateTime.Today);
            return Ok(_context.MemberCards.Include(c=>c.CardType).Where(w => w.Delete == false && w.ExpireDate >= Date).ToList());
        }
        public IActionResult GetUserInfo(int userid)
        {
            var user = _context.UserAccounts.Include(emp => emp.Employee).Where(w => w.ID == userid).ToList();
            return Ok(user);
        }
        [HttpGet]
        public IActionResult CheckOpenShift(int userid)
        {
            var open = _context.OpenShift.Where(w => w.UserID == userid && w.Open == true).ToList();
            return Ok(open);
        }
        [HttpGet]
        public IActionResult GetTimeByTable(int TableID)
        {
            var time=_pos.GetTimeByTable(TableID);
            return Ok(time);
        }
        //[HttpPost]
        public IActionResult Send(Order data,string print_type)
        {
           return Ok(_pos.SendOrder(data, print_type));
            
            //return Ok(_pos.GetOrder(data.TableID,0).ToList());
        }
        public IActionResult CreateCustomer(BusinessPartner business)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.BusinessPartners.Add(business);
                    _context.SaveChanges();
                    return Ok(business);
                }
            }
            catch (Exception)
            {
                return Ok(business);
            }
            return Ok(business);

        }
        public IActionResult SendSplit(Order data,Order addnew)
        {
            _pos.SendSplit(data, addnew);
            return Ok(data);
        }
        public IActionResult SendDataToSecondScreen(Order data)
        {
            _pos.SendDataToSecondScreen(data);
            return Ok();
        }
        public IActionResult PrintReceiptBill(int orderid,string print_type)
        {
            if(orderid>0)
            {
                _pos.PrintReceiptBill(orderid, print_type);
            }
            return Ok();
        }
        public IActionResult PrintReceiptReprint(int orderid, string print_type)
        {
            if (orderid > 0)
            {
                _pos.PrintReceiptReprint(orderid, print_type);
            }
            return Ok();
        }
        public IActionResult OpenShift(int userid,double cash)
        {
            return Ok(_pos.OpenShiftData(userid, cash));
        }
        public IActionResult CloseShift(int userid,double cashout)
        {
            return Ok(_pos.CloseShiftData(userid, cashout));
        }
        //Setting
        public IActionResult GetCustomer()
        {
            var customer = _context.BusinessPartners.Where(w => w.Type == "Customer" && w.Delete==false).ToList();
            return Ok(customer);
        }
        public IActionResult GetPriceList()
        {
            var pricelist = _context.PriceLists.Where(w => w.Delete == false).ToList();
            return Ok(pricelist);
        }
        public IActionResult GetPaymentMeans()
        {
            var paymentmeans = _context.PaymentMeans.Where(w => w.Delete == false).ToList();
            return Ok(paymentmeans);
        }
        public IActionResult GetWarehouse(int branchid)
        {
            var warehouse = _context.Warehouses.Where(w =>w.BranchID==branchid && w.Delete == false).ToList();
            return Ok(warehouse);
        }
        public IActionResult GetPrinterName()
        {
            var pritner = _context.PrinterNames.Where(w => w.Delete == false);
            return Ok(pritner);
        }
        public IActionResult UpdateSetting(GeneralSetting setting)
        {
            
            if(setting.ID==0)
            {
                var branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                var setting_update = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == branchid);
                if(setting_update!=null)
                {
                    var currency_local = _context.PriceLists.FirstOrDefault(w => w.ID == setting.PriceListID);
                    var conpany = _context.Company.FirstOrDefault();
                    var currency_sys = _context.PriceLists.FirstOrDefault(w => w.ID == conpany.PriceListID);
                    var exchage = _context.ExchangeRates.FirstOrDefault(w => w.CurrencyID == currency_local.CurrencyID);
                    setting_update.SysCurrencyID = currency_sys.CurrencyID;
                    setting_update.LocalCurrencyID = currency_local.CurrencyID;
                    setting_update.BranchID = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                    setting_update.CompanyID = Convert.ToInt32(@User.FindFirst("CompanyID").Value);
                    setting_update.RateIn = exchage.Rate;
                    setting_update.RateOut = exchage.RateOut;

                    setting_update.CustomerID = setting.CustomerID;
                    setting_update.DaulScreen = setting.DaulScreen;
                    setting_update.PaymentMeansID = setting.PaymentMeansID;
                    setting_update.PriceListID = setting.PriceListID;
                    setting_update.PrintCountReceipt = setting.PrintCountReceipt;
                    setting_update.PrintReceiptOrder = setting.PrintReceiptOrder;
                    setting_update.PrintReceiptTender = setting.PrintReceiptTender;
                    setting_update.QueueCount = setting.QueueCount;
  
                    setting_update.Receiptsize = setting.Receiptsize;
                    setting_update.ReceiptTemplate = setting.ReceiptTemplate;
                    setting_update.VatAble = setting.VatAble;
                    setting_update.VatNum = setting.VatNum;
                    setting_update.WarehouseID = setting.WarehouseID;
                    setting_update.Wifi = setting.Wifi;
                    _context.GeneralSetting.Update(setting_update);
                    _context.SaveChanges();
                }
                else
                {
                    var currency_local = _context.PriceLists.FirstOrDefault(w => w.ID == setting.PriceListID);
                    var conpany = _context.Company.FirstOrDefault();
                    var currency_sys = _context.PriceLists.FirstOrDefault(w => w.ID == conpany.PriceListID);
                    var exchage = _context.ExchangeRates.FirstOrDefault(w => w.CurrencyID == currency_local.CurrencyID);
                    setting.SysCurrencyID = currency_sys.CurrencyID;
                    setting.LocalCurrencyID = currency_local.CurrencyID;
                    setting.BranchID = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                    setting.CompanyID = Convert.ToInt32(@User.FindFirst("CompanyID").Value);
                    setting.RateIn = exchage.Rate;
                    setting.RateOut = exchage.RateOut;
                    _context.GeneralSetting.Add(setting);
                    _context.SaveChanges();
                
                }
               
            }
            else
            {
                var branchid = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                var setting_update = _context.GeneralSetting.FirstOrDefault(w => w.BranchID == branchid);
                var currency_local = _context.PriceLists.FirstOrDefault(w => w.ID == setting.PriceListID);
                var conpany = _context.Company.FirstOrDefault();
                var currency_sys = _context.PriceLists.FirstOrDefault(w => w.ID == conpany.PriceListID);
                var exchage = _context.ExchangeRates.FirstOrDefault(w => w.CurrencyID == currency_local.CurrencyID);
                setting_update.SysCurrencyID = currency_sys.CurrencyID;
                setting_update.LocalCurrencyID = currency_local.CurrencyID;
                setting_update.BranchID = Convert.ToInt32(@User.FindFirst("BranchID").Value);
                setting_update.CompanyID = Convert.ToInt32(@User.FindFirst("CompanyID").Value);
                setting_update.RateIn = exchage.Rate;
                setting_update.RateOut = exchage.RateOut;

                setting_update.CustomerID = setting.CustomerID;
                setting_update.DaulScreen = setting.DaulScreen;
                setting_update.PaymentMeansID = setting.PaymentMeansID;
                setting_update.PriceListID = setting.PriceListID;
                setting_update.PrintCountReceipt = setting.PrintCountReceipt;
                setting_update.PrintReceiptOrder = setting.PrintReceiptOrder;
                setting_update.PrintReceiptTender = setting.PrintReceiptTender;
                setting_update.QueueCount = setting.QueueCount;

                setting_update.Receiptsize = setting.Receiptsize;
                setting_update.ReceiptTemplate = setting.ReceiptTemplate;
                setting_update.VatAble = setting.VatAble;
                setting_update.VatNum = setting.VatNum;
                setting_update.WarehouseID = setting.WarehouseID;
                setting_update.Wifi = setting.Wifi;
                _context.GeneralSetting.Update(setting_update);
                _context.SaveChanges();
            }
           
            return Ok(setting);
        }
        public IActionResult SetPrinterName(PrinterName printer)
        {
            if (printer.Name != null)
            {
                var machine = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                var printerName = _context.PrinterNames.Where(w => w.Name == printer.Name && w.Delete == false && w.MachineName == machine);
                if (printerName.Count() > 0)
                {
                    printerName.First().Name = printer.Name;
                    _context.PrinterNames.Update(printerName.First());
                    _context.SaveChanges();
                    
                }
                else
                {
                    PrinterName addNew = new PrinterName
                    {
                        MachineName = machine,
                        Name = printer.Name
                    };
                    _context.PrinterNames.Add(addNew);
                    _context.SaveChanges();
                }
                return Ok();
            }
            return Ok();
        }
        public IActionResult GetUserPriviliges(int userid)
        {
            var privilige = _context.UserPrivilleges.Where(w => w.UserID == userid && w.Function.Type == "POS" && w.Delete == false);
            return Ok(privilige);

        }
        public IActionResult GetUserAccessAdmin(string username,string pass,string code)
        {
            pass = Encrypt(pass);
            var user = _context.UserAccounts.FirstOrDefault(w => w.Username == username && w.Password == pass);
            if (user != null)
            {
                var privillege = _context.UserPrivilleges.FirstOrDefault(w => w.UserID == user.ID && w.Code == code);
                return Ok(privillege);
            }
            return Ok();
        }
        public IActionResult VoidOrder(int orderid)
        {
            if(orderid>0)
            {
                var order = _context.Order.FirstOrDefault(w => w.OrderID == orderid);
                if (order.CheckBill == 'Y')
                {
                    return Ok("N");
                }
                else
                {
                    _pos.VoidOrder(orderid);
                    return Ok("Y");
                }
            }
           return Ok('N');
        }
        public string Encrypt(string clearText)
        {
            if (clearText != null)
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            else
            {
                return clearText;
            }

        }
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class ReceiptInformationController : Controller
    {
        private readonly DataContext _context;
        private readonly IReceiptInformation _receiptInformation;
        private readonly IHostingEnvironment _appEnvironment;

        public ReceiptInformationController(DataContext context, IReceiptInformation receiptInformation, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _receiptInformation = receiptInformation;
            _appEnvironment = hostingEnvironment;
        }

        // GET: ReceiptInformation
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Receipt";
            ViewBag.Administrator = "show";
            ViewBag.General = "show";
            ViewBag.Receipt = "highlight";
  
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A003");
            var data = _receiptInformation.GetReceiptInformation().OrderBy(X => X.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["TitleSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Title_desc" : "";
                ViewData["BranchSortParm"] = sortOrder == "branch" ? "branch_desc" : "branch";
                ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";
                ViewData["Tel1SortParm"] = sortOrder == "Tel1" ? "Tel1_desc" : "Tel1";
                ViewData["Tel2SortParm"] = sortOrder == "Tel2" ? "Tel2_desc" : "Tel2";
                ViewData["KhmerDescriptionSortParm"] = sortOrder == "KhmerDescription" ? "KhmerDescription_desc" : "KhmerDescription";
                ViewData["EnglishDescriptionSortParm"] = sortOrder == "EnglishDescription" ? "EnglishDescription_desc" : "EnglishDescription";
                ViewData["PowerBySortParm"] = sortOrder == "PowerBy" ? "PowerBy_desc" : "PowerBy";
                ViewData["LanguageSortParm"] = sortOrder == "Language" ? "Language_desc" : "Language";
                ViewData["LogoSortParm"] = sortOrder == "Logo" ? "Logo_desc" : "Logo";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                // var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Title.Contains(searchString) || s.Address.Contains(searchString) ||
                    s.Tel1.Contains(searchString) || s.Tel2.Contains(searchString) || s.KhmerDescription.Contains(searchString) ||
                    s.EnglishDescription.Contains(searchString) || s.PowerBy.Contains(searchString)  || s.Logo.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "Title_desc":
                        Cate = Cate.OrderByDescending(s => s.Title);
                        break;
                    case "Address":
                        Cate = Cate.OrderBy(s => s.Address);
                        break;
                    case "Address_desc":
                        Cate = Cate.OrderByDescending(s => s.Address);
                        break;
                    case "branch":
                        Cate = Cate.OrderBy(s => s.Branch.Name);
                        break;
                    case "branch_desc":
                        Cate = Cate.OrderByDescending(s => s.Branch.Name);
                        break;
                    case "Tel1":
                        Cate = Cate.OrderBy(s => s.Tel1);
                        break;
                    case "Tel1_desc":
                        Cate = Cate.OrderByDescending(s => s.Tel1);
                        break;
                    case "Tel2":
                        Cate = Cate.OrderBy(s => s.Tel2);
                        break;
                    case "Tel2_desc":
                        Cate = Cate.OrderByDescending(s => s.Tel2);
                        break;
                    case "KhmerDescription":
                        Cate = Cate.OrderBy(s => s.KhmerDescription);
                        break;
                    case "KhmerDescription_desc":
                        Cate = Cate.OrderByDescending(s => s.KhmerDescription);
                        break;
                    case "EnglishDescription":
                        Cate = Cate.OrderBy(s => s.EnglishDescription);
                        break;
                    case "EnglishDescription_desc":
                        Cate = Cate.OrderByDescending(s => s.EnglishDescription);
                        break;
                    case "PowerBy":
                        Cate = Cate.OrderBy(s => s.PowerBy);
                        break;
                    case "PowerBy_desc":
                        Cate = Cate.OrderByDescending(s => s.PowerBy);
                        break;
                   
                    case "Logo":
                        Cate = Cate.OrderBy(s => s.Logo);
                        break;
                    case "Logo_desc":
                        Cate = Cate.OrderByDescending(s => s.Logo);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Title);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<ReceiptInformation>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["TitleSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Title_desc" : "";
                    ViewData["BranchSortParm"] = sortOrder == "branch" ? "branch_desc" : "branch";
                    ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";
                    ViewData["Tel1SortParm"] = sortOrder == "Tel1" ? "Tel1_desc" : "Tel1";
                    ViewData["Tel2SortParm"] = sortOrder == "Tel2" ? "Tel2_desc" : "Tel2";
                    ViewData["KhmerDescriptionSortParm"] = sortOrder == "KhmerDescription" ? "KhmerDescription_desc" : "KhmerDescription";
                    ViewData["EnglishDescriptionSortParm"] = sortOrder == "EnglishDescription" ? "EnglishDescription_desc" : "EnglishDescription";
                    ViewData["PowerBySortParm"] = sortOrder == "PowerBy" ? "PowerBy_desc" : "PowerBy";
                    ViewData["LanguageSortParm"] = sortOrder == "Language" ? "Language_desc" : "Language";
                    ViewData["LogoSortParm"] = sortOrder == "Logo" ? "Logo_desc" : "Logo";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    // var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Title.Contains(searchString) || s.Address.Contains(searchString) ||
                        s.Tel1.Contains(searchString) || s.Tel2.Contains(searchString) || s.KhmerDescription.Contains(searchString) ||
                        s.EnglishDescription.Contains(searchString) || s.PowerBy.Contains(searchString)  || s.Logo.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "Title_desc":
                            Cate = Cate.OrderByDescending(s => s.Title);
                            break;
                        case "Address":
                            Cate = Cate.OrderBy(s => s.Address);
                            break;
                        case "Address_desc":
                            Cate = Cate.OrderByDescending(s => s.Address);
                            break;
                        case "branch":
                            Cate = Cate.OrderBy(s => s.Branch.Name);
                            break;
                        case "branch_desc":
                            Cate = Cate.OrderByDescending(s => s.Branch.Name);
                            break;
                        case "Tel1":
                            Cate = Cate.OrderBy(s => s.Tel1);
                            break;
                        case "Tel1_desc":
                            Cate = Cate.OrderByDescending(s => s.Tel1);
                            break;
                        case "Tel2":
                            Cate = Cate.OrderBy(s => s.Tel2);
                            break;
                        case "Tel2_desc":
                            Cate = Cate.OrderByDescending(s => s.Tel2);
                            break;
                        case "KhmerDescription":
                            Cate = Cate.OrderBy(s => s.KhmerDescription);
                            break;
                        case "KhmerDescription_desc":
                            Cate = Cate.OrderByDescending(s => s.KhmerDescription);
                            break;
                        case "EnglishDescription":
                            Cate = Cate.OrderBy(s => s.EnglishDescription);
                            break;
                        case "EnglishDescription_desc":
                            Cate = Cate.OrderByDescending(s => s.EnglishDescription);
                            break;
                        case "PowerBy":
                            Cate = Cate.OrderBy(s => s.PowerBy);
                            break;
                        case "PowerBy_desc":
                            Cate = Cate.OrderByDescending(s => s.PowerBy);
                            break;
                       
                        case "Logo":
                            Cate = Cate.OrderBy(s => s.Logo);
                            break;
                        case "Logo_desc":
                            Cate = Cate.OrderByDescending(s => s.Logo);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Title);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<ReceiptInformation>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        // GET: ReceiptInformation/Create
        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Receipt";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A003");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }  
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Address,Tel1,Tel2,KhmerDescription,EnglishDescription,PowerBy,Logo,BranchID,TeamCondition")] ReceiptInformation receiptInformation)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Receipt";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (receiptInformation.BranchID == 0 || receiptInformation.BranchID==null)
            {
               ViewBag.required_branch = "Please select branch !";
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var files = HttpContext.Request.Form.Files;
                    foreach (var Image in files)
                    {
                        if (Image != null && Image.Length > 0)
                        {
                            var file = Image;
                            var uploads = Path.Combine(_appEnvironment.WebRootPath, "Logo");
                            if (file.Length > 0)
                            {
                                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    receiptInformation.Logo = fileName;
                                }

                            }
                        }
                    }
                    await _receiptInformation.AddOrEdit(receiptInformation);
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
            return View(receiptInformation);
        }

        // GET: ReceiptInformation/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Receipt";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (id == null)
                {
                    return NotFound();
                }

                var receiptInformation = await _context.ReceiptInformation.FindAsync(id);
                if (receiptInformation == null)
                {
                    return NotFound();
                }
                ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
                return View(receiptInformation);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A003");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var receiptInformation = await _context.ReceiptInformation.FindAsync(id);
                    if (receiptInformation == null)
                    {
                        return NotFound();
                    }
                    ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
                    return View(receiptInformation);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }   
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Address,Tel1,Tel2,KhmerDescription,EnglishDescription,PowerBy,Logo,BranchID,TeamCondition")] ReceiptInformation receiptInformation)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Receipt";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";


            if (id != receiptInformation.ID)
            {
                return NotFound();
            }
            if (receiptInformation.BranchID == 0 || receiptInformation.BranchID == null)
            {
                ViewBag.required_branch = "Please select branch !";
            }
            else
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var files = HttpContext.Request.Form.Files;
                        foreach (var Image in files)
                        {
                            if (Image != null && Image.Length > 0)
                            {
                                var file = Image;
                                var uploads = Path.Combine(_appEnvironment.WebRootPath, "Logo");
                                if (file.Length > 0)
                                {
                                    var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                                    using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        receiptInformation.Logo = fileName;
                                    }

                                }
                            }
                        }
                        await _receiptInformation.AddOrEdit(receiptInformation);
                    }
                    catch (Exception)
                    {
                       
                    }
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BranchID"] = new SelectList(_context.Branches.Where(c => c.Delete == false), "ID", "Name");
            return View(receiptInformation);
        }



        // POST: ReceiptInformation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var receiptInformation = await _context.ReceiptInformation.FindAsync(id);
            _context.ReceiptInformation.Remove(receiptInformation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReceiptInformationExists(int id)
        {
            return _context.ReceiptInformation.Any(e => e.ID == id);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> AddRecirptInfoma(ReceiptInformation receipt)
        {
           await _receiptInformation.AddOrEdit(receipt);
            return Ok();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetRescirtp()
        {
            var list = _receiptInformation.GetReceiptInformation().OrderByDescending(x=>x.ID).ToList();
            return Ok(list);
        }
    }
}

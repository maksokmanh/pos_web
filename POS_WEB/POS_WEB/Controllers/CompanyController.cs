﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;

using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private readonly DataContext _context;
        private readonly ICompany _company;
        private readonly IHostingEnvironment _appEnvironment;
        public CompanyController(DataContext context, ICompany company, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _company = company;
            _appEnvironment = hostingEnvironment;
        }

        // GET: Company
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Company";
            ViewBag.Administrator = "show";
            ViewBag.General = "show";
            ViewBag.Company = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A001");
            var data = _company.GetCompany().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;

            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                // ViewData["ReceiptIDSortParm"] = sortOrder == "ReceiptID" ? "ReceiptID_desc" : "ReceiptID";
                ViewData["LocationSortParm"] = sortOrder == "Location" ? "Location_desc" : "Location";
                ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";
                ViewData["ProcessSortParm"] = sortOrder == "Process" ? "Process_desc" : "Process";
                ViewData["PriceListSortParm"] = sortOrder == "PriceList" ? "PriceList_desc" : "PriceList";
                ViewData["DefaultSortParm"] = sortOrder == "Default" ? "Default_desc" : "Default";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                // var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) ||
                    s.Address.Contains(searchString) || s.Location.Contains(searchString) ||
                    s.Process.Contains(searchString) ||
                    s.PriceList.Name.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;

                    case "Location":
                        Cate = Cate.OrderBy(s => s.Location);
                        break;
                    case "Location_desc":
                        Cate = Cate.OrderByDescending(s => s.Location);
                        break;
                    case "Address":
                        Cate = Cate.OrderBy(s => s.Address);
                        break;
                    case "Address_desc":
                        Cate = Cate.OrderByDescending(s => s.Address);
                        break;
                    case "Process":
                        Cate = Cate.OrderBy(s => s.Process);
                        break;
                    case "Process_desc":
                        Cate = Cate.OrderByDescending(s => s.Process);
                        break;
                    case "PriceList":
                        Cate = Cate.OrderBy(s => s.PriceListID);
                        break;
                    case "PriceList_desc":
                        Cate = Cate.OrderByDescending(s => s.PriceListID);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<Company>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if (permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["LocationSortParm"] = sortOrder == "Location" ? "Location_desc" : "Location";
                    ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address_desc" : "Address";
                    ViewData["ProcessSortParm"] = sortOrder == "Process" ? "Process_desc" : "Process";
                    ViewData["PriceListSortParm"] = sortOrder == "PriceList" ? "PriceList_desc" : "PriceList";
                    ViewData["DefaultSortParm"] = sortOrder == "Default" ? "Default_desc" : "Default";

                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;
                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) ||
                        s.Address.Contains(searchString) || s.Location.Contains(searchString) ||
                        s.Process.Contains(searchString) ||
                        s.PriceList.Name.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                    
                        case "Location":
                            Cate = Cate.OrderBy(s => s.Location);
                            break;
                        case "Location_desc":
                            Cate = Cate.OrderByDescending(s => s.Location);
                            break;
                        case "Address":
                            Cate = Cate.OrderBy(s => s.Address);
                            break;
                        case "Address_desc":
                            Cate = Cate.OrderByDescending(s => s.Address);
                            break;
                        case "Process":
                            Cate = Cate.OrderBy(s => s.Process);
                            break;
                        case "Process_desc":
                            Cate = Cate.OrderByDescending(s => s.Process);
                            break;
                        case "PriceList":
                            Cate = Cate.OrderBy(s => s.PriceListID);
                            break;
                        case "PriceList_desc":
                            Cate = Cate.OrderByDescending(s => s.PriceListID);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                  
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<Company>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }


        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Company";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            ViewBag.Highlight = "highlight";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name");
                ViewData["ReceiptID"] = new SelectList(_context.ReceiptInformation, "ID", "Title");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A001");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name");
                    ViewData["ReceiptID"] = new SelectList(_context.ReceiptInformation, "ID", "Title");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
       
        public async Task<IActionResult> Create([Bind("ID,Name,ReceiptID,Location,Address,Process,Delete,PriceListID,Logo")] Company company)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Company";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            ViewBag.Highlight = "highlight";

            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/company");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                company.Logo = fileName;
                            }

                        }
                    }
                }
                _context.Add(company);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }
            else
            {
                if (company.PriceListID == 0)
                    ViewData["error.receiptid"] = "Please select priceList !";
                
                if (company.Process == "0")
                    ViewData["error.Process"] = "Please select process !";
              
            }

            ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name", company.PriceList);
           // ViewData["ReceiptID"] = new SelectList(_context.ReceiptInformation, "ID", "Title", company.ReceiptID);
            return View(company);
        }

        // GET: Company/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Company";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            ViewBag.Highlight = "highlight";

            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (id == null)
                {
                    return NotFound();
                }

                var company = await _context.Company.FindAsync(id);
                if (company == null)
                {
                    return NotFound();
                }
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name", company.PriceList);
                //ViewData["ReceiptID"] = new SelectList(_context.ReceiptInformation, "ID", "Title", company.ReceiptID);
                return View(company);
            }
            int userid = int.Parse(User.FindFirst("UserID").Value);
            var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A001");
            if (permistion != null)
            {
                if (permistion.Used == true)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var company = await _context.Company.FindAsync(id);
                    if (company == null)
                    {
                        return NotFound();
                    }
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name", company.PriceList);
                    return View(company);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,ReceiptID,Location,Address,Process,Delete,PriceListID,Logo")] Company company)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "General";
            ViewBag.Subpage = "Company";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            ViewBag.Highlight = "highlight";

            if (id != company.ID)
            {
                return NotFound();
            }
            if (company.Process == "0")
            {
                ViewData["error.Process"] = "Please select process !";
                return View(company);
            }

            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/company");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                company.Logo = fileName;
                            }

                        }
                    }
                }

                _company.UpdateCurrencyWarehouseDetail(company.PriceListID);
                _context.Update(company);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }
            else
            {
                if (company.PriceListID == 0)
                    ViewData["error.receiptid"] = "Please select priceList !";
              
                if (company.Process == "0")
                    ViewData["error.Process"] = "Please select process !";

            }
            ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(c => c.Delete == false), "ID", "Name", company.PriceList);
  
            return View(company);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            await _company.DeleteCompany(id);
            return Ok();
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _context.Company.FindAsync(id);
            _context.Company.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyExists(int id)
        {
            return _context.Company.Any(e => e.ID == id);
        }
    }
}
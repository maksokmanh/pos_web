﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Controllers.Event;
using POS_WEB.Models.Services.Purchase.Print;
using POS_WEB.Models.SignalR;

namespace POS_WEB.Controllers
{
    public class WindowController : Controller
    {
        private readonly DataContext _context;
        private readonly TimeDelivery _timeDelivery;
        public WindowController(IHubContext<SignalRClient> hubcontext,DataContext context)
        {
            _timeDelivery = TimeDelivery.GetInstance(hubcontext);
            _timeDelivery.StartTimer();
            _context = context;
        }
        [HttpGet]
        public IActionResult PrintPurchaseAP(int PurchaseID)
        {
           var list=   GetPrintPurchaseAPs(PurchaseID).ToList();
            _timeDelivery.PrintPurchaseAP(list);
            return Ok();
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseAPs(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseAP @PurchaseID={0}",
          parameters:new[] {
              PurchaseID.ToString()
          });
        [HttpGet]
        public IActionResult PrintPurchaseOrder(int PurchaseID)
        {
            var list = GetPrintPurchaseOrder(PurchaseID).ToList();
            _timeDelivery.PrintPurchaseOrder(list);
            return Ok();
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseOrder(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseOrder @PurchaseID={0}",
            parameters:new[] {
                PurchaseID.ToString()
            });
        [HttpGet]
        public IActionResult PrintPurchaseCreditMemo(int PurchaseID)
        {
            var list = GetPrintPurchaseCreditMemo(PurchaseID).ToList();
            _timeDelivery.PrintPurchaseCreditMemo(list);
            return Ok(list);
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseCreditMemo(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseCreditMemo @PurchaseID={0}",
           parameters: new[] {
                PurchaseID.ToString()
           });
        [HttpGet]
        public IActionResult PrintGoodReceiptPO(int PurchaseID)
        {
            var list = GetPrintGoodReceiptPO(PurchaseID).ToList();
            _timeDelivery.PrintGoodReceiptPO(list);
            return Ok();
        }
        private IEnumerable<PrintPurchaseAP> GetPrintGoodReceiptPO(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintGoodReceiptPO @PurchaseID={0}",
            parameters: new[] {

                PurchaseID.ToString()
            });
        [HttpGet]
        public IActionResult PrintPurchaseQuotation(int PurchaseID)
        {
            var list = GetPrintPurchaseQuotation(PurchaseID).ToList();
            _timeDelivery.PirntPurchaseQuotation(list);
            return Ok();
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseQuotation(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPruchaseQuotation @PurchaseID={0}",
            parameters: new[] {
            PurchaseID.ToString()
          });

    }
}
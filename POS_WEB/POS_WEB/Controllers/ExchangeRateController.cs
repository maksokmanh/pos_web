﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;
using POS_WEB.Models.SeverClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class ExchangeRateController : Controller
    {
        private readonly IExchangeRate _IExchang;
        private readonly DataContext _context;

        public ExchangeRateController(IExchangeRate exchangeRate,DataContext dataContext)
        {
            _IExchang = exchangeRate;
            _context = dataContext;
        }
        [HttpGet]
        // GET: ExchangeRate
        public IActionResult Index()
        {
            string name="";
            ViewBag.style = "fa fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Exchange Rate";
            ViewBag.Subpage = "List";
            ViewBag.Banking = "show";
            ViewBag.ExchangeRate = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                IEnumerable<Currency> currencies = _IExchang.GetBaseCurrencyName();
                foreach (var item in currencies as IEnumerable<Currency>)
                {
                    name = item.Description;

                }
                ViewData["BaseCurrencyName"] = name;
                ViewData["Currency"] = _IExchang.GetListCurrency();
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A014");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    IEnumerable<Currency> currencies = _IExchang.GetBaseCurrencyName();
                    foreach (var item in currencies as IEnumerable<Currency>)
                    {
                        name = item.Description;

                    }
                    ViewData["BaseCurrencyName"] = name;
                    ViewData["Currency"] = _IExchang.GetListCurrency();
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }
        [HttpPost]
        public IActionResult Edit(ExchangeRateService data)
        {
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (ModelState.IsValid)
                {
                    _IExchang.UpdateExchangeRate(data);
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A014");
            if(permision!=null)
            {
                if (permision.Used)
                {
                    if (ModelState.IsValid)
                    {
                        _IExchang.UpdateExchangeRate(data);
                    }
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
            return Ok();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class GoodIssuesController : Controller
    {
        private readonly DataContext _context;
        private readonly IGoodIssuse _issuse;
        public GoodIssuesController(DataContext context,IGoodIssuse issuse)
        {
            _context = context;
            _issuse = issuse;
        }
        public IActionResult GoodIssues()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Transaction";
            ViewBag.Subpage = "Goods Issue";
            ViewBag.InventoryMenu = "show";
            ViewBag.Transaction = "show";
            ViewBag.GoodsIssue = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A040");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
           
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.GoodIssues.Count() + 1;
            var list = "SO-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousesFrom(int ID)
        {
            var list = _issuse.GetWarehouse_From(ID);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseFrom(int ID)
        {
            var list = _issuse.GetItemByWarehouse_From(ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveGoodIssues(GoodIssues issues)
        {
            List<GoodIssuesDetail> list = new List<GoodIssuesDetail>();
            foreach (var item in issues.GoodIssuesDetails.ToList())
            {
                var checkstock = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                          x.ExpireDate == item.ExpireDate &&
                                                                          x.UomID == item.UomID &&
                                                                          x.WarehouseID == issues.WarehouseID &&
                                                                          x.ItemID == item.ItemID);
                if (checkstock != null)
                {
                    if (item.Quantity > checkstock.InStock)
                    {
                        item.Check = "Over stock";
                        list.Add(item);
                    }
                }
                else
                {
                    item.Check = "Not found in stock ";
                    list.Add(item);
                }
            }
            if (list.Count() > 0)
            {
                return Ok(list);
            }
            else
            {
                _issuse.SaveGoodIssues(issues);
                return Ok();
            }
           
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            try
            {
                var list = _issuse.GetItemFindBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
    }
}

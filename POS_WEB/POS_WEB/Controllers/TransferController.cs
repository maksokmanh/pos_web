﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class TransferController : Controller
    {
        private readonly DataContext _context;
        public readonly ITransfer _transfer;
        public TransferController(DataContext context,ITransfer transfer)
        {
            _transfer = transfer;
            _context = context;
        }
        public IActionResult Transfer()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Transaction";
            ViewBag.Subpage = "Transfer";
            ViewBag.InventoryMenu = "show";
            ViewBag.Transaction = "show";
            ViewBag.Transfer = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A041");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.Transfers.Count() + 1;
            var list = "IM-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public  IActionResult GetWarehousesFrom(int BranchID)
        {
            var list = _transfer.GetFromWarehouse(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousesTo(int BranchID)
        {
            var list = _transfer.GetToWarehouse.Where(x=>x.BranchID==BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBranch()
        {
            var list = _transfer.GetBranches.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_By_filterBranch(int BranchID)
        {
            var list = _transfer.GetWarehouse_filter_Branch(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseFrom(int warehouesId)
        {
            var list = _transfer.GetItemMasterBy_Warehouse(warehouesId).ToList(); 
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveTransfer(Transfer transfer)
        {
            if(transfer.BranchID==transfer.BranchToID && transfer.WarehouseFromID == transfer.WarehouseToID)
            {
                var detail = transfer.TransferDetails.First();
                detail.Check = "Hello";
                return Ok(detail);
            }
            else
            {
                List<TransferDetail> list = new List<TransferDetail>();

                foreach (var item in transfer.TransferDetails.ToList())
                {
                    var checkStock_ount = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                             x.UomID == item.UomID &&
                                                                             x.ExpireDate == item.ExpireDate &&
                                                                             x.ItemID == item.ItemID &&
                                                                             x.WarehouseID == transfer.WarehouseFromID);
                    var checkStock_in = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                               x.UomID == item.UomID &&
                                                                               x.ExpireDate == item.ExpireDate &&
                                                                               x.ItemID == item.ItemID &&
                                                                               x.WarehouseID == transfer.WarehouseToID);
                    if (checkStock_ount != null)
                    {
                        if (checkStock_in != null)
                        {
                            if (item.Qty > checkStock_ount.InStock)
                            {
                                item.Check = "Over stock";
                                list.Add(item);
                            }
                        }
                        else
                        {
                            if (item.Qty > checkStock_ount.InStock)
                            {
                                item.Check = "Over stock";
                                list.Add(item);
                            }
                        }
                    }
                    else
                    {
                        item.Check = "Item not found in warehouse from";
                        list.Add(item);
                    }
                }
                if (list.Count() > 0)
                {
                    return Ok(list);
                }
                else
                {
                    if (transfer.UserRequestID == 0)
                    {
                        transfer.UserRequestID = transfer.UserID;
                    }
                    if (transfer.Time == null)
                    {
                        transfer.Time = DateTime.Now.ToLongTimeString();
                    }
                    _context.Transfers.Add(transfer);
                    _context.SaveChanges();
                    var TransferID = transfer.TarmsferID;
                    _transfer.SaveTrasfers(TransferID);
                    return Ok();
                }
            }
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID, string Barcode)
        {
            try
            {
                var list = _transfer.GetItemFindBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
    }
}

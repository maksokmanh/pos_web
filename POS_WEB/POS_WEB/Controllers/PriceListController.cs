﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PriceListController : Controller
    {
        private readonly DataContext _context;
        private readonly IPriceList _priceList;
        private static int PriceListID=0;
        private static int PriceID = 0;
        public PriceListController(DataContext context,IPriceList priceList)
        {
            _context = context;
            _priceList = priceList;
        }
        public IActionResult CopyItem(int ID)
        {
            var pricelist = _context.PriceLists.FirstOrDefault(w => w.ID == ID && w.Delete == false);
            ViewBag.PriceListFromID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Copy Item To " + pricelist.Name;
            ViewBag.InventoryMenu = "show";
            
            ViewBag.PriceList = "highlight";
            return View();
        }
        public IActionResult SetSalePrice(int ID)
        {
            var pricelist = _context.PriceLists.FirstOrDefault(w => w.ID == ID && w.Delete == false);
            ViewBag.PriceListID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Set Price -> " + pricelist.Name;
           
            return View();
        }
        public IActionResult UpdateSalePrice(int ID)
        {
            var pricelist = _context.PriceLists.FirstOrDefault(w => w.ID == ID && w.Delete == false);
            ViewBag.PriceListID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Update Price -> " + pricelist.Name;
            ViewBag.Menu = "show";
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Price list";
            ViewBag.InventoryMenu = "show";
            ViewBag.PriceList = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code =="A017");
            var data = _priceList.GetPriceLists().OrderByDescending(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
                ViewData["CurrencySortParm"] = sortOrder == "Currency" ? "Currency_desc" : "Currency";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Currency.Symbol.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "Name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Currency_desc":
                        Cate = Cate.OrderByDescending(s => s.CurrencyID);
                        break;
                    case "Currency":
                        Cate = Cate.OrderBy(s => s.CurrencyID);
                        break;

                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<PriceLists>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
                    ViewData["CurrencySortParm"] = sortOrder == "Currency" ? "Currency_desc" : "Currency";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Currency.Symbol.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "Name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Currency_desc":
                            Cate = Cate.OrderByDescending(s => s.CurrencyID);
                            break;
                        case "Currency":
                            Cate = Cate.OrderBy(s => s.CurrencyID);
                            break;

                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    // int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<PriceLists>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }

   
        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Price list";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A017");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Delete,CurrencyID")] PriceLists priceList)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Price list";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                  await _priceList.AddOrEdit(priceList);
                  return RedirectToAction(nameof(Index));             
                 
                                   
            }
            else
            {
                if (priceList.CurrencyID == 0)
                {
                    ViewBag.Error = "Please select currency !";
                    ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol", priceList.CurrencyID);
                }
              
            }
            ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol", priceList.CurrencyID);
            return View(priceList);
        }

        
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Price list";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var priceList = _priceList.GetId(id);
                if (priceList == null)
                {
                    return NotFound();
                }
                if (priceList.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol", priceList.CurrencyID);
                return View(priceList);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A017");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var priceList = _priceList.GetId(id);
                    if (priceList == null)
                    {
                        return NotFound();
                    }
                    if (priceList.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol", priceList.CurrencyID);
                    return View(priceList);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Delete,CurrencyID")] PriceLists priceList)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Price list";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                try
                {
                    await _priceList.AddOrEdit(priceList);
                }
                catch (Exception)
                {
                    ViewBag.Error = "Please select currency !";
                    ViewData["CurrencyID"] = new SelectList(_context.Currency, "ID", "Symbol", priceList.CurrencyID);
                    return View(priceList);
                }
                return RedirectToAction(nameof(Index));
            }
            
            return View(priceList);
        }
        [HttpDelete]
        public async Task<IActionResult> dletepricelist(int id)
        {
            await _priceList.Deletepricelist(id);
            return Ok();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> InsertPricelist(PriceLists priceList)
        {
            await  _priceList.AddOrEdit(priceList);
            return Ok();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetselectPricelist()
        {
            var list = _priceList.GetPriceLists().OrderByDescending(p=>p.ID).ToList();
            return Ok(list);
        }

        public IActionResult SetPrice(int ID)
        {
            PriceListID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Set Price";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A017");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        [HttpGet]
        public IActionResult GetPricelistItem()
        {
            var list = _priceList.ItemMasters("A", PriceListID).ToList();
            return Ok(list);
        }
        public IActionResult Update(int ID)
        {
            PriceID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Price List";
            ViewBag.Subpage = "Update Price";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }

            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A017");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetPriceListUpdate()
        {
            var list = _priceList.ItemMasters("M", PriceID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult UpdateSetPrice(ServiceDetail dataService)
        {
            _priceList.UpdatePriceListDetail(dataService);
            return Ok();
        }
        [HttpGet]
        public IActionResult GetPriceListByGroup1(int ID,int PriceListID)
        {
            if (ID == 0)
            {
                var list = _priceList.ItemMasters("A", PriceListID).ToList();
                return Ok(list);
            }
            else
            {
                var list = _priceList.ItemMasters("A", PriceListID).Where(x => x.Group1 == ID).ToList();
                return Ok(list);
            }
           
        }
        [HttpGet]
        public IActionResult GetPriceListByGroup1_M(int ID,int PriceListID)
        {
            if (ID == 0)
            {
                var list = _priceList.ItemMasters("M",PriceListID).ToList();
                return Ok(list);
            }
            else
            {
                var list = _priceList.ItemMasters("M",PriceListID).Where(x => x.Group1 == ID).ToList();
                return Ok(list);
            }

        }
        [HttpGet]
        public IActionResult GetPriceListDetailByGroup2(int ID,int PriceListID)
        {
            var list = _priceList.ItemMasters("A",PriceListID).Where(x => x.Group2 == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPriceListDetailByGroup2_M(int ID,int PriceListID)
        {
            var list = _priceList.ItemMasters("M",PriceListID).Where(x => x.Group2 == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPriceListDetailByGroup3(int ID,int PriceListID)
        {
            var list = _priceList.ItemMasters("A",PriceListID).Where(x => x.Group3 == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPriceListDetailByGroup3_M(int ID,int PriceListID)
        {
            var list = _priceList.ItemMasters("M",PriceListID).Where(x => x.Group3 == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetInventoryAuditByItem(int ItemID,int BranchID,int UomID)
        {
            var list = _priceList.GetInventoryAuditsByItem(ItemID, BranchID,UomID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult FilterSystemDate(string DateFrom,string DateTo,int ItemID,int BranchID,int UomID)
        {
            var list = _priceList.GetInventoryAuditsFilterSystemDate(DateFrom, DateTo, ItemID, BranchID,UomID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousefilter(int BranchID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID==BranchID).ToList();
            return Ok(list);
        }
        //Panha
        public IActionResult GetPriceList(int price_list_baseid)
        {
            var pricelist = _context.PriceLists.Where(w => w.Delete == false && w.ID!= price_list_baseid);
            return Ok(pricelist);
        }
        public IActionResult GetGroup1()
        {
            var groups = _context.ItemGroup1.Where(w => w.Delete == false);
            return Ok(groups);
        }
        public IActionResult GetGroup2(int group1)
        {
            var groups = _context.ItemGroup2.Where(w =>w.ItemG1ID==group1 && w.Delete == false && w.Name!="None");
            return Ok(groups);
        }
        public IActionResult GetGroup3(int group1, int group2)
        {
            var groups = _context.ItemGroup3.Where(w => w.ItemG1ID==group1 && w.ItemG2ID==group2 && w.Delete == false && w.Name != "None");
            return Ok(groups);
        }
        public IActionResult GetItemMasterToCopy(int pricelistbase,int pricelistid,int group1,int group2,int group3)
        {
            var items = _priceList.GetItemMasterToCopy(pricelistbase, pricelistid,group1,group2,group3).ToList();
            return Ok(items);
        }
        public IActionResult ItemCopyToPriceList(ItemCopyToPriceList ItemCopyToPriceList)
        {
            _priceList.InsertIntoPricelist(ItemCopyToPriceList);
            return Ok('Y');
        }
        //12.03.19
        public IActionResult GetItemsSetPrice(int PriceListID, int Group1, int Group2, int Group3)
        {
            IEnumerable<PricelistSetPrice> items = _context.PricelistSetPrice.FromSql("sp_GetItemSetPrice @PriceListID={0},@Group1={1},@Group2={2},@Group3={3},@Process={4}",
             parameters: new[] {
                    PriceListID.ToString(),
                    Group1.ToString(),
                    Group2.ToString(),
                    Group3.ToString(),
                    "Add"
             }).ToList();
            return Ok(items);
        }
        public IActionResult GetItemsUpdatePrice(int PriceListID, int Group1, int Group2, int Group3)
        {
            IEnumerable<PricelistSetPrice> items = _context.PricelistSetPrice.FromSql("sp_GetItemSetPrice @PriceListID={0},@Group1={1},@Group2={2},@Group3={3},@Process={4}",
             parameters: new[] {
                    PriceListID.ToString(),
                    Group1.ToString(),
                    Group2.ToString(),
                    Group3.ToString(),
                    "Update"
             }).ToList();
            return Ok(items);
        }
        public IActionResult SetAndUpdateSalePrice(PricelistSetUpdatePrice data)
        {
            if (data.PricelistSetPrice == null) { return Ok('N'); }
            if (data.PricelistSetPrice.Count > 0)
            {
                foreach (var item in data.PricelistSetPrice)
                {
                    var item_update = _context.PriceListDetails.Find(item.ID);
                    item_update.Cost = item.Cost;
                    item_update.UnitPrice = item.Price;
                    _context.PriceListDetails.Update(item_update);
                }
                _context.SaveChanges();
                return Ok('Y');
            }
            return Ok('N');
        }
        public JsonResult SetAndUpdateSalePrice1(PricelistSetUpdatePrice data)
        {
            string json;
            using (var reader = new StreamReader(Request.Body))
            {
                json = reader.ReadToEnd();
            }
            PricelistSetUpdatePrice dataVM = JsonConvert.DeserializeObject<PricelistSetUpdatePrice>(json);
            if (dataVM.PricelistSetPrice.Count > 0)
            {
                foreach (var item in dataVM.PricelistSetPrice.ToList())
                {
                    var item_update = _context.PriceListDetails.Find(item.ID);
                    item_update.Cost = item.Cost;
                    item_update.UnitPrice = item.Price;
                    _context.PriceListDetails.Update(item_update);
                }
                _context.SaveChanges();
                return Json('Y');
            }
            return Json('Y');
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class WarehouseController : Controller
    {
        private readonly IWarehouse _context;
        private readonly DataContext _dataContext;
      
        public WarehouseController(IWarehouse context,DataContext dataContext)
        {
            _context = context;
            _dataContext = dataContext;
        }
        public IActionResult CopyItem(int ID)
        {
            var warehouse = _context.Warehouse.FirstOrDefault(w => w.ID == ID && w.Delete == false);
            ViewBag.WHFromID = ID;
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse To " + warehouse.Name;
            ViewBag.Administrator = "show";
            ViewBag.Inventory = "show";
            ViewBag.Warehouse = "highlight";
            return View();
        }
        // GET: Warehouse
        public async  Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            //Title
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse";
            ViewBag.Administrator = "show";
            ViewBag.Inventory = "show";
            ViewBag.Warehouse = "highlight";
            //Filter
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _dataContext.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A008");
            
            var data = _context.Warehouses().OrderByDescending(o => o.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["CodeFilter"] = String.IsNullOrEmpty(sortOrder) ? "code_desc" : "";
                ViewData["NameFilter"] = sortOrder == "Name" ? "Name_desc" : "Name";
                ViewData["StockInFilter"] = sortOrder == "StockIn" ? "StockIn_desc" : "StockIn";
                ViewData["LocationFilter"] = sortOrder == "Location" ? "Location_desc" : "Location";
                ViewData["AddressFilter"] = sortOrder == "Address" ? "Address_desc" : "Address";
                ViewData["DefaultFilter"] = sortOrder == "Default" ? "Default_desc" : "Default";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Code.Contains(searchString) || s.StockIn.ToString().Contains(searchString) || s.Address.Contains(searchString) || s.Location.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "code_desc":
                        Cate = Cate.OrderByDescending(s => s.Code);
                        break;
                    case "Name":
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                    case "Name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "StockIn":
                        Cate = Cate.OrderBy(s => s.StockIn);
                        break;
                    case "StockIn_desc":
                        Cate = Cate.OrderByDescending(s => s.StockIn);
                        break;
                    case "Location":
                        Cate = Cate.OrderBy(s => s.Location);
                        break;
                    case "Location_desc":
                        Cate = Cate.OrderByDescending(s => s.Location);
                        break;
                    case "Address":
                        Cate = Cate.OrderBy(s => s.Address);
                        break;
                    case "Address_desc":
                        Cate = Cate.OrderByDescending(s => s.Address);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Code);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<Warehouse>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["CodeFilter"] = String.IsNullOrEmpty(sortOrder) ? "code_desc" : "";
                    ViewData["NameFilter"] = sortOrder == "Name" ? "Name_desc" : "Name";
                    ViewData["StockInFilter"] = sortOrder == "StockIn" ? "StockIn_desc" : "StockIn";
                    ViewData["LocationFilter"] = sortOrder == "Location" ? "Location_desc" : "Location";
                    ViewData["AddressFilter"] = sortOrder == "Address" ? "Address_desc" : "Address";
                    ViewData["DefaultFilter"] = sortOrder == "Default" ? "Default_desc" : "Default";

                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Code.Contains(searchString) || s.StockIn.ToString().Contains(searchString) || s.Address.Contains(searchString) || s.Location.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "code_desc":
                            Cate = Cate.OrderByDescending(s => s.Code);
                            break;
                        case "Name":
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                        case "Name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "StockIn":
                            Cate = Cate.OrderBy(s => s.StockIn);
                            break;
                        case "StockIn_desc":
                            Cate = Cate.OrderByDescending(s => s.StockIn);
                            break;
                        case "Location":
                            Cate = Cate.OrderBy(s => s.Location);
                            break;
                        case "Location_desc":
                            Cate = Cate.OrderByDescending(s => s.Location);
                            break;
                        case "Address":
                            Cate = Cate.OrderBy(s => s.Address);
                            break;
                        case "Address_desc":
                            Cate = Cate.OrderByDescending(s => s.Address);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Code);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<Warehouse>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _dataContext.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A008");
            if(permision!=null)
            {
                if (permision.Used)
                {
                    ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Name,StockIn,Default,Location,Address,Delete,BranchID")] Warehouse warehouse)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";
            if (warehouse.BranchID == 0)
            {
                ViewBag.requaridBranch = "Pleas select branch !";
                ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name", warehouse.BranchID);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    
                    await _context.AddOrEdit(warehouse);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {            
                    ViewBag.ErrorCode = "This code already exist !";
                    ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name", warehouse.BranchID);
                    return View(warehouse);
                }
            }
            ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name",warehouse.BranchID);
            return View(warehouse);
        }

        // GET: Warehouse/Edit/5
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (ModelState.IsValid)
                {
                    var warehouse = _context.GetId(id);
                    if (warehouse.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name");
                    return View(warehouse);
                }
                return Ok();
            }
            var permision = _dataContext.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A008");
            if(permision!=null)
            {
                if (permision.Used)
                {
                    if (ModelState.IsValid)
                    {
                        var warehouse = _context.GetId(id);
                        if (warehouse.Delete == true)
                        {
                            return RedirectToAction(nameof(Index));
                        }
                        ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name");
                        return View(warehouse);
                    }
                    return Ok();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,Name,StockIn,Default,Location,Address,Delete,BranchID")] Warehouse warehouse)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Warehouse";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            if (warehouse.BranchID == 0)
            {
                ViewBag.requaridBranch = "Pleas select branch !";
                ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name", warehouse.BranchID);
            }
            if (id != warehouse.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.AddOrEdit(warehouse);
                }
                catch (Exception)
                {
                    ViewBag.ErrorCode = "This code already exist !";
                    return View(warehouse);
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BranchID"] = new SelectList(_dataContext.Branches.Where(x => x.Delete == false), "ID", "Name",warehouse.BranchID);
            return View(warehouse);
        }

        // POST: Warehouse/Delete/5
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            await _context.Delete(id);
            return Ok();
        }
        // POST: Warehouse/SetDefault/5
        [HttpPost]
        public async Task<IActionResult> SetDefault(int id)
        {
            if(ModelState.IsValid)
            {
                await _context.SetDefault(id);
                return RedirectToAction(nameof(Index));
            }
            return Ok();
        }
        [HttpPost]
        public IActionResult AddWarehouse(Warehouse warehouse)
        {                   
             var list= _context.AddOrEdit(warehouse);
             return Ok(list);         
        }
        [HttpGet]
        public IActionResult GetWarehouse()
        {
            var list = _context.Warehouses().Where(w => w.Delete == false).ToList();
            return Ok(list);

        }
        [HttpGet]
        public IActionResult CheckCodeWarehouse(string Code)
        {
            var list = _context.Warehouse.Where(x => x.Code == Code);
            return Ok(list);

        }
        //Panha
        public IActionResult GetWarehouseTo()
        {
            var warehouse = _dataContext.Warehouses.Where(w => w.Delete == false);
            return Ok(warehouse);
        }
        public IActionResult GetGroup1()
        {
            var groups = _dataContext.ItemGroup1.Where(w => w.Delete == false);
            return Ok(groups);
        }
        public IActionResult GetGroup2(int group1)
        {
            var groups = _dataContext.ItemGroup2.Where(w => w.ItemG1ID == group1 && w.Delete == false && w.Name != "None");
            return Ok(groups);
        }
        public IActionResult GetGroup3(int group1, int group2)
        {
            var groups = _dataContext.ItemGroup3.Where(w => w.ItemG1ID == group1 && w.ItemG2ID == group2 && w.Delete == false && w.Name != "None");
            return Ok(groups);
        }
        public IActionResult GetItemMasterToCopy(int to_whid, int group1, int group2, int group3)
        {
            var items = _context.GetItemMasterToCopy(to_whid, group1, group2, group3).ToList();
            return Ok(items);
        }
        public IActionResult ItemCopyToWH(ItemCopyToWH ItemCopyToWH)
        {
            var userid = int.Parse(@User.FindFirst("UserID").Value);
            _context.InsertIntoPricelist(ItemCopyToWH, userid);
            return Ok('Y');
        }
    }
}

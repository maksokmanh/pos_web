﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers.Developer2
{
    [Authorize]
    public class ItemGroup3Controller : Controller
    {
        private readonly DataContext _context;
        public readonly IItemGroup3 _itemGroup3;
        private readonly IHostingEnvironment _appEnvironment;

        public ItemGroup3Controller(DataContext context,IItemGroup3 itemGroup3, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _itemGroup3 = itemGroup3;
            _appEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index(string minpage ="5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(3)";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemGroup = "show";
            ViewBag.ItemGroup3 = "highlight";
            int userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code =="A020");
            var data = _itemGroup3.ItemGroup3s().OrderByDescending(x=>x.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                ViewData["NameGroup2"] = sortOrder == "item2" ? "item2_desc" : "item2";
                ViewData["NameGroup1"] = sortOrder == "item1" ? "item1_desc" : "item1";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString) || s.ItemGroup2.Name.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Date":
                        Cate = Cate.OrderBy(s => s.Images);
                        break;
                    case "color_desc":
                        Cate = Cate.OrderBy(s => s.Colors.Name);
                        break;
                    case "color":
                        Cate = Cate.OrderByDescending(s => s.Colors.Name);
                        break;
                    case "item1_desc":
                        Cate = Cate.OrderBy(s => s.ItemGroup1.Name);
                        break;
                    case "item1":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup1.Name);
                        break;
                    case "item2_desc":
                        Cate = Cate.OrderBy(s => s.ItemGroup2.Name);
                        break;
                    case "item2":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup2.Name);
                        break;
                    case "back_desc":
                        Cate = Cate.OrderBy(s => s.Background.Name);
                        break;
                    case "back":
                        Cate = Cate.OrderByDescending(s => s.Background.Name);
                        break;
                    case "date_desc":
                        Cate = Cate.OrderByDescending(s => s.Images);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                int.TryParse(minpage, out MaxSize);
                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<ItemGroup3>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                    ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                    ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                    ViewData["NameGroup2"] = sortOrder == "item2" ? "item2_desc" : "item2";
                    ViewData["NameGroup1"] = sortOrder == "item1" ? "item1_desc" : "item1";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewData["CurrentFilter"] = searchString;
                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString) || s.ItemGroup2.Name.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Date":
                            Cate = Cate.OrderBy(s => s.Images);
                            break;
                        case "color_desc":
                            Cate = Cate.OrderBy(s => s.Colors.Name);
                            break;
                        case "color":
                            Cate = Cate.OrderByDescending(s => s.Colors.Name);
                            break;
                        case "item1_desc":
                            Cate = Cate.OrderBy(s => s.ItemGroup1.Name);
                            break;
                        case "item1":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup1.Name);
                            break;
                        case "item2_desc":
                            Cate = Cate.OrderBy(s => s.ItemGroup2.Name);
                            break;
                        case "item2":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup2.Name);
                            break;
                        case "back_desc":
                            Cate = Cate.OrderBy(s => s.Background.Name);
                            break;
                        case "back":
                            Cate = Cate.OrderByDescending(s => s.Background.Name);
                            break;
                        case "date_desc":
                            Cate = Cate.OrderByDescending(s => s.Images);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    int.TryParse(minpage, out MaxSize);
                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<ItemGroup3>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }

        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(3)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name");
                ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name");
                ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A020");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                    ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name");
                    ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name");
                    ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else{
                return RedirectToAction("AccessDenied", "Account");
            }  
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Delete,Images,ItemG1ID,ItemG2ID,ColorID,BackID")] ItemGroup3 itemGroup3)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(3)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (itemGroup3.ItemG1ID == 0)
            {
                ViewBag.Error = " Please select name !";

            }
            else if (itemGroup3.ItemG2ID == 0)
            {
                ViewBag.Error = "Please select  name !";
            }
            if (ModelState.IsValid)
            {

                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup3.Images = fileName;
                            }

                        }
                    }
                }

               await _itemGroup3.AddItemGroup3(itemGroup3);
               return RedirectToAction(nameof(Index));
            }
            ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name", itemGroup3.BackID);
            ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name", itemGroup3.ColorID);
            ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name", itemGroup3.ItemG1ID);
            ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name", itemGroup3.ItemG2ID);
            return View(itemGroup3);
        }

        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(3)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemGroup3 = _itemGroup3.GetByID(id);
                if (itemGroup3.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemGroup3 == null)
                {
                    return NotFound();
                }
                ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name", itemGroup3.BackID);
                ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name", itemGroup3.ColorID);
                ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name", itemGroup3.ItemG1ID);
                ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name", itemGroup3.ItemG2ID);
                return View(itemGroup3);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A020");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var itemGroup3 = _itemGroup3.GetByID(id);
                    if (itemGroup3.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (itemGroup3 == null)
                    {
                        return NotFound();
                    }
                    ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name", itemGroup3.BackID);
                    ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name", itemGroup3.ColorID);
                    ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name", itemGroup3.ItemG1ID);
                    ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name", itemGroup3.ItemG2ID);
                    return View(itemGroup3);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Delete,Images,ItemG1ID,ItemG2ID,ColorID,BackID")] ItemGroup3 itemGroup3)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(3)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup3.Images = fileName;
                            }

                        }
                    }
                  
                }
                await _itemGroup3.AddItemGroup3(itemGroup3);
                return RedirectToAction(nameof(Index));
            }
            ViewData["BackID"] = new SelectList(_context.Backgrounds, "BackID", "Name", itemGroup3.BackID);
            ViewData["ColorID"] = new SelectList(_context.Colors, "ColorID", "Name", itemGroup3.ColorID);
            ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1, "ItemG1ID", "Name", itemGroup3.ItemG1ID);
            ViewData["ItemG2ID"] = new SelectList(_context.ItemGroup2, "ItemG2ID", "Name", itemGroup3.ItemG2ID);
            return View(itemGroup3);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCateory(int ID)
        {
            await _itemGroup3.DeleteItemGroup3(ID);
            return Ok();
        }
        [HttpPost]
        public async Task<IActionResult> AddItemGroup3(ItemGroup3 itemGroup3)
        {
            var list = await  _itemGroup3.AddItemGroup3(itemGroup3);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetitemGroup3(int Item2ID,int Item1ID)
        {
            var list = _itemGroup3.ItemGroup3s().OrderByDescending(c=>c.ID).Where(x => x.ItemG2ID == Item2ID  && x.ItemG1ID== Item1ID).ToList();
            return Ok(list);
        }
    }
}

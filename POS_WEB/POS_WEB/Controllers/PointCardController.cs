﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PointCardController : Controller
    {
        private readonly ICardPoint _cardPoint;
        private readonly DataContext _cotext;
        public PointCardController(ICardPoint cardPoint,DataContext dataContext)
        {
            _cardPoint = cardPoint;
            _cotext = dataContext;
        }
        public IActionResult PointCard()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Point";
            ViewBag.Subpage = "Create Card";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _cotext.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A032");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetPoint()
        {
            var list = _cardPoint.GetPoints.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPointCard()
        {
            var list = _cardPoint.GetPointCards().Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> InsertPointCard(PointCard pointCard)
        {
           await _cardPoint.AddorEdit(pointCard);
           var list = _cardPoint.GetPointCards().Where(x => x.Delete == false).ToList();
           return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteCardPoint(int ID)
        {
           await _cardPoint.DeleteCardPoint(ID);
            var list = _cardPoint.GetPointCards().Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult CheckReferentNumber(string Check)
        {
            var list = _cotext.PointCards.Where(x => x.Ref_No == Check && x.Delete==false);
            return Ok(list);
        }
        public IActionResult Register()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Point";
            ViewBag.Subpage = "Register";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _cotext.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A031");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
            
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Responsitory;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;
using System.Reflection;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Pagination;
using Microsoft.AspNetCore.Authorization;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class BusinessPartnerController : Controller
    {
        private readonly IBusinessPartner _partner;
        private readonly DataContext _context;   
        public BusinessPartnerController(IBusinessPartner partner, DataContext context )
        {
            _partner = partner;
            _context = context;
                    
        }
        public async Task<IActionResult> Index(string minpage = "5", string sort = null, string search = null, int? page = null, string filter = null)
        {
            ViewBag.style = "fa-users";
            ViewBag.Main = "Business Partner";
            ViewBag.Page = "List";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.BizPartners = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);

            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A012");
            var bus = _partner.BusinessPartners;
            var prod = from b in bus select b;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sort;
                ViewData["IDSortParm"] = String.IsNullOrEmpty(sort);
                ViewData["NameSortParm"] = sort == "name" ? "name-descending" : "name";
                ViewData["CodeSortParm"] = sort == "code" ? "code-descending" : "code";
                ViewData["TypeSortParm"] = sort == "Type" ? "Type-descending" : "Type";
                ViewData["PhoneSortParm"] = sort == "Phone" ? "Phone-descending" : "Phone";
                ViewData["EmailSortParm"] = sort == "Email" ? "Email-descending" : "Email";
                ViewData["AddressSortParm"] = sort == "Address" ? "Address-descending" : "Address";
                ViewData["PriceListSortParm"] = sort == "PriceList" ? "PriceList-descending" : "PriceList";

                if (search != null)
                {
                    page = 1;
                }
                else
                {
                    search = filter;
                }
                ViewData["filter"] = search;
                //var prod = from b in bus select b;
                if (!String.IsNullOrEmpty(search))
                {
                    prod = prod.Where(predicate: p => p.Name.Contains(search) ||
                    p.Code.Contains(search) || p.Address.Contains(search) || p.Email.Contains(search)
                    || p.Phone.Contains(search) || p.PriceList.Name.Contains(search) || p.Type.Contains(search)
                    );
                }
                switch (sort)
                {
                    case "name-descending":
                        prod = prod.OrderByDescending(x => x.Name);
                        break;
                    case "name":
                        prod = prod.OrderBy(x => x.Name);
                        break;
                    case "code-descending":
                        prod = prod.OrderByDescending(x => x.Code).Take(10);
                        break;
                    case "code":
                        prod = prod.OrderBy(x => x.Code);
                        break;
                    case "Type":
                        prod = prod.OrderBy(x => x.Type);
                        break;
                    case "Type-descending":
                        prod = prod.OrderByDescending(x => x.Type);
                        break;
                    case "Phone":
                        prod = prod.OrderBy(x => x.Phone);
                        break;
                    case "Phone-descending":
                        prod = prod.OrderByDescending(x => x.Phone);
                        break;
                    case "Email":
                        prod = prod.OrderBy(x => x.Email);
                        break;
                    case "Email-descending":
                        prod = prod.OrderByDescending(x => x.Email);
                        break;
                    case "Address":
                        prod = prod.OrderBy(x => x.Address);
                        break;
                    case "Address-descending":
                        prod = prod.OrderByDescending(x => x.Address);
                        break;
                    case "PriceList":
                        prod = prod.OrderBy(x => x.PriceListID);
                        break;
                    case "PriceList-descending":
                        prod = prod.OrderByDescending(x => x.PriceListID);
                        break;
                    default:
                        prod = prod.OrderBy(x => x.ID);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = prod.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                var bp = await Pagination<BusinessPartner>.CreateAsync(prod.AsNoTracking(), page ?? 1, pageSize);
                return View(bp);
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sort;
                    ViewData["IDSortParm"] = String.IsNullOrEmpty(sort);
                    ViewData["NameSortParm"] = sort == "name" ? "name-descending" : "name";
                    ViewData["CodeSortParm"] = sort == "code" ? "code-descending" : "code";
                    ViewData["TypeSortParm"] = sort == "Type" ? "Type-descending" : "Type";
                    ViewData["PhoneSortParm"] = sort == "Phone" ? "Phone-descending" : "Phone";
                    ViewData["EmailSortParm"] = sort == "Email" ? "Email-descending" : "Email";
                    ViewData["AddressSortParm"] = sort == "Address" ? "Address-descending" : "Address";
                    ViewData["PriceListSortParm"] = sort == "PriceList" ? "PriceList-descending" : "PriceList";

                    if (search != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        search = filter;
                    }
                    ViewData["filter"] = search;
                    //var prod = from b in bus select b;
                    if (!String.IsNullOrEmpty(search))
                    {
                        prod = prod.Where(predicate: p => p.Name.Contains(search) ||
                        p.Code.Contains(search) || p.Address.Contains(search) || p.Email.Contains(search)
                        || p.Phone.Contains(search) || p.PriceList.Name.Contains(search) || p.Type.Contains(search)
                        );
                    }
                    switch (sort)
                    {
                        case "name-descending":
                            prod = prod.OrderByDescending(x => x.Name);
                            break;
                        case "name":
                            prod = prod.OrderBy(x => x.Name);
                            break;
                        case "code-descending":
                            prod = prod.OrderByDescending(x => x.Code).Take(10);
                            break;
                        case "code":
                            prod = prod.OrderBy(x => x.Code);
                            break;
                        case "Type":
                            prod = prod.OrderBy(x => x.Type);
                            break;
                        case "Type-descending":
                            prod = prod.OrderByDescending(x => x.Type);
                            break;
                        case "Phone":
                            prod = prod.OrderBy(x => x.Phone);
                            break;
                        case "Phone-descending":
                            prod = prod.OrderByDescending(x => x.Phone);
                            break;
                        case "Email":
                            prod = prod.OrderBy(x => x.Email);
                            break;
                        case "Email-descending":
                            prod = prod.OrderByDescending(x => x.Email);
                            break;
                        case "Address":
                            prod = prod.OrderBy(x => x.Address);
                            break;
                        case "Address-descending":
                            prod = prod.OrderByDescending(x => x.Address);
                            break;
                        case "PriceList":
                            prod = prod.OrderBy(x => x.PriceListID);
                            break;
                        case "PriceList-descending":
                            prod = prod.OrderByDescending(x => x.PriceListID);
                            break;
                        default:
                            prod = prod.OrderBy(x => x.ID);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = prod.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    var bp = await Pagination<BusinessPartner>.CreateAsync(prod.AsNoTracking(), page ?? 1, pageSize);
                    return View(bp);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }

        }
      public  IActionResult Create()
        {
            ViewBag.style = "fa-users";
            ViewBag.Main = "Business Partner";
            ViewBag.Page = "List";
           
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["B"] = new SelectList(_context.PriceLists.Where(x=>x.Delete==false), "ID", "Name");
                ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
                ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A012");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["B"] = new SelectList(_context.PriceLists.Where(x => x.Delete == false), "ID", "Name");
                    ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
                    ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
      public async Task<IActionResult> Create(BusinessPartner partner)
        {
            ViewBag.style = "fa-users";
            ViewBag.Main = "Business Partner";
            ViewBag.Page = "List";
          
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                try
                {
                    var vendortype = _context.VendorTypes.FirstOrDefault(x => x.ID == partner.VendorType);
                    var country = _context.Countries.FirstOrDefault(x => x.ID == partner.Country);
                    partner.VendorTypeName= vendortype.Type;
                    partner.CountryName = country.Name;
                    await _partner.Add(partner);
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (partner.PriceListID == 0)
                {
                    ViewBag.pricelist = "Please select price list !";
                }
                if (partner.Type == "0")
                {
                    ViewBag.Errortype = "Please select type !";
                    return View(partner);
                }
            }
            ViewData["B"] = new SelectList(_context.PriceLists.Where(x => x.Delete == false), "ID", "Name");
            ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
            ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
            return View(partner);
        }
      
        public async Task<IActionResult> Update(int? id)
        {

            ViewBag.style = "fa-users";
            ViewBag.Main = "Business Partner";
            ViewBag.Page = "List";
           
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (id == null)
                {
                    return NotFound();
                }
                var bs = await _context.BusinessPartners.FindAsync(id);
                if (bs == null)
                {

                }
                if (bs.Delete == true)
                {
                    return NotFound();
                }
                ViewData["B"] = new SelectList(_context.PriceLists.Where(x => x.Delete == false), "ID", "Name");
                ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
                ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
                return View(bs);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A012");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }
                    var bs = await _context.BusinessPartners.FindAsync(id);
                    if (bs == null)
                    {

                    }
                    if (bs.Delete == true)
                    {
                        return NotFound();
                    }
                    ViewData["B"] = new SelectList(_context.PriceLists.Where(x => x.Delete == false), "ID", "Name");
                    ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
                    ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
                    return View(bs);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Update (int? id, BusinessPartner partner)
        {
            ViewBag.style = "fa-users";
            ViewBag.Main = "Business Partner";
           
            ViewBag.Page = "List";
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                try
                {
                    var vendortype = _context.VendorTypes.FirstOrDefault(x => x.ID == partner.VendorType);
                    var country = _context.Countries.FirstOrDefault(x => x.ID == partner.Country);
                    partner.VendorTypeName = vendortype.Type;
                    partner.CountryName = country.Name;

                    await _partner.Update(id,partner);
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                    ViewData["B"] = new SelectList(_context.PriceLists, "ID", "Name", partner.PriceListID);
                    return View(partner);
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (partner.PriceListID == 0)
                {
                    ViewBag.pricelist = "Please select price list !";
                }
                if (partner.Type == "0")
                {
                    ViewBag.Errortype = "Please select type !";
                   
                }
               
            }
            ViewData["B"] = new SelectList(_context.PriceLists.Where(x => x.Delete == false), "ID", "Name");
            ViewData["Type"] = new SelectList(_context.VendorTypes.Where(x => x.Delete == false), "ID", "Type");
            ViewData["Country"] = new SelectList(_context.Countries.Where(x => x.Delete == false), "ID", "Name");
            return View(partner);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            await _partner.Delete(id);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public IActionResult GetVendorType() {
            var list = _context.VendorTypes.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveVendorType(VendorType type)
        {
            if (type.ID == 0)
            {
                _context.VendorTypes.Add(type);
                _context.SaveChanges();
            }
            else
            {
                _context.VendorTypes.Update(type);
                _context.SaveChanges();
            }
           
            return Ok();
        }
        [HttpPost]
        public  IActionResult DeleteVendorType(int ID)
        {
            var type = _context.VendorTypes.FirstOrDefault(x => x.ID == ID);
            type.Delete = true;
            _context.VendorTypes.Update(type);
            _context.SaveChanges();

            return Ok();
        }
        [HttpGet]
        public IActionResult GetCountry()
        {
            var list = _context.Countries.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveCountry(Country country)
        {
            if (country.ID == 0)
            { 
                _context.Countries.Add(country);
                _context.SaveChanges();
            }
            else
            {
                _context.Countries.Update(country);
                _context.SaveChanges();
            }
           
            return Ok();
        }
        [HttpPost]
        public IActionResult DeleteCountry(int ID)
        {
            var country = _context.Countries.FirstOrDefault(x => x.ID == ID);
            country.Delete = true;
            _context.Countries.Update(country);
            _context.SaveChanges();
        
            return Ok();
        }
        [HttpGet]
        public IActionResult GetAutoCode() {
            var count = _context.BusinessPartners.Count() + 1;
            var list = "BP-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class ItemMasterDataController : Controller
    {
        private readonly DataContext _context;
        private readonly IItemMasterData _itemMasterData;
        private readonly IHostingEnvironment _appEnvironment;

        public ItemMasterDataController(DataContext context,IItemMasterData itemMasterData, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _itemMasterData = itemMasterData;
            _appEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index(string minpage = "10", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data ";
            ViewBag.Subpage = "List";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemMasterData = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            var data = _itemMasterData.GetItemMasterData().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["CodeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "code-desc" : "";
                ViewData["KhmerSortParm"] = sortOrder == "Khmer" ? "Khmer-desc" : "Khmer";
                ViewData["EnglishSortParm"] = sortOrder == "English" ? "English-desc" : "English";
                ViewData["CostSortParm"] = sortOrder == "Cost" ? "Cost-desc" : "Cost";
                ViewData["UnitSortParm"] = sortOrder == "Unit" ? "Unit-desc" : "Unit";
                ViewData["TypeSortParm"] = sortOrder == "Type" ? "Type-desc" : "Type";
                ViewData["BarcodeSortParm"] = sortOrder == "Barcode" ? "Barcode-desc" : "Barcode";
                ViewData["ItemGroup1SortParm"] = sortOrder == "ItemGroup1" ? "ItemGroup1-desc" : "ItemGroup1";
                ViewData["ItemGroup2SortParm"] = sortOrder == "ItemGroup2" ? "ItemGroup2-desc" : "ItemGroup2";
                ViewData["ItemGroup3SortParm"] = sortOrder == "ItemGroup3" ? "ItemGroup3-desc" : "ItemGroup3";
                ViewData["StockSortParm"] = sortOrder == "Stock" ? "Stock-desc" : "Stock";
                ViewData["InventoryUoMSortParm"] = sortOrder == "Inventory" ? "Inventory-desc" : "Inventory";
                ViewData["CurrencySortParm"] = sortOrder == "Currency" ? "Currency-desc" : "Currency";
                ViewData["PrintToSortParm"] = sortOrder == "PrintTo" ? "PrintTo-desc" : "PrintTo";
                ViewData["DescriptionSortParm"] = sortOrder == "Description" ? "Description-desc" : "Description";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Code.Contains(searchString) || s.KhmerName.Contains(searchString) ||
                    s.EnglishName.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString) ||
                    s.ItemGroup2.Name.Contains(searchString) || s.ItemGroup3.Name.Contains(searchString) ||
                    s.Barcode.Contains(searchString) || s.Type.Contains(searchString) || s.Description.Contains(searchString)
                    );

                }
                switch (sortOrder)
                {
                    case "code-desc":
                        Cate = Cate.OrderByDescending(s => s.Code);
                        break;
                    case "Khmer":
                        Cate = Cate.OrderBy(s => s.KhmerName);
                        break;
                    case "Khmer-desc":
                        Cate = Cate.OrderByDescending(s => s.KhmerName);
                        break;
                    case "English":
                        Cate = Cate.OrderBy(s => s.EnglishName);
                        break;
                    case "English-desc":
                        Cate = Cate.OrderByDescending(s => s.EnglishName);
                        break;
                    case "Cost":
                        Cate = Cate.OrderBy(s => s.Cost);
                        break;
                    case "Cost-desc":
                        Cate = Cate.OrderByDescending(s => s.Cost);
                        break;
                    case "Unit":
                        Cate = Cate.OrderBy(s => s.UnitPrice);
                        break;
                    case "Unit-desc":
                        Cate = Cate.OrderByDescending(s => s.UnitPrice);
                        break;
                    case "Type":
                        Cate = Cate.OrderBy(s => s.Type);
                        break;
                    case "Type-desc":
                        Cate = Cate.OrderByDescending(s => s.Type);
                        break;
                    case "Barcode":
                        Cate = Cate.OrderBy(s => s.Barcode);
                        break;
                    case "Barcode-desc":
                        Cate = Cate.OrderByDescending(s => s.Barcode);
                        break;
                    case "ItemGroup1":
                        Cate = Cate.OrderBy(s => s.ItemGroup1);
                        break;
                    case "ItemGroup1-desc":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup1);
                        break;
                    case "ItemGroup2":
                        Cate = Cate.OrderBy(s => s.ItemGroup2);
                        break;
                    case "ItemGroup2-desc":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup2);
                        break;
                    case "ItemGroup3":
                        Cate = Cate.OrderBy(s => s.ItemGroup3);
                        break;
                    case "ItemGroup3-desc":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup3);
                        break;
                    case "Stock":
                        Cate = Cate.OrderBy(s => s.StockIn);
                        break;
                    case "Stock-desc":
                        Cate = Cate.OrderByDescending(s => s.StockIn);
                        break;
                    case "Inventory":
                        Cate = Cate.OrderBy(s => s.InventoryUoMID);
                        break;
                    case "Inventory-desc":
                        Cate = Cate.OrderByDescending(s => s.InventoryUoMID);
                        break;
                    case "Currency":
                        Cate = Cate.OrderBy(s => s.PriceListID);
                        break;
                    case "Currency-desc":
                        Cate = Cate.OrderByDescending(s => s.PriceListID);
                        break;
                    case "PrintTo":
                        Cate = Cate.OrderBy(s => s.PrintToID);
                        break;
                    case "PrintTo-desc":
                        Cate = Cate.OrderByDescending(s => s.PrintToID);
                        break;

                    case "Description":
                        Cate = Cate.OrderBy(s => s.Description);
                        break;
                    case "Description-desc":
                        Cate = Cate.OrderByDescending(s => s.Description);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Code);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                return View(await Pagination<ItemMasterData>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["CodeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "code-desc" : "";
                    ViewData["KhmerSortParm"] = sortOrder == "Khmer" ? "Khmer-desc" : "Khmer";
                    ViewData["EnglishSortParm"] = sortOrder == "English" ? "English-desc" : "English";
                    ViewData["CostSortParm"] = sortOrder == "Cost" ? "Cost-desc" : "Cost";
                    ViewData["UnitSortParm"] = sortOrder == "Unit" ? "Unit-desc" : "Unit";
                    ViewData["TypeSortParm"] = sortOrder == "Type" ? "Type-desc" : "Type";
                    ViewData["BarcodeSortParm"] = sortOrder == "Barcode" ? "Barcode-desc" : "Barcode";
                    ViewData["ItemGroup1SortParm"] = sortOrder == "ItemGroup1" ? "ItemGroup1-desc" : "ItemGroup1";
                    ViewData["ItemGroup2SortParm"] = sortOrder == "ItemGroup2" ? "ItemGroup2-desc" : "ItemGroup2";
                    ViewData["ItemGroup3SortParm"] = sortOrder == "ItemGroup3" ? "ItemGroup3-desc" : "ItemGroup3";
                    ViewData["StockSortParm"] = sortOrder == "Stock" ? "Stock-desc" : "Stock";
                    ViewData["InventoryUoMSortParm"] = sortOrder == "Inventory" ? "Inventory-desc" : "Inventory";
                    ViewData["CurrencySortParm"] = sortOrder == "Currency" ? "Currency-desc" : "Currency";
                    ViewData["PrintToSortParm"] = sortOrder == "PrintTo" ? "PrintTo-desc" : "PrintTo";
                    ViewData["DescriptionSortParm"] = sortOrder == "Description" ? "Description-desc" : "Description";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Code.Contains(searchString) || s.KhmerName.Contains(searchString) ||
                        s.EnglishName.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString) ||
                        s.ItemGroup2.Name.Contains(searchString) || s.ItemGroup3.Name.Contains(searchString) ||
                        s.Barcode.Contains(searchString) || s.Type.Contains(searchString) || s.Description.Contains(searchString)
                        );

                    }
                    switch (sortOrder)
                    {
                        case "code-desc":
                            Cate = Cate.OrderByDescending(s => s.Code);
                            break;
                        case "Khmer":
                            Cate = Cate.OrderBy(s => s.KhmerName);
                            break;
                        case "Khmer-desc":
                            Cate = Cate.OrderByDescending(s => s.KhmerName);
                            break;
                        case "English":
                            Cate = Cate.OrderBy(s => s.EnglishName);
                            break;
                        case "English-desc":
                            Cate = Cate.OrderByDescending(s => s.EnglishName);
                            break;
                        case "Cost":
                            Cate = Cate.OrderBy(s => s.Cost);
                            break;
                        case "Cost-desc":
                            Cate = Cate.OrderByDescending(s => s.Cost);
                            break;
                        case "Unit":
                            Cate = Cate.OrderBy(s => s.UnitPrice);
                            break;
                        case "Unit-desc":
                            Cate = Cate.OrderByDescending(s => s.UnitPrice);
                            break;
                        case "Type":
                            Cate = Cate.OrderBy(s => s.Type);
                            break;
                        case "Type-desc":
                            Cate = Cate.OrderByDescending(s => s.Type);
                            break;
                        case "Barcode":
                            Cate = Cate.OrderBy(s => s.Barcode);
                            break;
                        case "Barcode-desc":
                            Cate = Cate.OrderByDescending(s => s.Barcode);
                            break;
                        case "ItemGroup1":
                            Cate = Cate.OrderBy(s => s.ItemGroup1);
                            break;
                        case "ItemGroup1-desc":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup1);
                            break;
                        case "ItemGroup2":
                            Cate = Cate.OrderBy(s => s.ItemGroup2);
                            break;
                        case "ItemGroup2-desc":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup2);
                            break;
                        case "ItemGroup3":
                            Cate = Cate.OrderBy(s => s.ItemGroup3);
                            break;
                        case "ItemGroup3-desc":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup3);
                            break;
                        case "Stock":
                            Cate = Cate.OrderBy(s => s.StockIn);
                            break;
                        case "Stock-desc":
                            Cate = Cate.OrderByDescending(s => s.StockIn);
                            break;
                        case "Inventory":
                            Cate = Cate.OrderBy(s => s.InventoryUoMID);
                            break;
                        case "Inventory-desc":
                            Cate = Cate.OrderByDescending(s => s.InventoryUoMID);
                            break;
                        case "Currency":
                            Cate = Cate.OrderBy(s => s.PriceListID);
                            break;
                        case "Currency-desc":
                            Cate = Cate.OrderByDescending(s => s.PriceListID);
                            break;
                        case "PrintTo":
                            Cate = Cate.OrderBy(s => s.PrintToID);
                            break;
                        case "PrintTo-desc":
                            Cate = Cate.OrderByDescending(s => s.PrintToID);
                            break;

                        case "Description":
                            Cate = Cate.OrderBy(s => s.Description);
                            break;
                        case "Description-desc":
                            Cate = Cate.OrderByDescending(s => s.Description);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Code);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<ItemMasterData>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        public IActionResult IndexGrid(string Filergroup=null,string SearchString=null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data ";
            ViewBag.Subpage = "Grid";
            ViewBag.Menu = "show";
            ViewData["CurrentFilter"] = SearchString;
           
            var data = _itemMasterData.GetMasterDatas();
            var Cate = from s in data select s;
            if (!String.IsNullOrEmpty(SearchString))
            {
               Cate = Cate.Where(s => s.Code.Contains(SearchString) || s.KhmerName.Contains(SearchString) || s.EnglishName.Contains(SearchString));
            }
            return View(Cate);
        }
        [HttpGet]
        public IActionResult GetMaster()
        {
           
            var list = _itemMasterData.GetMasterDatas().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupI()
        {
            var list = _context.ItemGroup1.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupII(int ID)
        {
            var list = _context.ItemGroup2.Where(x => x.Delete == false && x.ItemG1ID==ID && x.Name!="None").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupIII(int ID ,int Group1ID)
        {
            var list = _context.ItemGroup3.Where(x => x.Delete == false && x.ItemG1ID==Group1ID && x.ItemG2ID==ID && x.Name!="None").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup1(int ID)
        {            
            if (ID == 0)
            {
                var data = _itemMasterData.GetMasterDatas().Where(x => x.Delete == false).ToList();
                return Ok(data);
            }
            else
             {                
               var list=  _itemMasterData.GetMasterDatasByCategory(ID);
                return Ok(list);
            }      
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup2(int ID)
        {           
             var list = _itemMasterData.GetMasterDatas().Where(x=>x.ItemGroup2ID==ID);
             return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup3(int ID)
        {
            var list = _itemMasterData.GetMasterDatas().Where(x => x.ItemGroup3ID == ID);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult DetailItemMasterData(int ID)
        {
            var list = _itemMasterData.GetMasterDatas().Where(x => x.ID == ID).ToList();
            return Ok(list);
        }
        public IActionResult Create()
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name != "None"), "ItemG2ID", "Name");
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false & d.Name != "None"), "ID", "Name");
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name" );
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name!="None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false & d.Name!="None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                    ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");

                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,ManageExpire,SourceID,ManufacturerID,GroupCode")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var count = 0;
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select pricelist !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select tyle !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if(itemMasterData.WarehouseID==0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name", itemMasterData.ItemGroup1ID);
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name!="None"), "ItemG2ID", "Name", itemMasterData.ItemGroup2ID);
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.Name!="None"), "ID", "Name", itemMasterData.ItemGroup3ID);
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                return View(itemMasterData);
            }
            else
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemMasterData.Image = fileName;
                            }

                        }
                    }
                }

                await _itemMasterData.AddOrEdit(itemMasterData);
                //   await _itemMasterData.AddOrEdit(itemMasterData);
                var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                 join
                                pl in _context.PriceLists on com.PriceListID equals pl.ID
                                 select new Company
                                 {
                                     PriceList = new PriceLists
                                     {
                                         CurrencyID = pl.CurrencyID
                                     }
                                 };
                var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID && x.Delete == false).ToList();
                var SysCurrency = 0;
                foreach (var item in companycur)
                {
                    SysCurrency = item.PriceList.CurrencyID;
                }

                //Warenouse Summary
                if(itemMasterData.Process!="Standard")
                {
                    WarehouseSummary warehouseSummary = new WarehouseSummary
                    {
                        WarehouseID = itemMasterData.WarehouseID,
                        ItemID = itemMasterData.ID,
                        InStock = 0,
                        ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                        SyetemDate = Convert.ToDateTime(DateTime.Today),
                        UserID = int.Parse(User.FindFirst("UserID").Value),
                        TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                        CurrencyID = SysCurrency,
                        UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                        Cost = 0,
                        Available = 0,
                        Committed = 0,
                        Ordered = 0

                    };
                    await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                }
               
                foreach (var item in defiendUom)
                {
                    //Standard
                    if (itemMasterData.Process == "Standard")
                    {
                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }

                    }
                    //FIFO, Average
                    else
                    {

                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);

                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0


                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        //Insert to warehoues detail

                        WarehouseDetail warehouseDetail = new WarehouseDetail
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = item.AltUOM,
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                    }

                }
                return RedirectToAction(nameof(Index));
            }  
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemMasterData = _itemMasterData.GetbyId(id);
                if (itemMasterData.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemMasterData == null)
                {
                    return NotFound();
                }
                ViewBag.Code = itemMasterData.Code;
                ViewBag.Barcode = itemMasterData.Barcode;
                var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                if (check > 0)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName!="None"), "ManuID", "ManuName");
                    ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                    ViewBag.Check = "Yes";
                    ViewBag.Type = itemMasterData.Type;
                    ViewBag.Process = itemMasterData.Process;
                    return View(itemMasterData);
                }
                else
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                    ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                    return View(itemMasterData);
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var itemMasterData = _itemMasterData.GetbyId(id);
                    if (itemMasterData.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (itemMasterData == null)
                    {
                        return NotFound();
                    }
                    ViewBag.Code = itemMasterData.Code;
                    ViewBag.Barcode = itemMasterData.Barcode;
                    var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                    if (check > 0)
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                        ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                        ViewBag.Check = "Yes";
                        ViewBag.Type = itemMasterData.Type;
                        ViewBag.Process = itemMasterData.Process;
                        return View(itemMasterData);
                    }
                    else
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID==itemMasterData.ItemGroup1ID && d.Name !="None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID==itemMasterData.ItemGroup1ID && d.ItemG2ID==itemMasterData.ItemGroup2ID && d.Name!="None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                        return View(itemMasterData);
                    }

                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }   
        }
        [HttpGet]
        public IActionResult EditGrid(int id)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemMasterData = _itemMasterData.GetbyId(id);
                if (itemMasterData.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemMasterData == null)
                {
                    return NotFound();
                }
                ViewBag.Code = itemMasterData.Code;
                ViewBag.Barcode = itemMasterData.Barcode;
                var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                if (check > 0)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                    ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                    ViewBag.Check = "Yes";
                    ViewBag.Type = itemMasterData.Type;
                    ViewBag.Process = itemMasterData.Process;
                    return View(itemMasterData);
                }
                else
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                    ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                    return View(itemMasterData);
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var itemMasterData = _itemMasterData.GetbyId(id);
                    if (itemMasterData.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (itemMasterData == null)
                    {
                        return NotFound();
                    }
                    ViewBag.Code = itemMasterData.Code;
                    ViewBag.Barcode = itemMasterData.Barcode;
                    var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                    if (check > 0)
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID==Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                        ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                        ViewBag.Check = "Yes";
                        ViewBag.Type = itemMasterData.Type;
                        ViewBag.Process = itemMasterData.Process;
                        return View(itemMasterData);
                    }
                    else
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID==Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                        ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                        return View(itemMasterData);
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,Photo,ManageExpire,SourceID,ManufacturerID,GroupCode")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var count = 0;
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select pricelist !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select tyle !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if (itemMasterData.WarehouseID == 0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                return View(itemMasterData);
            }
            else
            {
                var CheckItemProcess = _context.InventoryAudits.Where(x => x.ItemID == itemMasterData.ID).Count();
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemMasterData.Image = fileName;
                            }

                        }
                    }
                }
                await _itemMasterData.AddOrEdit(itemMasterData);

                if (CheckItemProcess <= 0)
                {

                    //Delete item in warehouse detail and price list detail
                    _itemMasterData.RemoveItmeInWarehous(itemMasterData.ID);

                    var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                    var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                     join
                                        pl in _context.PriceLists on com.PriceListID equals pl.ID
                                     select new Company
                                     {
                                         PriceList = new PriceLists
                                         {
                                             CurrencyID = pl.CurrencyID
                                         }
                                     };
                    var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID).ToList();
                    var SysCurrency = 0;
                    foreach (var item in companycur)
                    {
                        SysCurrency = item.PriceList.CurrencyID;
                    }
                    //Warenouse Summary
                    if (itemMasterData.Process != "Standard")
                    {
                        WarehouseSummary warehouseSummary = new WarehouseSummary
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                    }
                    foreach (var item in defiendUom)
                    {

                        //Standard
                        if (itemMasterData.Process == "Standard")
                        {
                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }

                        }
                        //FIFO, Average
                        else
                        {

                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);

                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0


                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            //Insert to warehoues detail

                            WarehouseDetail warehouseDetail = new WarehouseDetail
                            {
                                WarehouseID = itemMasterData.WarehouseID,
                                ItemID = itemMasterData.ID,
                                InStock = 0,
                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SyetemDate = Convert.ToDateTime(DateTime.Today),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                CurrencyID = SysCurrency,
                                UomID = item.AltUOM,
                                Cost = 0,
                                Available = 0,
                                Committed = 0,
                                Ordered = 0

                            };
                            await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                        }

                    }
                    return RedirectToAction(nameof(Index));
                }
                return RedirectToAction(nameof(Index));
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditGrid(int id, [Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,ManageExpire,SourceID,ManufacturerID,GroupCode")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var count = 0;
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select pricelist !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select tyle !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if (itemMasterData.WarehouseID == 0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                ViewData["ManuID"] = new SelectList(_context.Manufacturers.Where(d => d.Delete == false && d.ManuName != "None"), "ManuID", "ManuName");
                ViewData["SourceID"] = new SelectList(_context.Sources.Where(d => d.Delete == false && d.SouName != "None"), "SouID", "SouName");
                return View(itemMasterData);
            }
            else
            {
                var CheckItemProcess = _context.InventoryAudits.Where(x => x.ItemID == itemMasterData.ID).Count();
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemMasterData.Image = fileName;
                            }

                        }
                    }
                }
                await _itemMasterData.AddOrEdit(itemMasterData);

                if (CheckItemProcess <= 0)
                {

                    //Delete item in warehouse detail and price list detail
                    _itemMasterData.RemoveItmeInWarehous(itemMasterData.ID);

                    var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                    var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                     join
                                        pl in _context.PriceLists on com.PriceListID equals pl.ID
                                     select new Company
                                     {
                                         PriceList = new PriceLists
                                         {
                                             CurrencyID = pl.CurrencyID
                                         }
                                     };
                    var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID).ToList();
                    var SysCurrency = 0;
                    foreach (var item in companycur)
                    {
                        SysCurrency = item.PriceList.CurrencyID;
                    }
                    //Warenouse Summary
                    if (itemMasterData.Process != "Standard")
                    {
                        WarehouseSummary warehouseSummary = new WarehouseSummary
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                    }
                    foreach (var item in defiendUom)
                    {
                        //Standard
                        if (itemMasterData.Process == "Standard")
                        {
                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }

                        }
                        //FIFO, Average
                        else
                        {

                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);

                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0


                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            //Insert to warehoues detail

                            WarehouseDetail warehouseDetail = new WarehouseDetail
                            {
                                WarehouseID = itemMasterData.WarehouseID,
                                ItemID = itemMasterData.ID,
                                InStock = 0,
                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SyetemDate = Convert.ToDateTime(DateTime.Today),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                CurrencyID = SysCurrency,
                                UomID = item.AltUOM,
                                Cost = 0,
                                Available = 0,
                                Committed = 0,
                                Ordered = 0

                            };
                            await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                        }

                    }
                    return RedirectToAction(nameof(IndexGrid));
                }
                return RedirectToAction(nameof(IndexGrid));
            }
        }
        [HttpGet]
        public IActionResult GetPrinter()
        {
            var list = _itemMasterData.GetPrinter.Where(p => p.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCodeAuto()
        {
            var count = _context.ItemMasterDatas.Count() + 1;
            var list = count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        //[HttpGet]
        //public IActionResult FindeCode(string code)
        //{
        //    var codes = _context.ItemMasterDatas.Where(c => c.Code == code  && c.Delete==false);
        //    return Ok(codes);
        //}
        [HttpDelete]
        public async Task<IActionResult> DeleteItemMaster(int ID)
        {
           await _itemMasterData.DeleteItemMaster(ID);
            return Ok();
        }
        //[HttpGet] 
        //public IActionResult FindbarCode(string barcode)
        //{
        //    var bar = _context.ItemMasterDatas.Where(c => c.Barcode == barcode && c.Delete==false);
        //    return Ok(bar);
        //}
        [HttpPost]
        public IActionResult DeletItemViewGrid(int ID)
        {
             _itemMasterData.DeleteItemMaster(ID);
            var list = _itemMasterData.GetMasterDatas();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult CheckTransactionItem(int ItemID)
        {
            var check = _context.InventoryAudits.Where(x => x.ItemID == ItemID).Count();
            if (check > 0)
            {
                return Ok("Y");
            }
            else
            {
                return Ok("N");
            }
        }
        [HttpGet]
        public IActionResult GetManufacturer()
        {
            var list = _context.Manufacturers.Where(x => x.Delete == false && x.ManuName != "None").ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveManufacturer(Manufacturer Manu)
        {
            if (Manu.ManuID == 0)
            {
                _context.Manufacturers.Add(Manu);
                _context.SaveChanges();
            }
            else
            {
                _context.Manufacturers.Update(Manu);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteManufacturer(int ManuID)
        {
            var data= _context.Manufacturers.FirstOrDefault(x=>x.ManuID==ManuID);
            if (data != null)
            {
                data.Delete = true;
                _context.Manufacturers.Update(data);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpPost]
        public IActionResult Savesource(Source source)
        {
            if (source.SouID == 0)
            {
                _context.Sources.Add(source);
                _context.SaveChanges();
            }
            else
            {
                _context.Sources.Update(source);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteSource(int SourceID)
        {
            var source = _context.Sources.FirstOrDefault(x => x.SouID == SourceID);
            if (source != null)
            {
                source.Delete = true;
                _context.Sources.Update(source);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpGet]
        public IActionResult GetSource()
        {
            var list = _context.Sources.Where(x => x.Delete == false && x.SouName != "None").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetGroupCode()
        {
            var list = _context.ItemGroupCodes.ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveGroupCode(ItemGroupCode code)
        {
            if (code.ID == 0)
            {
                _context.ItemGroupCodes.Add(code);
                _context.SaveChanges();
            }
            else
            {
                _context.ItemGroupCodes.Update(code);
                _context.SaveChanges();
            }
            var list = _context.ItemGroupCodes.ToList();
            return Ok(list);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;
using Microsoft.AspNetCore.Authorization;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PointController : Controller
    {
        private readonly DataContext _context;
        private readonly IPromotion _promotion;
        private readonly IItemMasterData _itemMasterdata;
        private readonly IExchangeRate _exchangreate;
        public PointController(DataContext context,IPromotion promotion, IItemMasterData itemMasterData, IExchangeRate exchangreate)
        {
            _context = context;
            _promotion = promotion;
            _itemMasterdata = itemMasterData;
            _exchangreate = exchangreate;
        }
        
        public IActionResult point()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Point";
            ViewBag.Subpage = "Create Point";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code =="A030");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        private bool PointExists(int id)
        {
            return _context.Points.Any(e => e.ID == id);
        }
        [HttpGet]
        public IActionResult GetPointItemMaster()
        {
            var list = _promotion.ServicePointDetails.ToList();
            return Ok(list);
        }
       [HttpPost]
       public  IActionResult  InsertPoint(PointService servicedata)
       {
            _promotion.AddorEditPoint(servicedata);
            var list = _promotion.GetPoints().Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPointDetail(int ID)
        {
            var list = _promotion.GetPointDetails().Where(x => x.PointID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPoint()
        {
            var list = _promotion.GetPoints().Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpPost]
        public  IActionResult DeletePointDetail(int ID)
        {
            _promotion.DeletePointDelail(ID);
            return Ok();
        }
        [HttpPost]
        public async Task<IActionResult> DeletePoint(int ID)
        {
           await _promotion.DeletePoint(ID);
            var list = _promotion.GetPoints().Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBasecurrency()
        {
           var list =_exchangreate.GetBaseCurrencyName().ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult CheckSet_Point(int Check)
        {
            var list = _context.Points.Where(x => x.Quantity == Check && x.Delete == false);
            return Ok(list);
        }
    }
} 

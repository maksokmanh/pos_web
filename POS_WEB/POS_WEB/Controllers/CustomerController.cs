﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Customer;
using POS_WEB.Models.Services.Appointment;
using POS_WEB.Models.Services.HumanResources;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly DataContext _context;
        public CustomerController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Customer(int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Page = "Register Motocycle Customer";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "show";
            var brands = _context.InterestingBrands.Where(ib => ib.Active).ToList();
            var models = _context.InterestingModels.Where(im => im.Active).ToList();
            var yamaha_branches = _context.YamahaBranches.Where(ym => ym.Active).ToList();
            var colors = _context.ColorCustomers.Where(c => c.Active).ToList();
            var regions = _context.Regions.ToList();
            var sale_consultants = _context.Employees.Where(e => !e.Deleted).ToList();
            CustomerViewModel cvm = new CustomerViewModel
            {
                Customer = new Customer
                {
                    BirthDate = DateTime.Now.ToString("yyyy-MM-dd"),
                },
                InterestingBrands = brands ?? new List<InterestingBrand>(),
                InterestingModels = models ?? new List<InterestingModel>(),
                YamahaBranches = yamaha_branches ?? new List<YamahaBranch>(),
                ColorCustomers = colors ?? new List<ColorCustomer>(),
                Regions = regions ?? new List<Region>(),
                Districts = _context.Districts.Where(d => d.DistrictID == 1)
            };

            if (customerID != null)
            {
                cvm.Customer = _context.Customers.Find(customerID);
            }
            return View(cvm);
        }

        public IActionResult MotocycleCustomer(int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "Register Motocycle Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "highlight";
            ViewBag.PurchaseExpand = "hidden";
            ViewBag.Complete = "selected";
            ViewBag.Current = "current";
            var brands = _context.InterestingBrands.Where(ib => ib.Active).ToList();
            var models = _context.InterestingModels.Where(im => im.Active).ToList();
            var yamaha_branches = _context.YamahaBranches.Where(ym => ym.Active).ToList();
            var colors = _context.ColorCustomers.Where(c => c.Active).ToList();
            var regions = _context.Regions.ToList();
            var sale_consultant = new Employee();
            var customer_groups = _context.CustomerGroups.Where(cg => cg.Active).ToList();
            var warranties = _context.WarrantyTypes.Where(w => w.Active).ToList();
            var sources = _context.SourceOfInfos.Where(s => s.Active).ToList();

            var appointment = new CustomerAppointment
            {
                AppointmentCode = "AP-" + (_context.CustomerAppointments.Count() + 1).ToString().PadLeft(7, '0'),
                ActivityDate = DateTime.Now.ToString("yyyy-MM-dd"),
                ActivityTime = DateTime.Now.ToString("hh:mm tt")
            };

            CustomerViewModel cvm = new CustomerViewModel
            {
                Customer = new Customer {
                    Code = "MC-" + (_context.Customers.Count() + 1).ToString().PadLeft(7, '0'),
                    BirthDate = DateTime.Now.ToString("dd-MM-yyyy"),
                    StartDate = DateTime.Now.ToString("dd-MM-yyyy"),
                    EndDate = DateTime.Now.ToString("dd-MM-yyyy")
                },

                Appointment = appointment,
                SaleConsultant = sale_consultant,
                CustomerGroups = customer_groups ?? new List<CustomerGroup>(),
                InterestingBrands = brands ?? new List<InterestingBrand>(),
                InterestingModels = models ?? new List<InterestingModel>(),
                YamahaBranches = yamaha_branches ?? new List<YamahaBranch>(),
                SourceOfInfos = sources?? new List<SourceOfInfo>(),
                WarrantyTypes = warranties ?? new List<WarrantyType>(),
                ColorCustomers = colors ?? new List<ColorCustomer>(),
                Regions = regions ?? new List<Region>(),
                Districts = new List<District>()
            };

            if(customerID != null)
            {
                ViewBag.Complete = "selected";
                cvm.Customer = _context.Customers.Find(customerID);
                cvm.Districts = _context.Districts.Where(d => d.DistrictID == cvm.Customer.District);
                cvm.SaleConsultant = _context.Employees.Find(cvm.Customer.SaleConsultant);
                if(_context.CustomerAppointments.Any(a => a.CustomerID == customerID))
                {
                    cvm.Customer.BirthDate = cvm.Customer.BirthDate == null? "" : DateTime.Parse(cvm.Customer.BirthDate).ToString("dd-MM-yyyy");
                    cvm.Customer.StartDate = cvm.Customer.StartDate == null ? "" : DateTime.Parse(cvm.Customer.StartDate).ToString("dd-MM-yyyy");
                    cvm.Customer.EndDate = cvm.Customer.EndDate == null ? "" : DateTime.Parse(cvm.Customer.EndDate).ToString("dd-MM-yyyy");
                    ViewBag.HideWizzard = "hidden";
                }

                if (cvm.Customer.CustomerType == CustomerType.PURCHASE)
                {
                    ViewBag.PurchaseExpand = "";
                }
            }
            return View(cvm);
        }

        public IActionResult MotocycleCustomerUpdate(int customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "Edit Motocycle Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "highlight";
            ViewBag.PurchaseExpand = "hidden";
            ViewBag.Complete = "selected";
            ViewBag.Current = "current";
            var brands = _context.InterestingBrands.Where(ib => ib.Active).ToList();
            var models = _context.InterestingModels.Where(im => im.Active).ToList();
            var yamaha_branches = _context.YamahaBranches.Where(ym => ym.Active).ToList();
            var colors = _context.ColorCustomers.Where(c => c.Active).ToList();
            var regions = _context.Regions.ToList();
            var sale_consultant = new Employee();
            var customer_groups = _context.CustomerGroups.Where(cg => cg.Active).ToList();
            var warranties = _context.WarrantyTypes.Where(w => w.Active).ToList();
            var sources = _context.SourceOfInfos.Where(s => s.Active).ToList();

            CustomerViewModel cvm = new CustomerViewModel
            {
                Customer = new Customer
                {
                    Code = "MC-" + (_context.Customers.Count() + 1).ToString().PadLeft(7, '0'),
                    BirthDate = DateTime.Now.ToString("dd-MM-yyyy"),
                    StartDate = DateTime.Now.ToString("dd-MM-yyyy"),
                    EndDate = DateTime.Now.ToString("dd-MM-yyyy")
                },
                SaleConsultant = sale_consultant,
                CustomerGroups = customer_groups ?? new List<CustomerGroup>(),
                InterestingBrands = brands ?? new List<InterestingBrand>(),
                InterestingModels = models ?? new List<InterestingModel>(),
                YamahaBranches = yamaha_branches ?? new List<YamahaBranch>(),
                SourceOfInfos = sources ?? new List<SourceOfInfo>(),
                WarrantyTypes = warranties ?? new List<WarrantyType>(),
                ColorCustomers = colors ?? new List<ColorCustomer>(),
                Regions = regions ?? new List<Region>(),
                Districts = new List<District>()
            };

            cvm.Customer = _context.Customers.Find(customerID);
            if(cvm.Customer != null)
            {
                cvm.Districts = _context.Districts.Where(d => d.DistrictID == cvm.Customer.District);
                cvm.SaleConsultant = _context.Employees.Find(cvm.Customer.SaleConsultant);
                cvm.Customer.BirthDate = cvm.Customer.BirthDate == null ? "" : DateTime.Parse(cvm.Customer.BirthDate).ToString("dd-MM-yyyy");
                cvm.Customer.StartDate = cvm.Customer.StartDate == null ? "" : DateTime.Parse(cvm.Customer.StartDate).ToString("dd-MM-yyyy");
                cvm.Customer.EndDate = cvm.Customer.EndDate == null ? "" : DateTime.Parse(cvm.Customer.EndDate).ToString("dd-MM-yyyy");

                if (cvm.Customer.CustomerType == CustomerType.PURCHASE)
                {
                    ViewBag.PurchaseExpand = "";
                }
            }
            return View(cvm);
        }

        public IActionResult MotocycleCustomerDetail(int? customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Detail Service Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.ServiceCustomer = "show";
            ViewBag.ServiceSearch = "highlight";
            Customer c = _context.Customers.Find(customerID);
            if (c != null)
            {
                return View(new CustomerDetail
                {
                    CustomerID = c.CustomerID,
                    Code = c.Code,
                    Name = c.Name,
                    CustomerType = c.CustomerType,
                    CustomerGroup = c.CustomerGroup == 0 ? "(No group yet.)" : _context.CustomerGroups.Find(c.CustomerGroup).GroupName,
                    Gender = c.Gender,
                    BirthDate = string.IsNullOrEmpty(c.BirthDate) ? "(No birth-date yet.)" : DateTime.Parse(c.BirthDate).ToString("dd/MM/yyyy"),
                    Phone = c.Phone,
                    Email = c.Email?? "(No email yet.)",
                    Address = c.Address ?? "(No address yet.)",
                    Plate = c.Plate,
                    Frame = c.Frame,
                    Engine = c.Engine,
                    Region = c.Region == 0 ? "(No region yet.)" : _context.Regions.FirstOrDefault(r => r.RegionID == c.Region).Name,
                    District = c.District == 0 ? "(No district yet.)" : _context.Districts.FirstOrDefault(d => d.DistrictID == c.District).Name,
                    PaymentType = (PaymentType)c.PaymentType,
                    SaleConsultant = c.SaleConsultant == 0 ? "(No sale consultant yet.)" : _context.Employees.FirstOrDefault(sc => sc.ID == c.SaleConsultant).Name,
                    YamahaBranch = c.YamahaBranch == 0 ? "(No YAMAHA branch yet.)" : _context.YamahaBranches.FirstOrDefault(ym => ym.YmBranchID == c.YamahaBranch).Name,
                    SourceOfInfo = c.SourceOfInfo == 0 ? "(No source of info yet.)" : _context.SourceOfInfos.FirstOrDefault(sr => sr.SourceID == c.SourceOfInfo).SourceName,
                    Warranty = c.Warranty == 0 ? "(No warranty yet.)" : _context.WarrantyTypes.FirstOrDefault(w => w.WarrantyID == c.Warranty).WarrantyName
                });
            }

            return View(new CustomerDetail());
        }

        public IActionResult MotocycleAppointmentFollowUp(int appointmentID, int customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Appointment";
            ViewBag.Subpage = "Appointment Follow Up";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleAppointment = "highlight";
            AppointmentViewModel avm = new AppointmentViewModel
            {
                Appointment = new CustomerAppointment
                {
                    AppointmentCode = "AP-" + (_context.CustomerAppointments.Count() + 1).ToString().PadLeft(7, '0'),
                    ActivityDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    ActivityTime = DateTime.Now.ToString("hh:mm tt"),
                    CustomerID = customerID
                }
            };
          
            avm.Appointment = _context.CustomerAppointments.FirstOrDefault(a => a.AppointmentID == appointmentID && a.CustomerID == customerID);
            if(!IsTimely(avm.Appointment.ActivityDate, avm.Appointment.ActivityTime, avm.Appointment.Reminder))
            {
                return NotFound();
            }
            return View(avm);
        }  

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateMotocycleCustomer(Customer customer, bool onlyValidate = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "Register Motocycle Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "highlight";
            ModelMessage msg = new ModelMessage(ModelState);
            if (customer.CustomerType == CustomerType.WALKIN)
            {
                ValidateWalkinCustomer(customer);
            }

            if (customer.CustomerType == CustomerType.PURCHASE)
            {                             
                ValidatePurchaseCustomer(customer);
            }

            if (ModelState.IsValid)
            {               
                if (onlyValidate)
                {
                    msg.Action = ModelAction.CONFIRM;
                } else
                {
                    _context.Customers.Update(customer);
                    _context.SaveChanges();
                    msg.Redirect = "/Customer/MotocycleCustomerList";                
                }
               
            }

            return Ok(new { Model = msg.Bind(ModelState), Data = customer });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RegisterMotocycleCustomer(Customer customer, CustomerAppointment appointment)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            var _history = new AppointmentHistory
            {
                CustomerID = appointment.CustomerID,
                AppointmentCode = appointment.AppointmentCode,
                Remark = appointment.Remark,
                Reminder = appointment.Reminder,
                Location = appointment.Location,
                Priority = appointment.Priority
            };

            if (DateTime.TryParse(string.Format("{0} {1}", appointment.ActivityDate, appointment.ActivityTime), out DateTime _followUp))
            {
                if (IsTimely(appointment.ActivityDate, appointment.ActivityTime, appointment.Reminder))
                {
                    ModelState.AddModelError("appointment.FollowUp", "Next follow-up datetime must be later from now.");
                }

                _history.FollowUp = _followUp;
            }
            else
            {
                ModelState.AddModelError("appointment.FollowUp", "Invalid followup date or time.");
            }

            if (ModelState.IsValid)
            {
                using (var dbTransaction = _context.Database.BeginTransaction())
                {
                    _context.Customers.Update(customer);
                    _context.SaveChanges();

                    appointment.CustomerID = customer.CustomerID;
                    _context.CustomerAppointments.Update(appointment);
                    _context.SaveChanges();
                    _history.AppointmentID = appointment.AppointmentID;
                    _history.CustomerID = appointment.CustomerID;
                    _context.AppointmentHistories.Add(_history);
                    _context.SaveChanges();

                    dbTransaction.Commit();
                    msg.Add("customer.Success", "The customer has been registered successfully.");
                    msg.Action = ModelAction.CONFIRM;
                    return Ok(msg.Bind(ModelState));
                }
            }

            return Ok(msg.Bind(ModelState));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveMotocycleAppointment(CustomerAppointment appointment)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            var _history = new AppointmentHistory
            {
                CustomerID = appointment.CustomerID,
                AppointmentCode = appointment.AppointmentCode,
                Remark = appointment.Remark,
                Reminder = appointment.Reminder,
                Location = appointment.Location,
                Priority = appointment.Priority
            };

            if (DateTime.TryParse(string.Format("{0} {1}", appointment.ActivityDate, appointment.ActivityTime), out DateTime _followUp))
            {
                if (IsTimely(appointment.ActivityDate, appointment.ActivityTime, appointment.Reminder))
                {
                    ModelState.AddModelError("appointment.FollowUp", "Next follow-up datetime must be later from now.");
                }

                _history.FollowUp = _followUp;
            }
            else
            {
                ModelState.AddModelError("appointment.FollowUp", "Invalid followup date or time.");
            }


            if (ModelState.IsValid)
            {
                using (var dbTransaction = _context.Database.BeginTransaction())
                {
                    _context.CustomerAppointments.Update(appointment);
                    _context.SaveChanges();
                    _history.AppointmentID = appointment.AppointmentID;
                    _history.CustomerID = appointment.CustomerID;
                    _context.AppointmentHistories.Add(_history);
                    _context.SaveChanges();

                    dbTransaction.Commit();
                    msg.Add("appointment.Success", "Next follow-up appointment has been saved.");
                    msg.Action = ModelAction.CONFIRM;
                    return Ok(msg.Bind(ModelState));
                }
            }

            return Ok(msg.Bind(ModelState));
        }

        private void ValidateWalkinCustomer(Customer customer)
        {
            if (!string.IsNullOrEmpty(customer.Name))
            {
                if (_context.Customers.Any(c => c.Name.Replace(" ", "").ToLower() == customer.Name.Replace(" ", "")))
                {
                    ModelState.AddModelError("customer.Name", "The customer's name is already existed.");
                }          
            }

            if (customer.PaymentType == 0)
            {
                ModelState.AddModelError("customer.PaymentType", "Choose any payment method.");
            }

            if (customer.BirthDate != null)
            {
                if (GetValidDate(customer.BirthDate, "-") == "")
                {
                    ModelState.AddModelError("customer.BirthDate", "Date of Birth is invalid.");
                }
                else
                {
                    customer.BirthDate = GetValidDate(customer.BirthDate, "-");
                }
            }

        }

        private void ValidatePurchaseCustomer(Customer customer)
        {
            if (!string.IsNullOrEmpty(customer.Name))
            {
                if (_context.Customers.Any(c => c.Name.Replace(" ", "").ToLower() == customer.Name.Replace(" ", "")))
                {
                    ModelState.AddModelError("customer.Name", "The customer's name is already existed.");
                }
            }

            if (customer.PaymentType == 0)
            {
                ModelState.AddModelError("customer.PaymentType", "Choose any payment method.");
            }

            if (customer.Plate == null)
            {
                ModelState.AddModelError("customer.Plate", "Plate is required.");
            }
            if (customer.Frame == null)
            {
                ModelState.AddModelError("customer.Frame", "Frame is required.");
            }

            if (customer.Engine == null)
            {
                ModelState.AddModelError("customer.Engine", "Engine is required.");
            }

            if (customer.SaleConsultant == 0)
            {
                ModelState.AddModelError("customer.SaleConsultant", "Sale consultant is required.");
            }

            if (customer.StartDate != null)
            {
                if (GetValidDate(customer.StartDate, "-") == "")
                {
                    ModelState.AddModelError("customer.StartDate", "Start Date is invalid.");
                }
                else
                {
                    customer.StartDate = GetValidDate(customer.StartDate, "-");
                }

            } else
            {
                ModelState.AddModelError("customer.StartDate", "Start Date is required.");
            }

            if (customer.EndDate != null)
            {
                if (GetValidDate(customer.EndDate, "-") == "")
                {
                    ModelState.AddModelError("customer.EndDate", "End Date is invalid.");
                }
                else
                {                           
                    customer.EndDate = GetValidDate(customer.EndDate, "-");
                    if (!CanFollowUp(customer.StartDate, customer.EndDate))
                    {
                        ModelState.AddModelError("customer.EndDate", "End Date must be after Start Date.");
                    }
                }

            }
            else
            {
                ModelState.AddModelError("customer.EndDate", "End Date is required.");
            }
                      

            if (customer.BirthDate != null)
            {
                if (GetValidDate(customer.BirthDate, "-") == "")
                {
                    ModelState.AddModelError("customer.BirthDate", "Date of Birth is invalid.");
                }
                else
                {
                    customer.BirthDate = GetValidDate(customer.BirthDate, "-");
                }

            }

        }

        private bool CanFollowUp(string start, string end)
        {
            if (DateTime.TryParse(start, out DateTime _start) && DateTime.TryParse(end, out DateTime _end))
            {
                return start.CompareTo(end) < 0;
            }
            return false;
        }

        [HttpPost]
        public IActionResult DeleteCustomer(int CusID)
        {
            var delete = _context.Customers.FirstOrDefault(x => x.CustomerID == CusID);
            delete.Deleted = true;
            _context.Customers.Update(delete);
            _context.SaveChanges();
            var list = _context.Customers.Where(x => x.Deleted == false).ToList();
            return Ok(list);
        }    

        public IActionResult MotocycleCustomerList()
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "List of motocycle customers";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "highlight";
            
            if (!_context.Customers.Any(c => !c.Deleted))
            {
                return View(new List<Customer>());
            }
            return View(_context.Customers.Where(c => c.CustomerType != CustomerType.SERVICE && !c.Deleted).Take(10));
        }

        public IActionResult SearchMotocycleCustomers(string word = "", bool forServiceFollowUp = false, int filterBy = 0)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "List of motocycle customers";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleSearch = "highlight";

            var motocycle_customers = (from mc in _context.Customers.Where(c => c.CustomerType != CustomerType.SERVICE)
                                     where !mc.Deleted
                                     select new
                                     {
                                         mc.CustomerID,
                                         mc.Code,
                                         mc.CustomerType,
                                         mc.Name,
                                         Gender = mc.Gender == CustomerGender.Female ? "Female" : "Male",
                                         mc.Phone,
                                         mc.Address,                                       
                                         Plate = mc.Plate?? "",
                                         mc.Color,
                                         Frame = mc.Frame?? "",
                                         Engine = mc.Engine?? ""
                                     }).AsQueryable();
            if (forServiceFollowUp)
            {
                motocycle_customers = motocycle_customers.Where(c => _context.CustomerAppointments.Any(a => a.CustomerID == c.CustomerID 
                                      && IsTimely(a.ActivityDate, a.ActivityTime, a.Reminder)));
            }

            switch (filterBy)
            {
                case 1:
                    motocycle_customers = motocycle_customers.Where(c => c.CustomerType == CustomerType.WALKIN);
                    break;
                case 2:
                    motocycle_customers = motocycle_customers.Where(c => c.CustomerType == CustomerType.PURCHASE);
                    break;
            }

            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                motocycle_customers = motocycle_customers.Where(sc =>
                    sc.Name.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Code.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Phone.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Address.Replace(" ", string.Empty).ToLower().Contains(word)             
                   | sc.Plate.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Frame.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Engine.Replace(" ", string.Empty).ToLower().Contains(word)
                );

                return Ok(motocycle_customers);
            }

            return Ok(motocycle_customers.Take(10));

        }
        
        //Yamaha Branches
        public IActionResult GetYamahaBranches()
        {     
            return Ok(_context.YamahaBranches);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateYamahaBranches(IEnumerable<YamahaBranch> data)
        {
            var items = data.GroupBy(s => s.Name.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
           
            if (ModelState.IsValid)
            {
                _context.YamahaBranches.UpdateRange(data);
                _context.SaveChanges();
            }
            

            return Ok(new
            {
                Data = _context.YamahaBranches.Where(a => a.Active == true),
                Errors = errors
            });
        }

        //Interesting brand
        public IActionResult GetInterestingBrands()
        {
            return Ok(_context.InterestingBrands);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInterestingBrands(IEnumerable<InterestingBrand> data)
        {
            var items = data.GroupBy(s => s.BrandName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }

            if (ModelState.IsValid)
            {
                _context.UpdateRange(data);
                _context.SaveChanges();
            }

            return Ok(new
            {
                Data = _context.InterestingBrands.Where(a => a.Active == true),
                Errors = errors.Bind(ModelState)
            });
        }


        //Interesting model
        public IActionResult GetInterestingModels()
        {
            return Ok(_context.InterestingModels);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInterestingModels(IEnumerable<InterestingModel> data)
        {
            var items = data.GroupBy(s => s.ModelName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
       
            if (ModelState.IsValid)
            {
                _context.InterestingModels.UpdateRange(data);
                _context.SaveChanges();
            }
            
            return Ok(new
            {
                Data = _context.InterestingModels.Where(a => a.Active == true),
                Errors = errors.Bind(ModelState)
            });
        }

        //Source of Info
        public IActionResult GetSourceOfInfos()
        {
            return Ok(_context.SourceOfInfos);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateSourceOfInfos(IEnumerable<SourceOfInfo> data)
        {
            var items = data.GroupBy(s => s.SourceName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
           
            if (ModelState.IsValid)
            {
                _context.SourceOfInfos.UpdateRange(data);
                _context.SaveChanges();
            }
            

            return Ok(new
            {
                Data = _context.SourceOfInfos.Where(a => a.Active),
                Errors = errors.Bind(ModelState)
            });
        }


        //Warranty
        public IActionResult GetWarrantyTypes()
        {
            return Ok(_context.WarrantyTypes);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateWarrantyTypes(IEnumerable<WarrantyType> data)
        {
            var items = data.GroupBy(s => s.WarrantyName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }

            if (ModelState.IsValid)
            {
                _context.WarrantyTypes.UpdateRange(data);
                _context.SaveChanges();
            }
            

            return Ok(new
            {
                Data = _context.WarrantyTypes.Where(a => a.Active),
                Errors = errors.Bind(ModelState)
            });
        }


        //Customer Group
        public IActionResult GetCustomerGroups()
        {
            return Ok(_context.CustomerGroups);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateCustomerGroups(IEnumerable<CustomerGroup> data)
        {
            var items = data.GroupBy(s => s.GroupName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
           
            if (ModelState.IsValid)
            {
                _context.CustomerGroups.UpdateRange(data);
                _context.SaveChanges();
            }
            
            return Ok(new
            {
                Data = _context.CustomerGroups.Where(a => a.Active),
                Errors = errors.Bind(ModelState)
            });
        }

        //Color
        public IActionResult GetColorCustomers()
        {
            return Ok(_context.ColorCustomers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateColorCustomers(IEnumerable<ColorCustomer> data)
        {
            var items = data.GroupBy(s => s.ColorName.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
            
            if (ModelState.IsValid)
            {
                _context.ColorCustomers.UpdateRange(data);
                _context.SaveChanges();
            }
            

            return Ok(new
            {
                Data = _context.ColorCustomers.Where(a => a.Active == true),
                Errors = errors.Bind(ModelState)
            });
        }

        //Region 
        public IActionResult GetRegions()
        {
            return Ok(_context.Regions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateRegions(IEnumerable<Region> data)
        {
            var items = data.GroupBy(s => s.Name.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }
            
            if (ModelState.IsValid)
            {
                _context.Regions.UpdateRange(data);
                _context.SaveChanges();
            }
            
            return Ok(new
            {
                Data = _context.Regions,
                Errors = errors.Bind(ModelState)
            });
        }

        //District
        public IActionResult GetDistrictsByRegionID(int related_id)
        {
            return Ok(_context.Districts.Where(d => d.RegionID == related_id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateDistricts(IEnumerable<District> data)
        {
            
            var items = data.GroupBy(s => s.Name.ToLower().Replace(" ", ""))
                            .Where(g => g.Count() > 1)
                            .Select(g => g.Key);

            ModelMessage errors = new ModelMessage(ModelState);
            if (items.Count() > 0)
            {
                foreach (var error in items)
                {
                    ModelState.AddModelError(error, string.Format("existed -> "));
                }
            }

            if (ModelState.IsValid)
            {
                _context.Districts.UpdateRange(data);
                _context.SaveChanges();
            }
            
            return Ok(new
            {
                Data = _context.Districts,
                Errors = errors.Bind(ModelState)
            });
        }

        //Motocycle Customer
        public IActionResult SearchSaleConsultants(string word = "")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.Customer = "show";
            ViewBag.MotocycleRegister = "highlight";

            var sale_consultants = (from ge in _context.GroupEmployees join em in _context.Employees
                                       on ge.GroupID equals em.GroupID
                                       select new
                                       {
                                           em.ID,
                                           ge.GroupName,
                                           em.Name,
                                           em.Gender,
                                           em.Position,
                                           Hiredate = em.Hiredate == ""? "" : DateTime.Parse(em.Hiredate).ToString("dd-MM-yyyy"),
                                           em.Phone                             
                                       }).AsQueryable();

            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                sale_consultants = sale_consultants.Where(sc =>
                    sc.Name.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.GroupName.Replace(" ", string.Empty).ToLower().Contains(word)
                   | sc.Phone.Replace(" ", string.Empty).ToLower().Contains(word)                                  
                   | sc.Position.Replace(" ", string.Empty).ToLower().Contains(word)                 
                );

                return Ok(sale_consultants);
            }

            return Ok(sale_consultants.Take(10));

        }

        private string GetValidDate(string dmy, string separator)
        {
            var values = dmy.Split(separator);
            var date = values[2] + separator + values[1] + separator + values[0];
            var _valid = "";
            if (DateTime.TryParse(date, out DateTime validDate))
            {
                _valid = validDate.ToString("yyyy-MM-dd");
            }
            return _valid;
        }

        //Appointment
        private bool IsTimely(string date, string time, string reminder)
        {
            string datetime = string.Format("{0} {1}", date, time);
            string[] _reminder = reminder.Split("/");
            if(DateTime.TryParse(datetime, out DateTime _dateTime) && _reminder[0].Length <= 7)
            {
                TimeSpan timespan_reminder = new TimeSpan();
                switch (_reminder[1].ToLower())
                {
                    case "minutes":
                        timespan_reminder = TimeSpan.FromMinutes(double.Parse(_reminder[0]));
                        break;
                    case "hours":
                        timespan_reminder = TimeSpan.FromHours(double.Parse(_reminder[0]));
                        break;
                    case "days":
                        timespan_reminder = TimeSpan.FromDays(double.Parse(_reminder[0]));
                        break;
                }
                double reminder_in_minutes = timespan_reminder.TotalMinutes;
                return DateTime.Compare(DateTime.Now, _dateTime.AddMinutes(-reminder_in_minutes)) >= 0;
            }
            return false;
        }

        public IActionResult MotocycleTimelyAppointmentList(bool asJSON = false, string word = "")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "List of motocycle appointments";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleAppointment = "highlight";

            var appointments = (from ap in _context.CustomerAppointments
                                join cu in _context.Customers.Where(c => c.CustomerType != CustomerType.SERVICE) on
                                ap.CustomerID equals cu.CustomerID
                                where IsTimely(ap.ActivityDate, ap.ActivityTime, ap.Reminder) 
                                && string.IsNullOrWhiteSpace(ap.ItemServices) && !cu.Deleted && !ap.Paused && !ap.Closed
                                select new AppointmentSummary
                                {
                                    CustomerID = cu.CustomerID,
                                    AppointmentID = ap.AppointmentID,
                                    AppointmentCode = ap.AppointmentCode,
                                    CustomerName = cu.Name,
                                    Gender = cu.Gender == CustomerGender.Female ? "Female" : "Male",
                                    Phone = cu.Phone ?? "No phone yet.",
                                    Remark = ap.Remark ?? "No remark yet.",
                                    ActivityDate = DateTime.Parse(ap.ActivityDate).ToString("dd-MM-yyyy"),
                                    ActivityTime = ap.ActivityTime ?? "",
                                    Reminder = ap.Reminder ?? "0",
                                    Priority = ap.Priority,
                                    Paused = ap.Paused
                                }).AsQueryable();
            if (appointments.Count() > 0)
            {
                ViewBag.Timely = "fa-pulse fn-red";
            }

            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                appointments = appointments.Where(ap =>
                    ap.CustomerName.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.Phone.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.Remark.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.ActivityDate.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.ActivityTime.Replace(" ", string.Empty).ToLower().Contains(word)

                );
                return Ok(appointments);
            }

            if (asJSON)
            {
                return Ok(appointments);
            }
            return View(appointments);
        }

        public IActionResult MotocycleGeneralAppointmentList(string word = "", string priority = "ALL")
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer Appointment";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleAppointment = "highlight";

            var appointments = (from ap in _context.CustomerAppointments
                                join cu in _context.Customers.Where(c => c.CustomerType != CustomerType.SERVICE) on
                                ap.CustomerID equals cu.CustomerID
                                where string.IsNullOrWhiteSpace(ap.ItemServices) && !cu.Deleted && !ap.Closed
                                select new AppointmentSummary
                                {
                                    CustomerID = cu.CustomerID,
                                    AppointmentID = ap.AppointmentID,
                                    AppointmentCode = ap.AppointmentCode,
                                    CustomerName = cu.Name,
                                    Gender = cu.Gender == CustomerGender.Female ? "Female" : "Male",
                                    Phone = cu.Phone ?? "No phone yet.",
                                    Remark = ap.Remark ?? "No remark yet.",
                                    ActivityDate = DateTime.Parse(ap.ActivityDate).ToString("dd-MM-yyyy"),
                                    ActivityTime = ap.ActivityTime ?? "",
                                    Reminder = ap.Reminder ?? "0",
                                    Priority = ap.Priority,
                                    Paused = ap.Paused
                                }).AsQueryable();
            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                appointments = appointments.Where(ap =>
                    ap.CustomerName.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Phone.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Remark.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.ActivityDate.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.ActivityTime.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Reminder.Replace(" ", string.Empty).ToLower().Contains(word)
                   || ap.Priority.Replace(" ", string.Empty).ToLower().Contains(word)

                );
                return Ok(appointments);
            }

            if(priority != "ALL")
            {
                appointments = appointments.Where(a => a.Priority == priority);
            }

            return Ok(appointments.Take(10));
        }

        [HttpPost]
        public IActionResult ToggleAppointment(int appointmentID, bool paused)
        {
            ModelMessage msg = new ModelMessage(ModelState);
            if (ModelState.IsValid)
            {
                _context.CustomerAppointments.Find(appointmentID).Paused = paused;
                _context.SaveChanges();
            }
            return Ok(_context.CustomerAppointments.Find(appointmentID));
        }

        public IActionResult MotocycleAppointmentDetail(int customerID)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "Motocylce Appointment Detail";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.MotocycleAppointment = "highlight";
            var appointments = from ah in _context.AppointmentHistories where ah.CustomerID == customerID && string.IsNullOrWhiteSpace(ah.ItemServices) select ah;
            var customer = _context.Customers.FirstOrDefault(c => c.CustomerID == customerID);
            var appoint_details = new AppointmentViewModel
            {
                Customer = customer,
                AppointmentHistories = appointments.OrderByDescending(h => h.FollowUp)
            };
            return View(appoint_details);
        }
    
        //Default remark 
        public IActionResult DefaultRemark(bool onlyDefaultRemark = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Customer";
            ViewBag.Subpage = "Default Remark";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.DefaultRemark = "highlight";
            int count = _context.DefaultRemarks.Count() + 1;
            DefaultRemark defaultRemark = new DefaultRemark
            {
                RemarkCode = "R-" + count.ToString().PadLeft(7, '0'),
            };
            IEnumerable<DefaultRemark> defaultRemarks = new List<DefaultRemark>();

            if (onlyDefaultRemark)
            {
                return Ok(defaultRemark);
            }
            return View(new DefaultRemarkViewModel
            {
                DefaultRemark = defaultRemark,
                DefaultRemarks = _context.DefaultRemarks.Where(i => !i.Deleted).Take(10)
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DefaultRemark(DefaultRemark defaultRemark, bool isJSON = false)
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Service Customer";
            ViewBag.Subpage = "Item Service";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.DefaultRemark = "highlight";
            ModelMessage message = new ModelMessage(ModelState);
            if (!string.IsNullOrWhiteSpace(defaultRemark.RemarkText)
                && _context.DefaultRemarks.Any(s => s.RemarkText.Replace(" ", string.Empty).ToLower()
                == defaultRemark.RemarkText.Replace(" ", string.Empty).ToLower()))
            {
                ModelState.AddModelError("defaultRemark.ServiceName", "The service name is already existed.");
                message.Bind(ModelState);
            }

            if (!ModelState.IsValid && isJSON)
            {
                return Ok(new { DefaultRemark = defaultRemark, ModelMessage = message });
            }

            if (ModelState.IsValid)
            {
                defaultRemark.RemarkCode = defaultRemark.RemarkCode.Trim();
                defaultRemark.RemarkText = defaultRemark.RemarkText.Trim();
                defaultRemark.LastChanged = DateTime.Now;
                _context.DefaultRemarks.Update(defaultRemark);
                _context.SaveChanges();
                if (isJSON)
                {
                    return Ok(new { DefaultRemark = defaultRemark, ModelMessage = message });
                }
                return RedirectToAction(nameof(DefaultRemark));
            }

            int count = _context.DefaultRemarks.Count() + 1;
            IEnumerable<DefaultRemark> defaultRemarks = new List<DefaultRemark>();
            return View(new DefaultRemarkViewModel
            {
                DefaultRemark = defaultRemark,
                DefaultRemarks = _context.DefaultRemarks.Where(i => !i.Deleted).Take(10)
            });
        }

        public IActionResult SearchDefaultRemarks(string word = "")
        {
            var defaultRemarks = from s in _context.DefaultRemarks select s;

            if (!string.IsNullOrEmpty(word))
            {
                word = word.Replace(" ", string.Empty).ToLower();
                defaultRemarks = defaultRemarks.Where(ap =>
                    ap.RemarkCode.Replace(" ", string.Empty).ToLower().Contains(word)
                   | ap.RemarkText.Replace(" ", string.Empty).ToLower().Contains(word)
                );
                return Ok(defaultRemarks);
            }
            return Ok(defaultRemarks.Take(10));
        }

        //Invoice

        public IActionResult Invoice()
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Invoice";
            ViewBag.Subpage = "Create Invoice";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.DefaultRemark = "highlight";
            return View();
        }

        public IActionResult TaxInvoice()
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Invoice";
            ViewBag.Subpage = "Create Invoice";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleCustomer = "show";
            ViewBag.DefaultRemark = "highlight";
            return View();
        }
    }
}

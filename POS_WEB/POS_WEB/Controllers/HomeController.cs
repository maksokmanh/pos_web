﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models;
using POS_WEB.Models.Services;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly DataContext _context;
        private readonly IReport _report;
        public HomeController(DataContext context,IReport report)
        {
            _context = context;
            _report = report;
        }
        public IActionResult Test()
        {
            return View();
        }
        public IActionResult TestCss()
        {
            return View();
        }
        public IActionResult Dashboard()
        {
            ViewBag.style = "fa fa-home";
            ViewBag.Main = "Dashborad";
            ViewBag.Page = "Dashborad";
            
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
       
        [HttpPost]
        public IActionResult selectLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTimeOffset.UtcNow.AddDays(1) }
                );
            return LocalRedirect(returnUrl);
        }
        [HttpGet]
        public IActionResult DashboarbSaleSummary(int BranchID)
        {
            var list = _report.GetDashboardSaleSummary(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult DashboarbPurchaseSummary(int BranchID)
        {
            var list = _report.GetDashboardPurchaseSummary(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetDashboardTopSale()
        {
            var list = _report.GetDashboardTopSale.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetDashboardStockInwarehose()
        {
            var list = _report.GetDashboardStockInwaerhose.ToList();
            return Ok(list);
        }
    }
}
